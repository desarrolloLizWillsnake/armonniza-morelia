<?php

class Xmlrpc_client extends CI_Controller {

	public function index() {
        $id_factura = 1;
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        $this->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->xmlrpc->method('Ver_Factura');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_factura,
        );
        
        // Se realiza la petición al servidor
        $this->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->xmlrpc->send_request()) {
            $this->debugeo->imprimir_pre($this->xmlrpc->display_error());
        }
        // De lo contrario, se muestra la respuesta 
        else {
            $this->debugeo->imprimir_pre($this->xmlrpc->display_response());
        }
	}
}