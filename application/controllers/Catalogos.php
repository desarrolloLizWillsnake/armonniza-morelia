<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Validate;*/

class Catalogos extends CI_Controller {

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        $this->db->cache_off();
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de reportes
     */
    function index() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catalogos",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aqui empieza la sección de Catalogos */
    function indicecatalogos() {
        if($this->utilerias->get_permisos("catalogos")|| $this->utilerias->get_grupo() == 1) {
            $datos_header = array(
                "titulo_pagina" => "Armonniza | Catalogos",
                "usuario" => $this->tank_auth->get_username(),
                "tablas" => TRUE,
            );

            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/indices_catalogos_view');
            $this->load->view('front/footer_main_view', array("tablas" => TRUE, "indicecatalogos" => TRUE));
        } else {
            redirect('/auth/login');
        }

    }

    function proveedores() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Proveedores",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_proveedores_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_proveedores" => TRUE));
    }

    function tabla_proveedores() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        DataTables\Editor::inst( $db, 'cat_proveedores' )
            ->pkey( 'id_proveedores' )
            ->fields(
                DataTables\Editor\Field::inst( 'clave_prov' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'tipo' ),
                DataTables\Editor\Field::inst( 'RFC' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'curp' ),
                DataTables\Editor\Field::inst( 'nombre' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'calle' ),
                DataTables\Editor\Field::inst( 'no_exterior' ),
                DataTables\Editor\Field::inst( 'no_interior' ),
                DataTables\Editor\Field::inst( 'colonia' ),
                DataTables\Editor\Field::inst( 'cp' ),
                DataTables\Editor\Field::inst( 'ciudad' ),
                DataTables\Editor\Field::inst( 'delegacion_municipio' ),
                DataTables\Editor\Field::inst( 'estado' ),
                DataTables\Editor\Field::inst( 'pais' ),
                DataTables\Editor\Field::inst( 'cuenta' ),
                DataTables\Editor\Field::inst( 'clabe' ),
                DataTables\Editor\Field::inst( 'banco' ),
                DataTables\Editor\Field::inst( 'pago' ),
                DataTables\Editor\Field::inst( 'observaciones' )
            )
            ->where( 'activo', 1 )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function exportar_proveedores() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha exportado una consulta de balanza');

        $datos = $this->input->post();
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Catalogo de Proveedores');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:F1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'Servicios de Salud de Michoacán');
        $this->excel->getActiveSheet()->mergeCells('A2:F2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Catálogo de Proveedores');
        $this->excel->getActiveSheet()->getStyle("B3:F3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B4:F4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('D3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('D4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('F4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:F5');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Clave')->getColumnDimension("A")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Tipo')->getColumnDimension("B")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('C6', 'RFC')->getColumnDimension("C")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Nombre')->getColumnDimension("D")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Calle')->getColumnDimension("E")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('F6', 'CURP')->getColumnDimension("F")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('G6', 'No. Exterior')->getColumnDimension("G")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('H6', 'No. Interior')->getColumnDimension("H")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Colonia')->getColumnDimension("I")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('J6', 'C.P.')->getColumnDimension("J")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Delegación')->getColumnDimension("K")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Ciudad')->getColumnDimension("L")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Estado')->getColumnDimension("M")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('N6', 'Teléfono')->getColumnDimension("N")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('O6', 'CLABE')->getColumnDimension("O")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Banco')->getColumnDimension("P")->setWidth(25);
       
        $this->db->select('*')
                 ->from('cat_proveedores');
        $query = $this->db->get();
        $resultado = $query->result_array();

        $row = 7;

//        $this->benchmark->mark('code_start');

        foreach ($resultado as $key => $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila["clave_prov"]);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila["tipo"]);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $fila["RFC"]);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $fila["nombre"]);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $fila["calle"]);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $fila["curp"]);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, $fila["no_exterior"]);
            $this->excel->getActiveSheet()->setCellValue('H' . $row, $fila["no_interior"]);
            $this->excel->getActiveSheet()->setCellValue('I' . $row, $fila["colonia"]);
            $this->excel->getActiveSheet()->setCellValue('J' . $row, $fila["cp"]);
            $this->excel->getActiveSheet()->setCellValue('K' . $row, $fila["delegacion_municipio"]);
            $this->excel->getActiveSheet()->setCellValue('L' . $row, $fila["ciudad"]);
            $this->excel->getActiveSheet()->setCellValue('M' . $row, $fila["estado"]);
            $this->excel->getActiveSheet()->setCellValue('N' . $row, $fila["telefono"]);
            $this->excel->getActiveSheet()->setCellValue('O' . $row, $fila["clabe"]);
            $this->excel->getActiveSheet()->setCellValue('P' . $row, $fila["banco"]);
            $row += 1;
        }

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Catalogo de Proveedores.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function beneficiarios() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Beneficiarios",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_beneficiarios_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_beneficiarios" => TRUE));
    }

    function tabla_beneficiarios() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        DataTables\Editor::inst( $db, 'cat_beneficiarios' )
            ->pkey( 'id_beneficiarios' )
            ->fields(
                DataTables\Editor\Field::inst( 'numero_empleado' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'nombre' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'adscripcion' ),
                DataTables\Editor\Field::inst( 'cargo' )
            )
            ->process( $_POST )
            ->json();
//        $this->debugeo->imprimir_pre($resultado);

    }

    function bancos() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Bancos",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_bancos_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_bancos" => TRUE));
    }

    function tabla_bancos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_cuentas_bancarias' )
            ->pkey( 'id_cuentas_bancarias' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_cuentas_bancarias' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'cuenta' )->validator( 'DataTables\Editor\Validate::numeric' ),
                DataTables\Editor\Field::inst( 'banco' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'cta_contable' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'saldo' )->validator( 'DataTables\Editor\Validate::numeric' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function cucop() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Clasificador CUCOP",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/clasificador_cucop_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "clasificador_cucop" => TRUE));
    }

    function tabla_cucop() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_conceptos_gasto' )
            ->pkey( 'id_conceptos_gasto' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_conceptos_gasto' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'cucop' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'clave_ssm' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'descripcion' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'unidad' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

//        $this->debugeo->imprimir_pre($resultado);

    }

    function conceptos_bancarios() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Catálogo Conceptos Bancarios",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_concepto_bancario_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_conceptos" => TRUE));
    }

    function tabla_conceptos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_bancos_conceptos' )
            ->pkey( 'id_cat_bancos_conceptos' )
            ->fields(
                DataTables\Editor\Field::inst( 'id_cat_bancos_conceptos' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'concepto' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

        //       $this->debugeo->imprimir_pre($resultado);

    }

    function cadena_random_proveedor() {
        $random = '';

        do {
            $random = "A".substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
            $this->db->select('clave_prov')->from('cat_proveedores')->where('clave_prov', $random);
            $query = $this->db->get();
        } while ($query->row() != NULL);

        echo(json_encode(array("respuesta" => $random)));
    }

    function get_unidades_medida() {
        $this->db->select('clave, descripcion')->from('cat_unidad_medida');
        $query = $this->db->get();
        echo(json_encode($query->result_array()));
    }

    function medicamentos() {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Medicamentos",
            "usuario" => $this->tank_auth->get_username(),
            "editar_tabla" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/catalogos/catalogo_medicamentos_view');
        $this->load->view('front/footer_main_view', array( "exportar_tablas" => TRUE, "catalogo_medicamentos" => TRUE));
    }

    function tabla_medicamentos() {
        include( "./assets/datatables/extensions/Editor-1.3.3/php/DataTables.php" );

        $resultado = DataTables\Editor::inst( $db, 'cat_medicamentos' )
            ->pkey( 'id_medicamentos' )
            ->fields(
                DataTables\Editor\Field::inst( 'cucop' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'clave' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'descripcion' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'presentacion' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
//                DataTables\Editor\Field::inst( 'cantidad' ),
//                DataTables\Editor\Field::inst( 'tipo' )->validator( 'DataTables\Editor\Validate::notEmpty' ),
                DataTables\Editor\Field::inst( 'precio_unitario' )->validator( 'DataTables\Editor\Validate::notEmpty' )
            )
            ->process( $_POST )
            ->json();

        //       $this->debugeo->imprimir_pre($resultado);

    }

    function exportar_medicamentos() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha exportado una consulta de balanza');

        $datos = $this->input->post();
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Catalogo de Medicamentos');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:F1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'Servicios de Salud de Michoacán');
        $this->excel->getActiveSheet()->mergeCells('A2:F2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Catálogo de Medicamentos');
        $this->excel->getActiveSheet()->getStyle("B3:F3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("B4:F4")->applyFromArray($style_center);
        $this->excel->getActiveSheet()->setCellValue('D3', 'Fecha Emisión');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Hora Emisión');
        $this->excel->getActiveSheet()->setCellValue('D4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('F4', $hora);
        $this->excel->getActiveSheet()->getStyle("A6:F6")->applyFromArray($style);
        $this->excel->getActiveSheet()->mergeCells('A5:F5');

        //set cell A1 content with some text

        $this->excel->getActiveSheet()->getStyle("A")->applyFromArray($style_left);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Clave')->getColumnDimension("A")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Partida')->getColumnDimension("B")->setWidth(35);
        $this->excel->getActiveSheet()->setCellValue('C6', 'Descripción')->getColumnDimension("C")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('D6', 'Unidad de Medida')->getColumnDimension("D")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Cantidad')->getColumnDimension("E")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('F6', 'Tipo')->getColumnDimension("F")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Precio Unitario')->getColumnDimension("G")->setWidth(25);
       
        $this->db->select('*')
                 ->from('cat_medicamentos');
        $query = $this->db->get();
        $resultado = $query->result_array();

        $row = 7;

//        $this->benchmark->mark('code_start');

        foreach ($resultado as $key => $fila) {
//            $this->debugeo->imprimir_pre($fila);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila["clave"]);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila["partida"]);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $fila["descripcion"]);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $fila["unidad_medida"]);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $fila["cantidad"]);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $fila["tipo"]);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, $fila["precio_unitario"]);

            $row += 1;
        }

//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Catalogo de Medicamentos.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}