<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require( './assets/datatables/scripts/ssp.class.php' );

/**
 * Este controlador se encarga de mostrar la nómina y controlar las funciones
 **/
class Extranet extends CI_Controller
{

    var $sql_details;

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct(){
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        $this->sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'software',
            'host' => 'localhost',
        );
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla principal de la nómina
     */
    function index(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a nómina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Nómina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aquí empieza la sección de Empleados */
    function empleados(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Empleados",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/empleados_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
        ));
    }

    function estado_cuentas(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/estado_cuentas_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "estado_de_cuenta" => TRUE,
        ));
    }

    function tabla_proveedores_cuentas() {
        $this->datatables->select("RFC,
                                    nombre")
                ->from('cat_proveedores')
                ->add_column('acciones', '<a data-toggle="modal" data-target="#modal_proveedores" data-tooltip="Seleccionar Factura"><i class="fa fa-check"></i></a>');

        echo $this->datatables->generate();
    }

    function tabla_facturas_estado_cuenta() {
        
        $this->datatables->set_database('extranet');

        $proveedor = $this->input->post("proveedor", TRUE);

        if(!$proveedor || $proveedor == NULL || $proveedor == "") {
            $proveedor = "";
        }
    
        $this->datatables->select("id_facturas_xml,
                                    folio,
                                    fecha_emision,
                                    num_precompro,
                                    total,
                                    estatus,
                                    descripcion,
                                    clase
                                    ")
                ->from('facturas_xml')
                ->where('rfc_emisor', $proveedor)
                ->join('cat_estatus', 'cat_estatus.id_estatus = facturas_xml.estatus', 'inner')
                ->edit_column('estatus', '<button type="button" class="btn $1" readonly>$2</button>', 'clase, descripcion');

        echo $this->datatables->generate();
    }

    function datos_contratos(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/contratos_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
        ));
    }

    function datos_proveedores(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/extranet/proveedores_por_aprobar_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "proveedores_aprobar" => TRUE,
        ));
    }

    function tabla_proveedores_aprobar() {

        $this->db->cache_delete('extranet', 'tabla_proveedores_aprobar');
        $this->db->cache_off();
    
        $this->datatables->select("id_proveedores,
                                    clave_prov,
                                    RFC,
                                    nombre,
                                    cuenta,
                                    clabe,
                                    activo,
                                    id_usuario_extranet")
                ->from('cat_proveedores')
                ->where('activo', 0);
                echo $this->datatables->generate();
    }

    function tabla_proveedores_aprobados() {

        $this->db->cache_delete('extranet', 'tabla_proveedores_aprobados');
        $this->db->cache_off();

        $this->datatables->select("id_proveedores,
                                    clave_prov,
                                    RFC,
                                    nombre,
                                    cuenta,
                                    clabe,
                                    activo,
                                    aprobado_por,
                                    fecha_aprobado,
                                    id_usuario_extranet")
            ->from('cat_proveedores')
            ->where('activo', 1);
        echo $this->datatables->generate();
    }

    function activar_proveedor() {
        
        $this->db->cache_delete('extranet', 'activar_proveedor');
        $this->db->cache_off();

        $respuesta["mensaje"] = "";
        
        $proveedor = $this->input->post("proveedor", TRUE);
        $id_proveedor = $this->input->post("id_proveedor", TRUE);

        try {
            
            $this->db->trans_begin();

            $activar_proveedor = $this->extranet_model->activar_proveedor($id_proveedor);

            $resultado = $this->api_xmlrpc->activar_proveedor_remoto($proveedor);

            if($resultado["estatus"] == 200) {
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El proveedor se ha activado</div>';
                $this->db->trans_commit();
            } else {
                throw new Exception("Hubo un error al activar el proveedor.");
            }
            
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );
        }

        echo(json_encode($respuesta));

    }
    function ver_datos_proveedor() {

        $this->db->cache_delete('extranet', 'ver_datos_proveedor');
        $this->db->cache_off();

        $proveedor = $this->input->post("proveedor", TRUE);

        $resultado = $this->api_xmlrpc->ver_proveedor_remoto($proveedor);

        echo(json_encode($resultado));
    }
    function desactivar_proveedor() {
        
        $this->db->cache_delete('extranet', 'desactivar_proveedor');
        $this->db->cache_off();

        $respuesta["mensaje"] = "";
        
        $proveedor = $this->input->post("proveedor", TRUE);
        $id_proveedor = $this->input->post("id_proveedor", TRUE);

        try {
            
            $this->db->trans_begin();

            $desactivar_proveedor = $this->extranet_model->desactivar_proveedor($id_proveedor);

            $resultado = $this->api_xmlrpc->desactivar_proveedor_remoto($proveedor);

            if($resultado["estatus"] == 200) {
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>El proveedor se ha desaprobado correctamente</div>';
                $this->db->trans_commit();
            } else {
                throw new Exception("Hubo un error al desaprobar el proveedor.");
            }
            
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );
        }

        echo(json_encode($respuesta));
    }

    function alta_proveedores() {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a dar de alta un proveedor dentro de la extranet');

        // $this->debugeo->imprimir_pre($this->input->post());
        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'RFC', 'trim|required|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash');
        }
        
//        $this->form_validation->set_rules('pyme', 'PyME', 'trim|required');
        $this->form_validation->set_rules('tipo_razon_social_hidden', 'Razón Social', 'trim');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim');
        $this->form_validation->set_rules('pais', 'País', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');
        $this->form_validation->set_rules('ciudad', 'Ciudad', 'trim|required');
        $this->form_validation->set_rules('codigo_postal', 'Código Postal', 'trim|required');
        $this->form_validation->set_rules('colonia', 'Colonia', 'trim|required');
        $this->form_validation->set_rules('calle', 'Calle', 'trim|required');
        $this->form_validation->set_rules('numero_exterior', 'Número Exterior', 'trim|required');
        $this->form_validation->set_rules('numero_interior', 'Número Interior', 'trim');
        $this->form_validation->set_rules('nombre_uno', 'Nombre Contacto Facturas', 'trim|required');
        $this->form_validation->set_rules('apellido_pa_uno', 'Apellido Paterno Contacto Facturas', 'trim|required');
        $this->form_validation->set_rules('apellido_ma_uno', 'Apellido Materno Contacto Facturas', 'trim');
        $this->form_validation->set_rules('telefono_uno', 'Teléfono Contacto Facturas', 'trim|required');
        $this->form_validation->set_rules('email', 'Correo Electrónico Contacto Facturas', 'trim|required|valid_email');
        $this->form_validation->set_rules('nombre_dos', 'Nombre Contacto Ventas', 'trim|required');
        $this->form_validation->set_rules('apellido_pa_dos', 'Apellido Paterno Contacto Ventas', 'trim|required');
        $this->form_validation->set_rules('apellido_ma_dos', 'Apellido Materno Contacto Ventas', 'trim');
        $this->form_validation->set_rules('telefono_dos', 'Teléfono Contacto Ventas', 'trim|required');
        $this->form_validation->set_rules('email_dos', 'Correo Electrónico Contacto Ventas', 'trim|required|valid_email');
        // $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
        // $this->form_validation->set_rules('confirm_password', 'Confirmar Contraseña', 'trim|required|matches[password]');
        // $this->form_validation->set_rules('moneda', 'Moneda', 'trim|required');
        $this->form_validation->set_rules('banco', 'Banco', 'trim|required');
        $this->form_validation->set_rules('no_cuenta', 'No. de Cuenta', 'trim|required');
        $this->form_validation->set_rules('clabe', 'CLABE', 'trim|required|min_length[18]|max_length[18]');

        $captcha_registration   = $this->config->item('captcha_registration', 'tank_auth');
        $use_recaptcha          = $this->config->item('use_recaptcha', 'tank_auth');
        if ($captcha_registration) {
            if ($use_recaptcha) {
                $this->form_validation->set_rules('recaptcha_response_field', 'Código de Confirmación', 'trim|required|callback__check_recaptcha');
            } else {
                $this->form_validation->set_rules('captcha', 'Código de Confirmación', 'trim|required|callback__check_captcha');
            }
        }
        $data['errors'] = array();

        $email_activation = $this->config->item('email_activation', 'tank_auth');

        if ($this->form_validation->run()) {

            $datos_usuario = $this->input->post();
            // Se prepara la configuracion de la libreria "Upload"
            $config['upload_path'] = './upload/img/';
            $config['allowed_types'] = '*';
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            
            // Se carga la libreria con sus configuraciones correspondientes
            $this->load->library('upload', $config);
            
            // Si el server no logra subir el archivo, despliega un mensaje de error
            if ( ! $this->upload->do_upload('archivoSubir')) {
                // Se capturan los errores en una variable y se imprimen para debug
                $error = array('error' => $this->upload->display_errors());
                $this->debugeo->imprimir_pre($error);
            }
            else {
                ini_set('memory_limit', '-1');
                // Si el server logra subir el archivo, se toman los datos del archivo
                $datos_imagen = $this->upload->data();
                $datos_usuario["comprobante"] = base_url("upload/img/".$datos_imagen["file_name"]); 
                // $this->debugeo->imprimir_pre($datos_imagen);
            }
            
            $respuesta = array('mensaje' => "" );

            $this->db->trans_begin();

            $insertar_proveedor_remoto = $this->api_xmlrpc->insertar_proveedor_remoto($datos_usuario);

            if($insertar_proveedor_remoto["estatus"] == 200) {

                $insertar_proveedor = $this->extranet_model->insertar_proveedor($datos_usuario, $insertar_proveedor_remoto["id_usuario"]);

                if($insertar_proveedor) {
                    $respuesta["mensaje"] = "El proveedor se ha insertado con éxito";
                } else {
                    $respuesta["mensaje"] = "Hubo un error al insertar el proveedor";
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }

            } else {
                $respuesta["mensaje"] = $insertar_proveedor_remoto["mensaje"];
            }

            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Extranet Alta Proveedores",
                "usuario" => $this->tank_auth->get_username(),
                "precompromisocss" => TRUE,
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/resultado_archivo_view', $respuesta);
            $this->load->view('front/footer_main_view');
            return TRUE;

        }
        if ($captcha_registration) {
            if ($use_recaptcha) {
                $data['recaptcha_html'] = $this->_create_recaptcha();
            } else {
                $data['captcha_html'] = $this->_create_captcha();
            }
        }
        $data['use_username'] = $use_username;
        $data['captcha_registration'] = $captcha_registration;
        $data['use_recaptcha'] = $use_recaptcha;
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet Alta Proveedores",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/extranet/alta_proveedor_view', $data);
        $this->load->view('front/footer_main_view', ["alta_proveedores" => TRUE]);
          }

    function datos_noticias(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/noticias_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "noticias" => TRUE,
        ));
    }

    function insertar_noticia(){
        $respuesta["mensaje"] = '';
        $noticia = array(
            'asunto' => $this->input->post('asunto'),
            'fecha' => $this->input->post('fecha'),
            'observaciones' => $this->input->post('mensaje'),
        );

        $resultado = $this->noticia_extranet_model->insertar_mensaje_noticia($noticia);
        if($resultado) {
            $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La noticia fue insertada correctamente.</div>';
        } else {
            $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Hubo un error al insertar la noticia, por favor, contacte a su administrador.</div>';
        }

        echo(json_encode($respuesta));
    }

    function tabla_noticias_extranet() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->datatables->select("id_noticia,
                                    fecha,
                                    asunto,
                                    mensaje")
                ->from('cat_noticias');
        echo $this->datatables->generate();
    }

    function insertar_dato(){
        $noticia = array(
            'id_noticia' => $this->input->post('id_noticia'),
            'tipo_mensaje' => $this->input->post('asunto'),
            'fecha' => $this->input->post('fecha'),
            'observaciones' => $this->input->post('mensaje'),

        );

        $this->load->model('cat_noticias');
        $this->noticia_extranet_model->insertar_noticia($noticia);

    }

    function datos_autorizaciones(){
        $datos_vista = array();
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');

        $datos_vista["usuario_autoriza"] = $this->tank_auth->get_username();

        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/autorizaciones_view', $datos_vista);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
        ));
    }

    function tabla_facturas_proveedor() {

        $this->db->cache_delete('extranet', 'tabla_facturas_proveedor');
        $this->db->cache_off();

        $this->datatables->set_database('extranet');

        $proveedor = $this->input->post("proveedor", TRUE);

        if(!$proveedor || $proveedor == NULL || $proveedor == "") {
            $proveedor = "";
        }
    
        $this->datatables->select("id_facturas_xml,
                                    folio,
                                    fecha_emision,
                                    num_precompro,
                                    total,
                                    estatus,
                                    descripcion,
                                    clase")
                ->from('facturas_xml')
                ->where('rfc_emisor', $proveedor)
                ->where('estatus', 1)
                ->where('autorizado_1', NULL)
                ->where('fecha_autorizado_1', NULL)
                ->join('cat_estatus', 'cat_estatus.id_estatus = facturas_xml.estatus', 'inner')
                ->edit_column('estatus', '<button type="button" class="btn $1" readonly>$2</button>', 'clase, descripcion')
                ->add_column('acciones', '<a data-toggle="modal" data-target=".modal_autorizar" data-tooltip="Seleccionar Factura"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target=".modal_ver" data-tooltip="Ver Información"><i class="fa fa-eye"></i></a>
                                            <a target="_blank" href="'.base_url("extranet/imprimir_datos_factura/$1").'" data-tooltip="Imprimir Información"><i class="fa fa-print"></i></a>', 'id_facturas_xml');

        echo $this->datatables->generate();
    }
    function tomar_datos_factura() {
        
        $this->db->cache_delete('extranet', 'tomar_datos_factura');
        $this->db->cache_off();

        $factura = $this->input->post("factura", TRUE);

        $datos_factura = $this->api_xmlrpc->ver_datos_factura($factura);

        echo(json_encode($datos_factura));
    }
    function tabla_facturas_proveedor_glosa() {

        $this->db->cache_delete('extranet', 'tabla_facturas_proveedor_glosa');
        $this->db->cache_off();

        $this->datatables->set_database('extranet');

        $proveedor = $this->input->post("proveedor", TRUE);

        if(!$proveedor || $proveedor == NULL || $proveedor == "") {
            $proveedor = "";
        }
    
         $this->datatables->select("id_facturas_xml,
                                    folio,
                                    fecha_emision,
                                    num_precompro,
                                    total,
                                    estatus,
                                    descripcion,
                                    clase,
                                    autorizado_1,
                                    fecha_autorizado_1")
                ->from('facturas_xml')
                ->where('rfc_emisor', $proveedor)
                ->where('estatus', 2)
                ->where('autorizado_2', NULL)
                ->where('fecha_autorizado_2', NULL)
                ->join('cat_estatus', 'cat_estatus.id_estatus = facturas_xml.estatus', 'inner')
                ->edit_column('estatus', '<button type="button" class="btn $1" readonly>$2</button>', 'clase, descripcion')
                ->add_column('acciones', '<a data-toggle="modal" data-target=".modal_autorizar" data-tooltip="Seleccionar Factura"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target=".modal_ver" data-tooltip="Ver Información"><i class="fa fa-eye"></i></a>
                                            <a target="_blank" href="'.base_url("extranet/imprimir_datos_factura/$1").'" data-tooltip="Imprimir Información"><i class="fa fa-print"></i></a>', 'id_facturas_xml');

        echo $this->datatables->generate();
    }


    function tabla_facturas_aprobadas() {

        $this->db->cache_delete('extranet', 'tabla_facturas_aprobadas');
        $this->db->cache_off();

        $this->datatables->set_database('extranet');

        $proveedor = $this->input->post("proveedor", TRUE);

        if(!$proveedor || $proveedor == NULL || $proveedor == "") {
            $proveedor = "";
        }
    
        $this->datatables->select("id_facturas_xml,
                                    folio,
                                    fecha_emision,
                                    num_precompro,
                                    total,
                                    estatus,
                                    descripcion,
                                    clase,
                                    autorizado_2,
                                    fecha_autorizado_2")
                ->from('facturas_xml')
                ->where('rfc_emisor', $proveedor)
                ->where('estatus', 4)
                ->join('cat_estatus', 'cat_estatus.id_estatus = facturas_xml.estatus', 'inner')
                ->edit_column('estatus', '<button type="button" class="btn $1" readonly>$2</button>', 'clase, descripcion');

        echo $this->datatables->generate();

    }

    function imprimir_datos_factura($factura = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso la información de la factura' .$factura);

        $datos_factura = $this->api_xmlrpc->ver_datos_factura($factura);

        // $this->debugeo->imprimir_pre($datos_factura);

        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'cm', 'Letter', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Informacion de Factura');
        $pdf->SetSubject('Informacion de Factura');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(10, 10, 10);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// -----------------------------------------------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

//        $this->debugeo->imprimir_pre($datos);
//        $this->debugeo->imprimir_pre($datos_detalle);
//        $this->debugeo->imprimir_pre($datos_beneficiario);

        $pdf->AddPage();
        $titulo = '<table style="font-size: small;" cellspacing="6"><tr width="150"><td>SERVICIOS DE SALUD DE MICHOACAN</td></tr><tr><td>DELEGACION ADMINISTRATIVA</td></tr></table>';
        $tabla_titulo = '<table>
            <tr>
                <th align="left" width="1%"></th>
                <th align="left" width="30%"><img src="'.base_url("img/logo2.png").'" /></th>
                    <th align="left" width="16%"></th>
                <th align="right" width="53%"><br />'.$titulo.'</th>
            </tr>
        </table>';

        $tabla_datos_emisor = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>RFC</b></td>
                                <td align="left" width="39%">'.strtoupper($datos_factura["rfc_emisor"]).'</td>
                                <td align="left" width="14%"><b>Razón Social</b></td>
                                <td align="left" width="34%">'.$datos_factura["razon_social"].' '.$datos_factura["nombre"].' '.$datos_factura["apellidos"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Dirección</b></td>
                                <td align="left" width="39%">'.$datos_factura["calle_emisor"].' '.$datos_factura["numero_ext_emisor"].' '.$datos_factura["numero_ent_emisor"].' '.$datos_factura["colonia_emisor"].' '.$datos_factura["cp_emisor"].' '.$datos_factura["municipio_emisor"].' '.$datos_factura["estado_emisor"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Folio</b></td>
                                <td align="left" width="39%">'.$datos_factura["folio"].'</td>
                                <td align="left" width="14%"><b>Método de Pago</b></td>
                                <td align="left" width="34%">'.$datos_factura["metodo_pago"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Moneda</b></td>
                                <td align="left" width="90%">'.$datos_factura["moneda"].'</td>
                            </tr>
                    </table>';

        $tabla_datos_receptor = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>RFC</b></td>
                                <td align="left" width="39%">'.strtoupper($datos_factura["rfc_receptor"]).'</td>
                                <td align="left" width="14%"><b>Razón Social</b></td>
                                <td align="left" width="34%">'.$datos_factura["nombre_receptor"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Dirección</b></td>
                                <td align="left" width="39%">'.$datos_factura["calle_receptor"].' '.$datos_factura["numero_ext_receptor"].' '.$datos_factura["numero_int_receptor"].' '.$datos_factura["colonia_receptor"].' '.$datos_factura["cp_receptor"].' '.$datos_factura["municipio_receptor"].' '.$datos_factura["estado_receptor"].'</td>
                            </tr>
                    </table>';

        $tabla_datos_contrato = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>Contrato</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Cuenta</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Anexo</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Zona</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Jurisdicción</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Unidad</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Localidad</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Periodo</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Remisión</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Venta</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                    </table>';

        $tabla_datos_factura = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="100%"><b>Sello Digital CFDI</b></td>                                
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["sello"].'</td>                                
                            </tr>
                            <tr>
                                <td align="left" width="14%"><b>Sello del SAT</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["sello_sat"].'</td>                                
                            </tr>
                            <tr>
                                <td align="left" width="100%"><b>Cadena original del complemento del certificado SAT</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["certificado"].'</td>
                            </tr>
                    </table>';

        $tabla_conceptos = '<table class="cont-general2" style="font-size:6px;" cellspacing="0" cellpadding="4" >
                                <tr>
                                    <td class="borde-inf2 borde-der2" align="center" width="60%"><b>Descripción</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="10%" ><b>Cantidad</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%" ><b>Precio</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%"><b>Importe</b></td>
                                </tr>';
        for($i = 0; $i <= $datos_factura["conteo"]; $i++) {
            $tabla_conceptos .= '
                                <tr>
                                    <td class="borde-inf2 borde-der2" align="center" width="60%">'.$datos_factura["conceptos"]["descripcion".$i].'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="10%">'.$datos_factura["conceptos"]["cantidad".$i].'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%">$ '.number_format($datos_factura["conceptos"]["valor_unitario".$i], 2, '.', '').'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%">$ '.number_format($datos_factura["conceptos"]["importe".$i], 2, '.', '').'</td>
                                </tr>';
        }
        $tabla_conceptos .= '</table>';

        $html = '
                <style>
                    .cont-general{
                        border: 1px solid #eee;
                        border-radius: 1%;
                        margin: 2% 14%;
                    }
                    .cont-general2{
                        border: 1px solid #BDBDBD;
                    }
                    .cont-general .borde-inf{
                        border-bottom: 1px solid #eee;
                    }
                    .cont-general .borde-sup{
                        border-top: 1px solid #eee;
                    }
                    .cont-general .borde-der{
                        border-right: 1px solid #eee;
                    }
                    .cont-general2 .borde-inf2{
                        border-bottom: 1px solid #BDBDBD;
                    }
                    .cont-general2 .borde-der2{
                        border-right: 1px solid #BDBDBD;
                    }
                </style>

                <table  width="100%" class="cont-general" border="0" cellspacing="3" style="font-size: small;">
                    <tr>
                        <td class="borde-inf" width="100%">'.$tabla_titulo.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><H1><b>"Resumen de Emisión Factura Física"</b></H1></th>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Emisor</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_emisor.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Receptor</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_receptor.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Contrato</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_contrato.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos de la Factura</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_factura.'</td>
                    </tr>
                     <br>

                    <tr>
                        <td>
                            '.$tabla_conceptos.'
                        </td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Subtotal</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["subtotal"], 2, '.', '').'</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Tasa de IVA</b></td>

                        <td width="10%" align="center">'.$datos_factura["tasa_iva"].'%</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>IVA</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["valor_iva"], 2, '.', '').'</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Total</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["total"], 2, '.', '').'</td>
                    </tr>
                </table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Informacion de Factura.pdf', 'FI');
    }

    public function ver(){
        $data=array(
            'enlaces' => $this->datos_model->ver_tablas(),
            'dump' => 0
        );
    }

    function filtro_contratos(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a la lista de empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | filtro de contratos",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/filtro_contratos_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "contratos" => TRUE,
        ));
    }
    function autorizaciones_glosa(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/recursos_glosa_view', ["username" => $this->tank_auth->get_username()]);
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
        ));
    }
    function datos_inventario_extranet(){
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/inventario_extranet_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,

        ));
    }
    function tabla_proveedor_con_contrato() {
        
        $this->db->cache_delete('extranet', 'tabla_proveedor_con_contrato');
        $this->db->cache_off();

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
    
        $this->datatables->set_database('extranet');
    
        $this->datatables->select('users.username,
                                    CONCAT(datos_usuario.razon_social," ",datos_usuario.nombre," ",datos_usuario.apellidos),
                                    "",
                                    users.created')
                            ->from('users')
                            ->join('datos_usuario', 'users.id = datos_usuario.id_usuario')
                            ->join('facturas_xml', 'users.id = facturas_xml.id_usuario')
                            ->where('users.banned', 0)
                            ->where('EXISTS(SELECT id_facturas_xml
                                        FROM facturas_xml
                                        WHERE rfc_emisor = users.username)')
                            ->group_by("users.username")
                            ->add_column('acciones', '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Proveedor"><i class="fa fa-check"></i></a>');

        echo $this->datatables->generate();
    }
    function tabla_proveedor_sin_contrato() {
        
        $this->db->cache_delete('extranet', 'tabla_proveedor_sin_contrato');
        $this->db->cache_off();

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->datatables->set_database('extranet');
    
        $this->datatables->select('users.username,
                                    CONCAT(datos_usuario.razon_social," ",datos_usuario.nombre," ",datos_usuario.apellidos),
                                    users.created')
                            ->from('users')
                            ->join('datos_usuario', 'users.id = datos_usuario.id_usuario')
                            ->join('facturas_xml', 'users.id = facturas_xml.id_usuario')
                            ->where('users.banned', 0)
                            ->where('EXISTS(SELECT id_facturas_xml
                                        FROM facturas_xml
                                        WHERE rfc_emisor = users.username)')
                            ->group_by("users.username")
                            ->add_column('acciones', '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Proveedor"><i class="fa fa-check"></i></a>');

        echo $this->datatables->generate();
    }
    function autorizar_factura_1() {

        $this->db->cache_delete('extranet', 'autorizar_factura_1');
        $this->db->cache_off();
        
        $respuesta = array('mensaje' => '' );

        try {
            $factura = $this->input->post("factura", TRUE);
            $autorizacion = $this->input->post("autorizacion", TRUE);
            $titulo = $this->input->post("titulo", TRUE);
            $observaciones = $this->input->post("observaciones", TRUE);
            $rfc_proveedor = $this->input->post("rfc_proveedor", TRUE);

            if(!isset($factura) || $factura == NULL || $factura == "") {
                throw new Exception('Por favor, seleccione una factura para aprobar.');
            }

            if($autorizacion == "si") {
                $datos_factura = $this->api_xmlrpc->autorizar_factura_1($factura, date('Y-m-d H:i:s'));

                if($datos_factura["estatus"] != 200) {
                    throw new Exception('Hubo un error al autorizar la factura deseada, por favor comuniquese con su administrador.');
                }

                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La factura fue autorizada correctamente.</div>';
            } else {
                $datos_factura = $this->api_xmlrpc->rechazar_factura($factura, $observaciones);

                if($datos_factura["estatus"] != 200) {
                    throw new Exception('Hubo un error al rechazar la factura deseada, por favor comuniquese con su administrador.');
                }
                
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La factura fue rechazada correctamente.</div>';
            }

            if($titulo != "" && $observaciones != "") {
                $resultado = $this->extranet_model->enviar_comentarios($titulo, $observaciones, $rfc_proveedor);
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );
        }

        echo(json_encode($respuesta));
    }
    function autorizar_factura_2() {

        $this->db->cache_delete('extranet', 'autorizar_factura_2');
        $this->db->cache_off();
        
        $respuesta = array('mensaje' => '' );

        try {
            $factura = $this->input->post("factura", TRUE);            
            $precompromiso = $this->input->post("id_precompromiso", TRUE);
            $autorizacion = $this->input->post("autorizacion", TRUE);
            $titulo = $this->input->post("titulo", TRUE);
            $observaciones = $this->input->post("observaciones", TRUE);
            $rfc_proveedor = $this->input->post("rfc_proveedor", TRUE);

            if(!isset($factura) || $factura == NULL || $factura == "") {
                throw new Exception('Por favor, seleccione una factura para aprobar.');
            }

            if($autorizacion == "si") {

                $resultado_compromiso = $this->utilerias->generar_compromiso($precompromiso, $factura);

                if(isset($resultado_compromiso["compromiso"])) {

                    $respuesta["mensaje"] .= $resultado_compromiso["mensaje"];

                    $resultado_contrarecibo = $this->utilerias->generar_contrarecibo($resultado_compromiso["compromiso"], $factura);

                    if($resultado_contrarecibo["contrarecibo"]) {

                        $respuesta["mensaje"] .= $resultado_contrarecibo["mensaje"];

                        $datos_factura = $this->api_xmlrpc->autorizar_factura_2($factura, date('Y-m-d H:i:s'), $resultado_contrarecibo["contrarecibo"]);

                        if($datos_factura["estatus"] != 200) {
                            throw new Exception('Hubo un error al autorizar la factura deseada, por favor comuniquese con su administrador.');
                        }

                        $respuesta["mensaje"] .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La factura fue autorizada correctamente.</div>';

                    } else {
                        throw new Exception('Hubo un error al generar el contra recibo, por favor comuniquese con su administrador.');
                    }
                    
                } else {
                    // $this->debugeo->imprimir_pre($resultado_compromiso);
                    throw new Exception($resultado_compromiso["mensaje"]);
                }
                
            } else {
                $datos_factura = $this->api_xmlrpc->rechazar_factura($factura, $observaciones);

                if($datos_factura["estatus"] != 200) {
                    throw new Exception('Hubo un error al rechazar la factura deseada, por favor comuniquese con su administrador.');
                }
                
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La factura fue rechazada correctamente.</div>';
            }

            if($titulo != "" && $observaciones != "") {
                $resultado = $this->extranet_model->enviar_comentarios($titulo, $observaciones, $rfc_proveedor);
            }

        } catch (Exception $e) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'</div>',
            );
        }

        // $this->debugeo->imprimir_pre($respuesta);

        echo(json_encode($respuesta));
    }
    function tabla_proveedor_indice2() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        $this->datatables->select('id_proveedores, nombre, alta, RFC, email, presu_auto,presu_auto' )
//            ->from('cat_proveedores')
//            ->add_column('acciones', '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Proveedor"><i class="fa fa-check"></i></a>');
//
//        echo $this->datatables->generate();
//

        $resultado = $this->proveedor_extranet_model->get_datos_indice_extranet();

        $output = array();

        foreach($resultado as $key => $value) {
//             $this->debugeo->imprimir_pre($value);
            $output["data"][] = array(

                $value["id_proveedores"],
                $value["nombre"],
                $value["alta"],
                $value["RFC"],
                $value["email"],
                $value["presu_auto"],
                $value["presu_auto"],

            );
        }

//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }

    function tabla_proveedor_indice3() {
        
        $this->db->cache_delete('extranet', 'tabla_proveedor_indice3');
        $this->db->cache_off();

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $resultado = $this->proveedor_extranet_model->get_datos_indice_extranet();

        $output = array();

        foreach ($resultado as $key => $value) {
//             $this->debugeo->imprimir_pre($value);
            $output["data"][] = array(

                $value["id_proveedores"],
                $value["no_factura"],
                $value["modificado"],
                $value["contrato"],
                $value["nombre"],
                $value["importe"],
                $value["presu_auto"],
                $value["presu_rest"],

            );
        }
        echo json_encode($output);
    }

    function tabla_proveedor_glosa() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $resultado = $this->proveedor_extranet_model->get_datos_indice_extranet();

        $output = array();

        foreach ($resultado as $key => $value) {
//             $this->debugeo->imprimir_pre($value);
            $output["data"][] = array(

                $value["id_proveedores"],
                $value["no_factura"],
                $value["modificado"],
                $value["contrato"],
                $value["nombre"],
                $value["importe"],
                $value["presu_auto"],
                $value["presu_rest"],

            );
        }
        echo json_encode($output);
    }

    function tabla_proveedores_glosa() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $resultado = $this->proveedor_glosa_model->get_datos_indice_extranet();

        $output = array();

        foreach ($resultado as $key => $value) {
//             $this->debugeo->imprimir_pre($value);
            $output["data"][] = array(

                $value["id_proveedores"],
                $value["no_factura"],
                $value["modificado"],
                $value["contrato"],
                $value["nombre"],
                $value["importe"],
                $value["presu_auto"],
                $value["presu_rest"],

            );
        }
        echo json_encode($output);
    }

    function mostrar_datos() {

        $completar = $this->input->post("autocompletar");
        $this->db->trans_begin();

//            Se hace el query para tomar los datos de caratula del precompromiso
        $query_noticia = "SELECT * FROM cat_proveedores WHERE id_proveedores  = ?";
        $query_noticia = $this->db->query($query_noticia, array($completar));
        $result = $query_noticia->row_array();
            echo(json_encode($result));

    }

    function mostrar_datos1() {

        $completar = $this->input->post("autocompletar");
        $this->db->trans_begin();

//            Se hace el query para tomar los datos de caratula del precompromiso
        $query_noticia = "SELECT * FROM cat_proveedores WHERE id_proveedores  = ?";
        $query_noticia = $this->db->query($query_noticia, array($completar));
        $result = $query_noticia->row_array();
        echo(json_encode($result));

    }

    function mostrar_glosa() {

        $completa = $this->input->post("autocompletar");
        $this->db->trans_begin();

//            Se hace el query para tomar los datos de caratula del precompromiso
        $query_dato = "SELECT * FROM cat_proveedores_glosa WHERE id_proveedores  = ?";
        $query_dato = $this->db->query($query_dato, array($completa));
        $result = $query_dato->row_array();
        echo(json_encode($result));

    }

    /**
     * Create CAPTCHA image to verify user as a human
     *
     * @return  string
     */
    function _create_captcha()
    {
        $this->load->helper('captcha');

        $cap = create_captcha(array(
            'img_path'      => './'.$this->config->item('captcha_path', 'tank_auth'),
            'img_url'       => base_url().$this->config->item('captcha_path', 'tank_auth'),
            'font_path'     => './'.$this->config->item('captcha_fonts_path', 'tank_auth'),
            'font_size'     => $this->config->item('captcha_font_size', 'tank_auth'),
            'img_width'     => $this->config->item('captcha_width', 'tank_auth'),
            'img_height'    => $this->config->item('captcha_height', 'tank_auth'),
            'show_grid'     => $this->config->item('captcha_grid', 'tank_auth'),
            'expiration'    => $this->config->item('captcha_expire', 'tank_auth'),
        ));

        // Save captcha params in session
        $this->session->set_flashdata(array(
                'captcha_word' => $cap['word'],
                'captcha_time' => $cap['time'],
        ));

        return $cap['image'];
    }
// se llama los dartos de la tabla de contratos.

    function tabla_contrato_proveedor() {

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '-1');

            $this->datatables->select('RFC,
                                    nombre_proveedor,
                                    contrato,
                                    estatus
                                  ')
                ->from('cat_contratos')
                ->where('contrato !=', NULL)
                ->add_column('acciones', '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Proveedor"><i class="fa fa-check"></i></a>');

            echo $this->datatables->generate();
        }

    function tabla_consulta_contrato() {

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '-1');

            $this->datatables->select('RFC,
                                   nombre_proveedor
                                   ')
            ->from('cat_contratos')
            ->where('contrato !=', NULL)
            ->add_column('acciones', '<a class="seleccion_gasto" data-toggle="modal" data-dismiss="modal" title="Seleccionar Proveedor"><i class="fa fa-check"></i></a>');

        echo $this->datatables->generate();
    }

    function revisar_correo() {
        $this->output->unset_template();
        $this->load->model('tank_auth/users');

        $respuesta = array(
            "mensaje" => '',
        );

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = $this->security->xss_clean(trim($value));
        }

        $resultado = $this->users->revisar_existe_correo($datos["email"]);

        if(!$respuesta) {
            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Ese usuario ya está en uso.</div>',
                'hash' => $this->security->get_csrf_hash(),
            );
        } else {
            $respuesta = array(
                "mensaje" => '',
                'hash' => $this->security->get_csrf_hash(),
            );
        }

        echo(json_encode($respuesta));
    }
}


function revisar_usuario() {
    $this->output->unset_template();
    $this->load->model('tank_auth/users');

    $respuesta = array(
        "mensaje" => '',
    );

    $datos = $this->input->post();

    foreach ($datos as $key => $value) {
        $datos[$key] = $this->security->xss_clean(trim($value));
    }

    $resultado = $this->users->revisar_existe_usuario($datos["username"]);

    if(!$respuesta) {
        $respuesta = array(
            "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-times-circle fa-2x ic-msj"></i> Ese usuario ya está en uso.</div>',
            "tokenName" => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );
    } else {
        $respuesta = array(
            "mensaje" => '',
            "tokenName" => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );
    }

    echo(json_encode($respuesta));

}
