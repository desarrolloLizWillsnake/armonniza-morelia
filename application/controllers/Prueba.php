<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Prueba extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('prueba_model');
    }

    function index() {
        $this->load->view('prueba_view', array('error' => ' ' ));
        $this->load->library('image_lib');
    }

    function do_upload() {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_size'] = '100';
        // $config['max_width']  = '1024';
        // $config['max_height']  = '768';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('prueba_view', $error);
        }
        else {
            $data = array('upload_data' => $this->upload->data());

            $upload_data = $this->upload->data();

            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_data['full_path'];
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']     = 500;
            $config['height']   = 500;

            $this->load->library('image_lib', $config); 

            $this->image_lib->resize();

            if ( ! $this->image_lib->resize()) {
                $this->debugeo->imprimir_pre($this->image_lib->display_errors());
            }

            // $this->debugeo->imprimir_pre($upload_data);

            $imagen_insertada = $this->prueba_model->insertar_imagen($upload_data);

            if($imagen_insertada) {
                $nombre_archivo = $upload_data["raw_name"]."_thumb".$upload_data["file_ext"];
                $ruta = "uploads/".$nombre_archivo;
                $this->load->view('exito_prueba_view', array('archivo' => $ruta ));
                // $this->load->view('exito_prueba_view', $data);
            } else {
                $this->debugeo->imprimir_pre("Error al insertar la imagen");
            }

        }
    }

    function info() {
        echo(phpinfo());
    }

    function acomodar_precompromisos_terminados() {
        $sql = "SELECT numero_pre FROM mov_precompromiso_caratula WHERE estatus = 'Terminado';";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach ($result as $key => $value) {
            $datos_precompromiso = $this->ciclo_model->get_datos_precompromiso_detalle($value["numero_pre"]);

            $sql_caratula = "SELECT fecha_emision FROM mov_precompromiso_caratula WHERE numero_pre = ?";

            $datos_caratula_precompromiso = $this->ciclo_model->get_datos_precompromiso_caratula($value["numero_pre"], $sql_caratula);

            $mes_devolver = $this->utilerias->convertirFechaAMes($datos_caratula_precompromiso->fecha_emision);

            $query_compromisos_detalle = "SELECT importe, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle mcd
                                            JOIN mov_compromiso_caratula mcc
                                            ON mcd.numero_compromiso = mcc.numero_compromiso
                                            WHERE mcc.num_precompromiso = ".$value["numero_pre"]."
                                            AND mcc.enfirme = 1
                                            AND mcc.firma1 = 1
                                            AND mcc.firma2 = 1
                                            AND mcc.firma3 = 1
                                            AND mcc.cancelada = 0;";
            $datos_compromiso = $this->ciclo_model->get_arreglo_datos($query_compromisos_detalle);

//            $this->debugeo->imprimir_pre($datos_compromiso);

            $arreglo_precompromisos = array();
            $arreglo_compromisos = array();

            foreach($datos_precompromiso as $key_precompromiso => $value_precompromiso) {
                $estructura = json_decode($value_precompromiso->estructura, TRUE);
                $indice = $estructura["fuente_de_financiamiento"].".".$estructura["programa_de_financiamiento"].".".$estructura["centro_de_costos"].".".$estructura["capitulo"].".".$estructura["concepto"].".".$estructura["partida"];
                if (array_key_exists($indice, $arreglo_precompromisos)) {
                    $arreglo_precompromisos[$indice] += $value_precompromiso->importe;
                } else {
                    $arreglo_precompromisos[$indice] = $value_precompromiso->importe;
                }

            }

            unset($key_precompromiso);
            unset($value_precompromiso);

            foreach($datos_compromiso as $key_compromiso => $value_compromiso) {

                $estructura = json_decode($value_compromiso["estructura"], TRUE);
                $indice = $estructura["fuente_de_financiamiento"].".".$estructura["programa_de_financiamiento"].".".$estructura["centro_de_costos"].".".$estructura["capitulo"].".".$estructura["concepto"].".".$estructura["partida"];
                if (array_key_exists($indice, $arreglo_compromisos)) {
                    $arreglo_compromisos[$indice] += $value_compromiso["importe"];
                } else {
                    $arreglo_compromisos[$indice] = $value_compromiso["importe"];
                }

            }

            unset($key_compromiso);
            unset($value_compromiso);

            $total_precompromiso = 0;
            $importe_devolver = 0;
            $importe_devolver_total = 0;

            foreach($arreglo_precompromisos as $key_precompromiso => $value_precompromiso) {

                if (array_key_exists($key_precompromiso, $arreglo_compromisos)) {

                    $importe_devolver = abs($value_precompromiso - $arreglo_compromisos[$key_precompromiso]);

                    if($importe_devolver > 0) {
                        //        Se llama la funcion del modelo de egresos encargado de contar los niveles que existen
                        $total_egresos = $this->egresos_model->contar_egresos_elementos();
                //        Se llama a la funcion del model de egresos encargada de tomar los nombres delos niveles que existen
                        $nombres_egresos = $this->egresos_model->obtener_nombre_niveles();

                //        En este arreglo se van a guardar los nombres de los niveles
                        $nombre = array();

                //        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
                        foreach($nombres_egresos as $fila) {
                            array_push($nombre, $fila->descripcion);
                        }

                        $query_insertar = "INSERT INTO mov_precompromiso_detalle (numero_pre, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, mov_precompromiso_detalle.year, especificaciones, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";

                        for($i = 0; $i < $total_egresos->conteo; $i++){
                            $query_insertar .= "'".strtolower(str_replace(' ', '_', $nombre[$i]))."', ?, ";
                        }

                        $partida = explode(".",$key_precompromiso);

                        $query_insertar .= "'gasto', ?));";

                        $datos = array(
                            "ultimo_pre" => $value["numero_pre"],
                            "id_nivel" => 0,
                            "gasto" => "N/A",
                            "u_medida" => "N/A",
                            "cantidad" => 1,
                            "precio" => -1 * $importe_devolver,
                            "subtotal" => -1 * $importe_devolver,
                            "iva" => 0,
                            "importe" => -1 * $importe_devolver,
                            "titulo_gasto" => "Presupuesto Devuelto",
                            "descripcion_detalle" => "Presupuesto Devuelto",
                            "nivel1" => $partida[0],
                            "nivel2" => $partida[1],
                            "nivel3" => $partida[2],
                            "nivel4" => $partida[3],
                            "nivel5" => $partida[4],
                            "nivel6" => $partida[5],
                            "titulo_gasto" => "Presupuesto Devuelto",
                        );

                        $resultado = $this->ciclo_model->insertar_detalle_precompromiso($datos, $query_insertar);

                        $this->debugeo->imprimir_pre($resultado);
                        $this->debugeo->imprimir_pre("Estructura:".$key_precompromiso);
                        $this->debugeo->imprimir_pre("Precompromiso:".$value["numero_pre"]);
                        $this->debugeo->imprimir_pre($importe_devolver);

                    }

                } else {
//                    $this->debugeo->imprimir_pre("Entro");
                    $partida = explode(".",$key_precompromiso);
                    $this->debugeo->imprimir_pre('La partida '.$partida[0].' '.$partida[1].' '.$partida[2].' '.$partida[3].' '.$partida[4].' '.$partida[5].', no se encuentra del precompromiso.' );
                }
            }

        }
    }

    function acomodar_cuentas() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        // $cuenta_post = $this->input->post("poliza", TRUE);
        // $cuenta_inicial = substr($cuenta_post, 0);

        $this->benchmark->mark('code_start');

        $this->db->trans_begin();

        $sql_cuentas = "SELECT * FROM cat_cuentas_contables;";
        $query_cuentas = $this->db->query($sql_cuentas);
        $cuentas_contables = $query_cuentas->result_array();

        foreach ($cuentas_contables as $key => $value) {
            $padre = $this->BuscarPadre($value["cuenta"]);
            $nivel = $this->SetNivel($value["cuenta"]);

            $datos_actualizar = array(
                'cuenta_padre' => $padre,
                'nivel' => $nivel,
            );

            $this->db->where('cuenta', $value["cuenta"]);
            $this->db->update('cat_cuentas_contables', $datos_actualizar);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    private function BuscarPadre($ctaSeek = NULL) {
        $posicion_punto = strrpos($ctaSeek, '.', -1);
        $cuenta_cortada = substr($ctaSeek, 0, $posicion_punto);
        if (!$cuenta_cortada) {
            $cuenta_cortada = 0;
        }
        return $cuenta_cortada;
    }

    private function SetNivel($cuenta = NULL) {
        $arreglo_cuenta = explode(".", $cuenta);
        $nivel = count($arreglo_cuenta);
        return $nivel;
    }

    function revisar_partidas_duplicadas() {

        $this->db->cache_delete('prueba', 'revisar_partidas_duplicadas');
        $this->db->cache_off();
        
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $sql = "SELECT COLUMN_JSON(nivel) AS estructura
                    FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) != ''
                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) != ''
                    AND COLUMN_GET(nivel, 'capitulo' as char) != ''
                    AND COLUMN_GET(nivel, 'concepto' as char) != ''
                    AND COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {
            
            $estructura = json_decode($value["estructura"], TRUE);
            
            $sql2 = "SELECT COLUMN_JSON(nivel) AS estructura
                        FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            $query2 = $this->db->query($sql2, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
                ));
            
            if($query2->num_rows() != 1) {
                $this->debugeo->imprimir_pre($estructura["fuente_de_financiamiento"]. " ".
                $estructura["programa_de_financiamiento"]. " ".
                $estructura["centro_de_costos"]. " ".
                $estructura["capitulo"]. " ".
                $estructura["concepto"]. " ".
                $estructura["partida"]);
            }

        }
    }
    
}