<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Datos_remotos extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function tabla_precompromiso_detalle_remota() {
        $proveedor = $this->input->post("proveedor");

        if(!$proveedor) {
            $proveedor = 0;
        }
    
        $this->datatables->select(" '', numero_pre, tipo_requisicion, fecha_emision, total, total_pagado, estatus, fecha_cancelada")
                ->from('mov_precompromiso_caratula')
                ->where('id_proveedor', $proveedor)
                ->add_column('acciones', json_encode([
                                            'csrf_token' => $this->security->get_csrf_hash(),
                                            'csrf_name' => $this->security->get_csrf_token_name(),
                                        ]
                                    ));

        echo $this->datatables->generate();
    }
}