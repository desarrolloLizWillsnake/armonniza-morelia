<?php

class Funciones_consola extends CI_Controller {

    function ejecutar_funciones() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->benchmark->mark('code_start');

        $this->resetear_todas_las_partidas();
//        $this->fechas_adecuaciones_egresos();
        $this->checar_contrarecibos();
        $this->acomodar_saldos_precomprommiso_compromiso();
        $this->registrar_devengado();
        $this->registrar_ejercido();
        $this->registrar_pagado();

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function resetear_todas_las_partidas() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $sql = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, 'partida' as char) != '';";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();

        foreach($resultado as $key => $value){
            $this->resetear_partida($value["id_niveles"]);
//            $this->debugeo->imprimir_pre($value);
        }

        return TRUE;
    }

    function resetear_partida($id_nivel = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha reseteo una partida');

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

//        $id_nivel = 4167;

        $this->db->trans_start();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id_nivel));
        $resultado = $query->row_array();

        $estructura = json_decode($resultado["estructura"], TRUE);

//            echo("Estructura Inicial");
//            $this->debugeo->imprimir_pre($estructura);
//            $this->debugeo->imprimir_pre($resultado["id_niveles"]);

//            En esta parte se resetea el dinero de la partida a los saldos iniciales
        $estructura["enero_precompromiso"] = 0;
        $estructura["enero_compromiso"] = 0;
        $estructura["enero_devengado"] = 0;
        $estructura["enero_ejercido"] = 0;
        $estructura["enero_pagado"] = 0;
        $estructura["enero_saldo"] = $estructura["enero_inicial"];
        $estructura["febrero_precompromiso"] = 0;
        $estructura["febrero_compromiso"] = 0;
        $estructura["febrero_devengado"] = 0;
        $estructura["febrero_ejercido"] = 0;
        $estructura["febrero_pagado"] = 0;
        $estructura["febrero_saldo"] = $estructura["febrero_inicial"];
        $estructura["marzo_precompromiso"] = 0;
        $estructura["marzo_compromiso"] = 0;
        $estructura["marzo_devengado"] = 0;
        $estructura["marzo_ejercido"] = 0;
        $estructura["marzo_pagado"] = 0;
        $estructura["marzo_saldo"] = $estructura["marzo_inicial"];
        $estructura["abril_precompromiso"] = 0;
        $estructura["abril_compromiso"] = 0;
        $estructura["abril_devengado"] = 0;
        $estructura["abril_ejercido"] = 0;
        $estructura["abril_pagado"] = 0;
        $estructura["abril_saldo"] = $estructura["abril_inicial"];
        $estructura["mayo_precompromiso"] = 0;
        $estructura["mayo_compromiso"] = 0;
        $estructura["mayo_devengado"] = 0;
        $estructura["mayo_ejercido"] = 0;
        $estructura["mayo_pagado"] = 0;
        $estructura["mayo_saldo"] = $estructura["mayo_inicial"];
        $estructura["junio_precompromiso"] = 0;
        $estructura["junio_compromiso"] = 0;
        $estructura["junio_devengado"] = 0;
        $estructura["junio_ejercido"] = 0;
        $estructura["junio_pagado"] = 0;
        $estructura["junio_saldo"] = $estructura["junio_inicial"];
        $estructura["julio_precompromiso"] = 0;
        $estructura["julio_compromiso"] = 0;
        $estructura["julio_devengado"] = 0;
        $estructura["julio_ejercido"] = 0;
        $estructura["julio_pagado"] = 0;
        $estructura["julio_saldo"] = $estructura["julio_inicial"];
        $estructura["agosto_precompromiso"] = 0;
        $estructura["agosto_compromiso"] = 0;
        $estructura["agosto_devengado"] = 0;
        $estructura["agosto_ejercido"] = 0;
        $estructura["agosto_pagado"] = 0;
        $estructura["agosto_saldo"] = $estructura["agosto_inicial"];
        $estructura["septiembre_precompromiso"] = 0;
        $estructura["septiembre_compromiso"] = 0;
        $estructura["septiembre_devengado"] = 0;
        $estructura["septiembre_ejercido"] = 0;
        $estructura["septiembre_pagado"] = 0;
        $estructura["septiembre_saldo"] = $estructura["septiembre_inicial"];
        $estructura["octubre_precompromiso"] = 0;
        $estructura["octubre_compromiso"] = 0;
        $estructura["octubre_devengado"] = 0;
        $estructura["octubre_ejercido"] = 0;
        $estructura["octubre_pagado"] = 0;
        $estructura["octubre_saldo"] = $estructura["octubre_inicial"];
        $estructura["noviembre_precompromiso"] = 0;
        $estructura["noviembre_compromiso"] = 0;
        $estructura["noviembre_devengado"] = 0;
        $estructura["noviembre_ejercido"] = 0;
        $estructura["noviembre_pagado"] = 0;
        $estructura["noviembre_saldo"] = $estructura["noviembre_inicial"];
        $estructura["diciembre_precompromiso"] = 0;
        $estructura["diciembre_compromiso"] = 0;
        $estructura["diciembre_devengado"] = 0;
        $estructura["diciembre_ejercido"] = 0;
        $estructura["diciembre_pagado"] = 0;
        $estructura["diciembre_saldo"] = $estructura["diciembre_inicial"];
        $estructura["total_anual"] = 0;
        $estructura["ejercido_anual"] = 0;
        $estructura["devengado_anual"] = 0;
        $estructura["comprometido_anual"] = 0;
        $estructura["precomprometido_anual"] = 0;
        $estructura["pagado_anual"] = 0;

        $estructura["total_anual"] += $estructura["enero_inicial"];
        $estructura["total_anual"] += $estructura["febrero_inicial"];
        $estructura["total_anual"] += $estructura["marzo_inicial"];
        $estructura["total_anual"] += $estructura["abril_inicial"];
        $estructura["total_anual"] += $estructura["mayo_inicial"];
        $estructura["total_anual"] += $estructura["junio_inicial"];
        $estructura["total_anual"] += $estructura["julio_inicial"];
        $estructura["total_anual"] += $estructura["agosto_inicial"];
        $estructura["total_anual"] += $estructura["septiembre_inicial"];
        $estructura["total_anual"] += $estructura["octubre_inicial"];
        $estructura["total_anual"] += $estructura["noviembre_inicial"];
        $estructura["total_anual"] += $estructura["diciembre_inicial"];

//        echo("Estructura reiniciada");
//        $this->debugeo->imprimir_pre($estructura);

//            Esta es la parte para tomar todas las adecuaciones en firme que afectan a la partida
        $sql_adecuaciones = "SELECT md.mes_destino, md.total, md.texto
                            FROM mov_adecuaciones_egresos_detalle md
                            JOIN mov_adecuaciones_egresos_caratula mc
                            ON md.numero_adecuacion = mc.numero
                            WHERE COLUMN_GET(md.estructura, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(md.estructura, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(md.estructura, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(md.estructura, 'capitulo' as char) = ?
                            AND COLUMN_GET(md.estructura, 'concepto' as char) = ?
                            AND COLUMN_GET(md.estructura, 'partida' as char) = ?
                            AND mc.enfirme = 1
                            AND cancelada = 0;";
        $query_adecuaciones = $this->db->query($sql_adecuaciones, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $resultado_adecuaciones = $query_adecuaciones->result_array();

        foreach($resultado_adecuaciones as $llave => $valor) {
            if($valor["texto"] == "Ampliación") {
                $estructura[$valor["mes_destino"]."_saldo"] += $valor["total"];
                $estructura[$valor["mes_destino"]."_saldo"] = round($estructura[$valor["mes_destino"]."_saldo"], 2);
                $estructura["total_anual"] += $valor["total"];
                $estructura["total_anual"] = round($estructura["total_anual"], 2);
            } elseif($valor["texto"] == "Reducción") {
                $estructura[$valor["mes_destino"]."_saldo"] -= $valor["total"];
                $estructura[$valor["mes_destino"]."_saldo"] = round($estructura[$valor["mes_destino"]."_saldo"], 2);
                $estructura["total_anual"] -= $valor["total"];
                $estructura["total_anual"] = round($estructura["total_anual"], 2);
            }

        }

        unset($llave);
        unset($valor);

//        echo("Estructura después de Adecuaciones");
//        $this->debugeo->imprimir_pre($estructura);

//            Esta es la parte donde se toman todos los precompromisos en firme que afectan a la partida
        $sql_precompromiso = "SELECT mpc.numero_pre, mpc.fecha_emision, mpd.importe
                            FROM mov_precompromiso_detalle mpd
                            JOIN mov_precompromiso_caratula mpc
                            ON mpd.numero_pre = mpc.numero_pre
                            WHERE COLUMN_GET(mpd.nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(mpd.nivel, 'partida' as char) = ?
                            AND mpc.enfirme = 1
                            AND mpc.firma1 = 1
                            AND mpc.firma2 = 1
                            AND mpc.firma3 = 1
                            AND mpc.cancelada = 0;";
        $query_precompromiso = $this->db->query($sql_precompromiso, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $resultado_precompromiso = $query_precompromiso->result_array();

        foreach($resultado_precompromiso as $llave => $valor) {
//            $this->debugeo->imprimir_pre($valor);
            $mes_precompro = $this->utilerias->convertirFechaAMes($valor["fecha_emision"]);
            $estructura[$mes_precompro."_precompromiso"] += $valor["importe"];
            $estructura[$mes_precompro."_precompromiso"] = round($estructura[$mes_precompro."_precompromiso"], 2);
            $estructura["precomprometido_anual"] += $valor["importe"];
            $estructura["precomprometido_anual"] = round($estructura["precomprometido_anual"], 2);
        }

        unset($llave);
        unset($valor);

//        echo("Estructura despues de los precompromisos");
//        $this->debugeo->imprimir_pre($estructura);

        /**
         * Esta sección es para calcular todos los movimientos que tengan los compromisos desde un precompromiso
         */

//                Se verifica si existen compromisos asociados al precompromiso
        $sql_compromiso = "SELECT mcc.num_precompromiso, mcc.numero_compromiso, mcc.fecha_emision, mcd.importe
                            FROM mov_compromiso_detalle mcd
                            JOIN mov_compromiso_caratula mcc
                            ON mcd.numero_compromiso = mcc.numero_compromiso
                            WHERE COLUMN_GET(mcd.nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(mcd.nivel, 'partida' as char) = ?
                            AND mcc.enfirme = 1
                            AND mcc.firma1 = 1
                            AND mcc.firma2 = 1
                            AND mcc.firma3 = 1
                            AND mcc.cancelada = 0;";
        $query_compromiso = $this->db->query($sql_compromiso, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $resultado_compromiso = $query_compromiso->result_array();

        $arreglo_compromisos = array();

        foreach($resultado_compromiso as $llave_commpro => $valor_compro) {

//            $this->debugeo->imprimir_pre($valor_compro);
            $mes_compro = $this->utilerias->convertirFechaAMes($valor_compro["fecha_emision"]);

            if($valor_compro["num_precompromiso"] != 0){

                $arreglo_compromisos[] = $valor_compro["numero_compromiso"];

                $sql_importe_precompromiso = "SELECT fecha_emision, cancelada FROM mov_precompromiso_caratula WHERE numero_pre = ?;";
                $query_importe_precompromiso = $this->db->query($sql_importe_precompromiso, array($valor_compro["num_precompromiso"]));
                $resultado_importe_precompromiso = $query_importe_precompromiso->row_array();
//                    $this->debugeo->imprimir_pre($resultado_importe_precompromiso);

                if($resultado_importe_precompromiso["cancelada"] != 1){
                    $mes_autoriza_precompro = $this->utilerias->convertirFechaAMes($resultado_importe_precompromiso["fecha_emision"]);

                    $check_precompromiso = $estructura[$mes_autoriza_precompro."_precompromiso"] - $valor_compro["importe"];

                    if($check_precompromiso < 0) {
                        $estructura["precomprometido_anual"] -= $estructura[$mes_autoriza_precompro."_precompromiso"];
                        $estructura[$mes_autoriza_precompro."_precompromiso"] -= $estructura[$mes_autoriza_precompro."_precompromiso"];
                    } else {
//                        Se resta el importe del compromiso en al compromiso en la fecha que fue autorizado, y al precomprometido anual
                        $estructura[$mes_autoriza_precompro."_precompromiso"] -= $valor_compro["importe"];
//                        $this->debugeo->imprimir_pre($estructura[$mes_autoriza_precompro."_precompromiso"]);
                        $estructura[$mes_autoriza_precompro."_precompromiso"] = round($estructura[$mes_autoriza_precompro."_precompromiso"], 2);
//                        $this->debugeo->imprimir_pre($estructura[$mes_autoriza_precompro."_precompromiso"]);
                        $estructura["precomprometido_anual"] -= $valor_compro["importe"];
//                        $this->debugeo->imprimir_pre($estructura["precomprometido_anual"]);
                        $estructura["precomprometido_anual"] = round($estructura["precomprometido_anual"], 2);
//                        $this->debugeo->imprimir_pre($estructura["precomprometido_anual"]);
                    }


                    $estructura[$mes_compro."_compromiso"] += $valor_compro["importe"];
                    $estructura[$mes_compro."_compromiso"] = round($estructura[$mes_compro."_compromiso"], 2);
                    $estructura["comprometido_anual"] += $valor_compro["importe"];
                    $estructura["comprometido_anual"] = round($estructura["comprometido_anual"], 2);

//                    Se resta el comprometido al saldo mensual, y al aprobado anual
                    $estructura[$mes_compro."_saldo"] -= $valor_compro["importe"];
                    $estructura[$mes_compro."_saldo"] = round($estructura[$mes_compro."_saldo"], 2);
                    $estructura["total_anual"] -= $valor_compro["importe"];
                    $estructura["total_anual"] = round($estructura["total_anual"], 2);
                }

            } else {
//                    Se suma el importe al campo comprometido del mes, y al comprometido anual
                $estructura[$mes_compro."_compromiso"] += $valor_compro["importe"];
                $estructura[$mes_compro."_compromiso"] = round($estructura[$mes_compro."_compromiso"], 2);
                $estructura["comprometido_anual"] += $valor_compro["importe"];
                $estructura["comprometido_anual"] = round($estructura["comprometido_anual"], 2);

//                    Se resta el comprometido al saldo mensual, y al aprobado anual
                $estructura[$mes_compro."_saldo"] -= $valor_compro["importe"];
                $estructura[$mes_compro."_saldo"] = round($estructura[$mes_compro."_saldo"], 2);
                $estructura["total_anual"] -= $valor_compro["importe"];
                $estructura["total_anual"] = round($estructura["total_anual"], 2);

            }
        }

        unset($llave_commpro);
        unset($valor_compro);

//        echo("Estructura despues de los movimientos bancarios");
//        $this->debugeo->imprimir_pre($estructura);

//            Esta parte se encarga de actualizar los valores de la partida
        $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'enero_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'enero_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'enero_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'enero_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'enero_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'febrero_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'marzo_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'abril_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'mayo_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'junio_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'julio_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'agosto_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'septiembre_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'octubre_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'noviembre_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_precompromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_compromiso', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_devengado', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'diciembre_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'precomprometido_anual', ?),
                            nivel = COLUMN_ADD(nivel, 'comprometido_anual', ?),
                            nivel = COLUMN_ADD(nivel, 'devengado_anual', ?),
                            nivel = COLUMN_ADD(nivel, 'pagado_anual', ?),
                            nivel = COLUMN_ADD(nivel, 'ejercido_anual', ?),
                            nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(nivel, 'partida' as char) = ?;";
        $query_actualizar = $this->db->query($sql_actualizar, array(
            $estructura["enero_precompromiso"],
            $estructura["enero_compromiso"],
            $estructura["enero_devengado"],
            $estructura["enero_ejercido"],
            $estructura["enero_pagado"],
            $estructura["enero_saldo"],
            $estructura["febrero_precompromiso"],
            $estructura["febrero_compromiso"],
            $estructura["febrero_devengado"],
            $estructura["febrero_pagado"],
            $estructura["febrero_ejercido"],
            $estructura["febrero_saldo"],
            $estructura["marzo_precompromiso"],
            $estructura["marzo_compromiso"],
            $estructura["marzo_devengado"],
            $estructura["marzo_pagado"],
            $estructura["marzo_ejercido"],
            $estructura["marzo_saldo"],
            $estructura["abril_precompromiso"],
            $estructura["abril_compromiso"],
            $estructura["abril_devengado"],
            $estructura["abril_pagado"],
            $estructura["abril_ejercido"],
            $estructura["abril_saldo"],
            $estructura["mayo_precompromiso"],
            $estructura["mayo_compromiso"],
            $estructura["mayo_devengado"],
            $estructura["mayo_pagado"],
            $estructura["mayo_ejercido"],
            $estructura["mayo_saldo"],
            $estructura["junio_precompromiso"],
            $estructura["junio_compromiso"],
            $estructura["junio_devengado"],
            $estructura["junio_pagado"],
            $estructura["junio_ejercido"],
            $estructura["junio_saldo"],
            $estructura["julio_precompromiso"],
            $estructura["julio_compromiso"],
            $estructura["julio_devengado"],
            $estructura["julio_pagado"],
            $estructura["julio_ejercido"],
            $estructura["julio_saldo"],
            $estructura["agosto_precompromiso"],
            $estructura["agosto_compromiso"],
            $estructura["agosto_devengado"],
            $estructura["agosto_pagado"],
            $estructura["agosto_ejercido"],
            $estructura["agosto_saldo"],
            $estructura["septiembre_precompromiso"],
            $estructura["septiembre_compromiso"],
            $estructura["septiembre_devengado"],
            $estructura["septiembre_pagado"],
            $estructura["septiembre_ejercido"],
            $estructura["septiembre_saldo"],
            $estructura["octubre_precompromiso"],
            $estructura["octubre_compromiso"],
            $estructura["octubre_devengado"],
            $estructura["octubre_pagado"],
            $estructura["octubre_ejercido"],
            $estructura["octubre_saldo"],
            $estructura["noviembre_precompromiso"],
            $estructura["noviembre_compromiso"],
            $estructura["noviembre_devengado"],
            $estructura["noviembre_pagado"],
            $estructura["noviembre_ejercido"],
            $estructura["noviembre_saldo"],
            $estructura["diciembre_precompromiso"],
            $estructura["diciembre_compromiso"],
            $estructura["diciembre_devengado"],
            $estructura["diciembre_pagado"],
            $estructura["diciembre_ejercido"],
            $estructura["diciembre_saldo"],
            $estructura["precomprometido_anual"],
            $estructura["comprometido_anual"],
            $estructura["devengado_anual"],
            $estructura["pagado_anual"],
            $estructura["ejercido_anual"],
            $estructura["total_anual"],
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $this->db->trans_complete();

//            $this->debugeo->imprimir_pre($query_actualizar);

//            echo("Estructura Final");
//            $this->debugeo->imprimir_pre($estructura);

        return($query_actualizar);

    }

    function fechas_adecuaciones_egresos() {
//        $this->benchmark->mark('code_start');

        $this->db->trans_start();

        $sql = "SELECT med.*
                FROM mov_adecuaciones_egresos_detalle med
                JOIN mov_adecuaciones_egresos_caratula mac
                WHERE mac.enfirme = 1
                AND mac.cancelada != 1
                AND med.mes_destino != 'Mes del que se tomará/agregará el presupuesto'
                GROUP BY med.id_adecuaciones_egresos_detalle;";
        $query = $this->db->query($sql);
        $resultado = $query->result_array();
        foreach($resultado as $key => $value) {
            $fecha_aplicacion = explode("-", $value["fecha_aplicacion"]);
            $mes_destino = $this->mes_a_fecha($value["mes_destino"]);
            if($fecha_aplicacion[1] != $mes_destino) {
//                $this->debugeo->imprimir_pre($resultado[$key]);
//                $this->debugeo->imprimir_pre($value["mes_destino"]);
//                $this->debugeo->imprimir_pre($value["fecha_aplicacion"]);
                $resultado[$key]["fecha_aplicacion"] = $fecha_aplicacion[0]."-".$mes_destino."-".$fecha_aplicacion[2];
//                $this->debugeo->imprimir_pre($resultado[$key]);

                $datos_actualizar = array(
                    'fecha_aplicacion' => $resultado[$key]["fecha_aplicacion"],
                );

                $this->db->where('id_adecuaciones_egresos_detalle', $resultado[$key]["id_adecuaciones_egresos_detalle"]);
                $this->db->update('mov_adecuaciones_egresos_detalle', $datos_actualizar);

            }
//            $this->debugeo->imprimir_pre($mes_destino);
        }

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $this->db->trans_complete();

        log_message('info', 'Se ejecutó la función que acomoda las adecuaciones presupuestarias conforme a su mes.');
    }

    function mes_a_fecha($mes) {
        switch($mes) {
            case 'enero':
                $mes = 1;
                break;

            case 'febrero':
                $mes = 2;
                break;

            case 'marzo':
                $mes = 3;
                break;

            case 'abril':
                $mes = 4;
                break;

            case 'mayo':
                $mes = 5;
                break;

            case 'junio':
                $mes = 6;
                break;

            case 'julio':
                $mes = 7;
                break;

            case 'agosto':
                $mes = 8;
                break;

            case 'septiembre':
                $mes = 9;
                break;

            case 'octubre':
                $mes = 10;
                break;

            case 'noviembre':
                $mes = 11;
                break;

            case 'diciembre':
                $mes = 12;
                break;
        }

        return $mes;
    }

    function fecha_a_mes($mes) {
        switch($mes) {
            case 1:
                $mes = 'enero';
                break;

            case 2:
                $mes = 'febrero';
                break;

            case 3:
                $mes = 'marzo';
                break;

            case 4:
                $mes = 'abril';
                break;

            case 5:
                $mes = 'mayo';
                break;

            case 6:
                $mes = 'junio';
                break;

            case 7:
                $mes = 'julio';
                break;

            case 8:
                $mes = 'agosto';
                break;

            case 9:
                $mes = 'septiembre';
                break;

            case 10:
                $mes = 'octubre';
                break;

            case 11:
                $mes = 'noviembre';
                break;

            case 12:
                $mes = 'diciembre';
                break;
        }

        return $mes;
    }

    function checar_contrarecibos() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha checado los contrarecibos');
        $this->db->select('id_contrarecibo_caratula, numero_compromiso')->from('mov_contrarecibo_caratula')->where('numero_compromiso !=', '')->where('enfirme', 1)->where('cancelada', 0);
        $query = $this->db->get();
        $resultado = $query->result_array();
        foreach($resultado as $key => $value) {
            $this->db->select('numero_contrarecibo')->from('mov_contrarecibo_detalle')->where('numero_contrarecibo', $value["id_contrarecibo_caratula"]);
            $query_detalle = $this->db->get();
            $resultado_detalle = $query_detalle->row_array();
            if(!$resultado_detalle) {
//                $this->debugeo->imprimir_pre($value);
                $resultado = $this->ciclo_model->copiarDatosCompromiso($value["numero_compromiso"], $value["id_contrarecibo_caratula"]);
                if($resultado){
                    echo("Exito al copiar los datos del compromiso ".$value["numero_compromiso"].", al contrarecibo ".$value["id_contrarecibo_caratula"]);
                } else {
                    echo("Hubo un error al copiar los datos del compromiso ".$value["numero_compromiso"].", al contrarecibo ".$value["id_contrarecibo_caratula"]);
                }
            }
        }

        $this->db->trans_complete();
    }

    function resetar_devengado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach($result as $fila) {

            $estructura = json_decode($fila->estructura, TRUE);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'febrero_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'marzo_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'abril_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'mayo_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'junio_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'julio_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'agosto_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'septiembre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'octubre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'noviembre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'diciembre_devengado', 0),
                                nivel = COLUMN_ADD(nivel, 'devengado_anual', 0)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $this->db->query($sql_actualizar, array(
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function resetar_modificado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach($result as $fila) {

            $estructura = json_decode($fila->estructura, TRUE);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, 'enero_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'febrero_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'marzo_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'abril_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'mayo_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'junio_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'julio_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'agosto_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'septiembre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'octubre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'noviembre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'diciembre_saldo', 0),
                                nivel = COLUMN_ADD(nivel, 'total_anual', 0)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $this->db->query($sql_actualizar, array(
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function resetar_recaudado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT id_niveles, COLUMN_JSON(nivel) AS estructura
               FROM cat_niveles
               WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != ''";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach($result as $fila) {
//            $this->debugeo->imprimir_pre($fila);

            $estructura = json_decode($fila->estructura, TRUE);

            $sql_actualizar = "UPDATE cat_niveles SET
                                nivel = COLUMN_ADD(nivel, 'enero_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'febrero_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'marzo_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'abril_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'mayo_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'junio_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'julio_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'agosto_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'septiembre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'octubre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'noviembre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'diciembre_recaudado', 0),
                                nivel = COLUMN_ADD(nivel, 'recaudado_anual', 0)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $this->db->query($sql_actualizar, array(
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function acomodar_saldos_precomprommiso_compromiso() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        $data = array(
            'total_compromiso' => 0,
        );

        $this->db->update('mov_precompromiso_caratula', $data);

        $sql = "SELECT mpc.numero_pre, mpc.total, mpc.total_compromiso, mcd.numero_compromiso, mcd.importe
                    FROM mov_precompromiso_caratula mpc
                        JOIN mov_compromiso_caratula mcc
                            ON mpc.numero_pre = mcc.num_precompromiso
                        JOIN mov_compromiso_detalle mcd
                            ON mcc.numero_compromiso = mcd.numero_compromiso
                    WHERE mpc.enfirme = 1
                        AND mpc.firma1 = 1
                        AND mpc.firma2 = 1
                        AND mpc.firma3 = 1
                        AND mpc.cancelada != 1
                        AND mcc.enfirme = 1
                        AND mcc.firma1 = 1
                        AND mcc.firma2 = 1
                        AND mcc.firma3 = 1
                        AND mcc.cancelada != 1";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $key => $value) {
            $this->db->select('total_compromiso')->from('mov_precompromiso_caratula')->where('numero_pre', $value["numero_pre"]);
            $query_precompro = $this->db->get();
            $importe = $query_precompro->row_array();

//            $this->debugeo->imprimir_pre($importe["total_compromiso"]);

            $data = array(
                'total_compromiso' => $importe["total_compromiso"] + $value["importe"],
            );

            $this->db->where('numero_pre', $value["numero_pre"]);
            $this->db->update('mov_precompromiso_caratula', $data);
        }

        $this->db->trans_complete();
    }

    function acomodar_saldos_devengado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT mdd.*, COLUMN_JSON(nivel) AS estructura
                FROM mov_devengado_detalle mdd
                JOIN mov_devengado_caratula mdc
                ON mdd.numero_devengado = mdc.numero
                WHERE mdc.enfirme = 1;";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $fila){
            $sql_estructura = "SELECT COLUMN_JSON(nivel) AS estructura FROM cat_niveles WHERE id_niveles = ?;";
            $query_estructura = $this->db->query($sql_estructura, array($fila["id_nivel"]));
            $fila_estructura = $query_estructura->row_array();
            $estructura = json_decode($fila_estructura["estructura"], TRUE);

            $fecha = explode("-",$fila["fecha_aplicacion"]);
            $mes_destino = $this->fecha_a_mes($fecha[1]);

//            $this->debugeo->imprimir_pre($fila);
            $devengado_mes = $estructura[$mes_destino."_devengado"] + $fila["total_importe"];
            $devengado_anual = $estructura["devengado_anual"] + $fila["total_importe"];

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_destino."_devengado', ?),
                                nivel = COLUMN_ADD(nivel, 'devengado_anual', ?)
                                WHERE id_niveles = ?;";
            $resultado_actualizar = $this->db->query($sql_actualizar, array(
                round($devengado_mes, 2),
                round($devengado_anual, 2),
                $fila["id_nivel"],
            ));

//            $this->debugeo->imprimir_pre($resultado_actualizar);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function acomodar_saldos_recaudado_ingresos() {

        $this->db->trans_begin();

        $sql = "SELECT mrd.fecha_aplicacion AS fecha, mrd.total_importe, COLUMN_JSON(mrd.nivel) AS estructura
                FROM mov_recaudado_detalle mrd
                JOIN mov_recaudado_caratula mrc
                ON mrd.numero_recaudado = mrc.numero
                WHERE mrc.enfirme = 1
                AND mrc.cancelada = 0;";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $key => $value){
//            $this->debugeo->imprimir_pre($value);

            $estructura_inicial = json_decode($value["estructura"], TRUE);
            $sql_estructura = "SELECT COLUMN_JSON(nivel) AS estructura
                                FROM cat_niveles
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $query_estructura = $this->db->query($sql_estructura, array(
                $estructura_inicial["gerencia"],
                $estructura_inicial["centro_de_recaudacion"],
                $estructura_inicial["rubro"],
                $estructura_inicial["tipo"],
                $estructura_inicial["clase"],
                ));

            $fila_estructura = $query_estructura->row_array();

            $estructura = json_decode($fila_estructura["estructura"], TRUE);

            $fecha = explode("-", $value["fecha"]);
            $mes_destino = $this->fecha_a_mes($fecha[1]);

//            $this->debugeo->imprimir_pre($estructura[$mes_destino."_recaudado"]);
//            $this->debugeo->imprimir_pre($estructura["recaudado_anual"]);

            $recaudado_mes = $estructura[$mes_destino."_recaudado"] + $value["total_importe"];
            $recaudado_anual = $estructura["recaudado_anual"] + $value["total_importe"];

//            $this->debugeo->imprimir_pre($recaudado_mes);
//            $this->debugeo->imprimir_pre($recaudado_anual);

            $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_destino."_recaudado', ?),
                                nivel = COLUMN_ADD(nivel, 'recaudado_anual', ?)
                                WHERE COLUMN_GET(nivel, 'gerencia' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                AND COLUMN_GET(nivel, 'rubro' as char) = ?
                                AND COLUMN_GET(nivel, 'tipo' as char) = ?
                                AND COLUMN_GET(nivel, 'clase' as char) = ?;";
            $resultado_actualizar = $this->db->query($sql_actualizar, array(
                round($recaudado_mes, 2),
                round($recaudado_anual, 2),
                $estructura["gerencia"],
                $estructura["centro_de_recaudacion"],
                $estructura["rubro"],
                $estructura["tipo"],
                $estructura["clase"],
            ));

        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
        }
        else {
            $this->db->trans_commit();
            $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
        }
    }

    function tomar_facturas() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_begin();

        $DB1 = $this->load->database('pedidos', TRUE);

        $sql_facturas = "SELECT ssscta1, fecha, folio, modulo,
                            no_mov, edocfd, efecto,
                            cant AS cantidad, impbru AS importe, descto AS descuento,
                            iva, total, noclie,
                            receprfc, recepnombre
                            FROM fe_facturas
                            LEFT JOIN fe_claves
                            ON (fe_facturas.modulo=fe_claves.libreria)
                            WHERE fecha >= DATE(NOW())
                            AND efecto = 'I'
                            AND edocfd = 1
                            AND ssscta1 <> '';";
        $query_factura = $DB1->query($sql_facturas);

        $resultados_facturas = $query_factura->result_array();

        $conteo = 0;

        try {

            foreach($resultados_facturas as $key => $value) {

                //foreach ($value as $key_valores => $value_valores) {
                //     $value[$key_valores]=trim(utf8_encode($value_valores));
                //}

                $conteo += $value["importe"];

                $last = 0;

                //Se toma el numero del ultimo devengado
                $ultimo = $this->recaudacion_model->ultimo_devengado();

                if($ultimo) {
                    $last = $ultimo->ultimo + 1;
                }
                else {
                    $last = 1;
                }

                $sql_centro_recaudacion = "SELECT COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                                            FROM cat_niveles
                                            WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                                            GROUP BY COLUMN_GET(nivel, 'centro_de_recaudacion' as char);";
                $query_centro_recaudacion = $this->db->query($sql_centro_recaudacion, array($value["ssscta1"]));
                $nombre_centro_de_recaudacion = $query_centro_recaudacion->row_array();

                $value["ultimo"] = $last;
                $value["fecha_detalle"] = $value["fecha"];
                $value["subsidio"] = "Ingresos Propios";
                $value["descripcion_detalle"] = "Venta de la librería: ".$value["modulo"];
                $value["tipo_pago"] = "No Identificado";
                $value["nivel1"] = 103;
                $value["nivel2"] = $value["ssscta1"];
                $value["nivel3"] = 7;
                $value["nivel4"] = 72;
                $value["nivel5"] = 72001;
                $value["clasificacion"] = "Productos y/o Bienes";
                $value["num_movimiento"] = $value["folio"];
                $value["clave_cliente"] = $value["ssscta1"];
                $value["cliente"] = $nombre_centro_de_recaudacion["descripcion"];
                $value["fecha_solicitud"] = $value["fecha"];
                $value["descripcion"] = "Venta de la librería: ".$value["modulo"];
                $value["estatus_recaudado"] = 0;

                $value["check_firme"] = 1;

                if($value["edocfd"] == 0) {
                    $value["estatus"] = "Cancelada";
                    $value["cancelada"] = 1;
                    $value["fecha_cancelada"] = $value["fecha"];
                } else {
                    $value["estatus"] = "Activo";
                    $value["cancelada"] = 0;
                    $value["fecha_cancelada"] = "0000-00-00";
                }

                $total_ingresos = $this->recaudacion_model->contar_ingresos_elementos();

                $nombres_ingresos = $this->recaudacion_model->obtener_nombre_niveles();

                $nombre = array();

                foreach($nombres_ingresos as $fila) {
                    array_push($nombre, $fila->descripcion);
                }

                $query = "SELECT id_niveles FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ? ";

                for($i = 1; $i < $total_ingresos->conteo; $i++){
                    $query .= "AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[$i]))."' as char) = ? ";
                }

                $existe = $this->recaudacion_model->existeEstructura(array(
                    'nivel1' => 103,
                    'nivel2' => $value["ssscta1"],
                    'nivel3' => 7,
                    'nivel4' => 72,
                    'nivel5' => 72001,
                ), $query);

                if(!isset($existe->id_niveles)) {
                    throw new Exception('No existe la estructura ingresada del Centro de Recaudación '.$value["ssscta1"]);
                }

                $this->recaudacion_model->apartarDevengado($last);

                $value["id_nivel"] = $existe->id_niveles;

                $value["total_importe"] = round( ($value["importe"] - $value["descuento"]) + $value["iva"], 2);
                $value["importe_total"] = $value["total_importe"];

                $query_insertar = "INSERT INTO mov_devengado_detalle (numero_devengado, id_nivel, fecha_aplicacion, subsidio, cantidad, importe, descuento, iva, total_importe, descripcion_detalle, fecha_sql, hora_aplicacion, tipo_pago, nivel ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE(";

                for ($i = 0; $i < $total_ingresos->conteo; $i++) {
                    $query_insertar .= "'" . strtolower(str_replace(' ', '_', $nombre[$i])) . "', ?, ";
                }

                $query_insertar .= "'dato', ?));";

                $resultado_detalle = $this->recaudacion_model->insertar_detalle_devengado($value, $query_insertar);

                if ($resultado_detalle) {

                    $resultado_insertar_caratula = $this->recaudacion_model->insertar_caratula_devengado_automatico($value);

                    if($resultado_insertar_caratula) {
                        $this->recaudacion_model->marcarDevengado($value);

                        //$this->debugeo->imprimir_pre($value);

                        //$this->debugeo->imprimir_pre("El devengado con folio ".$value["folio"]." se insertó correctamente.");

                        $this->db->trans_commit();
                    } else {
                        throw new Exception('Hubo un error al insertar la caratula con folio '.$value["folio"]);
                    }

                } else {
                    throw new Exception('Hubo un error el detalle con folio '.$value["folio"]);
                }

            }

        } catch (Exception $e) {
            $this->db->trans_rollback();
            $this->debugeo->imprimir_pre($e->getMessage());
        }

        //$this->debugeo->imprimir_pre($conteo);

        $this->tomar_facturas_egresos();

    }

    function tomar_facturas_egresos() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha tomado facturas');

        $DB1 = $this->load->database('pedidos', TRUE);

        $sql_facturas = "SELECT ssscta1, fecha, folio, modulo,
                            no_mov, edocfd, efecto,
                            cant AS cantidad, impbru AS importe, descto AS descuento,
                            iva, total, noclie,
                            receprfc, recepnombre, mov
                            FROM fe_facturas
                            LEFT JOIN fe_claves
                            ON (fe_facturas.modulo=fe_claves.libreria)
                            WHERE fecha >= DATE(NOW())
                            AND efecto = 'E'
                            AND edocfd = 1
                            AND ssscta1 <> '';";
        $query_factura = $DB1->query($sql_facturas);

        $resultados_facturas = $query_factura->result_array();

        foreach($resultados_facturas as $key => $value) {

            foreach ($value as $key_valores => $value_valores) {
                $value[$key_valores]=trim(utf8_encode($value_valores));
            }

            unset($key_valores);
            unset($value_valores);

            try {

                $this->db->trans_begin();

                if($value["mov"] == "R" || $value["mov"] == "D" || $value["mov"] == "C") {
                    $this->utilerias->generar_poliza_facturas_egresos_directa($value);
                }

                $datos_insertar = array(
                    'no_mov' => $value["no_mov"],
                    'folio' => $value["folio"],
                    'modulo' => $value["modulo"],
                    'fecha' => $value["fecha"],
                    'impbru' => $value["importe"],
                    'descto' => $value["descuento"],
                    'iva' => $value["iva"],
                    'no_mov' => $value["no_mov"],
                    'total' => round( ($value["importe"] - $value["descuento"]) + $value["iva"], 2),
                    'efecto' => "E",
                    'mov' => $value["mov"],
                    'recepnombre' => $value["recepnombre"],
                    'receprfc' => $value["receprfc"],
                    'centro_recaudacion' => $value["ssscta1"],
                );

                $resultado_insertar = $this->db->insert('facturas_adjuntas', $datos_insertar);

                if ($resultado_insertar) {

                    $this->db->trans_commit();

                    $this->debugeo->imprimir_pre("El la factura de egresos con folio ".$value["folio"]." se insertó correctamente.");

                } else {
                    throw new Exception('Hubo un error al insertar la factura de egresos con folio '.$value["folio"]);
                }

            }
            catch (Exception $e) {
                $this->db->trans_rollback();
                $this->debugeo->imprimir_pre($e->getMessage());
            }

        }

    }

    function tomar_facturas_canceladas() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha tomado facturas canceladas');

        $DB1 = $this->load->database('pedidos', TRUE);

        $sql_facturas = "SELECT * FROM fe_cancelados;";
        $query_factura = $DB1->query($sql_facturas);

        $resultados_facturas = $query_factura->result_array();

        foreach($resultados_facturas as $key => $value) {

            foreach ($value as $key_valores => $value_valores) {
                $value[$key_valores]=trim(utf8_encode($value_valores));
            }

            unset($key_valores);
            unset($value_valores);

            try {
                //$this->debugeo->imprimir_pre($value);

                $sql_por_cancelar = "SELECT numero, cancelada, estatus FROM mov_devengado_caratula WHERE no_movimiento = ?;";
                $query_por_cancelar = $this->db->query($sql_por_cancelar, array($value["folio"]));
                $result_por_cancelar = $query_por_cancelar->row_array();

                $this->debugeo->imprimir_pre($result_por_cancelar);
            }
            catch (Exception $e) {
//                $this->db->trans_rollback();
//                $this->debugeo->imprimir_pre($e->getMessage());
            }

        }

    }

    function acomodar_modificado() {
        try {

//            $mes_anterior = date("m", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")));
            $mes_anterior = 10;
            $ultimo_dia_mes_anterior = cal_days_in_month(CAL_GREGORIAN, $mes_anterior, 2015);
            $mes_calcular = $this->fecha_a_mes($mes_anterior);
            $fecha = "2015-".$mes_anterior."-".$ultimo_dia_mes_anterior;

            $sql = "SELECT COLUMN_JSON(nivel) AS estructura
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'gerencia' as char) != ''
                AND COLUMN_GET(nivel, 'centro_de_recaudacion' as char) != ''
                AND COLUMN_GET(nivel, 'rubro' as char) != ''
                AND COLUMN_GET(nivel, 'tipo' as char) != ''
                AND COLUMN_GET(nivel, 'clase' as char) != '';";
            $query = $this->db->query($sql);
            $result = $query->result_array();

            $this->db->trans_begin();

            foreach($result as $key => $value) {

                $estructura = json_decode($value["estructura"], TRUE);

                if($estructura[$mes_calcular . "_devengado"] == 0) {
                    continue;
                }

                if ($estructura[$mes_calcular . "_inicial"] > $estructura[$mes_calcular . "_devengado"]) {
//                    $this->debugeo->imprimir_pre("Reducción");
//                $this->debugeo->imprimir_pre("Cantidades Inicial [".$estructura[$mes_calcular."_inicial"]."] Devengado [".$estructura[$mes_calcular."_devengado"]."]");
                    $value["tipo"] = "Reducción";
                    $value["total_hidden"] = round($estructura[$mes_calcular . "_inicial"] - $estructura[$mes_calcular . "_devengado"], 2);

                } elseif ($estructura[$mes_calcular . "_inicial"] < $estructura[$mes_calcular . "_devengado"]) {
//                    $this->debugeo->imprimir_pre("Ampliación");
//                $this->debugeo->imprimir_pre("Cantidades Inicial [".$estructura[$mes_calcular."_inicial"]."] Devengado [".$estructura[$mes_calcular."_devengado"]."]");
                    $value["tipo"] = "Ampliación";
                    $value["total_hidden"] = round($estructura[$mes_calcular . "_devengado"] - $estructura[$mes_calcular . "_inicial"], 2);
                } elseif ($estructura[$mes_calcular . "_inicial"] == $estructura[$mes_calcular . "_devengado"]) {
                    continue;
                }

                $value["fecha"] = $fecha;
                $value["mes_destino"] = $mes_calcular;
                $value["cantidad"] = 1;

                $resultado_crear = $this->agregar_adecuacion($value);

                if ($resultado_crear == "exito") {
//                    $this->debugeo->imprimir_pre("La adecuación se agregó con éxito");
                } elseif ($resultado_crear == "error_caratula") {
                    throw new Exception('Hubo un erro al insertar los datos de la carátula en la adecuación de la estructura siguiente,  Gerencia ['.$estructura["gerencia"]."] Centro de Recaudación [".$estructura["centro_de_recaudacion"]."] Rubro [".$estructura["rubro"]."] Tipo [".$estructura["clase"]."] Clase [".$estructura["clase"]."] <br /> Del mes [".$mes_calcular."]");
                } elseif ($resultado_crear == "error_detalle") {
                    throw new Exception('Hubo un erro al insertar los datos del detalle en la adecuación de la estructura siguiente,  Gerencia ['.$estructura["gerencia"]."] Centro de Recaudación [".$estructura["centro_de_recaudacion"]."] Rubro [".$estructura["rubro"]."] Tipo [".$estructura["clase"]."] Clase [".$estructura["clase"]."] <br /> Del mes [".$mes_calcular."]");
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->debugeo->imprimir_pre("Hubo un error en la transaccion");
            }
            else {
                $this->db->trans_commit();
                $this->debugeo->imprimir_pre("Exito al realizar la transaccion");
            }

        } catch (Exception $e) {
            $this->debugeo->imprimir_pre($e->getMessage());
        }

    }

    private function agregar_adecuacion($datos) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha agregado adecuaciones');

//        $this->db->trans_begin();

        $estructura = json_decode($datos["estructura"], TRUE);

        $last = 0;

        $ultimo = $this->ingresos_model->ultimo_adecuaciones();

        if($ultimo) {
            $last = $ultimo->ultimo + 1;
        }
        else {
            $last = 1;
        }

        $datos["check_firme"] = 1;
        $datos["estatus"] = "activo";
        $datos["fecha_autoriza"] = $datos["fecha"];
        $datos["fecha_solicitud"] = $datos["fecha"];
        $datos["fecha_aplicar_caratula"] = $datos["fecha"];
        $datos["clasificacion"] = "Productos y/o Bienes";
        $datos["ultimo"] = $last;
        $datos["texto"] = $datos["tipo"];
        $datos["fecha_aplicar_detalle"] = $datos["fecha"];
        $datos["enlace"] = 0;
        $datos["nivel1_inferior"] = $estructura["gerencia"];
        $datos["nivel2_inferior"] = $estructura["centro_de_recaudacion"];
        $datos["nivel3_inferior"] = $estructura["rubro"];
        $datos["nivel4_inferior"] = $estructura["tipo"];
        $datos["nivel5_inferior"] = $estructura["clase"];
        $datos["tipo_radio"] = 1;
        $datos["descripcion"] = "Modificado del mes: ".ucfirst($datos["mes_destino"]);

//        $this->debugeo->imprimir_pre($datos);

        $this->ingresos_model->apartarAdecuacion($last, $datos["tipo"]);

//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $resultado_insertar_detalle = $this->ingresos_model->insertarDetalle($datos);

        if($resultado_insertar_detalle) {
            $resultado_insertar_caratula = $this->ingresos_model->insertarCaratula($datos);
            if($resultado_insertar_caratula) {
//                $this->db->trans_commit();

//                $this->db->trans_begin();
                if($datos["tipo"] == "Ampliación") {
                    $this->actualizar_saldo_ampliacion($last);

//                    $this->db->trans_commit();
                } elseif($datos["tipo"] == "Reducción"){
                    $this->actualizar_saldo_reduccion($last);

//                    $this->db->trans_commit();
                }
                return "exito";
            } else {
//                $this->db->trans_rollback();
                return "error_caratula";
            }
        } else {
//            $this->db->trans_rollback();
            return "error_detalle";
        }

    }

    private function actualizar_saldo_ampliacion($id) {
//        $this->debugeo->imprimir_pre($id);
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado saldo de ampliación');
        $datos_caratula = $this->ingresos_model->get_datos_caratula_modificado($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula["numero"]);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($fila->mes_destino, $fila->id_nivel);

            if($saldo_mes["mes_saldo"] == 0) {
                $nuevo_saldo_mes = $saldo_mes["mes_inicial"] + $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] + $fila->total;
            } else {
                $nuevo_saldo_mes = $saldo_mes["mes_saldo"] + $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] + $fila->total;
            }

//            Se genera el movimiento
            $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    private function actualizar_saldo_reduccion($id) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha actualizado saldo de reducción');
        $datos_caratula = $this->ingresos_model->get_datos_caratula_modificado($id);
        $datos_detalle = $this->ingresos_model->get_datos_detalle_numero($datos_caratula["numero"]);

        foreach($datos_detalle as $fila) {
//            Se toma el saldo del mes y del año
            $saldo_mes = $this->ingresos_model->get_saldo_partida_inicial($fila->mes_destino, $fila->id_nivel);

            if($saldo_mes["mes_saldo"] == 0) {
                $nuevo_saldo_mes = $saldo_mes["mes_inicial"] - $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] - $fila->total;
            } else {
                $nuevo_saldo_mes = $saldo_mes["mes_saldo"] - $fila->total;
                $nuevo_saldo_year = $saldo_mes["total_anual"] - $fila->total;
            }

//            Se genera el movimiento
            $this->ingresos_model->actualizar_saldo_partida($fila->mes_destino, $nuevo_saldo_mes, $nuevo_saldo_year, $fila->id_nivel);
        }

    }

    function registrar_devengado() {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        $this->db->select('numero_compromiso, fecha_pago AS fecha, importe')->from('mov_contrarecibo_caratula')->where('enfirme', 1)->where('cancelada', 0)->where('numero_compromiso !=', 0);
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {
            $sql_consulta = "SELECT mcc.enfirme, mcc.fecha_emision, mcc.firma1, mcc.firma2, mcc.firma3, mcc.estatus, mcd.importe, COLUMN_JSON(mcd.nivel) AS estructura
                                FROM mov_compromiso_detalle mcd
                                JOIN mov_compromiso_caratula mcc
                                ON mcd.numero_compromiso = mcc.numero_compromiso
                                WHERE mcd.numero_compromiso = ?;";
            $query_consulta = $this->db->query($sql_consulta, array($value["numero_compromiso"]));
            $resultado_consulta = $query_consulta->result_array();

            foreach($resultado_consulta as $llave => $valor) {
                if($valor["enfirme"] == 1 && $valor["firma1"] == 1 && $valor["firma2"] == 1 && $valor["firma3"] == 1 && $valor["estatus"] == "activo") {
                    $estructura = json_decode($valor["estructura"], TRUE);

                    $mes_devengado = $this->utilerias->convertirFechaAMes($value["fecha"]);

                    $sql_nivel = "SELECT COLUMN_GET(nivel, '".$mes_devengado."_devengado' as char) AS mes_devengado,
                                        COLUMN_GET(nivel, 'devengado_anual' as char) AS devengado_anual
                                        FROM cat_niveles
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_nivel = $this->db->query($sql_nivel, array(
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                        ));
                    $resultado_nivel = $query_nivel->row_array();

                    $resultado_nivel["mes_devengado"] += round($valor["importe"], 2);
                    $resultado_nivel["devengado_anual"] += round($valor["importe"], 2);
//
//                    $this->debugeo->imprimir_pre($resultado_nivel);

                    $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_devengado."_devengado', ?),
                                        nivel = COLUMN_ADD(nivel, 'devengado_anual', ?)
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_actualizar = $this->db->query($sql_actualizar, array(
                        $resultado_nivel["mes_devengado"],
                        $resultado_nivel["devengado_anual"],
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));

//                    $this->debugeo->imprimir_pre($query_actualizar);
                }
            }

        }

        $this->db->trans_complete();

    }

    function registrar_ejercido() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha registrado un ejercicio');
//        $this->benchmark->mark('code_start');
        $total_ejercido = 0;

        $this->db->select('movimiento, fecha_emision AS fecha, contrarecibo')->from('mov_bancos_movimientos')->where('enfirme', 1)->where('cancelada =', 0)->where('t_movimiento', "presupuesto");
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {

            if(!isset($value["contrarecibo"]) || $value["contrarecibo"] == NULL || $value["contrarecibo"] == "") {
                $this->debugeo->imprimir_pre($value["movimiento"]);
                continue;
            }

            $sql_consulta = "SELECT numero_compromiso
                                FROM mov_contrarecibo_caratula
                                WHERE id_contrarecibo_caratula = ?;";
            $query_consulta = $this->db->query($sql_consulta, array($value["contrarecibo"]));
            $resultado_consulta = $query_consulta->row_array();

//            $this->debugeo->imprimir_pre($resultado_consulta);

            $sql_consulta_compromiso = "SELECT mcc.enfirme, mcc.fecha_emision, mcc.firma1, mcc.firma2, mcc.firma3, mcc.estatus, mcd.importe, COLUMN_JSON(mcd.nivel) AS estructura
                                FROM mov_compromiso_detalle mcd
                                JOIN mov_compromiso_caratula mcc
                                ON mcd.numero_compromiso = mcc.numero_compromiso
                                WHERE mcd.numero_compromiso = ?;";
            $query_consulta_compromiso = $this->db->query($sql_consulta_compromiso, array($resultado_consulta["numero_compromiso"]));
            $resultado_consulta_compromiso = $query_consulta_compromiso->result_array();

            foreach($resultado_consulta_compromiso as $llave => $valor) {

                if ($valor["enfirme"] == 1 && $valor["firma1"] == 1 && $valor["firma2"] == 1 && $valor["firma3"] == 1 && $valor["estatus"] == "activo") {
                    $estructura = json_decode($valor["estructura"], TRUE);
                    $mes_ejercido = $this->utilerias->convertirFechaAMes($value["fecha"]);

                    $sql_nivel = "SELECT COLUMN_GET(nivel, '" . $mes_ejercido . "_ejercido' as char) AS mes_ejercido,
                                        COLUMN_GET(nivel, 'ejercido_anual' as char) AS ejercido_anual
                                        FROM cat_niveles
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_nivel = $this->db->query($sql_nivel, array(
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                    $resultado_nivel = $query_nivel->row_array();

                    $resultado_nivel["mes_ejercido"] += round($valor["importe"], 2);
                    $resultado_nivel["ejercido_anual"] += round($valor["importe"], 2);

                    $total_ejercido += round($valor["importe"], 2);
//
//                    $this->debugeo->imprimir_pre($resultado_nivel);

                    $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . $mes_ejercido . "_ejercido', ?),
                                        nivel = COLUMN_ADD(nivel, 'ejercido_anual', ?)
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_actualizar = $this->db->query($sql_actualizar, array(
                        $resultado_nivel["mes_ejercido"],
                        $resultado_nivel["ejercido_anual"],
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                }
            }
        }

        $this->db->trans_complete();

//        $this->debugeo->imprimir_pre($total_ejercido);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function registrar_pagado() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->trans_start();

        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha registrado un ejercicio');
//        $this->benchmark->mark('code_start');
        $total_ejercido = 0;

        $this->db->select('movimiento, fecha_conciliado AS fecha, contrarecibo')->from('mov_bancos_movimientos')->where('enfirme', 1)->where('cancelada', 0)->where('fecha_conciliado !=', '');
        $query = $this->db->get();

        foreach($query->result_array() as $key => $value) {

            if(!isset($value["contrarecibo"]) || $value["contrarecibo"] == NULL || $value["contrarecibo"] == "") {
                $this->debugeo->imprimir_pre($value["movimiento"]);
                continue;
            }

            $sql_consulta = "SELECT numero_compromiso
                                FROM mov_contrarecibo_caratula
                                WHERE id_contrarecibo_caratula = ?;";
            $query_consulta = $this->db->query($sql_consulta, array($value["contrarecibo"]));
            $resultado_consulta = $query_consulta->row_array();

//            $this->debugeo->imprimir_pre($resultado_consulta);

            $sql_consulta_compromiso = "SELECT mcc.enfirme, mcc.fecha_emision, mcc.firma1, mcc.firma2, mcc.firma3, mcc.estatus, mcd.importe, COLUMN_JSON(mcd.nivel) AS estructura
                                FROM mov_compromiso_detalle mcd
                                JOIN mov_compromiso_caratula mcc
                                ON mcd.numero_compromiso = mcc.numero_compromiso
                                WHERE mcd.numero_compromiso = ?;";
            $query_consulta_compromiso = $this->db->query($sql_consulta_compromiso, array($resultado_consulta["numero_compromiso"]));
            $resultado_consulta_compromiso = $query_consulta_compromiso->result_array();

            foreach($resultado_consulta_compromiso as $llave => $valor) {

                if ($valor["enfirme"] == 1 && $valor["firma1"] == 1 && $valor["firma2"] == 1 && $valor["firma3"] == 1 && $valor["estatus"] == "activo") {
                    $estructura = json_decode($valor["estructura"], TRUE);
                    $mes_ejercido = $this->utilerias->convertirFechaAMes($value["fecha"]);

                    $sql_nivel = "SELECT COLUMN_GET(nivel, '" . $mes_ejercido . "_pagado' as char) AS mes_ejercido,
                                        COLUMN_GET(nivel, 'pagado_anual' as char) AS ejercido_anual
                                        FROM cat_niveles
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_nivel = $this->db->query($sql_nivel, array(
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                    $resultado_nivel = $query_nivel->row_array();

                    $resultado_nivel["mes_ejercido"] += round($valor["importe"], 2);
                    $resultado_nivel["ejercido_anual"] += round($valor["importe"], 2);

                    $total_ejercido += round($valor["importe"], 2);
//
//                    $this->debugeo->imprimir_pre($resultado_nivel);

                    $sql_actualizar = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '" . $mes_ejercido . "_pagado', ?),
                                        nivel = COLUMN_ADD(nivel, 'pagado_anual', ?)
                                        WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                        AND COLUMN_GET(nivel, 'partida' as char) = ?;";
                    $query_actualizar = $this->db->query($sql_actualizar, array(
                        $resultado_nivel["mes_ejercido"],
                        $resultado_nivel["ejercido_anual"],
                        $estructura["fuente_de_financiamiento"],
                        $estructura["programa_de_financiamiento"],
                        $estructura["centro_de_costos"],
                        $estructura["capitulo"],
                        $estructura["concepto"],
                        $estructura["partida"],
                    ));
                }
            }
        }

        $this->db->trans_complete();

//        $this->debugeo->imprimir_pre($total_ejercido);

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    function acomodar_contrarecibos_movimientos_bancarios() {

        $this->db->select('movimiento, numero')->from('mov_bancos_pagos_contrarecibos');
        $query = $this->db->get();
        foreach($query->result_array() as $key => $value) {
//            $this->debugeo->imprimir_pre($value);

            $datos_actualizar = array(
                'contrarecibo' => $value["numero"],
            );

            $this->db->where('movimiento', $value["movimiento"]);
            $this->db->update('mov_bancos_movimientos', $datos_actualizar);
        }
    }

    function revisar_cuentas_existen() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $this->db->select('cuenta')->from('checar_cuentas_existen');
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach($resultado as $key => $value) {
            $this->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', $value["cuenta"]);
            $query_cuenta_existe = $this->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();
            if(!$existe_cuenta) {
                $datos_insertar = array(
                    'cuenta_no_existe' => $value["cuenta"],
                );

                $this->db->insert('cuentas_no_existen', $datos_insertar);
                $this->debugeo->imprimir_pre("La cuenta: ".$value["cuenta"]." no existe dentro del plan de cuentas");
            }
        }

    }

    function generar_polizas_egresos($datos = NULL) {
        $this->db->select('*')->from('facturas_adjuntas')->where('mov !=', '');
        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach($resultado as $key => $value) {
            $this->debugeo->imprimir_pre($value);
            $value["descuento"] = $value["descto"];
            $value["ssscta1"] = $value["centro_recaudacion"];
            $poliza = $this->utilerias->generar_poliza_facturas_egresos_directa($value);
            $this->debugeo->imprimir_pre($poliza);
        }
    }

    function generar_conciliacion_por_fechas() {
        $fecha_inicial = "2015-01-01";
        $fecha_final = "2015-07-31";

        $this->db->select('fecha_pago AS fecha, movimiento')
            ->from('mov_bancos_movimientos')
            ->where('fecha_pago >=', $fecha_inicial)
            ->where('fecha_pago <=', $fecha_final)
            ->where('enfirme', 1)
            ->where('cancelada', 0);

        $query = $this->db->get();
        $resultado = $query->result_array();

        foreach($resultado as $key => $value) {
            $this->db->trans_begin();

            $datos_actualizar = array(
                'sifirme' => 1,
                'fecha_conciliado' => $value["fecha"],
            );

            $this->db->where('movimiento', $value["movimiento"]);
            $this->db->update('mov_bancos_movimientos', $datos_actualizar);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->debugeo->imprimir_pre("Hubo un error en la transaccion con el movimiento ".$value["movimiento"]);
            }
            else {
                $this->db->trans_commit();
                $this->debugeo->imprimir_pre("Se ha realizado con exito la transaccion del movimiento ".$value["movimiento"]);
            }
        }
    }

}