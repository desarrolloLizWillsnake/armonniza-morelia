<?php

class Api_xmlrpc extends CI_Controller {

    // Se crea el constructor para el controlador
    public function index() {
            // parent::__construct();
            
            // Se cargan las librerías que se encargan de crear el servidor XML RPC
            $this->load->library('xmlrpc');
            $this->load->library('xmlrpcs');

            // Se agregan la funciones a la configuración del servidor XML RPC
            $config['functions']['Insertar_Proveedor'] = array('function' => 'Api_xmlrpc.Insertar_Proveedor');
            $config['functions']['Actualizar_Proveedor'] = array('function' => 'Api_xmlrpc.Actualizar_Proveedor');

            // Se inicia el servidor
            $this->xmlrpcs->initialize($config);
            $this->xmlrpcs->serve();
    }


    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Insertar_Proveedor($request) {
            // Se toman los parametros enviados por el cliente
            $parameters = $request->output_parameters();

            // Aquí se tienen que insertar los datos del proveedor
            $this->api_model->insertar_proveedor($parameters);

            $response = array(
                    array(
                            'you_said'  => $parameters['0'],
                            'i_respond' => 'Not bad at all.'),
                    'struct'
            );

            // Se envía la respueta al cliente
            return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Actualizar_Proveedor($request) {
            // Se toman los parametros enviados por el cliente
            $parameters = $request->output_parameters();

            // Aquí se tienen que insertar los datos del proveedor
            $this->api_model->actualizar_proveedor($parameters);

            $response = array(
                    array(
                            'you_said'  => $parameters['0'],
                            'i_respond' => 'Not bad at all.'),
                    'struct'
            );

            // Se envía la respueta al cliente
            return $this->xmlrpc->send_response($response);
    }
}
?>