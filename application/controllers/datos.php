<?php
class Datos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Datos_model');
    }

    function index()
    {
        $data['proveedor'] = $this->Datos_model->mensajes();
        $this->load->view('front/nomina/inventario_extranet_view', $data);

    }

    function mostrar_datos()
    {

        $id = $this->input->post('id');

        $edicion = $this->datos_model->obtener($id);

        $nombre = array(
            'name' => 'nombre',
            'id' => 'nombre',
            'value' => $edicion->nombre
        );
        $email = array(
            'name' => 'email',
            'id' => 'email',
            'value' => $edicion->email
        );
        $clave_prov = array(
            'name' => 'clave',
            'id' => 'clave',
            'value' => $edicion->clave_prov
        );

        $ciudad = array(
            'name' => 'ciudad',
            'id' => 'ciudad',
            'cols' => '50',
            'rows' => '6',
            'value' => $edicion->ciudad
        );
        $submit = array(
            'name' => 'editando',
            'id' => 'editando',
            'value' => 'Editar mensaje'
        );
        $oculto = array(
            'id_mensaje' => $id
        );

        ?>
        <?= form_open(base_url() . 'datos/actualizar_datos','', $oculto) ?>
        <?= form_label('Nombre') ?>
        <?= form_input($nombre) ?>
        <?= form_label('Email') ?>
        <?= form_input($email) ?>
        <?= form_label('Clave Proveedor') ?>
        <?= form_input($clave_prov) ?>
        <?= form_label('Ciudad') ?>
        <?= form_textarea($ciudad) ?>

        <?= form_submit($submit) ?>
        <?php echo form_close() ?>
        <?php
    }

        function actualizar_datos()
    {
        $id = $this->input->post('id_mensaje');
        $nombre = $this->input->post('nombre');
        $email = $this->input->post('email');
        $asunto = $this->input->post('clave');
        $mensaje = $this->input->post('ciudad');

        $actualizar = $this->datos_model->actualizar_mensaje($id,$nombre,$email,$asunto,$mensaje);

        if($actualizar)
        {
            $this->session->set_flashdata('actualizado', 'los datos, fueron actualizados correctamente');
            redirect('../datos', 'refresh');
        }
    }
}