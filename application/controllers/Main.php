<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla de principal del sistema
     */
    function index() {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha ingresado al main del sistema');
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Inicio",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE, "main"=>TRUE));
    }

    /**
     * En esta funcion, podemos ver un ejemplo de la consulta de datos de mariadb para los datos tipo NoSQL
     */
    function prueba() {
//        $this->load->view('prueba_view');
        $this->load->library('treeview');
        $treeview = $this->egresos_model->getTreeview();
        $resultado = $this->treeview->buildTree($treeview);

        echo($resultado);
    }

    /**
     * Esta funcion es para hacer pruebas de lectura de un archivo csv
     * */
    function pruebaexcel() {
        $file = base_url("application/archivos/prueba.csv");
        echo($file);

        $this->load->library('csvreader');
        $result =   $this->csvreader->parse_file($file);

        $data['csvData'] =  $result;
        $this->load->view('view_csv', $data);

    }
}