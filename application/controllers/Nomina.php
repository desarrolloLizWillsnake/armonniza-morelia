<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require( './assets/datatables/scripts/ssp.class.php' );

/**
 * Este controlador se encarga de mostrar la nómina y controlar las funciones
 **/
class Nomina extends CI_Controller
{

    var $sql_details;

    /**
     * Se revisa si el usuario esta logueado, si no esta logueado, se reenvia a la pantalla de login
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        }
        $this->sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'software',
            'host' => 'localhost',
        );
    }

    /**
     * Esta funcion es la principal, donde se muestra la pantalla principal de la nómina
     */
    function index()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a nómina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Nómina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/main_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    /** Aquí empieza la sección de Empleados */
    function empleados()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Empleados",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/empleados_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
        ));
    }


    function datos_factura()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a Recursos Humano, sección Empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Extranet",
            "usuario" => $this->tank_auth->get_username(),
            "tablas" => TRUE,
            "precompromisocss" => TRUE,
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/datos_factura_view');
        $this->load->view('front/footer_main_view', array(
            "tablas" => TRUE,
            "indice_empleados" => TRUE,
        ));
    }



    function nuevo_empleado()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a la lista de empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Nuevo Empleado",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/nuevo_empleado_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function consulta_empleados()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ', ha entrado a la lista de empleados');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | Lista de empleados",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/lista_empleados_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }

    function exportar_empleados()
    {
        $datos_header = array(
            "titulo_pagina" => "Armonniza | Personal",
            "usuario" => $this->tank_auth->get_username(),
            "precompromisocss" => TRUE,
        );

        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/seleccion_exportar_empleados_view');
        $this->load->view('front/footer_main_view', array(
            "devengado" => TRUE,
        ));
    }

    function exportar_empleados_formato()
    {
        $datos = $this->input->post();
//        $this->debugeo->imprimir_pre($datos);
        $this->load->library('excel');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Listado Personal');
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Educal');
        $objDrawing->setPath('img/logo2.png');
        $objDrawing->setOffsetX(50);
        $objDrawing->setHeight(80);
        // $objDrawing->setWidth(10);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($this->excel->getActiveSheet());

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $style_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $style_left = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $this->excel->getActiveSheet()->mergeCells('A1:V1')->getStyle("A1")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A1', 'SERVICIOS DE SALUD DE MICHOACAN');
        $this->excel->getActiveSheet()->mergeCells('A2:V2')->getStyle("A2")->applyFromArray($style);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Personal');
        $this->excel->getActiveSheet()->getStyle("C3:V3")->applyFromArray($style);
        $this->excel->getActiveSheet()->getStyle("C4:V4")->applyFromArray($style_center);

        $this->excel->getActiveSheet()->setCellValue('E3', 'Periodo');
        $this->excel->getActiveSheet()->setCellValue('L3', 'Fecha Emision');
        $this->excel->getActiveSheet()->setCellValue('T3', 'Hora Emision');
        $this->excel->getActiveSheet()->setCellValue('E4', $datos["fecha_inicial"] . ' al ' . $datos["fecha_final"]);
        $this->excel->getActiveSheet()->setCellValue('L4', $fecha);
        $this->excel->getActiveSheet()->setCellValue('T4', $hora);
        $this->excel->getActiveSheet()->mergeCells('A5:V5');
        $this->excel->getActiveSheet()->getStyle("A6:V6")->applyFromArray($style);

        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A6', 'No.')->getColumnDimension("A")->setWidth(9);
        $this->excel->getActiveSheet()->setCellValue('B6', 'Clave')->getColumnDimension("B")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('C6', 'A. Paterno')->getColumnDimension("C")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('D6', 'A. Materno')->getColumnDimension("D")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('E6', 'Nombre(s)')->getColumnDimension("E")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('F6', 'RFC')->getColumnDimension("F")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Curp')->getColumnDimension("G")->setWidth(20);
        $this->excel->getActiveSheet()->setCellValue('H6', 'Sexo')->getColumnDimension("H")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('I6', 'Fecha Ingreso Gob.')->getColumnDimension("I")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('J6', 'Crespo')->getColumnDimension("J")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('K6', 'Descripcion')->getColumnDimension("K")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('L6', 'Clave Clues')->getColumnDimension("L")->setWidth(15);
        $this->excel->getActiveSheet()->setCellValue('M6', 'Prog_subp')->getColumnDimension("M")->setWidth(10);

        $this->excel->getActiveSheet()->setCellValue('N6', 'Unidad')->getColumnDimension("N")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('O6', 'Ptda')->getColumnDimension("O")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('P6', 'Puesto')->getColumnDimension("P")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('Q6', 'Proy')->getColumnDimension("Q")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('R6', 'No. Plaza')->getColumnDimension("R")->setWidth(10);
        $this->excel->getActiveSheet()->setCellValue('S6', 'Descripcion Puesto')->getColumnDimension("S")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('T6', 'Rama')->getColumnDimension("T")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('U6', 'Fuente Financiamiento')->getColumnDimension("U")->setWidth(25);
        $this->excel->getActiveSheet()->setCellValue('V6', 'Tipo Contrato')->getColumnDimension("V")->setWidth(25);

        $resultado = $this->nomina_model->get_datos_filtros_personal($datos);
        //$this->debugeo->imprimir_pre($resultado);
        $row = 7;

        foreach ($resultado as $fila) {
            $this->excel->getActiveSheet()->getStyle("A:V")->applyFromArray($style_left);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $fila->id_usuarios_nomina);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $fila->clave);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $fila->a_paterno);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $fila->a_materno);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $fila->nombre);
            $this->excel->getActiveSheet()->setCellValue('F' . $row, $fila->rfc);
            $this->excel->getActiveSheet()->setCellValue('G' . $row, $fila->curp);
            $this->excel->getActiveSheet()->setCellValue('H' . $row, $fila->sexo);
            $this->excel->getActiveSheet()->setCellValue('I' . $row, $fila->fech_ing_gob);
            $this->excel->getActiveSheet()->setCellValue('J' . $row, $fila->crespo);
            $this->excel->getActiveSheet()->setCellValue('K' . $row, $fila->descripcion);
            $this->excel->getActiveSheet()->setCellValue('L' . $row, $fila->clave_clues);
            $this->excel->getActiveSheet()->setCellValue('M' . $row, $fila->prog_subp);

            $this->excel->getActiveSheet()->setCellValue('N' . $row, $fila->unidad);
            $this->excel->getActiveSheet()->setCellValue('O' . $row, $fila->ptda);
            $this->excel->getActiveSheet()->setCellValue('P' . $row, $fila->puesto);
            $this->excel->getActiveSheet()->setCellValue('Q' . $row, $fila->proy);
            $this->excel->getActiveSheet()->setCellValue('R' . $row, $fila->num_plaza);
            $this->excel->getActiveSheet()->setCellValue('S' . $row, $fila->descripcion_puesto);
            $this->excel->getActiveSheet()->setCellValue('T' . $row, $fila->rama);
            $this->excel->getActiveSheet()->setCellValue('U' . $row, $fila->fuente_financiamiento);
            $this->excel->getActiveSheet()->setCellValue('V' . $row, $fila->tipo_contrato);

            $row += 1;
        }
//        $this->benchmark->mark('code_start');
//        $this->benchmark->mark('code_end');

//        echo $this->benchmark->elapsed_time('code_start', 'code_end');

        $filename = "Personal.xls"; // Agregar fecha en que se generó
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
//        save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//        if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//        force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');


    }

    /** Aquí empieza la sección de Nómina */

    function nominas()
    {
        log_message('info', 'El usuario ' . $this->tank_auth->get_username() . ',  ha entrado a Recursos Humanos, sección Nómina');
        $datos_header = array(
            "titulo_pagina" => "Armonniza  | N&oacute;mina",
            "usuario" => $this->tank_auth->get_username(),
        );
        $this->parser->parse('front/header_main_view', $datos_header);
        $this->load->view('front/nomina/nomina_view');
        $this->load->view('front/footer_main_view', array("graficas" => TRUE));
    }


//    function insertar_datos()
//    {
//        $this->load->model('nuevo_empleado_view');
//
//
//        //si se ha pulsado el botón submit validamos el formulario
//        if ($this->input->post('submit')) {
//            //hacemos las comprobaciones que deseemos en nuestro formulario
//            $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|xss_clean|max_lenght[50]|min_length[2]');
//            $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|xss_clean|max_lenght[50]|min_length[2]');
//            $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|xss_clean|max_lenght[50]|min_length[2]');
//
//            $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|xss_clean');
//            $this->form_validation->set_rules('asunto', 'asunto', 'trim|required|xss_clean|max_lenght[250]|min_length[10]');
//            $this->form_validation->set_rules('mensaje', 'mensaje', 'trim|required|xss_clean');
//
//            //validamos que se introduzcan los campos requeridos
//            $this->form_validation->set_message('required', 'Campo %s es obligatorio');
//            //validamos el email con la función de ci valid_email
//            $this->form_validation->set_message('valid_email', 'El %s no es v&aacute;lido');
//            //comprobamos que se cumpla el mínimo de caracteres introducidos
//            $this->form_validation->set_message('min_length', 'Campo %s debe tener al menos %s car&aacute;cteres');
//            //comprobamos que se cumpla el máximo de caracteres introducidos
//            $this->form_validation->set_message('max_length', 'Campo %s debe tener menos %s car&aacute;cteres');
//
//
//
//            if (!$this->form_validation->run()) {
//                //si no pasamos la validación volvemos al formulario mostrando los errores
//                $this->index();
//            } //si pasamos la validación correctamente pasamos a hacer la inserción en la base de datos
//            else {
//
//                $id_usuarios_nomina = $this->input->post('Num de usuario');
//                $rfc = $this->input->post('RFC');
//                $a_paterno = $this->input->post('Apellido Paterno');
//                $a_materno = $this->input->post('Apellido Materno');
//                $nombre = $this->input->post('Nombre');
//                $curp = $this->input->post('CURP');
//                $sexo = $this->input->post('Sexo');
//                $fech_ing_god = $this->input->post('Fecha Ingreso');
//                $crespo = $this->input->post('CRESPO');
//                $prog_subp = $this->input->post('Prog. Subp.');
//                $unidad = $this->input->post('Unidad');
//                $ptda = $this->input->post('Partida');
//                $puesto = $this->input->post('Puesto');
//                $proy = $this->input->post('Proy');
//                $num_plaza = $this->input->post('N&uacute;mero Plaza');
//                $descripcion_puesto = $this->input->post('Descripci&oacute;n del Puesto');
//                $rama = $this->input->post('Rama');
//                $fuente_financiamiento = $this->input->post('Fuente de Financiamiento');
//                $tipo_contrato = $this->input->post('Tipo de Contrato');
//                $juris = $this->input->post('Juridicción');
//                $municipio = $this->input->post('Municipio');
//                $tplaza = $this->input->post('Tipo de Plaza');
//                $nacionalidad = $this->input->post('Nacionalidad');
//                $dias_lab = $this->input->post('Días Laborales');
//                $email = $this->input->post('E-mail');
//                $escolaridad = $this->input->post('Escolaridad');
//                $jornada = $this->input->post('Jornada');
//                $tu = $this->input->post('TU');
//                $clues_desc_real = $this->input->post('CLUES Adscripción Real');
//                $clues_desc_nomina = $this->input->post('CLUES Adscripción en Nómina');
//                $entidad_federativa_plaza = $this->input->post('Entidad Federativa de la Plaza');
//                $descripcion_clues2 = $this->input->post('Descripción CLUES Real2');
//                $descripcion_crespo2 = $this->input->post('Descripción CRESPO 2');
//                $actividad_principal = $this->input->post('Actividad Principal Desempeño');
//                $area_servicio_trab2 = $this->input->post('Area/Servicio de Trabajo2');
//                $Inst_pert_plaza = $this->input->post('Institución de Pertenencia de la Plaza');
//                $cedula_pro = $this->input->post('Cédula Profesional');
//                $hora_entrada = $this->input->post('Hora de Entrada');
//                $hora_salida = $this->input->post('Hora de Salida');
//                $escolar2 = $this->input->post('Escolaridad 2');
//                $escolar3 = $this->input->post('Escolaridad 3');
//                $cedula_pro2 = $this->input->post('Cédula Profesional 2');
//                $titulo_pro2 = $this->input->post('Título Profesional 2');
//                $titulo_pro3 = $this->input->post('Título Profesional 3');
//                $resident_medica = $this->input->post('Residencia Médica');
//                $fecha_anual_residencia = $this->input->post('A&ntilde;o de la Residencia');
//                $especialidad = $this->input->post('Especialidad');
//                $cert_por = $this->input->post('Certificado Por');
//                $cuenta_con_fiel = $this->input->post('Cuenta con FIEL');
//                $vigencia_fiel = $this->input->post('Vigencia FIEL');
//                $estatus_registro = $this->input->post('Estatus del Registro');
//                $fecha_ingreso_registro = $this->input->post('Fecha de Ingreso del Registro');
//                $percepciones = $this->input->post('Percepciones');
//                $deducciones = $this->input->post('Deducciones');
//                $total_neto = $this->input->post('Total Neto');
//
//
//                //conseguimos la fecha y hora
//                $fecha = date('Y-m-d');
//                $hora = date("H:i:s");
//                //ahora procesamos los datos hacía el modelo que debemos crear
//                $nueva_insercion = $this->comentarios_model->nuevo_usuarios(
//
//                    $id_usuarios_nomina,
//                    $rfc,
//                    $a_paterno,
//                    $a_materno,
//                    $nombre,
//                    $curp,
//                    $sexo,
//                    $fech_ing_god,
//                    $crespo,
//                    $prog_subp,
//                    $unidad,
//                    $ptda,
//                    $puesto,
//                    $proy,
//                    $num_plaza,
//                    $descripcion_puesto,
//                    $rama,
//                    $fuente_financiamiento,
//                    $tipo_contrato,
//                    $juris,
//                    $municipio,
//                    $tplaza,
//                    $nacionalidad,
//                    $dias_lab,
//                    $email,
//                    $escolaridad,
//                    $jornada,
//                    $tu,
//                    $clues_desc_real,
//                    $clues_desc_nomina,
//                    $entidad_federativa_plaza,
//                    $descripcion_clues2,
//                    $descripcion_crespo2,
//                    $actividad_principal,
//                    $area_servicio_trab2,
//                    $Inst_pert_plaza,
//                    $cedula_pro,
//                    $hora_entrada,
//                    $hora_salida,
//                    $escolar2,
//                    $escolar3,
//                    $cedula_pro2,
//                    $titulo_pro2,
//                    $titulo_pro3,
//                    $resident_medica,
//                    $fecha_anual_residencia,
//                    $especialidad,
//                    $cert_por,
//                    $cuenta_con_fiel,
//                    $vigencia_fiel,
//                    $estatus_registro,
//                    $fecha_ingreso_registro,
//                    $percepciones,
//                    $deducciones,
//                    $total_neto,
//                    $fecha,$hora
//
//
//            );
//                redirect(base_url("usuario nuevo"), "refresh");
//            }
//        }
//    }

    function guardar()
    {
        //El metodo is_ajax_request() de la libreria input permite verificar
        //si se esta accediendo mediante el metodo AJAX
        if ($this->input->is_ajax_request()) {
            $nombres = $this->input->post("nombres");
            $apellidos = $this->input->post("apellido_paterno");
            $apellido_materno = $this->input->post("apellido_materno");
            $rfc = $this->input->post("rfc");
            $curp = $this->input->post("curp");
            $telefono = $this->input->post("telefono");
            $estado_empleado = $this->input->post("estado_empleado");
            $genero = $this->input->post("genero");
            $edad = $this->input->post("edad");
            $no_emple = $this->input->post("no_emple");
            $calle = $this->input->post("calle");
            $no_exterior = $this->input->post("no_exterior");
            $no_interior = $this->input->post("no_interior");
            $colonia = $this->input->post("colonia");
            $pais = $this->input->post("pais");
            $ciudad = $this->input->post("ciudad");
            $cp = $this->input->post("cp");
            $delegacion = $this->input->post("delegacion");
            $lugar_nacimiento = $this->input->post("lugar_nacimiento");
            $fechade_nacimiento = $this->input->post("fechade_nacimiento");
            $email = $this->input->post("email");
            $puesto = $this->input->post("puesto");
            $num_seguro_social = $this->input->post("num_seguro_social");
            $crespo = $this->input->post("crespo");
            $fecha_alta = $this->input->post("fecha_alta");
            $fecha_baja = $this->input->post("fecha_baja");
            $unidad = $this->input->post("unidad");
            $percepcion = $this->input->post("percepcion");
            $deducciones = $this->input->post("deducciones");
            $total_neto = $this->input->post("total_neto");
            $jurisd = $this->input->post("jurisd");
            $municipio = $this->input->post("municipio");
            $prog_subp = $this->input->post("prog_subp");
            $partida = $this->input->post("partida");
            $proy = $this->input->post("proy");
            $numero_plaza = $this->input->post("numero_plaza");
            $descripcion_puesto = $this->input->post("descripcion_puesto");
            $rama = $this->input->post("rama");
            $fuente_finan = $this->input->post("fuente_finan");
            $tipo_cont = $this->input->post("tipo_cont");
            $tipo_plaza = $this->input->post("tipo_plaza");
            $nacional = $this->input->post("nacional");
            $dias_lab = $this->input->post("dias_lab");
            $actividad_prin = $this->input->post("actividad_prin");
            $jornada = $this->input->post("jornada");
            $tu = $this->input->post("tu");
            $area_serv = $this->input->post("area_serv");
            $clues_adscrip = $this->input->post("clues_adscrip");
            $clues_nomina = $this->input->post("clues_nomina");
            $entidad_federal = $this->input->post("entidad_federal");
            $descripcion_real2 = $this->input->post("descripcion_real2");
            $desc_crespo = $this->input->post("desc_crespo");
            $acti_desemp = $this->input->post("acti_desemp");
            $area_trabajo2 = $this->input->post("area_trabajo2");
            $inst_plaza = $this->input->post("inst_plaza");
            $cedula_pro = $this->input->post("cedula_pro");


            $hora_ent = $this->input->post("hora_ent");
            $hora_sal = $this->input->post("hora_sal");
            $escolare2 = $this->input->post("escolar2");
            $escolar3 = $this->input->post("escolar3");
            $cedula_pro2 = $this->input->post("cedula_pro2");
            $titulo_pro2 = $this->input->post("titulo_pro2");
            $dias_lab = $this->input->post("dias_lab");
            $titulo_pro3 = $this->input->post("titulo_pro3");
            $residen_medica = $this->input->post("residen_medica");
            $ingreso_residen = $this->input->post("ingreso_residen");
            $especialidad = $this->input->post("especialidad");
            $cert_por = $this->input->post("cert_por");
            $cuenta_fiel = $this->input->post("cuenta_fiel");
            $vigencia_fiel = $this->input->post("vigencia_fiel");
            $estatus_reg = $this->input->post("estatus_reg");
            $fecha_ingreso_reg = $this->input->post("fecha_ingreso_reg");


            $datos = array(
//                "id_usuarios_nomina" => $
            "no_empleado" => $no_emple,
            "tipo_de_recurso2" => $nombres,
            "apellidos_empleado" => $apellidos,
            "dni_empleado" => $rfc,
            "telefono_empleado" => $telefono,
            "email_empleado" => $email

            );
            if ($this->Empleados_model->guardar($datos) == true)
                echo "Registro Guardado";
            else
                echo "No se pudo guardar los datos";
        } else {
            show_404();
        }
    }

////    function Conectarse()
////    {
////        if (!($link = mysql_connect("n_host", "n_usuario",))) {
////            echo "Error conectando a la base de datos.";
////            exit();
////        }
////        if (!mysql_select_db("nombre_based", $link)) {
////            echo "Error seleccionando la base de datos.";
////            exit();
////        }
////        return $link;
////    }
////}
////        $id_articulo = $_POST["id_articulo"];
////        $url = $_SERVER[''];
////    // ajusta la hora
////        $time = time() + 7200;
////        $fecha_hora = date('d-m-Y H:i:s', $time);
////        $nombre = $_POST["nombre"];
////        $email = $_POST["email"];
////        $comentario = substr($_POST["mensaje"], 0, 500);
////        $link = Conectarse();
////    //devuelve el ultimo comentario, muy necesario para evitar doble envio de los datos
////        $verif = mysql_query("SELECT * FROM comentarios ORDER BY id_comentario DESC LIMIT 1",
////        $link);
////        while($row_a = mysql_fetch_row($verif)){
////        $ultimo_com = $row_a[7];
////            }
////        if ($nombre != "" and filter_var($email, FILTER_VALIDATE_EMAIL)
////        and $comentario != $ultimo_com) {
////            mysql_query("INSERT INTO comentarios
////         id_articulo, ip, url, fecha_hora, nombre, email, comentario)
////        VALUES ('$id_articulo', '$url', '$fecha_hora', '$nombre', '$email', '$comentario')",
////                $link);
////        }
//
//
//
////    function guardardatos ($resultados)
////    {
////
////        $id_usuarios_nomina= $_POST['nombre'];
////        $rfc= $_POST['apellidos'];
////        $a_paterno= $_POST['fech_nac'];
////        $a_materno= $_POST['email'];
////        $nombre= $_POST['pass'];
////        $curp= $_POST['estado'];
////        $sexo= $_POST['region'];
////        $fech_ing_god= $_POST['domicilio'];
////        $crespo= $_POST['cp'];
////        $prog_subp= $_POST['prog'];
////        $unidad= $_POST['unidad'];
////        $ptda= $_POST['apellidos'];
////        $puesto= $_POST['fech_nac'];
////        $proy= $_POST['email'];
////        $num_plaza= $_POST['pass'];
////        $descripcion_puesto= $_POST['estado'];
////        $rama= $_POST['region'];
////        $fuente_financiamiento= $_POST['domicilio'];
////        $tipo_contrato= $_POST['cp'];
////        $juris= $_POST['prog'];
////        $municipio= $_POST['unidad'];
////        $tplaza= $_POST['apellidos'];
////        $nacionalidad= $_POST['fech_nac'];
////        $dias_lab= $_POST['email'];
////        $email= $_POST['pass'];
////        $escolaridad= $_POST['estado'];
////        $jornada= $_POST['region'];
////        $tu= $_POST['domicilio'];
////        $clues_desc_real= $_POST['cp'];
////        $clues_desc_nomina= $_POST['prog'];
////
////        $entidad_federativa_plaza= $_POST['fech_nac'];
////        $descripcion_clues2= $_POST['email'];
////        $descripcion_crespo2= $_POST['pass'];
////        $actividad_principal= $_POST['estado'];
////        $area_servicio_trab2= $_POST['region'];
////        $Inst_pert_plaza= $_POST['domicilio'];
////        $cedula_pro= $_POST['cp'];
////        $hora_entrada= $_POST['prog'];
////        $hora_salida= $_POST['unidad'];
////        $escolar2= $_POST['apellidos'];
////        $escolar3= $_POST['fech_nac'];
////        $cedula_pro2= $_POST['email'];
////        $titulo_pro2= $_POST['pass'];
////        $titulo_pro3= $_POST['estado'];
////        $resident_medica= $_POST['region'];
////        $fecha_anual_residencia= $_POST['domicilio'];
////        $especialidad= $_POST['cp'];
////        $cert_por= $_POST['prog'];
////        $cuenta_con_fiel= $_POST['unidad'];
////        $vigencia_fiel= $_POST['apellidos'];
////        $estatus_registro= $_POST['fech_nac'];
////        $fecha_ingreso_registro= $_POST['email'];
////        $percepciones= $_POST['pass'];
////        $deducciones= $_POST['estado'];
////        $total_neto= $_POST['region'];
//
//
////$conexion = mysql_connect($host,$user,$Pw)or die(‘Problemas al conectarse’);
////mysql_select_db($db,$conexion)or die(‘Problemas al seleccionar la BD’);
//
////$sql(“INSERT INTO Usuario(nombre,apellidos,fech_nac,email,pass,estado,region,domicilio,cp,tel)values(‘$nombre’,'$apellidos’,'$fech_nac’,'$email’,'$pass’,'$estado’,'$region’,'$domicilio’,'$cp’,'$tel’)”);
//
////echo “Sus datos fueron guardados correctamente”;
////
////- See more at: http://www.phonegapspain.com/topic/error-al-agregar-datos-a-una-base-de-datos-mysql-por-medio-de-un-formulario/#sthash.h3RAp7Hj.dpuf
////
///
////        $sql =“INSERT INTO cat_usuarios_nomina VALUES  (NULL,'”.$_POST[‘Nombre’].”‘,'” .$_POST[‘’].”‘,'”.$_POST[‘Materno’].”‘,'”.$_POST[‘sexo’].”‘)”;
////                $result = mysql_query($sql);
////                if (! $result){
////                    echo “La consulta SQL contiene errores.”.mysql_error();
////                               exit();
////                }else {echo “<center><font color=’RED’>DATOS INSERTADOS CORRECTAMENTE</font><a
////
////}



    function consulta_nomina() {
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Lista de N&oacute;minas",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/lista_nomina_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function nueva_nomina() {
            log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha entrado a la lista de empleados');
            $datos_header = array(
                "titulo_pagina" => "Armonniza  | Nueva N&oacute;mina",
                "usuario" => $this->tank_auth->get_username(),
            );
            $this->parser->parse('front/header_main_view', $datos_header);
            $this->load->view('front/nomina/nueva_nomina_view');
            $this->load->view('front/footer_main_view', array("graficas" => TRUE));

    }

    function tabla_empleados_indice() {
        $this->db->cache_delete('nomina', 'tabla_empleados_indice');
        $this->db->cache_off();

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $resultado = $this->nomina_model->get_datos_indice_empleados();

        $output = array();

        foreach($resultado as $key => $value) {
            // $this->debugeo->imprimir_pre($value);
            $output["data"][] = array(

                $value["id_proveedores"],
                $value["clave_prov"],
                $value["nombre"],
                $value["email"],
                $value["calle"],
                $value["telefono"],
                $value["no_exterior"],
                $value["no_interior"],
                $value["crespo"],
                $value["colonia"],
                $value["cp"],
                $value["ciudad"],
                $value["delegacion_municipio"],
                $value["pais"],
                $value["contacto"],
                $value["RFC"],
                $value["sucursal"],
                $value["cuenta"],
                $value["clabe"],
                $value["banco"],
                $value["pago"],
                $value["mypyme"],
                $value["observaciones"],
                $value["alta"],
                $value["modificado"],
                $value["tipo"],
                $value["curp"],
                $value["estado"],
                $value["activo"],
                $value["tu"],
                $value["clues_desc_real"],
                $value["clues_desc_nomina"],
                $value["entidad_federativa_plaza"],
                $value["descripcion_clues2"],
                $value["descripcion_crespo2"],
                $value["actividad_principal"],
                $value["area_servicio_trab2"],
                $value["Inst_pert_plaza"],
                $value["cedula_pro"],
                $value["hora_entrada"],
                $value["hora_salida"],
                $value["escolar2"],
                $value["escolar3"],
                $value["cedula_pro2"],
                $value["titulo_pro2"],
                $value["titulo_pro3"],
                $value["resident_medica"],
                $value["fecha_anual_residencia"],
                $value["especialidad"],
                $value["cert_por"],
                $value["cuenta_con_fiel"],
                $value["vigencia_fiel"],
                $value["estatus_registro"],
                $value["fecha_ingreso_registro"],
                $value["percepciones"],
                $value["deducciones"],
                $value["total_neto"],
















            );
        }
//        $this->debugeo->imprimir_pre($output);
        echo json_encode($output);
    }
}


