
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proveedor_glosa_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get_datos_indice_glosa() {
        $this->db->select('*')
            ->from('cat_proveedores_glosa');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_datos_filtros_personal($datos){
        $sql = "SELECT * FROM cat_contratos
                WHERE contrato LIKE ?
                AND rfc LIKE ?Proveedor_glosa_model.php
                ";
        $query = $this->db->query($sql, array("%".$datos["contrato"]."%", $datos["rfc"]));
        return $query->result();
    }

}