
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proveedor_extranet_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get_datos_indice_extranet() {
        $this->db->select('*')
            ->from('cat_proveedores');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_datos_filtros_personal($datos){
        $sql = "SELECT * FROM cat_proveedores
                WHERE proveedor LIKE ?
                AND id_proveedores LIKE ?
                AND clave_prov LIKE ?
                AND nombre LIKE ?
                AND email LIKE ?
                AND calle LIKE ?
                AND RFC LIKE ?
                AND activo LIKE ?Proveedor_extranet_model.php
                AND pago LIKE ?
                AND estado >= ?
                AND ciudad <= ?;";
        $query = $this->db->query($sql, array("%".$datos["proveedor"]."%", "%".$datos["id_proveedor"]."%", "%".$datos["clave_prov"]."%", "%".$datos["nombre"]."%",
            "%".$datos["email"]."%", "%".$datos["calle"]."%", "%".$datos["RFC"]."%", "%".$datos["activo"]."%", "%".$datos["pago"]."%", $datos["estado"], $datos["ciudad"]));
        return $query->result();
    }

}