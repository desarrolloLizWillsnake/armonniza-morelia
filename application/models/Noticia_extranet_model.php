
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class noticia_extranet_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get_datos_indice_noticia() {
        $this->db->select('*')
            ->from('cat_noticias');
        $query_noticia = $this->db->get();
        return $query_noticia->result_array();
    }

    function insertar_mensaje_noticia($datos) {

        $this->db->trans_begin();

        $query_noticia = "INSERT INTO cat_noticias ( asunto, mensaje, fecha ) VALUES ( ?, ?, ? ); ";

        $this->db->query($query_noticia, array($datos["asunto"], $datos["observaciones"], date('Y-m-d H:i:s'),));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

}