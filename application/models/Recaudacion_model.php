<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recaudacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    /**Sección Devengado*/
    /** Esta función realizá la consulta con la base de datos para obtener la información que se mostrará en la tabla principal*/
    function get_datos_devengado() {
        $sql = "SELECT * FROM mov_devengado_caratula ORDER BY numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function get_datos_devengado_centro() {
        $sql = "SELECT r1.*, COLUMN_JSON(r2.nivel) AS estructura FROM mov_devengado_caratula r1
                INNER JOIN mov_devengado_detalle r2 ON r1.numero = r2.numero_devengado GROUP BY r1.numero ORDER BY r1.numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function get_datos_devengado_caratula($devengado, $query) {
        $query = $this->db->query($query, array($devengado));
        $result = $query->row();
        return $result;
    }
    function get_datos_devengadoDetalle($devengado, $sql) {
        $query = $this->db->query($sql, array($devengado));
        $result = $query->result();
        return $result;
    }
    function get_datos_devengado_detalle($devengado){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_devengado_detalle WHERE numero_devengado = ?;";
        $query = $this->db->query($query, array($devengado));
        $result = $query->result();
        return $result;
    }
    function get_datos_recaudado_detalle_automatico($recaudado){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_recaudado_detalle WHERE numero_recaudado = ?;";
        $query = $this->db->query($query, array($recaudado));
        $result = $query->result();
        return $result;
    }
    function get_datos_filtros_devengado($datos){
        $sql = "SELECT r1.*, COLUMN_JSON(r2.nivel) AS estructura, r2.importe, r2.iva, r2.descripcion_detalle, r2.fecha_aplicacion AS fecha_aplicacion_detalle, r2.total_importe
                FROM mov_devengado_caratula r1
                INNER JOIN mov_devengado_detalle r2 ON r1.numero = r2.numero_devengado
                WHERE COLUMN_GET(r2.nivel, 'centro_de_recaudacion' as char) LIKE ?
                AND r1.clasificacion LIKE ?
                AND r1.no_movimiento LIKE ?
                AND r1.enfirme = 1
                AND r1.cancelada = 0
                AND r1.fecha_solicitud >= ?
                AND r1.fecha_solicitud <= ?;";
        $query = $this->db->query($sql, array("%".$datos["centro_recaudacion"]."%", "%".$datos["clasificacion"]."%",
                                              "%".$datos["num_movimiento"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        return $query->result();
    }
    function get_datos_filtros_recaudado($datos){
        $sql = "SELECT r1.*, COLUMN_JSON(r2.nivel) AS estructura, r2.importe, r2.iva, r2.descripcion_detalle, r2.fecha_aplicacion AS fecha_aplicacion_detalle, r2.total_importe
                FROM mov_recaudado_caratula r1
                INNER JOIN mov_recaudado_detalle r2 ON r1.numero = r2.numero_recaudado
                WHERE COLUMN_GET(r2.nivel, 'centro_de_recaudacion' as char) LIKE ?
                AND r1.clasificacion LIKE ?
                AND r1.no_movimiento LIKE ?
                AND r1.enfirme = 1
                AND r1.cancelada = 0
                AND r1.fecha_solicitud >= ?
                AND r1.fecha_solicitud <= ?;";
        $query = $this->db->query($sql, array("%".$datos["centro_recaudacion"]."%", "%".$datos["clasificacion"]."%",
            "%".$datos["num_movimiento"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        return $query->result();
    }

    function datos_subsidio() {
        $sql = "SELECT * FROM cat_clasificador_fuentes_financia WHERE id_clasificador_fuentes_financia = 1 OR id_clasificador_fuentes_financia = 4;;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }
    function datos_librerias() {
        $sql = "SELECT * FROM cat_centro_libreria;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function ultimo_devengado() {
        $sql = "SELECT numero as ultimo FROM mov_devengado_caratula ORDER BY numero DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }
    function apartarDevengado($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero' => $ultimo,
            'estatus' => "Pendiente",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );
        $query = $this->db->insert('mov_devengado_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_devengado($datos) {
        $datos["tipo"] = "Devengado";
        $datos["estatus_recaudado"] = 0;

//        Este arreglo son los datos que van a ser insertados en la caratula del devengado
        $data = array(
            "numero" => $datos["ultimo"],
            'tipo' =>  $datos["tipo"],
            'clasificacion' => $datos["clasificacion"],
            'no_movimiento' => $datos["num_movimiento"],
            'clave_cliente' => $datos["clave_cliente"],
            'cliente' => $datos["cliente"],
            'importe_total' => $datos["importe_total"],
            'fecha_solicitud' => $datos["fecha_solicitud"],
            'descripcion' => $datos["descripcion"],
            'enfirme' => $datos["check_firme"],
            'estatus_recaudado' => $datos["estatus_recaudado"],
            'estatus' => $datos["estatus"],
            'fecha_sql' => date( 'Y-m-d H:i:s'),
            'year' => date("Y"),
            'cancelada' => 0,
            'fecha_cancelada' => "0000-00-00",
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('numero', $datos["ultimo"]);
        $query = $this->db->update('mov_devengado_caratula', $data);
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_caratula_devengado_automatico($datos) {
        $datos["tipo"] = "Devengado";
        $datos["estatus_recaudado"] = 0;

//        Este arreglo son los datos que van a ser insertados en la caratula del devengado
        $data = array(
            "numero" => $datos["ultimo"],
            'tipo' =>  $datos["tipo"],
            'ticket' =>  $datos["no_mov"],
            'clasificacion' => $datos["clasificacion"],
            'no_movimiento' => $datos["num_movimiento"],
            'clave_cliente' => $datos["clave_cliente"],
            'cliente' => $datos["cliente"],
            'importe_total' => $datos["importe_total"],
            'fecha_solicitud' => $datos["fecha_solicitud"],
            'descripcion' => $datos["descripcion"],
            'enfirme' => $datos["check_firme"],
            'estatus_recaudado' => $datos["estatus_recaudado"],
            'estatus' => $datos["estatus"],
            'fecha_sql' => date( 'Y-m-d H:i:s'),
            'year' => date("Y"),
            'cancelada' => $datos["cancelada"],
            'fecha_cancelada' => $datos["fecha_cancelada"],
        );

//        Esta función es la que se encarga de actualizar el devengado
        $this->db->where('numero', $datos["ultimo"]);
        $query = $this->db->update('mov_devengado_caratula', $data);
//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_detalle_devengado($datos, $query) {
//        $this->debugeo->imprimir_pre($datos);
//        $this->debugeo->imprimir_pre($query);
        $datos["tipo"] = "Devengado";
        $query = $this->db->query($query, array(
            $datos["ultimo"],
            $datos["id_nivel"],
            $datos["fecha_detalle"],
            $datos["subsidio"],
            $datos["cantidad"],
            $datos["importe"],
            $datos["descuento"],
            //$datos["descuento_gravado"],
            //$datos["descuento_no_gravado"],
            $datos["iva"],
            //$datos["importe_grabado"],
            //$datos["importe_no_grabado"],
            $datos["total_importe"],
            $datos["descripcion_detalle"],
            date( 'Y-m-d H:i:s'),
            date( 'H:i'),
            $datos["tipo_pago"],
            //$datos["cuenta_bancaria"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["tipo"]));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function borrarDetalleDevengado($devengado) {
        $this->db->where('id_devengado_detalle', $devengado);
        $this->db->select('total_importe, id_nivel');
        $query = $this->db->get('mov_devengado_detalle');
        $result = $query->row();

        $this->db->where('id_devengado_detalle', $devengado);
        $query = $this->db->delete('mov_devengado_detalle');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    function cancelarDevengado($devengado) {

        $this->db->where('id_devengado_detalle', $devengado);
        $this->db->select('*');
        $query = $this->db->get('mov_devengado_detalle');
        $result = $query->row();

        $this->db->where('id_devengado', $devengado);
        $this->db->select('fecha_aplicacion');
        $query2 = $this->db->get('mov_devengado_caratula');
        $result_caratula = $query2->row();

        $fecha = $result_caratula->fecha_aplicacion;
    }

    function get_saldo_partida_devengado($mes, $id) {
        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_devengado' as char) as mes_devengado,
                       COLUMN_GET(nivel, 'devengado_anual' as char) as devengado_anual
                       FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }
    function actualizar_saldo_partida_devengado($mes, $mes_devengado, $devengado_anual, $id) {
        $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes."_devengado', ?),
                              nivel = COLUMN_ADD(nivel, 'devengado_anual', ?)
                              WHERE id_niveles = ?;";
        $query = $this->db->query($query_transaccion, array($mes_devengado, $devengado_anual, $id));
        return $query;
    }

    function actualizar_saldo_recaudado($datos) {
//        Se inicializa la variable de la respuesta de la inserción de datos
        $respuesta = array(
            "mensaje" => ''
        );

//        Se toman los datos del detalle del compromiso
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_recaudado_detalle WHERE numero_recaudado = ?;";
        $query = $this->db->query($sql, array($datos["ultimo"]));
        $result = $query->result();

        foreach($result as $fila) {

//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder manejar la información
            $estructura = json_decode($fila->estructura);

//            En caso de que exista la estructura, se toma el mes de donde se va a calcular el presupuesto
            $mes_actual = $this->utilerias->convertirFechaAMes($fila->fecha_aplicacion);

//                Se tiene que comprobar si hay dinero para hacer el compromiso
            $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '" . $mes_actual . "_recaudado' as char) AS mes_recaudado,
                                            COLUMN_GET(nivel, 'recaudado_anual' as char) AS recaudado_anual
                                            FROM cat_niveles WHERE id_niveles = ?";

            $resultado_dinero = $this->ciclo_model->revisarDinero($fila->id_nivel, $query_revisar_dinero);

//                    Si el resultado es mayor o igual a cero, se suma el importe al total comprometido del mes
            $recaudado_mes = round($resultado_dinero->mes_recaudado, 2) + round($fila->total_importe, 2);

//                    Si el resultado es mayor o igual a cero, se suma el importe al total comprometido del año
            $recaudado_anual = round($resultado_dinero->recaudado_anual, 2) + round($fila->total_importe, 2);

            $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_recaudado', ?),
                              nivel = COLUMN_ADD(nivel, 'recaudado_anual', ?)
                              WHERE id_niveles = ?;";

            $resultado_actualizar = $this->db->query($query_transaccion, array($recaudado_mes, $recaudado_anual, $fila->id_nivel));

//                    Si el resultado es exitoso, se manda un mensaje al usuario marcando que fue un exito
            if($resultado_actualizar) {
                $respuesta["mensaje"] .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos guardados correctamente en la clase '.$estructura->clase.'.</div>';
            }
//                    De lo contrario, se mandao un error de que no hay suficiente dinero
            else {
                $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Hubo un error al actualizar una partida, por favor comuníquese con su administrador.</div>';
            }
        }

        return $respuesta;

    }

    function marcarDevengado($datos) {
        $this->db->select('*')->from('mov_devengado_detalle')->where('numero_devengado', $datos["ultimo"]);
        $query = $this->db->get();
        $resultado_detalle = $query->result_array();

        try{

            foreach($resultado_detalle as $fila) {
                $this->db->select('COLUMN_JSON(nivel) AS estructura')->from('cat_niveles')->where('id_niveles', $fila["id_nivel"]);
                $query_detalle = $this->db->get();
                $fila_detalle = $query_detalle->row_array();
                $estructura = json_decode($fila_detalle["estructura"]);

                $mes_actual = $this->utilerias->convertirFechaAMes($fila["fecha_aplicacion"]);

                $sql_actualizar_devengado = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_devengado', ?),
                                                nivel = COLUMN_ADD(nivel, 'devengado_anual', ?) WHERE id_niveles = ?;";
                $resultado_actualizar = $this->db->query($sql_actualizar_devengado, array($fila["total_importe"], $fila["total_importe"], $fila["id_nivel"]));

                if(!$resultado_actualizar) {
                    throw new Exception();
                }

            }

            return TRUE;

        } catch(Exception $e) {
            return FALSE;
        }

    }

    function tomar_cuentas_devengado($centro_recaudacion) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave = ? AND destino = 'Ingreso devengado';";
        $query = $this->db->query($sql, array($centro_recaudacion));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_datos_devengado_recaudado_exportar($fecha_inicial, $fecha_final) {
        $sql = "SELECT mdc.numero, mdc.fecha_solicitud, COLUMN_GET(mdd.nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion,
                mdc.no_movimiento, mdd.importe, mdd.iva, mdd.total_importe, fad.folio AS factura_egreso, fad.total AS total_egreso
                FROM mov_devengado_detalle mdd
                LEFT JOIN mov_devengado_caratula mdc
                ON mdd.numero_devengado = mdc.numero
                LEFT JOIN facturas_adjuntas fad
                ON mdc.ticket = fad.no_mov
                WHERE mdc.fecha_solicitud >= ?
                AND mdc.fecha_solicitud <= ?
                GROUP BY mdc.numero;";
        $query = $this->db->query($sql, array($fecha_inicial, $fecha_final));
        return $query->result_array();
    }

    function get_datos_devengado_info_recaudado_exportar($datos) {
        $sql = "SELECT mdc.numero, mdc.fecha_solicitud, COLUMN_GET(mdd.nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion,
                mdc.no_movimiento, mdd.importe, mdd.iva, mdd.total_importe, fad.folio AS factura_egreso, fad.total AS total_egreso,
				mrc.numero AS numero_recaudado, mrc.importe_total AS importe_recaudado, mrc.fecha_recaudado
                FROM mov_devengado_detalle mdd
                LEFT JOIN mov_devengado_caratula mdc
                ON mdd.numero_devengado = mdc.numero
                LEFT JOIN facturas_adjuntas fad
                ON mdc.ticket = fad.no_mov
				LEFT JOIN mov_recaudado_caratula mrc
				ON mrc.numero_devengado = mdd.numero_devengado
                WHERE mdc.fecha_solicitud >= ?
                AND mdc.fecha_solicitud <= ?
                GROUP BY mdc.numero;";
        $query = $this->db->query($sql, array($datos["fecha_inicial"], $datos["fecha_final"]));
        return $query->result_array();
    }

    function get_devengado_por_recaudar() {
        $sql = "SELECT DISTINCT mdc.numero, mdc.fecha_solicitud, mdc.no_movimiento, mdc.importe_total
                FROM mov_devengado_caratula mdc
                WHERE NOT EXISTS
                (SELECT mrc.numero
                    FROM mov_recaudado_caratula mrc
                    WHERE mdc.numero = mrc.numero_devengado);";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    /**Cancelar devengado caratula*/
    function cancelarDevengadoCaratula($devengado, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $devengado,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    /**Sección Recaudado*/

    function apartarRecaudado($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero' => $ultimo,
            'estatus' => "Pendiente",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );
        $query = $this->db->insert('mov_recaudado_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function get_datos_recaudado_caratula($devengado, $query) {
        $query = $this->db->query($query, array($devengado));
        $result = $query->row();
        return $result;
    }
    function get_datos_recaudado() {
        $sql = "SELECT * FROM mov_recaudado_caratula ORDER BY numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function get_datos_recaudado_centro() {
        $sql = "SELECT r1.*, COLUMN_JSON(r2.nivel) AS estructura FROM mov_recaudado_caratula r1
                INNER JOIN mov_recaudado_detalle r2 ON r1.numero = r2.numero_recaudado
                GROUP BY r1.numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function get_datos_devengadoRecaudado($devengado, $sql) {
        $query = $this->db->query($sql, array($devengado));
        $result = $query->result();
        return $result;
    }
    function get_datos_recaudado_detalle($devengado){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_recaudado_detalle WHERE numero_recaudado = ?;";
        $query = $this->db->query($query, array($devengado));
        $result = $query->result();
        return $result;
    }
    function ultimo_recaudado() {
        $sql = "SELECT numero as ultimo FROM mov_recaudado_caratula ORDER BY numero DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }
    function actualizarEstatusRecaudado($devengado, $query) {
        $query = $this->db->query($query, array(
            $devengado,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_recaudado($datos, $query) {
//        $this->debugeo->imprimir_pre($datos);
//        $this->debugeo->imprimir_pre($query);
        $datos["tipo"] = "Recaudado";
        $query = $this->db->query($query, array(
            $datos["ultimo"],
            $datos["id_nivel"],
            $datos["fecha_detalle"],
            $datos["subsidio"],
            $datos["cantidad"],
            $datos["importe"],
            $datos["descuento"],
            //$datos["descuento_gravado"],
            //$datos["descuento_no_gravado"],
            $datos["iva"],
            //$datos["importe_grabado"],
            //$datos["importe_no_grabado"],
            $datos["total_importe"],
            $datos["descripcion_detalle"],
            date( 'Y-m-d H:i:s'),
            date( 'H:i'),
            $datos["tipo_pago"],
            //$datos["cuenta_bancaria"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["tipo"]));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_recaudacion($datos) {

        if(!$datos["importe_total"]) {
            $datos["importe_total"] = 0;
        }

        if(!$datos["fecha_recaudado"]) {
            $datos["fecha_recaudado"] = "0000-00-00";
        }

        $data = array(
            'numero_devengado' => $datos["devengado_hidden"],
            'no_movimiento' => $datos["no_factura"],
            'fecha_solicitud' => $datos["fecha_recaudado"],
            'fecha_recaudado' => $datos["fecha_recaudado"],
            'hora_recaudado' => date("H:i"),
            'importe_total' => $datos["importe_total"],
            'fecha_sql' => date("Y-m-d H:i:s"),
            'year' => date("Y"),
            'clasificacion' => $datos["clasificacion"],
            'importe_total' => $datos["importe_total"],
            'fecha_cancelada' => "0000-00-00",
            'descripcion' => $datos["descripcion"],
            'estatus' => $datos["estatus"],
            'enfirme' => $datos["check_firme"],
            'cancelada' => 0,
            'clave_cliente' => $datos["clave_cliente"],
            'cliente' => $datos["cliente"],
        );

        $this->db->where('numero', $datos["ultimo"]);
        $query = $this->db->update('mov_recaudado_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function tomar_cuentas_recaudacion($clase) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave = ? AND destino LIKE '%Ingreso cobrado%';";
        $query = $this->db->query($sql, array($clase));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_saldo_partida($mes, $id) {
        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_saldo' as char) as mes_saldo,
                       COLUMN_GET(nivel, '".$mes."_devengado' as char) as mes_devengado,
                       COLUMN_GET(nivel, 'total_anual' as char) as total_anual,
                       COLUMN_GET(nivel, 'recaudado_anual' as char) as recaudado_anual
                       FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }
    function actualizar_saldo_partida($mes, $saldo_mes, $recaudado_anual, $total_anual, $id) {
        $query_transaccion = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes."_recaudado', ?),
                              nivel = COLUMN_ADD(nivel, 'recaudado_anual', ?),
                              nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                              WHERE id_niveles = ?;";
        $query = $this->db->query($query_transaccion, array($saldo_mes, $recaudado_anual, $total_anual, $id));
        return $query;
    }

    function get_datos_devengado_recaudado() {
        $sql = "SELECT d1.*, COLUMN_JSON(d2.nivel) AS estructura FROM mov_devengado_caratula d1
                JOIN mov_devengado_detalle d2 ON d1.numero = d2.numero_devengado
                WHERE d1.enfirme = 1 AND d1.importe_total > d1.importe_total_recaudado GROUP BY d1.numero ORDER BY d1.numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function copiarDatosDevengado($devengado, $recaudado) {

//        Se inicializa el estatus del precompromiso
        $estatus = "";

//        Se borra el detalle del compromiso en caso de que no se haya hecho ya
        $this->db->delete('mov_recaudado_detalle', array('numero_recaudado' => $recaudado));

//        Se toma el la información de la caratula del compromiso para poder determinar la fecha de donde se va a tomar el presupuesto
        $sql_caratula = "SELECT * FROM mov_devengado_caratula WHERE numero = ?;";
        $query_caratula = $this->db->query($sql_caratula, array($devengado));
        $result_caratula = $query_caratula->row();

//        Se toma el detalle del precompromiso que se va a copiar
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_devengado_detalle WHERE numero_devengado = ?;";
        $query = $this->db->query($sql, array($devengado));
        $result = $query->result();

        foreach($result as $fila) {
//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder trabajar con la información
            $estructura = json_decode($fila->estructura);

//            Se toma el mes del presupuesto a calcular
            $mes_actual = $this->utilerias->convertirFechaAMes($result_caratula->fecha_solicitud);

//            Se el resultado que sea, se inserta el detalle la table del detalle del compromiso
            $sql = "INSERT INTO mov_recaudado_detalle (numero_recaudado, id_nivel, iva, total_importe, descripcion_detalle, tipo_pago, cantidad, importe, descuento, subsidio, hora_aplicacion, fecha_sql, fecha_aplicacion,  nivel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            $resultado = $this->db->query($sql, array($recaudado, $fila->id_nivel, $fila->iva, $fila->total_importe, $fila->descripcion_detalle, $fila->tipo_pago, $fila->cantidad, $fila->importe, $fila->descuento, $fila->subsidio, $fila->hora_aplicacion, $fila->fecha_sql, $fila->fecha_aplicacion, $fila->nivel));

        }

        return $resultado;

    }

    function copiarDatosDevengadoArchivo($devengado, $recaudado, $datos) {

//        Se inicializa el estatus del precompromiso
        $estatus = "";

//        Se borra el detalle del compromiso en caso de que no se haya hecho ya
        $this->db->delete('mov_recaudado_detalle', array('numero_recaudado' => $recaudado));

//        Se toma el la información de la caratula del compromiso para poder determinar la fecha de donde se va a tomar el presupuesto
        $sql_caratula = "SELECT * FROM mov_devengado_caratula WHERE no_movimiento = ?;";
        $query_caratula = $this->db->query($sql_caratula, array($devengado));
        $result_caratula = $query_caratula->row();

//        $this->debugeo->imprimir_pre($result_caratula);

//        Se toma el detalle del precompromiso que se va a copiar
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_devengado_detalle WHERE numero_devengado = ?;";
        $query = $this->db->query($sql, array($result_caratula->numero));
        $result = $query->result();

        foreach($result as $fila) {
//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder trabajar con la información
            $estructura = json_decode($fila->estructura);

//            Se toma el mes del presupuesto a calcular
            $mes_actual = $this->utilerias->convertirFechaAMes($result_caratula->fecha_solicitud);

//            Se el resultado que sea, se inserta el detalle la table del detalle del compromiso
            $sql = "INSERT INTO mov_recaudado_detalle (numero_recaudado, id_nivel, iva, total_importe, descripcion_detalle,
                                                        tipo_pago, cantidad, importe, descuento, subsidio,
                                                        hora_aplicacion, fecha_sql, fecha_aplicacion,  nivel) VALUES (
                                                        ?, ?, ?, ?, ?,
                                                        ?, ?, ?, ?, ?,
                                                        ?, ?, ?, ?);";
            $resultado = $this->db->query($sql, array($recaudado, $fila->id_nivel, $datos["F"], $datos["H"], $fila->descripcion_detalle,
                                                        $fila->tipo_pago, $fila->cantidad, $datos["E"], 0, $fila->subsidio,
                                                        $fila->hora_aplicacion, $fila->fecha_sql, $datos["B"], $fila->nivel));

        }

        return $resultado;

    }

    function datos_Caratula_Devengado_Recaudado($devengado) {
        $sql = "SELECT * FROM mov_devengado_caratula WHERE numero = ?;";
        $query = $this->db->query($sql, array($devengado));
        $result = $query->row();
        return $result;
    }

    function datos_Caratula_Devengado_Recaudado_Acomodado($devengado) {
        $sql = "SELECT * FROM mov_devengado_caratula WHERE no_movimiento = ?;";
        $query = $this->db->query($sql, array($devengado));
        $result = $query->row_array();
        return $result;
    }

    /**Sección Estructura*/
    /**
     * Esta función sirve para saber si existe la estructura ingresada
     * @param $datos
     * @param $query
     * @return mixed
     */
    function existeEstructura($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"]));
        $result = $query->row();
        return $result;
    }
    /**
     * Esta funcion sirve para contar los niveles dentro de la estructura de los Ingresos
     * @return conteo
     */
    function contar_ingresos_elementos() {
        $sql = "SELECT COUNT(id_ingresos_elementos) AS conteo FROM cat_ingresos_elementos;";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row;
    }
    /**
     * Esta funcion sirve para obtener los nombres de los niveles de la estructura de Ingresos
     * @return arreglo de nombres
     */
    function obtener_nombre_niveles()
    {
        $sql = "SELECT descripcion FROM cat_ingresos_elementos;";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }
    /**
     * Esta funcion pasa a minusculas el nombre del primer nivel, y reemplaza los espacios en blanco por guión bajo, para su busqueda en la base de datos
     * @param $nombre del primer nivel
     * @return nombre del primer nivel y su valor
     */
    function datos_primerNivel($nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre)) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre)) . ", COLUMN_GET(nivel, 'descripcion' as char) AS descripcion FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre))."' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre)) . ";";
        //$this->debugeo->imprimir_pre($sql);
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }
    function get_id_titulo_estructura($datos, $nombre) {
        $sql = "SELECT id_niveles as ID, COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1_superior"], $datos["nivel2_superior"], $datos["nivel3_superior"], $datos["nivel4_superior"]));
        $resultado = $query->row();
        return $resultado;
    }

    function tomarCliente($centro) {
        $sql = "SELECT COLUMN_GET(nivel, 'centro_de_recaudacion' as char) AS centro_de_recaudacion,
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'centro_de_recaudacion' as char) = ?
                GROUP BY centro_de_recaudacion;";
        $query = $this->db->query($sql, array($centro));
        $row = $query->row_array();
        return $row;
    }

}

