<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ingresos_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Esta funcion sirve para contar los niveles dentro de la estructura de los ingresos
     * @return conteo
     */
    function contar_ingresos_elementos() {
        $sql = "SELECT COUNT(id_ingresos_elementos) AS conteo FROM cat_ingresos_elementos;";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row;
    }

    /**
     * Esta funcion sirve para obtener los nombres de los niveles de la estructura de ingresos
     * @return arreglo de nombres
     */
    function obtener_nombre_niveles()
    {
        $sql = "SELECT descripcion FROM cat_ingresos_elementos;";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion pasa a minusculas el nombre del primer nivel, y reemplaza los espacios en blanco por guión bajo, para su busqueda en la base de datos
     * @param $nombre del primer nivel
     * @return nombre del primer nivel y su valor
     */
    function datos_primerNivel($nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre)) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre)) . ", COLUMN_GET(nivel, 'descripcion' as char) AS descripcion FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre))."' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre)) . ";";
//        $this->debugeo->imprimir_pre($sql);
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel1($nivel_buscar1, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[0])) . ",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) != ''
                GROUP BY " . strtolower(str_replace(' ', '_', $nombre[0])) . ";";
        $query = $this->db->query($sql, array("%" . $nivel_buscar1 . "%"));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el segundo nivel, conforme al primer nivel de la estructura de ingresos
     * @param $valor del primer nivel
     * @return nombre y valor del segundo nivel
     */
    function datos_nivel1($nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre))."' as char) AS ".strtolower(str_replace(' ', '_', $nombre)).", COLUMN_GET(nivel, 'descripcion' as char) AS descripcion FROM cat_niveles WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre))."' as char) != '' GROUP BY ".strtolower(str_replace(' ', '_', $nombre))." ;";
        $query = $this->db->query($sql);
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el segundo nivel, conforme al primer nivel de la estructura de ingresos
     * @param $valor del primer nivel
     * @return nombre y valor del segundo nivel
     */
    function datos_nivel2($nivel1, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[1])) . ",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre[1])) . ";";
        $query = $this->db->query($sql, array($nivel1));
        echo($query);
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel2($nivel_buscar1, $nivel_buscar2, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[1])) . ",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) != ''
                GROUP BY " . strtolower(str_replace(' ', '_', $nombre[1])) . ";";
        $query = $this->db->query($sql, array("%" . $nivel_buscar2 . "%", $nivel_buscar1));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el tercer nivel, conforme al primer y segundo nivel de la estructura de ingresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @return nombre y valor del tercer nivel
     */
    function datos_nivel3($nivel1, $nivel2, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[2])) . ",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre[2])) . ";";
        $query = $this->db->query($sql, array($nivel1, $nivel2));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel3($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[2])) . ",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) != ''
                GROUP BY " . strtolower(str_replace(' ', '_', $nombre[2])) . ";";
        $query = $this->db->query($sql, array("%" . $nivel_buscar3 . "%", $nivel_buscar1, $nivel_buscar2));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el cuarto nivel, conforme al primer, segundo y tercer nivel de la estructura de ingresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @return nombre y valor del cuarto nivel
     */
    function datos_nivel4($nivel1, $nivel2, $nivel3, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[3])) . ",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre[3])) . ";";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel4($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[3])) . ",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ''
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) != ''
                GROUP BY " . strtolower(str_replace(' ', '_', $nombre[3])) . ";";
        $query = $this->db->query($sql, array("%" . $nivel_buscar4 . "%", $nivel_buscar1, $nivel_buscar2, $nivel_buscar3));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el quinto nivel, conforme al primer, segundo, tercer y cuarto nivel de la estructura de ingresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @param $nombre cuarto nivel
     * @return nombre y valor del quinto nivel
     */
    function datos_nivel5($nivel1, $nivel2, $nivel3, $nivel4, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[4])) . ",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre[4])) . ";";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3, $nivel4));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel5($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[4])) . ",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) != ''
                GROUP BY " . strtolower(str_replace(' ', '_', $nombre[4])) . ";";
        $query = $this->db->query($sql, array("%" . $nivel_buscar5 . "%", $nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca el sexto nivel, conforme al primer, segundo, tercer, cuarto y quinto nivel de la estructura de ingresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @param $nombre cuarto nivel
     * @param $nombre quinto nivel
     * @return nombre y valor del sexto nivel
     */
    function datos_nivel6($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[5])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[5])) . ",
                    COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[5])) . "' as char) != '' GROUP BY " . strtolower(str_replace(' ', '_', $nombre[5])) . ";";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3, $nivel4, $nivel5));
        $resultado = $query->result();
        return $resultado;
    }

    function buscar_nivel6($nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5, $nivel_buscar6, $nombre)
    {
        $sql = "SELECT COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[5])) . "' as char) AS " . strtolower(str_replace(' ', '_', $nombre[5])) . ",
                COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                FROM cat_niveles
                WHERE COLUMN_GET(nivel, 'descripcion' as char) LIKE ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ?
                AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[5])) . "' as char) != ''
                GROUP BY " . strtolower(str_replace(' ', '_', $nombre[5])) . ";";
        $query = $this->db->query($sql, array("%" . $nivel_buscar6 . "%", $nivel_buscar1, $nivel_buscar2, $nivel_buscar3, $nivel_buscar4, $nivel_buscar5));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion busca datos en especifico del ultimo nivel de la estructura de ingresos
     * @param $nombre primer nivel
     * @param $nombre segundo nivel
     * @param $nombre tercer nivel
     * @param $nombre cuarto nivel
     * @param $nombre quinto nivel
     * @param $nombre sexto nivel
     * @param $mes actual en minusculas y en español
     * @return dinero del mes actual, dinero comprometido del mes, dinero precomprometido del mes, saldo del mes, total anual del dinero en la partida
     */
    function datos_de_ultimo_nivel($nivel1, $nivel2, $nivel3, $nivel4, $nivel5, $mes, $nombre) {
        $sql = "SELECT COLUMN_GET(nivel, '" . $mes . "_inicial' as char) AS mes_inicial,
                    COLUMN_GET(nivel, '" . $mes . "_recaudado' as char) AS mes_compromiso,
				    COLUMN_GET(nivel, '" . $mes . "_devengado' as char) AS mes_precompromiso,
				    COLUMN_GET(nivel, '" . $mes . "_saldo' as char) AS saldo,
				    COLUMN_GET(nivel, 'devengado_anual' as char) AS precomprometido_anual,
				    COLUMN_GET(nivel, 'recaudado_anual' as char) AS comprometido_anual,
				    COLUMN_GET(nivel, 'total_anual' as char) AS total_anual
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[0])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[1])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[2])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[3])) . "' as char) = ?
                    AND COLUMN_GET(nivel, '" . strtolower(str_replace(' ', '_', $nombre[4])) . "' as char) = ?;";
        $query = $this->db->query($sql, array($nivel1, $nivel2, $nivel3, $nivel4, $nivel5));
        $resultado = $query->result();
        return $resultado;
    }

    function get_datos_caratula($id) {
        $query = $this->db->get_where('mov_adecuaciones_ingresos_caratula', array('id_adecuaciones_ingresos_caratula' => $id));
        return $query->row();
    }

    function get_datos_caratula_cancelar($adecuacion) {
        $query = $this->db->get_where('mov_adecuaciones_ingresos_caratula', array('numero' => $adecuacion));
        return $query->row();
    }

    function get_datos_caratula_modificado($last) {
        $this->db->select('numero')->from('mov_adecuaciones_ingresos_caratula')->where('numero', $last);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_datos_detalle($id) {
        $sql = "SELECT * FROM mov_adecuaciones_ingresos_detalle WHERE id_adecuaciones_ingresos_detalle = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }

    function get_datos_detalle_numero($numero) {
        $query = $this->db->get_where('mov_adecuaciones_ingresos_detalle', array('numero_adecuacion' => $numero));
        return $query->result();
    }

    function get_saldo_partida($mes, $id) {
        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_inicial' as char) as mes_saldo,
                       COLUMN_GET(nivel, '".$mes."_devengado' as char) as mes_devengado,
                       COLUMN_GET(nivel, '".$mes."_recaudado' as char) as mes_recaudado,
                       COLUMN_GET(nivel, 'total_anual' as char) as total_anual,
                       COLUMN_GET(nivel, 'recaudado_anual' as char) as recaudado_anual,
                       COLUMN_GET(nivel, 'devengado_anual' as char) as devengado_anual
                       FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row();
    }

    function get_saldo_partida_inicial($mes, $id) {
        $sql = "SELECT COLUMN_GET(nivel, '".$mes."_inicial' as char) as mes_inicial,
                       COLUMN_GET(nivel, '".$mes."_saldo' as char) as mes_saldo,
                       COLUMN_GET(nivel, '".$mes."_devengado' as char) as mes_devengado,
                       COLUMN_GET(nivel, '".$mes."_recaudado' as char) as mes_recaudado,
                       COLUMN_GET(nivel, 'total_anual' as char) as total_anual,
                       COLUMN_GET(nivel, 'recaudado_anual' as char) as recaudado_anual,
                       COLUMN_GET(nivel, 'devengado_anual' as char) as devengado_anual
                       FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id));
        return $query->row_array();
    }

    function actualizar_saldo_partida($mes, $saldo_mes, $total_anual, $id) {
        $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes."_saldo', ?),
                              nivel = COLUMN_ADD(nivel, 'total_anual', ?)
                              WHERE id_niveles = ?;";
        $query = $this->db->query($query_transaccion, array($saldo_mes, $total_anual, $id));
        return $query;
    }

    function cancelarAdecuacion($adecuacion) {
        $data = array(
            'enfirme' => 0,
            'cancelada' => 1,
            'fecha_cancelada' => date( 'Y-m-d' ),
            'estatus' => "cancelado",
        );

        $this->db->where('numero', $adecuacion);
        $this->db->update('mov_adecuaciones_ingresos_caratula', $data);
    }

    function datos_transferenciaDetalle($sql, $numero) {
        $query = $this->db->query($sql, array($numero));
        return $query->result();
    }

    function borrar_adecuacion_detalle($id){
        $this->db->where('id_adecuaciones_ingresos_detalle', $id);
        $query = $this->db->delete('mov_adecuaciones_ingresos_detalle');
        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Seccion Transferencia
     */

    function insertarCaratula($datos) {
        $id = $this->tank_auth->get_user_id();

        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "tipo" => $datos["tipo"],
            "numero" => $datos["ultimo"],
            "fecha_solicitud" => $datos["fecha_solicitud"],
            "fecha_aplicacion" => $datos["fecha_aplicar_caratula"],
            "fecha_sql" => date( 'Y-m-d H:i:s'),
            "clasificacion" => $datos["clasificacion"],
            "importe_total" => $datos["total_hidden"],
            "enfirme" => $datos["check_firme"],
            "year" => date("Y"),
            "cancelada" => 0,
            "fecha_cancelada" => 0,
            "tipo_gasto" => $datos["tipo_radio"],
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["descripcion"],
            "estatus" => $datos["estatus"],
        );

        $this->db->where('numero', $datos["ultimo"]);
        $query = $this->db->update('mov_adecuaciones_ingresos_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertarCaratulaArchivo($datos) {

        switch($datos["C"]) {
            case "Gasto Corriente":
                $datos["C"] = 1;
                break;
            case "Gasto de Capital":
                $datos["C"] = 2;
                break;
            case "Amortización de la cuenta y disminución de pasivos":
                $datos["C"] = 3;
                break;
        }

        $id = $this->tank_auth->get_user_id();

        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "tipo" => $datos["tipo"],
            "numero" => $datos["ultimo"],
            "fecha_solicitud" => $datos["D"],
            "fecha_aplicacion" => $datos["E"],
            "fecha_sql" => date( 'Y-m-d H:i:s'),
            "clasificacion" => $datos["B"],
            "importe_total" => $datos["total"],
            "enfirme" => 0,
            "year" => date("Y"),
            "cancelada" => 0,
            "fecha_cancelada" => 0,
            "tipo_gasto" => $datos["C"],
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["F"],
            "estatus" => "espera",
        );

        $query = $this->db->insert('mov_adecuaciones_ingresos_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function get_datos_adecuaciones() {
        $sql = "SELECT * FROM mov_adecuaciones_ingresos_caratula ORDER BY numero DESC;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_datos_adecuaciones_tipo($sql) {
        $query = $this->db->query($sql);
        return $query->result();
    }

    function datos_de_partida($datos, $nombre) {
        $sql = "SELECT id_niveles AS ID, COLUMN_JSON(nivel) as estructura
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1"], $datos["nivel2"], $datos["nivel3"], $datos["nivel4"], $datos["nivel5"]));
        $resultado = $query->result();
        return $resultado;
    }

    /**
     * Esta funcion se encarga de insertar las partidas en sus correspondientes niveles, junto con el dinero que les corresponde, si es que son del ultimo nivel
     * @param $arreglo con los datos de la fila por insertar
     * @param $nombres de los niveles
     * @return devuelve true si se inserto la fila, de lo contrario devuelve false
     */
    function insertar_niveles_ingresos($field, $nombres) {

        if(array_key_exists('E', $field)) {
            $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
            $query = $this->db->query($sql, array($field['S']));
            $resultado = $query->row();

            $total = $field['G'] + $field['H'] + $field['I'] + $field['J'] + $field['K'] + $field['L'] + $field['M'] + $field['N'] + $field['O'] + $field['P'] + $field['Q'] + $field['R'];
            $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $field['A'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $field['B'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $field['C'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $field['D'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $field['E'] . "',
                                                                    'descripcion', '" . $field['F'] . "',
                                                                    'enero_inicial', '" . $field['G'] . "',
                                                                    'febrero_inicial', '" . $field['H'] . "',
                                                                    'marzo_inicial', '" . $field['I'] . "',
                                                                    'abril_inicial', '" . $field['J'] . "',
                                                                    'mayo_inicial', '" . $field['K'] . "',
                                                                    'junio_inicial', '" . $field['L'] . "',
                                                                    'julio_inicial', '" . $field['M'] . "',
                                                                    'agosto_inicial', '" . $field['N'] . "',
                                                                    'septiembre_inicial', '" . $field['O'] . "',
                                                                    'octubre_inicial', '" . $field['P'] . "',
                                                                    'noviembre_inicial', '" . $field['Q'] . "',
                                                                    'diciembre_inicial', '" . $field['R'] . "',
                                                                    'enero_devengado', 0.0,
                                                                    'febrero_devengado', 0.0,
                                                                    'marzo_devengado', 0.0,
                                                                    'abril_devengado', 0.0,
                                                                    'mayo_devengado', 0.0,
                                                                    'junio_devengado', 0.0,
                                                                    'julio_devengado', 0.0,
                                                                    'agosto_devengado', 0.0,
                                                                    'septiembre_devengado', 0.0,
                                                                    'octubre_devengado', 0.0,
                                                                    'noviembre_devengado', 0.0,
                                                                    'diciembre_devengado', 0.0,
                                                                    'enero_recaudado', 0.0,
                                                                    'febrero_recaudado', 0.0,
                                                                    'marzo_recaudado', 0.0,
                                                                    'abril_recaudado', 0.0,
                                                                    'mayo_recaudado', 0.0,
                                                                    'junio_recaudado', 0.0,
                                                                    'julio_recaudado', 0.0,
                                                                    'agosto_recaudado', 0.0,
                                                                    'septiembre_recaudado', 0.0,
                                                                    'octubre_recaudado', 0.0,
                                                                    'noviembre_recaudado', 0.0,
                                                                    'diciembre_recaudado', 0.0,
                                                                    'enero_saldo', '" . $field['G'] . "',
                                                                    'febrero_saldo', '" . $field['H'] . "',
                                                                    'marzo_saldo', '" . $field['I'] . "',
                                                                    'abril_saldo', '" . $field['J'] . "',
                                                                    'mayo_saldo', '" . $field['K'] . "',
                                                                    'junio_saldo', '" . $field['L'] . "',
                                                                    'julio_saldo', '" . $field['M'] . "',
                                                                    'agosto_saldo', '" . $field['N'] . "',
                                                                    'septiembre_saldo', '" . $field['O'] . "',
                                                                    'octubre_saldo', '" . $field['P'] . "',
                                                                    'noviembre_saldo', '" . $field['Q'] . "',
                                                                    'diciembre_saldo', '" . $field['R'] . "',
                                                                    'total_anual', '" . $total . "',
                                                                    'devengado_anual', 0.0,
                                                                    'recaudado_anual', 0.0,
                                                                    'clave_financiamiento', '".$resultado->descripcion."'));";
        } else {
            if (!array_key_exists('B', $field)) {
                $field['B'] = '';
            } if(!array_key_exists('C', $field)) {
                $field['C'] = '';
            } if(!array_key_exists('D', $field)) {
                $field["D"] = '';
            } if(!array_key_exists('E', $field)) {
                $field["E"] = '';
            } if(!array_key_exists('F', $field)) {
                $field["F"] = '';
            } if(!array_key_exists('G', $field)) {
                $field["G"] = '';
            } if(!array_key_exists('H', $field)) {
                $field["H"] = '';
            } if(!array_key_exists('I', $field)) {
                $field["I"] = '';
            } if(!array_key_exists('J', $field)) {
                $field["J"] = '';
            } if(!array_key_exists('K', $field)) {
                $field["K"] = '';
            } if(!array_key_exists('L', $field)) {
                $field["L"] = '';
            } if(!array_key_exists('M', $field)) {
                $field["M"] = '';
            } if(!array_key_exists('N', $field)) {
                $field["N"] = '';
            } if(!array_key_exists('O', $field)) {
                $field["O"] = '';
            } if(!array_key_exists('P', $field)) {
                $field["P"] = '';
            } if(!array_key_exists('Q', $field)) {
                $field["Q"] = '';
            } if(!array_key_exists('R', $field)) {
                $field["R"] = '';
            } if(!array_key_exists('S', $field)) {
                $field["S"] = '';
            } if(!array_key_exists('T', $field)) {
                $field["T"] = '';
            } if(!array_key_exists('U', $field)) {
                $field["U"] = '';
            } if(!array_key_exists('V', $field)) {
                $field["V"] = '';
            } if(!array_key_exists('W', $field)) {
                $field["W"] = '';
            } if(!array_key_exists('X', $field)) {
                $field["X"] = '';
            } if(!array_key_exists('Y', $field)) {
                $field["Y"] = '';
            } if(!array_key_exists('Z', $field)) {
                $field["Z"] = '';
            }

            $sql = "INSERT INTO cat_niveles (id_niveles,  nivel) VALUES (NULL, COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombres[0])) . "','" . $field['A'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[1])) . "', '" . $field['B'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[2])) . "', '" . $field['C'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[3])) . "', '" . $field['D'] . "',
                                                                    '" . strtolower(str_replace(' ', '_', $nombres[4])) . "', '" . $field['E'] . "',
                                                                    'descripcion', '" . $field['F'] . "'));";
        }

        $query = $this->db->query($sql);

//        Seccion para saber si se isertaron las filas
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function insertar_nivel($sql) {
        $this->db->trans_start();

        $query = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertarDetalle($datos) {
//        Se llama a la funcion del model de ingresos encargada de tomar los nombres delos niveles que existen
        $nombres_ingresos = $this->ingresos_model->obtener_nombre_niveles();

//        En este arreglo se van a guardar los nombres de los niveles
        $nombre = array();

//        En este ciclo se toman los nombres del arreglo del modelo y se guardan en otro arreglo
        foreach($nombres_ingresos as $fila) {
            array_push($nombre, $fila->descripcion);
        }

        $id_nivel = $this->get_id_titulo_estructura($datos, $nombre);

//        $this->debugeo->imprimir_pre($id_nivel);

//        estructura, capitulo, mes_destino, total, titulo, texto, id_enlace
        $sql = "SELECT COLUMN_GET(nivel, 'clave_financiamiento' as char) AS clave FROM cat_niveles WHERE id_niveles = ?;";
        $query = $this->db->query($sql, array($id_nivel->ID));
        $datos_subsidio = $query->row();

        $sql = "INSERT INTO mov_adecuaciones_ingresos_detalle
                  (numero_adecuacion, tipo, subsidio, year, fecha_aplicacion,
                  hora_aplicacion, fecha_sql, partida, id_nivel,
                  estructura,
                  capitulo, mes_destino, total, titulo, texto, id_enlace) VALUES
                  (?, ?, ?, ?, ?,
                  ?, ?, ?, ?,
                  COLUMN_CREATE('" . strtolower(str_replace(' ', '_', $nombre[0])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[1])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[2])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[3])) . "', ?,
                  '" . strtolower(str_replace(' ', '_', $nombre[4])) . "', ?),
                  ?, ?, ?, ?, ?, ?);";
        $query = $this->db->query($sql, array(
            $datos["ultimo"], $datos["tipo"], $datos_subsidio->clave, date("Y"), $datos["fecha_aplicar_detalle"],
            date( 'H:i'), date( 'Y-m-d H:i:s'), $datos["nivel5_inferior"], $id_nivel->ID,
            $datos["nivel1_inferior"], $datos["nivel2_inferior"], $datos["nivel3_inferior"], $datos["nivel4_inferior"], $datos["nivel5_inferior"],
            $datos["nivel3_inferior"],
            $datos["mes_destino"],
            $datos["cantidad"],
            $id_nivel->descripcion,
            $datos["tipo"],
            $datos["enlace"] ));

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function ultimo_adecuaciones() {
        $sql = "SELECT numero as ultimo FROM mov_adecuaciones_ingresos_caratula ORDER BY numero DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarAdecuacion($ultimo, $tipo) {
        $id = $this->tank_auth->get_user_id();

        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'tipo' => $tipo,
            'numero' => $ultimo,
            'estatus' => "espera",
            'creado_por' => $nombre_completo,
            'fecha_solicitud' => date("Y-m-d"),
        );

        $query = $this->db->insert('mov_adecuaciones_ingresos_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function get_id_titulo_estructura($datos, $nombre) {
        $sql = "SELECT id_niveles as ID, COLUMN_GET(nivel, 'descripcion' as char) AS descripcion
                    FROM cat_niveles
                    WHERE COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[0]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[1]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[2]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[3]))."' as char) = ?
                    AND COLUMN_GET(nivel, '".strtolower(str_replace(' ', '_', $nombre[4]))."' as char) = ?;";
        $query = $this->db->query($sql, array($datos["nivel1_inferior"], $datos["nivel2_inferior"], $datos["nivel3_inferior"], $datos["nivel4_inferior"], $datos["nivel5_inferior"]));
        $resultado = $query->row();
        return $resultado;
    }

    function editar_nivel($sql) {
        $this->db->trans_start();

        $query = $this->db->query($sql);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }
}