<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extranet_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function activar_proveedor($id_proveedor) {
    	$this->db->trans_begin();

    	$id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

    	$datos_actualizar = array(
    		'activo' => 1,
    		'aprobado_por' => $nombre_completo,
    		'fecha_aprobado' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id_proveedores', $id_proveedor);
		$this->db->update('cat_proveedores', $datos_actualizar);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		} else {
			$this->db->trans_commit();
			return TRUE;
		}

    }

    function desactivar_proveedor($id_proveedor) {
        $this->db->trans_begin();

        $this->db->where('id_proveedores', $id_proveedor);
        $this->db->delete('cat_proveedores');

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function insertar_proveedor($data, $id_extranet) {
        $this->db->trans_begin();

        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $nombre = "";
//        $pyme = "";

//        if($data["pyme"] == "No") {
//            $pyme = 0;
//        } else {
//            $pyme = 1;l
//        }

        if($data["tipo_razon_social_hidden"] == "fisica") {
            $nombre = $data["nombre"]." ".$data["apellidos"];
        } else{
            $nombre = $data["razon_social"];
        }

        $datos_proveedor = array(
            'clave_prov' => $this->utilerias->cadena_random_proveedor(),
            'nombre' => $nombre,
            'email' => $data["email"],
            'calle' => $data["calle"],
            'no_exterior' => $data["numero_exterior"],
            'no_interior' => $data["numero_interior"],
            'colonia' => $data["colonia"],
            'cp' => $data["codigo_postal"],
            'ciudad' => $data["ciudad"],
            'pais' => $data["pais"],
            'contacto' => $data["nombre_uno"]." ".$data["apellido_pa_uno"]." ".$data["apellido_ma_uno"],
            'telefono' => $data["telefono_uno"],
            'RFC' => $data["username"],
            'cuenta' => $data["no_cuenta"],
            'clabe' => $data["clabe"],
            'banco' => $data["banco"],
//            'mypyme' => $pyme,
            'estado' => $data["estado"],
            'activo' => 1,
            'aprobado_por' => $nombre_completo,
            'fecha_aprobado' => date('Y-m-d H:i:s'),
            'id_usuario_extranet' => $id_extranet,
        );

        $this->db->insert('cat_proveedores', $datos_proveedor);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function enviar_comentarios($asunto, $mensaje, $rfc_proveedor) {
        $this->db->trans_begin();

        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $query_correo = "SELECT email FROM users WHERE id = ?;";
        $resultado_query_correo = $this->db->query($query_correo, array($id));
        $email_encontrado = $resultado_query_correo->row_array();
        $email_remitente = $email_encontrado["email"];

        $DB1 = $this->load->database('extranet', TRUE);

        $query_id_proveedor = "SELECT id FROM users WHERE username = ?;";
        $resultado_id_proveedor = $DB1->query($query_id_proveedor, array($rfc_proveedor));
        $final_id_proveedor = $resultado_id_proveedor->row_array();
        $id_proveedor = $final_id_proveedor["id"];

        $query_correos_proveedor = "SELECT email_facturas, email_ventas FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_correos_proveedor = $DB1->query($query_correos_proveedor, array($id_proveedor));
        $correos_proveedor = $resultado_correos_proveedor->row_array();

        $this->utilerias->enviar_correo($correos_proveedor["email_facturas"],
                                        $email_remitente,
                                        $nombre_completo,
                                        $asunto,
                                        $mensaje);

        $this->utilerias->enviar_correo($correos_proveedor["email_ventas"],
                                        $email_remitente,
                                        $nombre_completo,
                                        $asunto,
                                        $mensaje);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function tomar_encabezado_precompromiso($numero_precompro) {
        $query = $this->db->select('*')
                            ->where('numero_pre', $numero_precompro)
                            ->get('mov_precompromiso_caratula');
        return $query->row_array();
    }

    function tomar_detalle_precompromiso($numero_precompro) {
        $query = $this->db->select('*, COLUMN_JSON(nivel) AS estructura')
                            ->where('numero_pre', $numero_precompro)
                            ->get('mov_precompromiso_detalle');
        return $query->row_array();
    }

    function tomar_encabezado_compromiso($numero_compromiso) {
        $query = $this->db->select('*')
                            ->where('numero_compromiso', $numero_compromiso)
                            ->get('mov_compromiso_caratula');
        return $query->row_array();
    }

    function tomar_detalle_compromiso($numero_compromiso) {
        $query = $this->db->select('*, COLUMN_JSON(nivel) AS estructura')
                            ->where('numero_compromiso', $numero_compromiso)
                            ->get('mov_compromiso_detalle');
        return $query->row_array();
    }

}