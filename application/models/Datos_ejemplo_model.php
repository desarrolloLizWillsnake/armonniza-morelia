<?php class datos_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function vista_proveedores($data)
    {
        $this->db->insert('cat_proveedores', $data);
    }
    function ver_tablas(){

        $query = $this->db->get('cat_proveedores');
        if ($query->num_rows()> 0) {
            return $query;
        }else{
            return FALSE;
        }

    }

}