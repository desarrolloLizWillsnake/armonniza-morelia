<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertar_proveedor($data) {

    	$this->db->trans_begin();

        $nombre = "";
        $pyme = "";

    	if($data[2] == "No") {
            $pyme = 0;
    	} else {
    		$pyme = 1;
    	}

        if($data[1] == "fisica") {
            $nombre = $data[4]." ".$data[5];
        } else{
            $nombre = $data[3];
        }
        
        $datos_proveedor = array(
            'clave_prov' => $this->utilerias->cadena_random_proveedor(),
            'nombre' => $nombre,
            'email' => $data[17],
            'calle' => $data[11],
            'no_exterior' => $data[12],
            'no_interior' => $data[13],
            'colonia' => $data[10],
            'cp' => $data[9],
            'ciudad' => $data[8],
            'pais' => $data[6],
            'contacto' => $data[14]." ".$data[15]." ".$data[16],
            'RFC' => $data[0],
            'cuenta' => $data[23],
            'clabe' => $data[24],
            'banco' => $data[22],
            'mypyme' => $pyme,
            'estado' => $data[7],
            'id_usuario_extranet' => $data[26],
        );

        $this->db->insert('cat_proveedores', $datos_proveedor);
        
        // $datos_pago = array(
        //     'moneda' => $data["moneda"],
        //     'banco' => $data["banco"],
        //     'no_cuenta' => $data["no_cuenta"],
        //     'CLABE' => $data["clabe"],
        //     'comprobante' => $data["comprobante"],
        //     'id_usuario' => $data["user_id"],  
        // );

        // $this->db->insert('forma_pago_usuario', $datos_pago);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }
		

    }

    public function actualizar_proveedor($data) {

        $this->db->trans_begin();

        $nombre = "";
        $pyme = "";

        if($data[2] == "No") {
            $pyme = 0;
        } else {
            $pyme = 1;
        }

        if($data[1] == "fisica") {
            $nombre = $data[4]." ".$data[5];
        } else{
            $nombre = $data[3];
        }
        
        $datos_proveedor = array(
            'nombre' => $nombre,
            'email' => $data[17],
            'calle' => $data[11],
            'no_exterior' => $data[12],
            'no_interior' => $data[13],
            'colonia' => $data[10],
            'cp' => $data[9],
            'ciudad' => $data[8],
            'pais' => $data[6],
            'contacto' => $data[14]." ".$data[15]." ".$data[16],
            'RFC' => $data[0],
            // 'cuenta' => $data[23],
            // 'clabe' => $data[24],
            // 'banco' => $data[22],
            'mypyme' => $pyme,
            'estado' => $data[7],
            'id_usuario_extranet' => $data[22],
        );

        $this->db->update('cat_proveedores', $datos_proveedor, array('id_usuario_extranet' => $data[22]));
        
        // $datos_pago = array(
        //     'moneda' => $data["moneda"],
        //     'banco' => $data["banco"],
        //     'no_cuenta' => $data["no_cuenta"],
        //     'CLABE' => $data["clabe"],
        //     'comprobante' => $data["comprobante"],
        //     'id_usuario' => $data["user_id"],  
        // );

        // $this->db->insert('forma_pago_usuario', $datos_pago);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }
        

    }

}