<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ciclo_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->db->cache_on();
    }

    function get_datos_proveedor($id) {
        $query = "SELECT * FROM cat_proveedores WHERE clave_prov = ?;";
        $query = $this->db->query($query, array($id));
        $result = $query->row();
        return $result;
    }

    function get_datos_beneficiario($id) {
        $query = "SELECT * FROM cat_beneficiarios WHERE id_beneficiarios = ?;";
        $query = $this->db->query($query, array($id));
        $result = $query->row();
        return $result;
    }

    function ultimo_precompromiso() {
        $sql = "SELECT numero_pre as ultimo FROM mov_precompromiso_caratula ORDER BY numero_pre DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarPrecompromiso($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_pre' => $ultimo,
            'estatus' => "espera",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_precompromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_precompromisoCaratula(){
        $sql = "SELECT * FROM mov_precompromiso_caratula ORDER BY numero_pre DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_precompromisoCaratulaEstatus($sql){
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_precompromisoDetalle($precompromiso, $sql) {
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->result();
        return $result;
    }

    function datos_gasto() {
        $sql = "SELECT * FROM cat_conceptos_gasto;";
        $query = $this->db->query($sql);
        $result = $query->result();

        return $result;
    }

    function cancelarPrecompromisoCaratula($precompromiso, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $precompromiso,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function cancelarPrecompromiso($precompromiso) {

        $this->db->where('id_precompromiso_detalle', $precompromiso);
        $this->db->select('*');
        $query = $this->db->get('mov_precompromiso_detalle');
        $result = $query->row();

        $this->db->where('numero_pre', $result->numero_pre);
        $this->db->select('fecha_autoriza');
        $query2 = $this->db->get('mov_precompromiso_caratula');
        $result_caratula = $query2->row();

        $fecha = $result_caratula->fecha_autoriza;

        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

//        Se tiene que comprobar si hay dinero para hacer el precompromiso
        $query_revisar_dinero = "SELECT COLUMN_GET(nivel, 'total_anual' as char) AS total_anual,
                                    COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) AS mes_saldo,
                                    COLUMN_GET(nivel, '".$mes_actual."_precompromiso' as char) AS mes_precompromiso,
                                    COLUMN_GET(nivel, 'precomprometido_anual' as char) AS precomprometido_anual
                                    FROM cat_niveles WHERE id_niveles = ?";

        $resultado_dinero = $this->ciclo_model->revisarDinero($result->id_nivel, $query_revisar_dinero);

        $precompromiso_mes = $resultado_dinero->mes_precompromiso - $result->importe;

        $precompromiso_anual = $resultado_dinero->precomprometido_anual - $result->importe;

        $query_transaccion = "UPDATE cat_niveles
                              SET nivel = COLUMN_ADD(nivel, '".$mes_actual."_precompromiso', ?),
                              nivel = COLUMN_ADD(nivel, 'precomprometido_anual', ?)
                              WHERE id_niveles = ?;";

        $resultado_actualizar = $this->ciclo_model->actualizar_total_anual_precompromiso($query_transaccion, $precompromiso_mes, $precompromiso_anual, $result->id_nivel);

        if ($resultado_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function borrarDetallePrecompromiso($precompromiso) {
        $this->db->where('id_precompromiso_detalle', $precompromiso);
        $query = $this->db->delete('mov_precompromiso_detalle');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function borrarDetallePrecompromisoViatico($viatico) {
        
        $this->db->where('id_tabla_lugares_periodos', $viatico);
        $query = $this->db->delete('mov_tabla_lugares_periodos');

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function tomarDetalleBorrar($precompromiso) {
        $this->db->select('id_precompromiso_detalle, COLUMN_JSON(nivel) AS estructura, COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado, importe')->from('mov_precompromiso_detalle')->where('numero_pre', $precompromiso);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Esta función sirve para saber si existe la estructura ingresada
     * @param $datos
     * @param $query
     * @return mixed
     */
    function existeEstructura($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"]));
        $result = $query->row();
        return $result;
    }

    function existeEstructuraArchivo($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["TIPO PRECOMPROMISO"],
            $datos["PROVEEDOR"],
            $datos["TIPO DE GARANTIA"],
            $datos["PORCENTAJE DE GARANTIA"],
            $datos["IMPORTE DE RESPONSABILIDAD CIVIL"],
            $datos["IVA"]));
        $result = $query->row();
        return $result;
    }

    function revisarDinero($id_nivel, $query) {
        $query = $this->db->query($query, array($id_nivel));
        $result = $query->row();
        return $result;
    }

    function revisarDineroEstructuraCompleta($datos_estructura, $query) {
        $query = $this->db->query($query, array(
            $datos_estructura[0],
            $datos_estructura[1],
            $datos_estructura[2],
            $datos_estructura[3],
            $datos_estructura[4],
            $datos_estructura[5],
            ));
        $result = $query->row_array();
        return $result;
    }

    /**
     * Esta función se encarga de insertar los datos del precompromiso en la carátula.
     * @param $datos
     * @return bool
     */
    function insertar_caratula_precompromiso($datos) {

        if(!$datos["total_hidden"]) {
            $datos["total_hidden"] = 0;
        }

        if(!$datos["fecha_entrega"]) {
            $datos["fecha_entrega"] = "0000-00-00";
        }
        
        if(!$datos["numero_plazo"]) {
            $datos["numero_plazo"] = 0;
        }

        if(!$datos["importe_civil"]) {
            $datos["importe_civil"] = 0;
        }

//        Este arreglo son los datos que van a ser insertados en la caratula del precompromiso
        $data = array(
            "numero_pre" => $datos["ultimo_pre"],
            "no_contrato" => $datos["no_contrato"],
            "tipo_requisicion" => $datos["tipo_precompromiso"],
            "tipo_gasto" => $datos["tipo_radio"],
            "enfirme" => $datos["check_firme"],
            "firma1" => $datos["firma1"],
            "firma2" => $datos["firma2"],
            "firma3" => $datos["firma3"],
            "fecha_emision" => $datos["fecha_solicitud"],
            "fecha_autoriza" => $datos["fecha_autoriza"],
            "fecha_programada" => $datos["fecha_entrega"],
            "total" => $datos["total_hidden"],
            "estatus" => $datos["estatus"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "ley_aplicable" => $datos["ley"],
            "articulo_procedencia" => $datos["articulo"],
            "lugar_entrega" => $datos["lugar_entrega"],
            "plazo_numero" => $datos["numero_plazo"],
            "plazo_tipo" => $datos["tipo_plazo"],
            "plurianualidad" => $datos["plurianualidad"],
            "tipo_garantia" => $datos["tipo_garantia"],
            "porciento_garantia" => $datos["porciento_garantia"],
            "anexos" => $datos["anexos"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "importe_responsabilidad_civil" => $datos["importe_civil"],
            "lugar_adquisiciones" => $datos["lugar_adquisicion"],
            "existencia_almacen" => $datos["almacen"],
            "norma_inspeccion" => $datos["normas"],
            "registros_sanitarios" => $datos["registros"],
            "capacitacion" => $datos["capacitacion"],
            "id_persona" => $datos["id_persona"],
            "nombre_completo" => $datos["nombre_persona"],
            "puesto" => $datos["puesto"],
            "clave" => $datos["clave"],
            "area" => $datos["area"],
            "no_empleado" => $datos["no_empleado"],
            "forma_pago" => $datos["forma_pago"],
            "transporte" => $datos["transporte"],
            "grupo_jerarquico" => $datos["grupo_jerarquico"],
            "zona_marginada" => $datos["zona_marginada_check"],
            "zona_mas_economica" => $datos["zona_mas_economica_check"],
            "zona_menos_economica" => $datos["zona_menos_economica_check"],
        );

//        Esta función es la que se encarga de actualizar el precompromiso
        $this->db->where('numero_pre', $datos["ultimo_pre"]);
        $query = $this->db->update('mov_precompromiso_caratula', $data);

//        Si se pudieron insertar los datos, se devuelve una variable booleana como verdadera
        if($query) {
            return TRUE;
        }
//        De lo contrario devuelve una variable booleana falsa
        else {
            return FALSE;
        }

    }

    function insertar_caratula_precompromisoArchivo($datos, $ultimo) {
        $id = $this->tank_auth->get_user_id();
        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        /** En esta parte se toma el nomnbre del proveedor */
        $query_proveedor = "SELECT nombre FROM cat_proveedores WHERE clave_prov = ?;";
        $resultado_query_proveedor = $this->db->query($query_proveedor, array($datos["B"]));
        $nombre_proveedor = $resultado_query_proveedor->row();

        if(!$nombre_proveedor) {
            $DB1 = $this->load->database('pedidos', TRUE);
            
            $sql_caratula = "SELECT nomedi AS nombre
                                FROM editor
                                WHERE cveedi = ?";
            $query_caratula = $DB1->query($sql_caratula, array($datos["B"]));
            $nombre_proveedor = $query_caratula->row();
        }

        if($datos["S"] == "Si") {
            $anexos = 1;
        } elseif($datos["S"] == "No") {
            $anexos = 0;
        }

        if(trim($datos["H"]) == "Gasto Corriente") {
            $datos["H"] = 1;
        } elseif(trim($datos["E"]) == "Gasto de Capital") {
            $datos["H"] = 2;
        } elseif(trim($datos["E"]) == "Amortización de la cuenta y disminución de pasivos") {
            $datos["H"] = 3;
        }

        $fecha = ($datos['O'] - 25569) * 86400;
        $fecha = date('Y-m-d', $fecha);

        $data = array(
            "numero_pre" => $ultimo,
            "tipo_requisicion" => ucfirst(strtolower($datos["A"])),
            "tipo_gasto" => $datos["H"],
            "enfirme" => 0,
            "firma1" => 0,
            "firma2" => 0,
            "firma3" => 0,
            "fecha_emision" => date("Y-m-d"),
            "fecha_autoriza" => 0,
            "fecha_programada" => $fecha,
            "subtotal" => 0,
            "total" => 0,
            "estatus" => "espera",
            "id_usuario" => $this->tank_auth->get_user_id(),
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["T"],
            "ley_aplicable" => $datos["I"],
            "articulo_procedencia" => $datos["J"],
            "lugar_entrega" => $datos["F"],
            "plazo_numero" => $datos["Q"],
            "plazo_tipo" => $datos["R"],
            "plurianualidad" => $datos["K"],
            "tipo_garantia" => $datos["C"],
            "porciento_garantia" => $datos["D"],
            "anexos" => $anexos,
            "id_proveedor" => $datos["B"],
            "proveedor" => $nombre_proveedor->nombre,
            "importe_responsabilidad_civil" => $datos["E"],
            "lugar_adquisiciones" => $datos["F"],
            "existencia_almacen" => $datos["G"],
            "norma_inspeccion" => $datos["L"],
            "registros_sanitarios" => $datos["M"],
            "capacitacion" => $datos["N"],
        );

        $query = $this->db->insert('mov_precompromiso_caratula', $data);

        if($query) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_precompromiso_cancelado($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        if(!$datos["total_hidden"]) {
            $datos["total_hidden"] = 0;
        }

        $data = array(
            "numero_pre" => $datos["ultimo_pre"],
            "tipo_requisicion" => $datos["tipo_precompromiso"],
            "tipo_gasto" => $datos["tipo_radio"],
            "enfirme" => $datos["check_firme"],
            "firma1" => $datos["firma1"],
            "firma2" => $datos["firma2"],
            "firma3" => $datos["firma3"],
            "fecha_emision" => $datos["fecha_solicitud"],
            "fecha_autoriza" => "0000-00-00",
            "fecha_programada" => "0000-00-00",
            "total" => $datos["total_hidden"],
            "estatus" => $datos["estatus"],
            "id_usuario" => $this->tank_auth->get_user_id(),
            "creado_por" => $nombre_completo,
            "descripcion" => $datos["descripcion_general"],
            "ley_aplicable" => $datos["ley"],
            "articulo_procedencia" => $datos["articulo"],
            "lugar_entrega" => $datos["lugar_entrega"],
            "plazo_numero" => 0,
            "plazo_tipo" => $datos["tipo_plazo"],
            "plurianualidad" => $datos["plurianualidad"],
            "tipo_garantia" => $datos["tipo_garantia"],
            "porciento_garantia" => $datos["porciento_garantia"],
            "anexos" => $datos["anexos"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "importe_responsabilidad_civil" => 0,
            "lugar_adquisiciones" => $datos["lugar_adquisicion"],
            "existencia_almacen" => $datos["almacen"],
            "norma_inspeccion" => $datos["normas"],
            "registros_sanitarios" => $datos["registros"],
            "capacitacion" => $datos["capacitacion"],
            'usada' => 0,
            'cancelada' => 1,
            'fecha_cancelada' => date("Y-m-d"),
        );

        $this->db->where('numero_pre', $datos["ultimo_pre"]);
        $query = $this->db->update('mov_precompromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function get_datos_precompromiso_caratula($precompromiso, $query) {
        $query = $this->db->query($query, array($precompromiso));
        $result = $query->row();
        return $result;
    }

    function get_datos_precompromiso_detalle($precompromiso){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_precompromiso_detalle WHERE numero_pre = ?;";
        $query = $this->db->query($query, array($precompromiso));
        $result = $query->result();
        return $result;
    }

    function get_datos_precompromiso_lugares($precompromiso){
        $query = "SELECT * FROM mov_tabla_lugares_periodos WHERE numero_precompromiso = ?;";
        $query = $this->db->query($query, array($precompromiso));
        $result = $query->result();
        return $result;
    }

    function insertar_detalle_precompromiso($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["ultimo_pre"],
            $datos["id_nivel"],
            $datos["gasto"],
            $datos["u_medida"],
            $datos["cantidad"],
            $datos["precio"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            date("Y"),
            $datos["descripcion_detalle"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"],
            $datos["titulo_gasto"]));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_detalle_precompromisoArchivo($datos, $query) {
        /** En esta parte se toma el nombre del gasto */
        $query_gasto = "SELECT descripcion FROM cat_conceptos_gasto WHERE articulo = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array($datos["G"]));
        $nombre_gasto = $resultado_query_gasto->row();

        $query = $this->db->query($query, array(
            $datos["precompromiso"],
            $datos["id_nivel"],
            $datos["G"],
            $datos["H"],
            $datos["I"],
            $datos["K"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $nombre_gasto->descripcion,
            date("Y"),
            $datos["L"],
            $datos["A"],
            $datos["B"],
            $datos["C"],
            $datos["D"],
            $datos["E"],
            $datos["F"],
            $nombre_gasto->descripcion));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_detalle_viaticos_precompromiso($datos) {

        $data = array(
            'numero_precompromiso' => $datos['ultimo_pre'],
            'lugar' => $datos['lugar_comision'],
            'periodo_inicio' => $datos['fecha_inicio'],
            'periodo_fin' => $datos['fecha_termina'],
            'cuota_diaria' => $datos['cuota_diaria'],
            'dias' => $datos['dias'],
            'importe' => $datos['importe'],
        );

        $query = $this->db->insert('mov_tabla_lugares_periodos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function editarDetallePrecompromiso($datos) {

        $this->db->where('id_precompromiso_detalle', $datos["id_precompromiso"]);
        $this->db->select('id_nivel, id_precompromiso_detalle AS numero_precompromiso');
        $query = $this->db->get('mov_precompromiso_detalle');
        $result = $query->row();

        $data = array(
            'cantidad' => $datos["cantidad"],
            'p_unitario' => $datos["precio"],
            'subtotal' => $datos["subtotal"],
            'iva' => $datos["iva"],
            'importe' => $datos["importe"],
        );

        $this->db->where('id_precompromiso_detalle', $datos["id_precompromiso"]);
        $this->db->update('mov_precompromiso_detalle', $data);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function actualizarTotalCaratulaPrecompromiso($id, $total){
        $data = array(
            'total' => $total,
        );

        $this->db->where('id_precompromiso_caratula', $id);
        $this->db->update('mov_precompromiso_caratula', $data);
    }

    function actualizar_total_anual_precompromiso($query, $mes, $anual, $nivel) {
        $query = $this->db->query($query, array(
            $mes,
            $anual,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_viaticosDetalle($precompromiso) {
        $this->db->select('id_tabla_lugares_periodos, lugar, periodo_inicio, periodo_fin, cuota_diaria, dias, importe')->from('mov_tabla_lugares_periodos')->where('numero_precompromiso', $precompromiso);
        $query = $this->db->get();
        return $query->result();
    }

    function solicitar_terminar_precompromiso($precompromiso) {
        
        $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado,
                            COLUMN_JSON(nivel) AS estructura')
                        ->from('mov_precompromiso_detalle')
                        ->where('numero_pre', $precompromiso);
        $query = $this->db->get();
        $resultado = $query->result_array();

        $total_disminuir = 0;

        foreach ($resultado as $key => $value) {
            
            $presupuesto_tomado = json_decode($value["presupuesto_tomado"], TRUE);
            $estructura = json_decode($value["estructura"], TRUE);

            // Se toma la estructura completa
            $query_revisar_dinero = "SELECT COLUMN_JSON(nivel) AS estructura
                                     FROM cat_niveles
                                     WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                     AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                     AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                     AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            $resultado_dinero = $this->ciclo_model->revisarDineroEstructuraCompleta(array(
                0 => $estructura["fuente_de_financiamiento"],
                1 => $estructura["programa_de_financiamiento"],
                2 => $estructura["centro_de_costos"],
                3 => $estructura["capitulo"],
                4 => $estructura["concepto"],
                5 => $estructura["partida"],), $query_revisar_dinero);

            $estructura_partida = json_decode($resultado_dinero["estructura"], TRUE);

            foreach ($presupuesto_tomado as $key_presupuesto => $value_presupuesto) {
                $total_disminuir += $value_presupuesto;
                $estructura_partida[$key_presupuesto."_saldo_precompromiso"] += $value_presupuesto;
                $estructura_partida[$key_presupuesto."_precompromiso"] -= $value_presupuesto;
                $estructura_partida["precomprometido_anual"] -= $value_presupuesto;
            }
            
            $this->actualizar_partida_total($estructura_partida);
        }

        $this->db->select('total')
                    ->from('mov_precompromiso_caratula')
                    ->where('numero_pre', $precompromiso);
        $query_caratula = $this->db->get();
        $caratula = $query_caratula->row_array();
        $operacion = $caratula["total"] - $total_disminuir;

        $datos_actualizar_caratula = array(
            'solicitar_terminar' => 1,
            'terminado' => 1,
            'estatus' => "Terminado",
            "total" => $operacion,
        );

        $this->db->where('numero_pre', $precompromiso);
        $query_actualizar_caratula = $this->db->update('mov_precompromiso_caratula', $datos_actualizar_caratula);

        if($query_actualizar_caratula) {
            return TRUE;
        } else  {
            return FALSE;
        }
    }

    function checar_comprmisos_cancelar_precompromisos($precompromiso){
        $this->db->select('numero_compromiso')->from('mov_compromiso_caratula')->where('num_precompromiso', $precompromiso)->where('enfirme', 1)->where('firma1', 1)->where('firma2', 1)->where('firma3', 1)->where('cancelada', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    function actualizar_partida_precompromiso($estructura) {
        $query_gasto = "UPDATE cat_niveles SET nivel=COLUMN_ADD(nivel, 'enero_precompromiso', '".$estructura["enero_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_precompromiso', '".$estructura["febrero_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_precompromiso', '".$estructura["marzo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_precompromiso', '".$estructura["abril_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_precompromiso', '".$estructura["mayo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_precompromiso', '".$estructura["junio_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_precompromiso', '".$estructura["julio_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_precompromiso', '".$estructura["agosto_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_precompromiso', '".$estructura["septiembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_precompromiso', '".$estructura["octubre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_precompromiso', '".$estructura["noviembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_precompromiso', '".$estructura["diciembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_saldo_precompromiso', '".$estructura["enero_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_saldo_precompromiso', '".$estructura["febrero_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_saldo_precompromiso', '".$estructura["marzo_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_saldo_precompromiso', '".$estructura["abril_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_saldo_precompromiso', '".$estructura["mayo_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_saldo_precompromiso', '".$estructura["junio_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_saldo_precompromiso', '".$estructura["julio_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_saldo_precompromiso', '".$estructura["agosto_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_saldo_precompromiso', '".$estructura["septiembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_saldo_precompromiso', '".$estructura["octubre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_saldo_precompromiso', '".$estructura["noviembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_saldo_precompromiso', '".$estructura["diciembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_modificado', '".$estructura["enero_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_modificado', '".$estructura["febrero_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_modificado', '".$estructura["marzo_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_modificado', '".$estructura["abril_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_modificado', '".$estructura["mayo_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_modificado', '".$estructura["junio_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_modificado', '".$estructura["julio_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_modificado', '".$estructura["agosto_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_modificado', '".$estructura["septiembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_modificado', '".$estructura["octubre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_modificado', '".$estructura["noviembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_modificado', '".$estructura["diciembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_saldo', '".$estructura["enero_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_saldo', '".$estructura["febrero_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_saldo', '".$estructura["marzo_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_saldo', '".$estructura["abril_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_saldo', '".$estructura["mayo_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_saldo', '".$estructura["junio_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_saldo', '".$estructura["julio_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_saldo', '".$estructura["agosto_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_saldo', '".$estructura["septiembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_saldo', '".$estructura["octubre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_saldo', '".$estructura["noviembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_saldo', '".$estructura["diciembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'precomprometido_anual', '".$estructura["precomprometido_anual"]."')
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(nivel, 'partida' as char) = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));

        if($resultado_query_gasto) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_partida_compromiso($estructura) {
        $query_gasto = "UPDATE cat_niveles SET nivel=COLUMN_ADD(nivel, 'enero_precompromiso', '".$estructura["enero_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_precompromiso', '".$estructura["febrero_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_precompromiso', '".$estructura["marzo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_precompromiso', '".$estructura["abril_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_precompromiso', '".$estructura["mayo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_precompromiso', '".$estructura["junio_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_precompromiso', '".$estructura["julio_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_precompromiso', '".$estructura["agosto_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_precompromiso', '".$estructura["septiembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_precompromiso', '".$estructura["octubre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_precompromiso', '".$estructura["noviembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_precompromiso', '".$estructura["diciembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_saldo_precompromiso', '".$estructura["enero_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_saldo_precompromiso', '".$estructura["febrero_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_saldo_precompromiso', '".$estructura["marzo_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_saldo_precompromiso', '".$estructura["abril_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_saldo_precompromiso', '".$estructura["mayo_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_saldo_precompromiso', '".$estructura["junio_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_saldo_precompromiso', '".$estructura["julio_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_saldo_precompromiso', '".$estructura["agosto_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_saldo_precompromiso', '".$estructura["septiembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_saldo_precompromiso', '".$estructura["octubre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_saldo_precompromiso', '".$estructura["noviembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_saldo_precompromiso', '".$estructura["diciembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_modificado', '".$estructura["enero_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_modificado', '".$estructura["febrero_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_modificado', '".$estructura["marzo_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_modificado', '".$estructura["abril_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_modificado', '".$estructura["mayo_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_modificado', '".$estructura["junio_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_modificado', '".$estructura["julio_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_modificado', '".$estructura["agosto_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_modificado', '".$estructura["septiembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_modificado', '".$estructura["octubre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_modificado', '".$estructura["noviembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_modificado', '".$estructura["diciembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_saldo', '".$estructura["enero_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_saldo', '".$estructura["febrero_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_saldo', '".$estructura["marzo_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_saldo', '".$estructura["abril_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_saldo', '".$estructura["mayo_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_saldo', '".$estructura["junio_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_saldo', '".$estructura["julio_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_saldo', '".$estructura["agosto_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_saldo', '".$estructura["septiembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_saldo', '".$estructura["octubre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_saldo', '".$estructura["noviembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_saldo', '".$estructura["diciembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_compromiso', '".$estructura["enero_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_compromiso', '".$estructura["febrero_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_compromiso', '".$estructura["marzo_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_compromiso', '".$estructura["abril_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_compromiso', '".$estructura["mayo_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_compromiso', '".$estructura["junio_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_compromiso', '".$estructura["julio_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_compromiso', '".$estructura["agosto_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_compromiso', '".$estructura["septiembre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_compromiso', '".$estructura["octubre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_compromiso', '".$estructura["noviembre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_compromiso', '".$estructura["diciembre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'precomprometido_anual', '".$estructura["precomprometido_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'total_anual', '".$estructura["total_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'modificado_anual', '".$estructura["modificado_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'comprometido_anual', '".$estructura["comprometido_anual"]."')
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(nivel, 'partida' as char) = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));

        if($resultado_query_gasto) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_partida_total($estructura) {
        $query_gasto = "UPDATE cat_niveles SET nivel=COLUMN_ADD(nivel, 'enero_precompromiso', '".$estructura["enero_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_precompromiso', '".$estructura["febrero_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_precompromiso', '".$estructura["marzo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_precompromiso', '".$estructura["abril_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_precompromiso', '".$estructura["mayo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_precompromiso', '".$estructura["junio_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_precompromiso', '".$estructura["julio_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_precompromiso', '".$estructura["agosto_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_precompromiso', '".$estructura["septiembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_precompromiso', '".$estructura["octubre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_precompromiso', '".$estructura["noviembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_precompromiso', '".$estructura["diciembre_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_saldo', '".$estructura["enero_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_saldo', '".$estructura["febrero_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_saldo', '".$estructura["marzo_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_saldo', '".$estructura["abril_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_saldo', '".$estructura["mayo_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_saldo', '".$estructura["junio_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_saldo', '".$estructura["julio_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_saldo', '".$estructura["agosto_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_saldo', '".$estructura["septiembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_saldo', '".$estructura["octubre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_saldo', '".$estructura["noviembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_saldo', '".$estructura["diciembre_saldo"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_saldo_precompromiso', '".$estructura["enero_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_saldo_precompromiso', '".$estructura["febrero_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_saldo_precompromiso', '".$estructura["marzo_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_saldo_precompromiso', '".$estructura["abril_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_saldo_precompromiso', '".$estructura["mayo_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_saldo_precompromiso', '".$estructura["junio_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_saldo_precompromiso', '".$estructura["julio_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_saldo_precompromiso', '".$estructura["agosto_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_saldo_precompromiso', '".$estructura["septiembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_saldo_precompromiso', '".$estructura["octubre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_saldo_precompromiso', '".$estructura["noviembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_saldo_precompromiso', '".$estructura["diciembre_saldo_precompromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_modificado', '".$estructura["enero_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_modificado', '".$estructura["febrero_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_modificado', '".$estructura["marzo_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_modificado', '".$estructura["abril_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_modificado', '".$estructura["mayo_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_modificado', '".$estructura["junio_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_modificado', '".$estructura["julio_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_modificado', '".$estructura["agosto_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_modificado', '".$estructura["septiembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_modificado', '".$estructura["octubre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_modificado', '".$estructura["noviembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_modificado', '".$estructura["diciembre_modificado"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_compromiso', '".$estructura["enero_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_compromiso', '".$estructura["febrero_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_compromiso', '".$estructura["marzo_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_compromiso', '".$estructura["abril_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_compromiso', '".$estructura["mayo_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_compromiso', '".$estructura["junio_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_compromiso', '".$estructura["julio_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_compromiso', '".$estructura["agosto_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_compromiso', '".$estructura["septiembre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_compromiso', '".$estructura["octubre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_compromiso', '".$estructura["noviembre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_compromiso', '".$estructura["diciembre_compromiso"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_devengado', '".$estructura["enero_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_devengado', '".$estructura["febrero_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_devengado', '".$estructura["marzo_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_devengado', '".$estructura["abril_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_devengado', '".$estructura["mayo_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_devengado', '".$estructura["junio_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_devengado', '".$estructura["julio_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_devengado', '".$estructura["agosto_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_devengado', '".$estructura["septiembre_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_devengado', '".$estructura["octubre_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_devengado', '".$estructura["noviembre_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_devengado', '".$estructura["diciembre_devengado"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_ejercido', '".$estructura["enero_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_ejercido', '".$estructura["febrero_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_ejercido', '".$estructura["marzo_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_ejercido', '".$estructura["abril_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_ejercido', '".$estructura["mayo_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_ejercido', '".$estructura["junio_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_ejercido', '".$estructura["julio_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_ejercido', '".$estructura["agosto_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_ejercido', '".$estructura["septiembre_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_ejercido', '".$estructura["octubre_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_ejercido', '".$estructura["noviembre_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_ejercido', '".$estructura["diciembre_ejercido"]."'),
                            nivel=COLUMN_ADD(nivel, 'enero_pagado', '".$estructura["enero_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'febrero_pagado', '".$estructura["febrero_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'marzo_pagado', '".$estructura["marzo_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'abril_pagado', '".$estructura["abril_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'mayo_pagado', '".$estructura["mayo_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'junio_pagado', '".$estructura["junio_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'julio_pagado', '".$estructura["julio_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'agosto_pagado', '".$estructura["agosto_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'septiembre_pagado', '".$estructura["septiembre_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'octubre_pagado', '".$estructura["octubre_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'noviembre_pagado', '".$estructura["noviembre_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'diciembre_pagado', '".$estructura["diciembre_pagado"]."'),
                            nivel=COLUMN_ADD(nivel, 'total_anual', '".$estructura["total_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'comprometido_anual', '".$estructura["comprometido_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'modificado_anual', '".$estructura["modificado_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'precomprometido_anual', '".$estructura["precomprometido_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'devengado_anual', '".$estructura["devengado_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'ejercido_anual', '".$estructura["ejercido_anual"]."'),
                            nivel=COLUMN_ADD(nivel, 'pagado_anual', '".$estructura["pagado_anual"]."')
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                            AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                            AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                            AND COLUMN_GET(nivel, 'concepto' as char) = ?
                            AND COLUMN_GET(nivel, 'partida' as char) = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));

        if($resultado_query_gasto) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_presupuesto_tomado_precompromiso($sql_precompro) {

        $query = $this->db->query($sql_precompro);
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }


    /**
     * Aqui comienza la seccion de compromiso
     */

    function ultimo_compromiso() {
        $sql = "SELECT numero_compromiso as ultimo FROM mov_compromiso_caratula ORDER BY numero_compromiso DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function ultimo_tipoimpresion($datos) {
        $sql = "SELECT num_impresion as ultimo FROM mov_compromiso_caratula WHERE tipo_impresion = ? ORDER BY num_impresion DESC LIMIT 1;";
        $query = $this->db->query($sql, array($datos["tipo"]));
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarcompromiso($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'numero_compromiso' => $ultimo,
            'estatus' => "espera",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function apartartipoimpresion($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'num_impresion' => $ultimo,
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_precompromisoCompromiso() {
        $sql = "SELECT * FROM mov_precompromiso_caratula WHERE enfirme = 1 AND firma1 = 1 AND firma2 = 1 AND firma3 = 1 AND cancelada = 0 AND solicitar_terminar != 1 AND terminado != 1 AND total > total_compromiso ORDER BY numero_pre DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_Caratula_Precompromiso_Compromiso($precompromiso) {
        $sql = "SELECT * FROM mov_precompromiso_caratula WHERE numero_pre = ?;";
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->row();
        return $result;
    }

    function copiarDatosPrecompromiso($precompromiso, $compromiso) {

//        Se inicializa el estatus del precompromiso
        $estatus = "";

//        Se borra el detalle del compromiso en caso de que no se haya hecho ya
        $this->db->delete('mov_compromiso_detalle', array('numero_compromiso' => $compromiso));

//        Se toma el la información de la caratula del compromiso para poder determinar la fecha de donde se va a tomar el presupuesto
        $sql_caratula = "SELECT * FROM mov_precompromiso_caratula WHERE numero_pre = ?;";
        $query_caratula = $this->db->query($sql_caratula, array($precompromiso));
        $result_caratula = $query_caratula->row();

//        Se toma el detalle del precompromiso que se va a copiar
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_precompromiso_detalle WHERE numero_pre = ?;";
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->result();

        foreach($result as $fila) {
//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder trabajar con la información
            $estructura = json_decode($fila->estructura);

//            Se toma el mes del presupuesto a calcular
            $mes_actual = $this->utilerias->convertirFechaAMes($result_caratula->fecha_emision);

//            Se tiene que comprobar si hay dinero para hacer el compromiso
            $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) AS saldo_mes,
                                    COLUMN_GET(nivel, '".$mes_actual."_compromiso' as char) AS mes_compromiso,
                                    COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual
                                    FROM cat_niveles WHERE id_niveles = ?";

//            Se llama a la función que se encarga de tomar los datos de la partida para revisar si hay dinero disponible
            $resultado_dinero = $this->ciclo_model->revisarDinero($fila->id_nivel, $query_revisar_dinero);

//            Se suma el importe del detalle del precompromiso al total del dinero comprometido del mes seleccionado
            $check = $resultado_dinero->mes_compromiso + $fila->importe;

//            Se hace la comprobación para poder determinar si el dinero a comprometer es mayor a 0 y menor o igual al saldo del mes
            if($resultado_dinero->saldo_mes > 0 && $check <= $resultado_dinero->saldo_mes) {
//                Se pasa un estatus de exito
                $estatus .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos de la partida '.$estructura->partida.' del mes de '.ucfirst($mes_actual).' copiados correctamente.</div>';
            } else {
//                Se pasa un estatus de error
                $estatus .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Presupuesto mensual insuficiente en la partida '.$estructura->partida.' dentro del mes '.ucfirst($mes_actual).'</div>';
            }
//            Se el resultado que sea, se inserta el detalle la table del detalle del compromiso
            $sql = "INSERT INTO mov_compromiso_detalle (id_precompromiso_detalle, numero_compromiso, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, year, especificaciones, presupuesto_tomado, nivel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE('enero', 0.0, 'febrero', 0.0, 'marzo', 0.0, 'abril', 0.0, 'mayo', 0.0, 'junio', 0.0, 'julio', 0.0, 'agosto', 0.0, 'septiembre', 0.0, 'octubre', 0.0, 'noviembre', 0.0, 'diciembre', 0.0 ), ?);";
            $resultado = $this->db->query($sql, array($fila->id_precompromiso_detalle, $compromiso, $fila->id_nivel, $fila->gasto, $fila->unidad_medida, $fila->cantidad, $fila->p_unitario, $fila->subtotal, $fila->iva, $fila->importe, $fila->titulo, $fila->year, $fila->especificaciones, $fila->nivel));

        }

//        Se revisa si el precompromiso tiene viáticos asociados
        $this->db->select('numero_precompromiso')->from('mov_tabla_lugares_periodos')->where('numero_precompromiso', $precompromiso)->limit(1);
        $query = $this->db->get();
        if($query->row()) {
            $datos_actualizar = array(
                'numero_compromiso' => $compromiso,
            );

            $this->db->where('numero_precompromiso', $precompromiso);
            $this->db->update('mov_tabla_lugares_periodos', $datos_actualizar);
        }

        return $estatus;

    }

    function copiarDatosPrecompromisoAutomatico($precompromiso, $compromiso, $datos_factura) {
        
        $this->db->cache_off();

//        Se inicializa el estatus del precompromiso
        $estatus = "";

//        Se borra el detalle del compromiso en caso de que no se haya hecho ya
        $this->db->delete('mov_compromiso_detalle', array('numero_compromiso' => $compromiso));

//        Se toma el la información de la caratula del compromiso para poder determinar la fecha de donde se va a tomar el presupuesto
        $sql_caratula = "SELECT * FROM mov_precompromiso_caratula WHERE numero_pre = ?;";
        $query_caratula = $this->db->query($sql_caratula, array($precompromiso));
        $result_caratula = $query_caratula->row();

//        Se toma el detalle del precompromiso que se va a copiar
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_precompromiso_detalle WHERE numero_pre = ?;";
        $query = $this->db->query($sql, array($precompromiso));
        $result = $query->result();

        foreach($result as $fila) {
//            Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder trabajar con la información
            $estructura = json_decode($fila->estructura);

//            Se toma el mes del presupuesto a calcular
            $mes_actual = $this->utilerias->convertirFechaAMes($result_caratula->fecha_emision);

//            Se tiene que comprobar si hay dinero para hacer el compromiso
            $query_revisar_dinero = "SELECT COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) AS saldo_mes,
                                    COLUMN_GET(nivel, '".$mes_actual."_compromiso' as char) AS mes_compromiso,
                                    COLUMN_GET(nivel, 'comprometido_anual' as char) AS comprometido_anual
                                    FROM cat_niveles WHERE id_niveles = ?";

//            Se llama a la función que se encarga de tomar los datos de la partida para revisar si hay dinero disponible
            $resultado_dinero = $this->ciclo_model->revisarDinero($fila->id_nivel, $query_revisar_dinero);

//            Se suma el importe del detalle del precompromiso al total del dinero comprometido del mes seleccionado
            $check = $resultado_dinero->mes_compromiso + $datos_factura["total"];

//            Se hace la comprobación para poder determinar si el dinero a comprometer es mayor a 0 y menor o igual al saldo del mes
            if($resultado_dinero->saldo_mes > 0 && $check <= $resultado_dinero->saldo_mes) {
//                Se pasa un estatus de exito
                $estatus .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Datos de la partida '.$estructura->partida.' del mes de '.ucfirst($mes_actual).' copiados correctamente.</div>';
            } else {
//                Se pasa un estatus de error
                $estatus .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Presupuesto mensual insuficiente en la partida '.$estructura->partida.' dentro del mes '.ucfirst($mes_actual).'</div>';
            }
//            Se el resultado que sea, se inserta el detalle la table del detalle del compromiso
            $sql = "INSERT INTO mov_compromiso_detalle (numero_compromiso, id_nivel, gasto, unidad_medida, cantidad, p_unitario, subtotal, iva, importe, titulo, year, especificaciones, presupuesto_tomado, nivel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, COLUMN_CREATE('enero', 0.0, 'febrero', 0.0, 'marzo', 0.0, 'abril', 0.0, 'mayo', 0.0, 'junio', 0.0, 'julio', 0.0, 'agosto', 0.0, 'septiembre', 0.0, 'octubre', 0.0, 'noviembre', 0.0, 'diciembre', 0.0 ), ?);";
            $resultado = $this->db->query($sql, array($compromiso, $fila->id_nivel, $fila->gasto, $fila->unidad_medida, $datos_factura["conceptos"]["cantidad0"], $datos_factura["conceptos"]["importe0"], $datos_factura["conceptos"]["importe0"] * $datos_factura["conceptos"]["cantidad0"], $datos_factura["valor_iva"], $datos_factura["total"], $fila->titulo, $fila->year, $fila->especificaciones, $fila->nivel));

        }

//        Se revisa si el precompromiso tiene viáticos asociados
        $this->db->select('numero_precompromiso')->from('mov_tabla_lugares_periodos')->where('numero_precompromiso', $precompromiso)->limit(1);
        $query = $this->db->get();
        if($query->row()) {
            $datos_actualizar = array(
                'numero_compromiso' => $compromiso,
            );

            $this->db->where('numero_precompromiso', $precompromiso);
            $this->db->update('mov_tabla_lugares_periodos', $datos_actualizar);
        }

        return $estatus;

    }

    function actualizar_saldo_compromiso($datos) {
        
        // Se inicializa la transacción
        $this->db->trans_begin();

//        Se inicializa la variable de la respuesta de la inserción de datos
        $respuesta = array(
            "mensaje" => ''
        );

//        Se toman los datos del detalle del compromiso
        $sql = "SELECT *, COLUMN_JSON(nivel) as estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($datos["ultimo_compromiso"]));
        $result = $query->result();

        // Se inicia un ciclo para poder recorrer todos los elementos del compromiso
        foreach($result as $fila) {

            $query_devolver_presupuesto = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_ADD( presupuesto_tomado,";

            $query_devolver_presupuesto_compromiso = "UPDATE mov_compromiso_detalle SET presupuesto_tomado = COLUMN_ADD( presupuesto_tomado,";

            // Se toma el mes de donde se quiere comprometer el dinero
            $fecha_emitido = $this->utilerias->convertirFechaAMes($datos["fecha_solicitud"]);

            // Se toma la estructura del detalle y se convierte de JSON a un arreglo para poder manejar la información
            $estructura = json_decode($fila->estructura, TRUE);

            // Se toma la estructura completa
            $query_revisar_dinero = "SELECT COLUMN_JSON(nivel) AS estructura
                                     FROM cat_niveles
                                     WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                     AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                     AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                     AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            // Se tiene que comprobar si hay dinero para hacer el compromiso
            $resultado_dinero = $this->revisarDineroEstructuraCompleta(array(
                0 => $estructura["fuente_de_financiamiento"],
                1 => $estructura["programa_de_financiamiento"],
                2 => $estructura["centro_de_costos"],
                3 => $estructura["capitulo"],
                4 => $estructura["concepto"],
                5 => $estructura["partida"],), $query_revisar_dinero);

            // Se guarda la partida en una variable
            $estructura_partida = json_decode($resultado_dinero["estructura"], TRUE);

            // Se suma el importe al comprometido anual
            $estructura_partida["comprometido_anual"] += $fila->importe;

            // Se suma el importe al mes comprometido
            $estructura_partida[$fecha_emitido."_compromiso"] += $fila->importe;

            // Se resta el importe al total anual
            $estructura_partida["total_anual"] -= $fila->importe;

            // Se resta el importe al disponible mensual
            $estructura_partida[$fecha_emitido."_saldo"] -= $fila->importe;

            // Si existe un ID  de precompromiso, entra en este ciclo
            if($fila->id_precompromiso_detalle != 0) {

                // Se genera el query que toma el presupuesto tomado del ID detalle del precompromiso
                $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado', FALSE)
                            ->from('mov_precompromiso_detalle')
                            ->where('id_precompromiso_detalle', $fila->id_precompromiso_detalle);
                $query_precompromiso_detalle = $this->db->get();
                $resultado_precompromiso_detalle = $query_precompromiso_detalle->row_array();

                // Se guarda el resultado del query en una variable y se transforma el JSON en un arreglo
                $presupuesto_tomado_precompromiso = json_decode($resultado_precompromiso_detalle["presupuesto_tomado"], TRUE);

                for ($i = 1; $i <= 12; $i++) {
                    // Se toma el año en curso
                    $year = date("Y");

                    // Se transforma el numero del mes a texto
                    $fecha_interna = $this->utilerias->convertirFechaAMes($year."-".$i."-01");

                    if($presupuesto_tomado_precompromiso[$fecha_interna] > 0 && $fecha_interna == $fecha_emitido) {
                        $estructura_partida[$fecha_interna."_precompromiso"] -= $fila->importe;

                        $estructura_partida["precomprometido_anual"] -= $fila->importe;

                        $presupuesto_tomado_precompromiso[$fecha_interna] -= $fila->importe;

                        $query_devolver_presupuesto .= " '".$fecha_interna."', '".$presupuesto_tomado_precompromiso[$fecha_interna]."',";

                        $query_devolver_presupuesto_compromiso .= " '".$fecha_interna."', '".$fila->importe."',";

                    } else {

                        $query_devolver_presupuesto .= " '".$fecha_interna."', ".(!isset($presupuesto_tomado_precompromiso[$fecha_interna]) || $presupuesto_tomado_precompromiso[$fecha_interna] == NULL ? 0.00 : ($presupuesto_tomado_precompromiso[$fecha_interna])).",";

                        $query_devolver_presupuesto_compromiso .= " '".$fecha_interna."', 0.0,";
                    }

                }

                // Se quita la última coma del query
                $query_devolver_presupuesto = substr($query_devolver_presupuesto, 0, -1);

                $query_devolver_presupuesto .= " ) WHERE id_precompromiso_detalle = ".$fila->id_precompromiso_detalle.";";

                $query_devolver_presupuesto_compromiso = substr($query_devolver_presupuesto_compromiso, 0, -1);

                $query_devolver_presupuesto_compromiso .= " ) WHERE id_compromiso_detalle = ".$fila->id_compromiso_detalle.";";

                // Se actualiza el campo del presupuesto tomado
                $this->ciclo_model->actualizar_presupuesto_tomado_precompromiso($query_devolver_presupuesto);

                // Se actualiza el campo del presupuesto tomado
                $this->ciclo_model->actualizar_presupuesto_tomado_precompromiso($query_devolver_presupuesto_compromiso);

            } else {

                // Se resta el importe al saldo disponible del mes
                $estructura_partida[$fecha_emitido."_saldo_precompromiso"] -= $fila->importe;
            }

            // Se actualiza la partida
            $this->ciclo_model->actualizar_partida_compromiso($estructura_partida);

        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function datos_compromisoCaratula(){
        $sql = "SELECT * FROM mov_compromiso_caratula ORDER BY numero_compromiso DESC ;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_compromisoCaratulaEstatus($sql){
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_compromisoDetalle($compromiso, $sql) {
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->result();
        return $result;
    }

    function cancelarcompromisoCaratula($compromiso, $query) {

        $query = $this->db->query($query, array(
            date("Y-m-d"), $compromiso,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function existeDetalleCompromiso($compromiso) {
        $this->db->where('numero_compromiso', $compromiso);
        $query = $this->db->get('mov_compromiso_detalle');
        $result = $query->row();
        return $result;
    }

    function borrarDetallecompromiso($compromiso) {
        $this->db->where('id_compromiso_detalle', $compromiso);
        $query = $this->db->delete('mov_compromiso_detalle');

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }


    }

    function editarDetallecompromiso($datos) {
        $this->db->trans_start();

        $data = array(
            'cantidad' => $datos["cantidad"],
            'p_unitario' => $datos["precio"],
            'subtotal' => $datos["subtotal"],
            'iva' => $datos["iva"],
            'importe' => $datos["importe"],
        );

        $this->db->where('id_compromiso_detalle', $datos["id_compromiso"]);
        $this->db->update('mov_compromiso_detalle', $data);

        $this->db->trans_complete();
        
        return '';

    }

    function revisarDineroCompromiso($id_nivel, $query) {
        $query = $this->db->query($query, array($id_nivel));
        $result = $query->row();
        return $result;
    }

    function insertar_caratula_compromiso($datos) {

        if(!$datos["total_hidden"]) {
            $datos["total_hidden"] = 0;
        }

        if(!$datos["fecha_entrega"]) {
            $datos["fecha_entrega"] = "0000-00-00";
        }

        if(!$datos["id_tipo_impresion"]) {
            $datos["id_tipo_impresion"] = 0;
        }

        $data = array(
            'num_precompromiso' => $datos["no_precompromiso"],
            'id_proveedor' => $datos["id_proveedor"],
            'nombre_proveedor' => $datos["proveedor"],
            'lugar_entrega' => $datos["lugar_entrega"],
            'condicion_entrega' => $datos["condicion_entrega"],
            'total' => $datos["total_hidden"],
            'tipo_compromiso' => $datos["tipo_compromiso"],
            'tipo_gasto' => $datos["tipo_radio"],
            'enfirme' => $datos["check_firme"],
            'firma1' => $datos["firma1"],
            'firma2' => $datos["firma2"],
            'firma3' => $datos["firma3"],
            'fecha_emision' => $datos["fecha_solicitud"],
            'fecha_autoriza' => $datos["fecha_autoriza"],
            'fecha_programada' => $datos["fecha_entrega"],
            'estatus' => $datos["estatus"],
            'cancelada' => 0,
            'fecha_cancelada' => 0,
            'concepto_especifico' => $datos["concepto_especifico"],
            'descripcion_general' => $datos["descripcion_general"],
            'tipo_impresion' => $datos["tipo_impresion"],
            'num_impresion' => $datos["id_tipo_impresion"],
            "id_persona" => $datos["id_persona"],
            "nombre_completo" => $datos["nombre_persona"],
            "puesto" => $datos["puesto"],
            "clave" => $datos["clave"],
            "area" => $datos["area"],
            "no_empleado" => $datos["no_empleado"],
            "forma_pago" => $datos["forma_pago"],
            "transporte" => $datos["transporte"],
            "grupo_jerarquico" => $datos["grupo_jerarquico"],
            "zona_marginada" => $datos["zona_marginada_check"],
            "zona_mas_economica" => $datos["zona_mas_economica_check"],
            "zona_menos_economica" => $datos["zona_menos_economica_check"],
        );

        $this->db->where('numero_compromiso', $datos["ultimo_compromiso"]);
        $query = $this->db->update('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_viaticos_compromiso($datos) {

        $data = array(
            'numero_compromiso' => $datos['ultimo_compromiso'],
            'lugar' => $datos['lugar_comision'],
            'periodo_inicio' => $datos['fecha_inicio'],
            'periodo_fin' => $datos['fecha_termina'],
            'cuota_diaria' => $datos['cuota_diaria'],
            'dias' => $datos['dias'],
            'importe' => $datos['importe'],
        );

        $query = $this->db->insert('mov_tabla_lugares_periodos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_compromisoArchivo($datos, $ultimo) {
        foreach ($datos as $key => $value) {
            if($datos[$key]) {
                $datos[$key] = trim($value);
            } else {
                $datos[$key] = '';
            }
        }

        $id = $this->tank_auth->get_user_id();
        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        /** En esta parte se toma el nomnbre del proveedor */

        $nombre_proveedor = array(
            "nombre" => ''
        );

        if(isset($datos["B"])){
            $query_proveedor = "SELECT nombre FROM cat_proveedores WHERE clave_prov = ?;";
            $resultado_query_proveedor = $this->db->query($query_proveedor, array($datos["B"]));
            $proveedor = $resultado_query_proveedor->row_array();

            if(!$proveedor) {
                $DB1 = $this->load->database('pedidos', TRUE);

                $sql_caratula = "SELECT nomedi AS nombre
                                FROM editor
                                WHERE cveedi = ?";
                $query_caratula = $DB1->query($sql_caratula, array($datos["B"]));
                $proveedor = $query_caratula->row_array();
                if(!$proveedor) {
                    $nombre_proveedor["nombre"] = '';
                } else {
                    $nombre_proveedor["nombre"] = $proveedor["nombre"];
                }
            } else {
                $nombre_proveedor["nombre"] = $proveedor["nombre"];
            }
        } else {
            $datos["B"] = "";
            $nombre_proveedor["nombre"] = '';
        }

//        $this->debugeo->imprimir_pre($nombre_proveedor);

        if(strtolower(trim($datos["E"])) == strtolower("Gasto Corriente")) {
            $datos["E"] = 1;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Gasto de Capital")) {
            $datos["E"] = 2;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Amortización de la cuenta y disminución de pasivos")) {
            $datos["E"] = 3;
        }

        if(isset($datos["Q"]) && strtolower($datos["Q"]) == strtolower("Si")){
            $datos["Q"] = 1;
        } else{
            $datos["Q"] = 0;
        }

        if(isset($datos["R"]) && strtolower($datos["R"]) == strtolower("Si")){
            $datos["R"] = 1;
        } else{
            $datos["R"] = 0;
        }

        if(isset($datos["S"]) && strtolower($datos["S"]) == strtolower("Si")){
            $datos["S"] = 1;
        } else{
            $datos["S"] = 0;
        }

        $fecha = ($datos['F'] - 25569) * 86400;
        $fecha_programada = date('Y-m-d', $fecha);

        if(isset($datos["H"])) {
            $sql = "SELECT num_impresion as ultimo FROM mov_compromiso_caratula WHERE tipo_impresion = ? ORDER BY num_impresion DESC LIMIT 1;";
            $query = $this->db->query($sql, array($datos["H"]));
            $num_impresion = $query->row();
            if(!$num_impresion) {
                $datos["H"] = 0;
            } else {
                $datos["H"] = $num_impresion->ultimo;
            }
        } else {
            $datos["H"] = 0;
        }

        if(array_key_exists('D', $datos) == FALSE || $datos["D"] == NULL || $datos["D"] == '') {
            $datos["D"] = '';
        }

        if(array_key_exists('G', $datos) == FALSE || $datos["G"] == NULL || $datos["G"] == '') {
            $datos["G"] = '';
        }

        if(array_key_exists('I', $datos) == FALSE || $datos["I"] == NULL || $datos["I"] == '') {
            $datos["I"] = '';
        }

        if(array_key_exists('J', $datos) == FALSE || $datos["J"] == NULL || $datos["J"] == '') {
            $datos["J"] = 0;
        }

        if(array_key_exists('K', $datos) == FALSE || $datos["K"] == NULL || $datos["K"] == '') {
            $datos["K"] = '';
        }

        if(array_key_exists('L', $datos) == FALSE || $datos["L"] == NULL || $datos["L"] == '') {
            $datos["L"] = '';
        }

        if(array_key_exists('M', $datos) == FALSE || $datos["M"] == NULL || $datos["M"] == '') {
            $datos["M"] = '';
        }

        if(array_key_exists('N', $datos) == FALSE || $datos["N"] == NULL || $datos["N"] == '') {
            $datos["N"] = '';
        }

        if(array_key_exists('O', $datos) == FALSE || $datos["O"] == NULL || $datos["O"] == '') {
            $datos["O"] = '';
        }

        if(array_key_exists('P', $datos) == FALSE || $datos["P"] == NULL || $datos["P"] == '') {
            $datos["P"] = '';
        }

        if(array_key_exists('Q', $datos) == FALSE || $datos["Q"] == NULL || $datos["Q"] == '') {
            $datos["Q"] = 0;
        }

        if(array_key_exists('R', $datos) == FALSE || $datos["R"] == NULL || $datos["R"] == '') {
            $datos["R"] = 0;
        }

        if(array_key_exists('S', $datos) == FALSE || $datos["S"] == NULL || $datos["S"] == '') {
            $datos["S"] = 0;
        }

        $data = array(
            'id_proveedor' => $datos["B"],
            'numero_compromiso' => $ultimo,
            'nombre_proveedor' => $nombre_proveedor["nombre"],
            'lugar_entrega' => $datos["C"],
            'condicion_entrega' => $datos["D"],
            'total' => 0,
            'tipo_compromiso' => $datos["A"],
            'tipo_gasto' => $datos["E"],
            'enfirme' => 0,
            'firma1' => 0,
            'firma2' => 0,
            'firma3' => 0,
            'fecha_emision' => date("Y-m-d"),
            'fecha_autoriza' => "0000-00-00",
            'fecha_programada' => $fecha_programada,
            'estatus' => "espera",
            'cancelada' => 0,
            'fecha_cancelada' => 0,
            'id_usuario' => $id,
            'creado_por' => $nombre_completo,
            'descripcion_general' => $datos["G"],
            'tipo_impresion' => $datos["H"],
            'num_impresion' => $datos["H"],
            'nombre_completo' => $datos["I"],
            'no_empleado' => $datos["J"],
            'puesto' => $datos["J"],
            'area' => $datos["L"],
            'clave' => substr($datos["M"], 0, 3),
            'forma_pago' => $datos["N"],
            'transporte' => $datos["O"],
            'grupo_jerarquico' => $datos["P"],
            'zona_marginada' => $datos["Q"],
            'zona_mas_economica' => $datos["R"],
            'zona_menos_economica' => $datos["S"],
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_compromisoArchivoMasivo($datos, $ultimo) {
        foreach ($datos as $key => $value) {
            if($datos[$key]) {
                $datos[$key] = trim($value);
            } else {
                $datos[$key] = '';
            }
        }

        $id = $this->tank_auth->get_user_id();
        /** En esta parte se toma el nombre del usuario */
        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        /** En esta parte se toma el nomnbre del proveedor */

        $nombre_proveedor = array(
            "nombre" => ''
        );

        if(isset($datos["B"])){
            $query_proveedor = "SELECT nombre FROM cat_proveedores WHERE clave_prov = ?;";
            $resultado_query_proveedor = $this->db->query($query_proveedor, array($datos["B"]));
            $proveedor = $resultado_query_proveedor->row_array();

            if(!$proveedor) {
                $DB1 = $this->load->database('pedidos', TRUE);

                $sql_caratula = "SELECT nomedi AS nombre
                                FROM editor
                                WHERE cveedi = ?";
                $query_caratula = $DB1->query($sql_caratula, array($datos["B"]));
                $proveedor = $query_caratula->row_array();
                if(!$proveedor) {
                    $nombre_proveedor["nombre"] = '';
                } else {
                    $nombre_proveedor["nombre"] = utf8_encode($proveedor["nombre"]);
                }
            } else {
                $nombre_proveedor["nombre"] = $proveedor["nombre"];
            }
        } else {
            $nombre_proveedor["nombre"] = '';
        }

//        $this->debugeo->imprimir_pre($nombre_proveedor);

        if(strtolower(trim($datos["E"])) == strtolower("Gasto Corriente")) {
            $datos["E"] = 1;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Gasto de Capital")) {
            $datos["E"] = 2;
        } elseif(strtolower(trim($datos["E"])) == strtolower("Amortización de la cuenta y disminución de pasivos")) {
            $datos["E"] = 3;
        }

        if(isset($datos["Q"]) && strtolower($datos["Q"]) == strtolower("Si")){
            $datos["Q"] = 1;
        } else{
            $datos["Q"] = 0;
        }

        if(isset($datos["R"]) && strtolower($datos["R"]) == strtolower("Si")){
            $datos["R"] = 1;
        } else{
            $datos["R"] = 0;
        }

        if(isset($datos["S"]) && strtolower($datos["S"]) == strtolower("Si")){
            $datos["S"] = 1;
        } else{
            $datos["S"] = 0;
        }

        if(array_key_exists('D', $datos) == FALSE || $datos["D"] == NULL || $datos["D"] == '') {
            $datos["D"] = '';
        }

        if(array_key_exists('G', $datos) == FALSE || $datos["G"] == NULL || $datos["G"] == '') {
            $datos["G"] = '';
        }

        if(array_key_exists('I', $datos) == FALSE || $datos["I"] == NULL || $datos["I"] == '') {
            $datos["I"] = '';
        }

        if(array_key_exists('J', $datos) == FALSE || $datos["J"] == NULL || $datos["J"] == '') {
            $datos["J"] = 0;
        }

        if(array_key_exists('K', $datos) == FALSE || $datos["K"] == NULL || $datos["K"] == '') {
            $datos["K"] = '';
        }

        if(array_key_exists('L', $datos) == FALSE || $datos["L"] == NULL || $datos["L"] == '') {
            $datos["L"] = '';
        }

        if(array_key_exists('M', $datos) == FALSE || $datos["M"] == NULL || $datos["M"] == '') {
            $datos["M"] = '';
        }

        if(array_key_exists('N', $datos) == FALSE || $datos["N"] == NULL || $datos["N"] == '') {
            $datos["N"] = '';
        }

        if(array_key_exists('O', $datos) == FALSE || $datos["O"] == NULL || $datos["O"] == '') {
            $datos["O"] = '';
        }

        if(array_key_exists('P', $datos) == FALSE || $datos["P"] == NULL || $datos["P"] == '') {
            $datos["P"] = '';
        }

        if(array_key_exists('Q', $datos) == FALSE || $datos["Q"] == NULL || $datos["Q"] == '') {
            $datos["Q"] = 0;
        }

        if(array_key_exists('R', $datos) == FALSE || $datos["R"] == NULL || $datos["R"] == '') {
            $datos["R"] = 0;
        }

        if(array_key_exists('S', $datos) == FALSE || $datos["S"] == NULL || $datos["S"] == '') {
            $datos["S"] = 0;
        }

        $data = array(
            'id_proveedor' => $datos["B"],
            'numero_compromiso' => $ultimo,
            'num_precompromiso' => 0,
            'nombre_proveedor' => $nombre_proveedor["nombre"],
            'lugar_entrega' => $datos["C"],
            'condicion_entrega' => $datos["D"],
            'total' => 0,
            'tipo_compromiso' => $datos["A"],
            'tipo_gasto' => $datos["E"],
            'enfirme' => 1,
            'firma1' => 1,
            'firma2' => 1,
            'firma3' => 1,
            'fecha_emision' => $datos["F"],
            'fecha_autoriza' => $datos["F"],
            'fecha_programada' => $datos["F"],
            'estatus' => "espera",
            'cancelada' => 0,
            'fecha_cancelada' => 0,
            'id_usuario' => $id,
            'creado_por' => $nombre_completo,
            'descripcion_general' => $datos["G"],
            'tipo_impresion' => $datos["H"],
            'num_impresion' => $datos["numero_impresion"],
            'nombre_completo' => $datos["I"],
            'no_empleado' => $datos["J"],
            'puesto' => $datos["J"],
            'area' => $datos["L"],
            'clave' => substr($datos["M"], 0, 3),
            'forma_pago' => $datos["N"],
            'transporte' => $datos["O"],
            'grupo_jerarquico' => $datos["P"],
            'zona_marginada' => $datos["Q"],
            'zona_mas_economica' => $datos["R"],
            'zona_menos_economica' => $datos["S"],
        );

        $query = $this->db->insert('mov_compromiso_caratula', $data);

        if($query) {
            return $this->db->insert_id();
        }
        else {
            return FALSE;
        }
    }

    function insertar_caratula_compromiso_cancelado($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'num_precompromiso' => $datos["no_precompromiso"],
            'id_proveedor' => $datos["id_proveedor"],
            'nombre_proveedor' => $datos["proveedor"],
            'lugar_entrega' => $datos["lugar_entrega"],
            'condicion_entrega' => $datos["condicion_entrega"],
            'subtotal' => $datos["subtotal_hidden"],
            'iva' => $datos["iva"],
            'total' => $datos["importe"],
            'tipo_compromiso' => $datos["tipo_compromiso"],
            'tipo_gasto' => $datos["tipo_radio"],
            'enfirme' => $datos["check_firme"],
            'firma1' => $datos["firma1"],
            'firma2' => $datos["firma2"],
            'firma3' => $datos["firma3"],
            'fecha_emision' => $datos["fecha_solicitud"],
            'fecha_autoriza' => $datos["fecha_autoriza"],
            'fecha_programada' => $datos["fecha_entrega"],
            'estatus' => $datos["estatus"],
            'creado_por' => $nombre_completo,
            'descripcion_general' => $datos["descripcion_general"],
            'cancelada' => 1,
            'fecha_cancelada' => date("Y-m-d"),
        );

        $this->db->where('numero_compromiso', $datos["ultimo_compromiso"]);
        $query = $this->db->update('mov_compromiso_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_detalle_compromisoArchivo($datos, $query, $ultimo) {
        /** En esta parte se toma el nombre del gasto */
        $query_gasto = "SELECT descripcion FROM cat_conceptos_gasto WHERE articulo = ?;";
        $resultado_query_gasto = $this->db->query($query_gasto, array($datos["G"]));
        $nombre_gasto = $resultado_query_gasto->row();

        $query = $this->db->query($query, array(
            $ultimo,
            $datos["id_nivel"],
            $datos["G"],
            $datos["H"],
            $datos["I"],
            $datos["K"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $nombre_gasto->descripcion,
            date("Y"),
            $datos["L"],
            $datos["A"],
            $datos["B"],
            $datos["C"],
            $datos["D"],
            $datos["E"],
            $datos["F"],
            $nombre_gasto->descripcion));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function get_datos_compromiso_caratula($compromiso, $query) {
        $query = $this->db->query($query, array($compromiso));
        $result = $query->row();
        return $result;
    }

    function get_datos_compromiso_detalle($compromisomiso){
        $query = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($query, array($compromisomiso));
        $result = $query->result();
        return $result;
    }

    function get_fila_precompromiso_detalle_compromiso_detalle($id_precompromiso){
        $query = "SELECT *, COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado FROM mov_precompromiso_detalle WHERE id_precompromiso_detalle = ?;";
        $query = $this->db->query($query, array($id_precompromiso));
        $result = $query->row_array();
        return $result;
    }

    function get_datos_compromiso_lugares($compromiso){
        $query = "SELECT * FROM mov_tabla_lugares_periodos WHERE numero_compromiso = ?;";
        $query = $this->db->query($query, array($compromiso));
        $result = $query->result();
        return $result;
    }

    function insertar_detalle_compromiso($datos, $query) {
        $query = $this->db->query($query, array(
            $datos["ultimo_compromiso"],
            $datos["id_nivel"],
            $datos["gasto"],
            $datos["u_medida"],
            $datos["cantidad"],
            $datos["precio"],
            $datos["subtotal"],
            $datos["iva"],
            $datos["importe"],
            $datos["titulo_gasto"],
            date("Y"),
            $datos["descripcion_detalle"],
            $datos["nivel1"],
            $datos["nivel2"],
            $datos["nivel3"],
            $datos["nivel4"],
            $datos["nivel5"],
            $datos["nivel6"],
            $datos["gasto"]));
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_total_compromiso($query, $mes, $saldo_mes, $anual, $total_anual, $nivel) {
        $query = $this->db->query($query, array(
            $mes,
            $saldo_mes,
            $anual,
            $total_anual,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_total_compromiso_con_precompromiso($query, $compro_mes, $saldo_mes, $precompro_mes, $anual_compro, $anual_precompro, $total_anual, $nivel) {
        $query = $this->db->query($query, array(
            $compro_mes,
            $saldo_mes,
            $precompro_mes,
            $anual_compro,
            $anual_precompro,
            $total_anual,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_saldo_activo_compromiso($query, $cantidad, $nivel) {
        $query = $this->db->query($query, array(
            $cantidad,
            $nivel));

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizarTotalCaratulaCompromiso($id, $total){
        $data = array(
            'total' => $total,
        );

        $this->db->where('id_compromiso_caratula', $id);
        $this->db->update('mov_compromiso_caratula', $data);
    }

    function datosProveedores() {
        $this->db->select('id_proveedores, nombre');
        $query = $this->db->get('cat_proveedores');
        $result = $query->result();
        return $result;
    }

    function tomar_tipo_impresion($datos) {
        $this->db->select('tipo_impresion, num_impresion')->from('mov_compromiso_caratula')->where('tipo_impresion', $datos["tipo"])->where('num_impresion', $datos["numero"]);
        $query = $this->db->get();
        $resultado = $query->row();

        if(!$resultado) {
            return FALSE;
        } else {
            return $resultado->num_impresion;
        }
    }

    function datos_viaticosDetalleCompromiso($compromiso) {
        $this->db->select('id_tabla_lugares_periodos, lugar, periodo_inicio, periodo_fin, cuota_diaria, dias, importe')->from('mov_tabla_lugares_periodos')->where('numero_compromiso', $compromiso);
        $query = $this->db->get();
        return $query->result();
    }

    function tomarDetalleBorrarCompromiso($compromiso) {
        $this->db->select('id_compromiso_detalle, COLUMN_JSON(nivel) AS estructura, importe')->from('mov_compromiso_detalle')->where('numero_compromiso', $compromiso);
        $query = $this->db->get();
        return $query->result_array();
    }

    function datosProveedoresEDUCAL() {
        $DB1 = $this->load->database('pedidos', TRUE);
        $sql = "SELECT cveedi, nomedi FROM editor;";
        $query = $DB1->query($sql);
        return $query->result();
    }

    function checar_contrarecibo_cancelar_compromisos($compromiso){
        $this->db->select('id_contrarecibo_caratula')->from('mov_contrarecibo_caratula')->where('numero_compromiso', $compromiso)->where('enfirme', 1)->where('cancelada', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Aqui empieza la sección del Contra Recibo de Pago
     */

    function ultimo_contrarecibo() {
        $sql = "SELECT id_contrarecibo_caratula as ultimo FROM mov_contrarecibo_caratula ORDER BY id_contrarecibo_caratula DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function apartarContrarecibo($ultimo) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'id_contrarecibo_caratula' => $ultimo,
            'estatus' => "espera",
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
        );

        $query = $this->db->insert('mov_contrarecibo_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function datos_contrareciboCaratula() {
        $sql = "SELECT * FROM mov_contrarecibo_caratula ORDER BY id_contrarecibo_caratula DESC ;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_contrareciboCaratulaEstatus($sql){
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datos_compromisoContrarecibo() {
        $sql = "SELECT * FROM mov_compromiso_caratula WHERE enfirme = 1 AND firma1 = 1 AND firma2 = 1 AND firma3 = 1 AND cancelada = 0 AND usado = 0 ORDER BY numero_compromiso DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function copiarDatosCompromiso($compromiso, $contrarecibo) {
        $this->db->trans_start();
        $this->db->delete('mov_contrarecibo_detalle', array('numero_contrarecibo' => $contrarecibo));

        $sql = "SELECT * FROM mov_compromiso_caratula WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->row();

        $sql = "SELECT * FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result2 = $query->result();

        $subtotal = 0;
        $iva = 0;
        $total = 0;

        foreach ($result2 as $row) {
            $subtotal += $row->subtotal;
            if(is_numeric($row->iva)) {
                $iva += $row->iva;
            }
            $total += $row->importe;
        }

//        $this->debugeo->imprimir_pre($result);

        $sql = "INSERT INTO mov_contrarecibo_detalle (numero_contrarecibo, numero_compromiso, nombre, tipo, id_proveedor, subtotal, iva, total, descripcion ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? );";
        $resultado = $this->db->query($sql, array($contrarecibo, $compromiso, $result->nombre_proveedor, $result->tipo_compromiso, $result->id_proveedor, $subtotal, $iva, $total, $result->descripcion_general));
        $this->db->trans_complete();

        if($resultado) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function datos_Caratula_Compromiso_Contrarecibo($compromiso) {
        $sql = "SELECT * FROM mov_compromiso_caratula WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->row();
        return $result;
    }

    function insertar_caratula_contrarecibo($datos) {
        $sql_check_compromiso = "SELECT id_contrarecibo_caratula as ID FROM mov_contrarecibo_caratula WHERE numero_compromiso = ? AND cancelada != 1;";
        $query_check_compromiso = $this->db->query($sql_check_compromiso, array($datos["num_compromiso"]));
        $result_check_compromiso = $query_check_compromiso->row_array();

        // $this->debugeo->imprimir_pre($result_check_compromiso);

        if(isset($result_check_compromiso["ID"]) && $result_check_compromiso["ID"] != NULL && $result_check_compromiso["ID"] != "") {
            if($result_check_compromiso["ID"] != $datos["contrarecibo"]) {
                return "usado";
            }
        }

        $data = array(
            "usado" => 1
        );

        $this->db->where('numero_compromiso', $datos["num_compromiso"]);
        $this->db->update('mov_compromiso_caratula', $data);

        $informacion_insertar = array(
            'id_proveedor' => $datos["clave_proveedor"],
            'proveedor' => $datos["proveedor"],
            'concepto' => $datos["concepto"],
            'concepto_especifico' => $datos["concepto_especifico"],
            'descripcion' => $datos["descripcion"],
            'documento' => $datos["documento"],
            'fecha_pago' => $datos["fecha_pago"],
            'fecha_emision' => $datos["fecha_solicita"],
            'enfirme' => $datos["firme"],
            'numero_compromiso' => $datos["num_compromiso"],
            'destino' => $datos["tipo"],
            'importe' => $datos["importe"],
            'tipo_documento' => $datos["tipo_documento"],
            'estatus' => $datos["estatus"],
            'pagada' => 0,
            'sifirme' => 0,
            'cancelada' => 0,
            'documentacion_anexa' => $datos["documentacion_anexa"],
            'id_persona' => $datos["id_persona"],
            'nombre_completo' => $datos["nombre_completo"],
            'tipo_impresion' => $datos["tipo_impresion"],
        );

        $this->db->where('id_contrarecibo_caratula', $datos["contrarecibo"]);
        $query = $this->db->update('mov_contrarecibo_caratula', $informacion_insertar);

        if($query) {

            $informacion = array(
                "usado" => 1,
            );
            $this->db->where('numero_compromiso', $datos["num_compromiso"]);
            $query2 = $this->db->update('mov_compromiso_caratula', $informacion);
            if($query2) {
                return "exito";
            }
            else {
                return "error";
            }

        }
        else {
            return "error";
        }
    }

    function get_datos_contrarecibo_caratula($contrarecibo, $query) {
        $query = $this->db->query($query, array($contrarecibo));
        $result = $query->row();
        return $result;
    }

    function get_datos_contrarecibo_detalle($contrarecibo) {
        $query = "SELECT * FROM mov_contrarecibo_detalle WHERE numero_contrarecibo = ?;";
        $query = $this->db->query($query, array($contrarecibo));
        $result = $query->result();
        return $result;
    }

    function get_datos_contrarecibo_compromiso($contrarecibo){
        $query = "SELECT p1.numero_compromiso, p1.numero_contrarecibo,
        p2.numero_compromiso, p2.gasto, p2.titulo,  p2.unidad_medida, p2.cantidad, p2.p_unitario, p2.subtotal, p2.iva, p2.importe
        FROM mov_contrarecibo_detalle p1
        INNER JOIN mov_compromiso_detalle p2 ON p2.numero_compromiso = p1.numero_compromiso
        WHERE numero_contrarecibo = ?;";
        $query = $this->db->query($query, array($contrarecibo));
        $result = $query->result();
        return $result;
    }

    function borrarDetalleContrarecibo($contrarecibo) {

        $this->db->where('id_contrarecibo_detalle', $contrarecibo);
        $query = $this->db->delete('mov_contrarecibo_detalle');

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function insertar_caratula_contrarecibo_cancelado($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'alterno' => $datos["alterno"],
            'id_proveedor' => $datos["clave_proveedor"],
            'proveedor' => $datos["proveedor"],
            'concepto' => $datos["concepto"],
            'descripcion' => $datos["descripcion"],
            'documento' => $datos["documento"],
            'fecha_pago' => $datos["fecha_pago"],
            'fecha_emision' => $datos["fecha_solicita"],
            'enfirme' => $datos["firme"],
            'numero_compromiso' => $datos["num_compromiso"],
            'destino' => $datos["tipo"],
            'importe' => $datos["importe"],
            'tipo_documento' => $datos["tipo_documento"],
            'creado_por' => $nombre_completo,
            'pagada' => 0,
            'sifirme' => 0,
            'cancelada' => 1,
            'fecha_cancelada' => date("Y-m-d"),
            'estatus' => $datos["estatus"],
        );

        $this->db->where('id_contrarecibo_caratula', $datos["contrarecibo"]);
        $query = $this->db->update('mov_contrarecibo_caratula', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function activarCompromiso($compromiso) {

        $datos = array(
            "usado" => 0,
        );

        $this->db->where('numero_compromiso', $compromiso);
        $query = $this->db->update('mov_compromiso_caratula', $datos);

        if($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function cancelarcompromisoContrarecibo($contrarecibo, $query) {
        $query = $this->db->query($query, array(
            date("Y-m-d"), $contrarecibo,
        ));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function marcar_poliza_usada($contrarecibo, $query) {
        $query = $this->db->query($query, array($contrarecibo));
        if($query){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function marcar_devengado($compromiso, $fecha) {

        $sql = "SELECT * FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query = $this->db->query($sql, array($compromiso));
        $result = $query->result();

        foreach($result as $row) {

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $sql_devengado = "SELECT COLUMN_GET(nivel, '".$mes."_devengado' as char) AS devengado_mes FROM cat_niveles WHERE id_niveles = ?;";
            $query_devengado = $this->db->query($sql_devengado, array($row->id_nivel));
            $resultado_nivel = $query_devengado->row();

            $total_devengado = round($row->importe, 2) + round($resultado_nivel->devengado_mes, 2);

            $sql_actualizar_devengado = "UPDATE cat_niveles SET nivel = COLUMN_ADD(nivel, '".$mes."_devengado', ?) WHERE id_niveles = ?;";
            $this->db->query($sql_actualizar_devengado, array($total_devengado, $row->id_nivel));
        }

    }

    function tomarSubsidio($clave) {
        $this->db->select('descripcion')->from('cat_clasificador_fuentes_financia')->where('codigo', $clave);
        $query = $this->db->get();
        return $query->row()->descripcion;
    }

    function tomar_cuentas_contrarecibo($partida) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave LIKE BINARY ? AND destino = 'Devengado Gasto';";
        $query = $this->db->query($sql, array($partida));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function ultima_poliza() {
        $sql = "SELECT numero_poliza as ultimo FROM mov_polizas_cabecera ORDER BY numero_poliza DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function insertar_detalle_poliza($query, $datos) {
        $query = $this->db->query($query, $datos);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function checar_movimiento_cancelar_contrarecibo($contrarecibo){
        $sql = "SELECT mbpc.movimiento
                FROM mov_bancos_pagos_contrarecibos mbpc
                JOIN mov_bancos_movimientos mbm
                ON mbpc.movimiento = mbm.movimiento
                WHERE mbm.enfirme = 1
                AND mbm.cancelada = 0
                AND mbm.contrarecibo = ?";
        $query = $this->db->query($sql, array($contrarecibo));
        return $query->result_array();
    }

    /**
     * Aqui empieza la parte de movimientos bancarios
     */

    function apartarMovimientoBancario($ultimo, $cuenta, $id_cuenta, $t_movimiento) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            'movimiento' => $ultimo,
            'id_cuenta' => $id_cuenta,
            'cuenta' => $cuenta,
            'id_usuario' => $this->tank_auth->get_user_id(),
            'creado_por' => $nombre_completo,
            't_movimiento' => $t_movimiento,
        );

        $query = $this->db->insert('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function cancelarCaratulaMovimiento($datos) {
        $this->db->select('*')->from('mov_bancos_movimientos')->where('movimiento', $datos["movimiento"]);
        $query = $this->db->get();
        $firme = $query->row_array();

        if($firme["enfirme"] == 1) {
            $this->db->where('id_cuentas_bancarias', $firme["id_cuenta"]);
            $this->db->select('saldo');

            $query = $this->db->get('cat_cuentas_bancarias');

            $cuenta =  $query->row();

            $saldo = $cuenta->saldo - $firme["importe"];

            $data = array(
                'saldo' => $saldo,
            );

            $this->db->where('id_cuentas_bancarias', $firme["id_cuenta"]);
            $this->db->update('cat_cuentas_bancarias', $data);
        }

        if($firme["t_movimiento"] == "presupuesto") {
            $this->ciclo_model->desmarcar_ejercido($firme["contrarecibo"], $firme["fecha_pago"]);
        }

        $datos_actualizar = array(
            "cancelada" => 1,
            "enfirme" => 0,
            "fecha_cancelada" => date("Y-m-d"),
        );

        $this->db->where('movimiento', $datos["movimiento"]);
        $query_actualizar = $this->db->update('mov_bancos_movimientos', $datos_actualizar);

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getCuentasBancarias() {
        //$query = $this->db->get('cat_cuentas_bancarias');
        //return $query->result();
        $sql = "SELECT id_cuentas_bancarias, banco, cuenta, moneda FROM cat_cuentas_bancarias;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function getCuentasBancariasConciliacion() {
        $query = $this->db->get('cat_cuentas_bancarias');
        return $query->result();
    }

    function getCuenta($id) {
        $query = $this->db->get_where('cat_cuentas_bancarias', array('id_cuentas_bancarias' => $id));
        return $query->row();
    }

    function datos_CuentaBancaria($id) {
        $sql = "SELECT * FROM mov_bancos_movimientos WHERE id_cuenta = ? ORDER BY movimiento DESC;";
        $query = $this->db->query($sql, array('id_cuenta' => $id));
        return $query->result();
    }

    function datosMovimiento($id) {
        $query = $this->db->get_where('mov_bancos_movimientos', array('movimiento' => $id));
        return $query->row();
    }

    function ultimo_movimientoBancario() {
        $sql = "SELECT movimiento as ultimo FROM mov_bancos_movimientos ORDER BY movimiento DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

    function insertar_movimiento_bancario($datos) {

        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "movimiento" => $datos["movimiento"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "extra",
            "fecha_emision" => $datos["fecha_emision"],
            "fecha_emision_real" => date("Y-m-d"),
            "fecha_movimiento" => date("Y-m-d"),
            "fecha_movimiento_real" => date("Y-m-d"),
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'creado_por' => $nombre_completo,
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
        );

        $this->db->where('movimiento', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function insertar_movimiento_bancario_presupuesto($datos) {
        $sql_check_contrarecibo = "SELECT contrarecibo FROM mov_bancos_movimientos WHERE contrarecibo = ?;";
        $query_check_contrarecibo = $this->db->query($sql_check_contrarecibo, array($datos["numero_contrarecibo"]));
        $result_check_contrarecibo = $query_check_contrarecibo->row_array();

        // $this->debugeo->imprimir_pre($result_check_compromiso);

        if(isset($result_check_contrarecibo["contrarecibo"]) && $result_check_contrarecibo["contrarecibo"] != NULL && $result_check_contrarecibo["contrarecibo"] != "") {
            if($result_check_contrarecibo["contrarecibo"] != $datos["numero_contrarecibo"]) {
                return "usado";
            }
        }

        $usado = array(
            "usado" => 1
        );

        $this->db->where('id_contrarecibo_caratula', $datos["numero_contrarecibo"]);
        $this->db->update('mov_contrarecibo_caratula', $usado);

        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        // $this->debugeo->imprimir_pre($datos);

        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "movimiento" => $datos["ultimo_movimiento"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "presupuesto",
            "fecha_emision" => $datos["fecha_emision"],
            "fecha_emision_real" => date("Y-m-d"),
            "fecha_movimiento" => date("Y-m-d"),
            "fecha_movimiento_real" => date("Y-m-d"),
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "neto" => $datos["neto"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'creado_por' => $nombre_completo,
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
            'contrarecibo' => $datos["numero_contrarecibo"],
            'honorarios' => $datos["honorarios"],
        );

        $this->db->where('movimiento', $datos["ultimo_movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return "exito";
        }
        else {
            return "error";
        }
    }

    function insertar_movimiento_bancario_conciliacion($datos) {
        $id = $this->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "movimiento" => $datos["ultimo_movimiento"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => $datos["tipo_hidden"],
            "fecha_emision" => $datos["fecha_emision"],
            "fecha_emision_real" => date("Y-m-d"),
            "fecha_movimiento" => date("Y-m-d"),
            "fecha_movimiento_real" => date("Y-m-d"),
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "importe" => $datos["importe"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            "cuenta_cargo" => $datos["cuenta_cargo"],
            "descripcion_cuenta_cargo" => $datos["descripcion_cuenta_cargo"],
            "cuenta_abono" => $datos["cuenta_abono"],
            "descripcion_cuenta_abono" => $datos["descripcion_cuenta_abono"],
            'creado_por' => $nombre_completo,
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
        );

        $query = $this->db->insert('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizarPoliza($datos) {
        $data = array(
            'cuenta_cargo' => $datos["cuenta_cargo"],
            'descripcion_cuenta_cargo' => $datos["descripcion_cuenta_cargo"],
            'cuenta_abono' => $datos["cuenta_abono"],
            'descripcion_cuenta_abono' => $datos["descripcion_cuenta_abono"],
            'enfirme' => $datos["firme"],
        );

        $this->db->where('id_bancos_movimientos', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        return FALSE;
    }

    function actualizar_movimiento_bancario($datos) {

        $data = array(
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "extra",
            "fecha_emision" => $datos["fecha_emision"],
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "concepto_especifico" => $datos["concepto_especifico"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'enfirme' => $datos["check_firme"],
        );

        $this->db->where('id_bancos_movimientos', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function actualizar_movimiento_bancario_presupuesto($datos) {
        $sql_check_contrarecibo = "SELECT contrarecibo FROM mov_bancos_movimientos WHERE contrarecibo = ?;";
        $query_check_contrarecibo = $this->db->query($sql_check_contrarecibo, array($datos["numero_contrarecibo"]));
        $result_check_contrarecibo = $query_check_contrarecibo->row_array();

        // $this->debugeo->imprimir_pre($datos);

        if(isset($result_check_contrarecibo["contrarecibo"]) && $result_check_contrarecibo["contrarecibo"] != NULL && $result_check_contrarecibo["contrarecibo"] != "") {
            if($result_check_contrarecibo["contrarecibo"] != $datos["numero_contrarecibo"]) {
                return "usado";
            }
        }


        $data = array(
            "id_cuenta" => $datos["hidden_id_cuenta"],
            "cuenta" => $datos["hidden_cuenta"],
            "clave" => $datos["hidden_clave_concepto"],
            "concepto" => $datos["concepto"],
            "t_movimiento" => "presupuesto",
            "fecha_emision" => $datos["fecha_emision"],
            "hora_movimiento" => $datos["tiempo_movimiento"],
            "cheque" => $datos["cheque"],
            "movimiento_bancario" => $datos["movimiento_bancario"],
            "importe" => $datos["importe"],
            "neto" => $datos["neto"],
            "cargo" => $datos["importe"],
            "id_proveedor" => $datos["id_proveedor"],
            "proveedor" => $datos["proveedor"],
            "fecha_pago" => $datos["fecha_pago"],
            "descripcion" => $datos["descripcion_general"],
            "tipo" => $datos["tipo_movimiento"],
            'enfirme' => $datos["check_firme"],
            'id_usuario' => $this->tank_auth->get_user_id(),
            'contrarecibo' => $datos["numero_contrarecibo"],
        );

        $this->db->where('movimiento', $datos["movimiento"]);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if($query) {
            return "exito";
        }
        else {
            return "error";
        }
    }

    function datosBancosConceptos() {
        $sql = "SELECT * FROM cat_bancos_conceptos;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCuentasContables() {
        $sql = "SELECT * FROM cat_cuentas_contables WHERE tipo = 'D';";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function datosCuentasContablesDetalle($numero) {
        $sql = "SELECT * FROM mov_bancos_pagos_contrarecibos WHERE movimiento = ?;";
        $query = $this->db->query($sql, array($numero));
        $result = $query->result();
        return $result;
    }

    function conciliarMovimiento($id, $fecha) {

        $this->db->trans_begin();

        $sql_contrarecibo = "SELECT contrarecibo FROM mov_bancos_movimientos WHERE id_bancos_movimientos = ?;";
        $query_contrarecibo = $this->db->query($sql_contrarecibo, array($id));
        $contrarecibo = $query_contrarecibo->row_array();        

        $sql = "SELECT numero_compromiso FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo["contrarecibo"]));
        $resultado = $query->row_array();

        $sql_compromiso = "SELECT COLUMN_JSON(nivel) AS estructura, importe FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_compromiso = $this->db->query($sql_compromiso, array($resultado["numero_compromiso"]));
        $result = $query_compromiso->result_array();

        $query_actualizar = FALSE;

        foreach($result as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $sql_detalle = "SELECT COLUMN_GET(nivel, '".$mes."_pagado' as char) as pagado_mes,
                    COLUMN_GET(nivel, 'pagado_anual' as char) as pagado_anual
                        FROM cat_niveles
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                AND COLUMN_GET(nivel, 'partida' as char) = ?";
            $query_detalle = $this->db->query($sql_detalle, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
            $result_detalle = $query_detalle->row_array();

            $result_detalle["pagado_mes"] += round($value["importe"], 2);
            $result_detalle["pagado_anual"] += round($value["importe"], 2);

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes."_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'pagado_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result_detalle["pagado_mes"],
                $result_detalle["pagado_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        }

        $data = array(
            "sifirme" => 1,
            "fecha_conciliado" => $fecha,
        );

        $this->db->where('id_bancos_movimientos', $id);
        $this->db->update('mov_bancos_movimientos', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function quitarConciliarMovimiento($id) {

        $this->db->trans_begin();

        $this->db->select('fecha_conciliado AS fecha')
                    ->from('mov_bancos_movimientos')
                    ->where('id_bancos_movimientos', $id);
        $query = $this->db->get();
        $fecha = $query->row_array();

        $sql_contrarecibo = "SELECT contrarecibo FROM mov_bancos_movimientos WHERE id_bancos_movimientos = ?;";
        $query_contrarecibo = $this->db->query($sql_contrarecibo, array($id));
        $contrarecibo = $query_contrarecibo->row_array();        

        $sql = "SELECT numero_compromiso FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo["contrarecibo"]));
        $resultado = $query->row_array();

        $sql_compromiso = "SELECT COLUMN_JSON(nivel) AS estructura, importe FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_compromiso = $this->db->query($sql_compromiso, array($resultado["numero_compromiso"]));
        $result = $query_compromiso->result_array();

        $query_actualizar = FALSE;

        foreach($result as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);

            $mes = $this->utilerias->convertirFechaAMes($fecha["fecha"]);

            $sql_detalle = "SELECT COLUMN_GET(nivel, '".$mes."_pagado' as char) as pagado_mes,
                    COLUMN_GET(nivel, 'pagado_anual' as char) as pagado_anual
                        FROM cat_niveles
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                AND COLUMN_GET(nivel, 'partida' as char) = ?";
            $query_detalle = $this->db->query($sql_detalle, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
            $result_detalle = $query_detalle->row_array();

            $result_detalle["pagado_mes"] -= round($value["importe"], 2);
            $result_detalle["pagado_anual"] -= round($value["importe"], 2);

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes."_pagado', ?),
                            nivel = COLUMN_ADD(nivel, 'pagado_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result_detalle["pagado_mes"],
                $result_detalle["pagado_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        }

        $data = array(
            "sifirme" => 0,
            "fecha_conciliado" => "0000-00-00",
        );

        $this->db->where('id_bancos_movimientos', $id);
        $query = $this->db->update('mov_bancos_movimientos', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }
        
    }

    function datos_contrarecibo() {
        $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE enfirme = 1 AND cancelada = 0 AND usado = 0 ORDER BY id_contrarecibo_caratula DESC;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function copiarDatosContrarecibo($contrarecibo, $cuenta, $movimiento) {

    $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
    $query = $this->db->query($sql, array($contrarecibo));
    $result = $query->row();

//        $this->debugeo->imprimir_pre($result);

    $sql = "INSERT INTO mov_bancos_pagos_contrarecibos (cuenta, movimiento, numero, nombre, importe) VALUES (?, ?, ?, ?, ?);";
    $resultado = $this->db->query($sql, array($cuenta, $movimiento, $result->id_contrarecibo_caratula, $result->proveedor, $result->importe));

    if($resultado) {
        return TRUE;
    }
    else {
        return FALSE;
    }

}

    function datos_Caratula_Contrarecibo_Movimiento($contrarecibo) {
        $sql = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $result = $query->row();
        return $result;
    }

    function borrarDetalleMovimiento($movimiento) {

        $this->db->where('movimiento', $movimiento);
        $query = $this->db->delete('mov_bancos_pagos_contrarecibos');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function borrarMovimiento($movimiento) {

        $this->db->where('movimiento', $movimiento);
        $query = $this->db->delete('mov_bancos_pagos_contrarecibos');
        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    function datosMovimientos($id_cuenta) {
        $sql = "SELECT * FROM mov_bancos_movimientos m
                JOIN cat_cuentas_bancarias c
                ON c.cuenta = m.cuenta where m.id_cuenta = $id_cuenta and m.enfirme = 1";

        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function imprimirMovimientos($datos) {
        $sql = "SELECT * FROM mov_bancos_movimientos m
                JOIN cat_cuentas_bancarias c ON c.cuenta = m.cuenta
                WHERE m.id_cuenta = ?
				AND m.t_movimiento LIKE ?
				AND m.tipo LIKE ?
				AND m.id_proveedor LIKE ?
				AND m.movimiento LIKE ?
                AND m.enfirme = 1
                -- AND m.cancelada = 0
                AND m.fecha_pago >= ?
                AND m.fecha_pago <= ?;";

        $query = $this->db->query($sql, array($datos["id_cuenta"], "%".$datos["tipo"]."%", "%".$datos["tipo_movimiento"]."%",
                                            "%".$datos["id_proveedor"]."%", "%".$datos["movimiento"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        $result = $query->result();
        return $result;
    }

    function get_centro_costo_API($libreria) {
        $sql = "SELECT clave_centro_costo FROM cat_centro_libreria WHERE clave_libreria = ?";
        $query = $this->db->query($sql, array($libreria));
        $result = $query->row();
        return $result->clave_centro_costo;
    }

    function tomar_cuentas_movimiento($partida) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave = ? AND destino = 'Pagado Gasto';";
        $query = $this->db->query($sql, array($partida));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function tomar_cuentas_movimiento_egresos($partida) {
//        Se toman los datos de la cuenta que le corresponde a la partida a buscar
        $sql = "SELECT * FROM cat_correlacion_partidas_contables WHERE clave = ?;";
        $query = $this->db->query($sql, array($partida));
//        Si existe la clave a buscar, se le agrega el centro de costos para saber si existe esa clave
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Aqui empieza la parte de conciliación bancaria
     */

    function datosConciliacion($cuenta) {
        $sql = "SELECT * FROM mov_bancos_movimientos WHERE id_cuenta = $cuenta AND fecha_conciliado != '0000-00-00'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function imprimirConciliacion($datos) {
        $sql = "SELECT * FROM mov_bancos_movimientos m
                JOIN cat_cuentas_bancarias c ON c.cuenta = m.cuenta
                WHERE m.id_cuenta = ?
				AND m.tipo LIKE ?
				AND m.id_proveedor LIKE ?
				AND m.movimiento LIKE ?
                AND m.enfirme = 1
				AND m.cancelada = 0
                AND m.fecha_emision >= ?
                AND m.fecha_emision <= ?;";

        $query = $this->db->query($sql, array($datos["id_cuenta"], "%".$datos["tipo_movimiento"]."%",
            "%".$datos["id_proveedor"]."%", "%".$datos["movimiento"]."%", $datos["fecha_inicial"], $datos["fecha_final"]));
        $result = $query->result();
        return $result;
    }

    function get_datos_conciliacion($cuenta, $query) {
        $query = $this->db->query($query, array($cuenta));
        $result = $query->result();
        return $result;
    }

    function get_nombre_banco() {
        $sql = "SELECT c1.cuenta,  c2.banco
                FROM mov_bancos_movimientos c1
                LEFT JOIN cat_cuentas_bancarias c2 ON c1.cuenta = c2.cuenta
                GROUP BY c1.cuenta;";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function marcar_ejercido($contrarecibo, $fecha) {

        $this->db->trans_begin();

        $sql = "SELECT numero_compromiso FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $resultado = $query->row_array();

        $sql_compromiso = "SELECT COLUMN_JSON(nivel) AS estructura, importe FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_compromiso = $this->db->query($sql_compromiso, array($resultado["numero_compromiso"]));
        $result = $query_compromiso->result_array();

        $query_actualizar = FALSE;

        foreach($result as $key => $value) {

            $estructura = json_decode($value["estructura"], TRUE);

            // Se toma la estructura completa
            $query_revisar_dinero = "SELECT COLUMN_JSON(nivel) AS estructura
                                     FROM cat_niveles
                                     WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                     AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                     AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                     AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            $resultado_dinero = $this->ciclo_model->revisarDineroEstructuraCompleta(array(
                0 => $estructura["fuente_de_financiamiento"],
                1 => $estructura["programa_de_financiamiento"],
                2 => $estructura["centro_de_costos"],
                3 => $estructura["capitulo"],
                4 => $estructura["concepto"],
                5 => $estructura["partida"],), $query_revisar_dinero);

            $estructura_partida = json_decode($resultado_dinero["estructura"], TRUE);

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $estructura_partida[$mes."_ejercido"] += number_format($value["importe"], 2, '.', '');
            $estructura_partida["ejercido_anual"] += number_format($value["importe"], 2, '.', '');
            
            $this->actualizar_partida_total($estructura_partida);

        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
        
    }

    function desmarcar_ejercido($contrarecibo, $fecha) {
        $sql = "SELECT numero_compromiso FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";
        $query = $this->db->query($sql, array($contrarecibo));
        $resultado = $query->row_array();

//        $this->debugeo->imprimir_pre($resultado);

        $sql_compromiso = "SELECT COLUMN_JSON(nivel) AS estructura, importe FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";
        $query_compromiso = $this->db->query($sql_compromiso, array($resultado["numero_compromiso"]));
        $result = $query_compromiso->result_array();

//        $this->debugeo->imprimir_pre($result);

        $query_actualizar = FALSE;

        foreach($result as $key => $value) {
            $estructura = json_decode($value["estructura"], TRUE);

            $mes = $this->utilerias->convertirFechaAMes($fecha);

            $sql_detalle = "SELECT COLUMN_GET(nivel, '".$mes."_ejercido' as char) as ejercido_mes,
                    COLUMN_GET(nivel, 'ejercido_anual' as char) as ejercido_anual
                        FROM cat_niveles
                            WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                AND COLUMN_GET(nivel, 'partida' as char) = ?";
            $query_detalle = $this->db->query($sql_detalle, array(
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
            $result_detalle = $query_detalle->row_array();

            $result_detalle["ejercido_mes"] -= round($value["importe"], 2);
            $result_detalle["ejercido_anual"] -= round($value["importe"], 2);

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes."_ejercido', ?),
                            nivel = COLUMN_ADD(nivel, 'ejercido_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result_detalle["ejercido_mes"],
                $result_detalle["ejercido_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        }

        if($query_actualizar) {

//            $this->debugeo->imprimir_pre($contrarecibo);

            $sql_actualizar_usado = "UPDATE mov_contrarecibo_caratula SET usado = 0 WHERE id_contrarecibo_caratula = ?;";
            $query_actualizar_usado = $this->db->query($sql_actualizar_usado, array(
                $contrarecibo
            ));

//            if($query_actualizar_usado) {
//                $this->debugeo->imprimir_pre("Exito al actualizar el contrarecibo");
//            } else {
//                $this->debugeo->imprimir_pre("Hubo un error");
//            }

            return TRUE;
        } else {
            return FALSE;
        }
    }

    function devolver_dinero_cancelado($fecha, $estructura, $importe, $tipo_movimiento) {
        $anual = "";

        if($tipo_movimiento == "precompromiso") {
            $anual = "precomprometido";
        } elseif($tipo_movimiento == "compromiso") {
            $anual = "comprometido";
        } elseif($tipo_movimiento == "devengado") {
            $anual = "devengado";
        }

        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

        $sql = "SELECT COLUMN_GET(nivel, '".$mes_actual."_".$tipo_movimiento."' as char) as movimiento_mes,
                COLUMN_GET(nivel, '".$mes_actual."_".$tipo_movimiento."' as char) as devengado_mes,
                COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) as saldo_mes,
                COLUMN_GET(nivel, 'total_anual' as char) as saldo_anual,
                COLUMN_GET(nivel, 'devengado_anual' as char) as devengado_anual,
                COLUMN_GET(nivel, '".$anual."_anual' as char) as movimiento_anual
                FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?";
        $query = $this->db->query($sql, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
            ));
        $result = $query->row_array();

        $query_actualizar = FALSE;

        if($tipo_movimiento == "compromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $result["saldo_mes"] += $importe;
            $result["saldo_anual"] += $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'total_anual', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["saldo_mes"],
                $result["saldo_anual"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        } elseif($tipo_movimiento == "precompromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
        } elseif($tipo_movimiento == "devengado") {
            $result["devengado_mes"] -= $importe;
            $result["devengado_anual"] -= $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["devengado_mes"],
                $result["devengado_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
        }

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function devolver_dinero_precompromiso_compromiso($compromiso, $fecha) {

        $this->db->trans_begin();

        $this->db->select('COLUMN_JSON(nivel) as estructura,
                            COLUMN_JSON(presupuesto_tomado) as presupuesto_tomado,
                            importe,
                            id_precompromiso_detalle')
                    ->from('mov_compromiso_detalle')
                    ->where('numero_compromiso', $compromiso);
        $query = $this->db->get();

        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {

            // Se inicializa la variable que tiene el query para poder guardar el presupuesto tomado
            $query_devolver_presupuesto = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_ADD( nivel,";

            // Se toma el mes de donde se quiere comprometer el dinero
            $fecha_emitido = $this->utilerias->convertirFechaAMes($fecha);

            $estructura = json_decode($value["estructura"], TRUE);
            $presupuesto = json_decode($value["presupuesto_tomado"], TRUE);

            // Se toma la estructura completa
            $query_revisar_dinero = "SELECT COLUMN_JSON(nivel) AS estructura
                                     FROM cat_niveles
                                     WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                     AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                     AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                     AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            // Se tiene que comprobar si hay dinero para hacer el compromiso
            $resultado_dinero = $this->revisarDineroEstructuraCompleta(array(
                0 => $estructura["fuente_de_financiamiento"],
                1 => $estructura["programa_de_financiamiento"],
                2 => $estructura["centro_de_costos"],
                3 => $estructura["capitulo"],
                4 => $estructura["concepto"],
                5 => $estructura["partida"],), $query_revisar_dinero);

            // Se guarda la partida en una variable
            $estructura_partida = json_decode($resultado_dinero["estructura"], TRUE);

            // Se resta el importe al comprometido anual
            $estructura_partida["comprometido_anual"] -= $value["importe"];

            // Se resta el importe al mes comprometido
            $estructura_partida[$fecha_emitido."_compromiso"] -= $value["importe"];

            // Se suma el importe al total anual
            $estructura_partida["total_anual"] += $value["importe"];

            // Se suma el importe al disponible mensual
            $estructura_partida[$fecha_emitido."_saldo"] += $value["importe"];

            // Se genera el query que toma el presupuesto tomado del ID detalle del precompromiso
            $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado', FALSE)
                        ->from('mov_precompromiso_detalle')
                        ->where('id_precompromiso_detalle', $value["id_precompromiso_detalle"]);
            $query_precompromiso_detalle = $this->db->get();
            $resultado_precompromiso_detalle = $query_precompromiso_detalle->row_array();

            // Se guarda el resultado del query en una variable y se transforma el JSON en un arreglo
            $presupuesto_tomado_precompromiso = json_decode($resultado_precompromiso_detalle["presupuesto_tomado"], TRUE);

            // Se crea un ciclo para recorrer todos los meses
            for ($i = 1; $i <= 12; $i++) {

                // Se toma el año en curso
                $year = date("Y");

                // Se transforma el numero del mes a texto
                $fecha_interna_mes = $this->utilerias->convertirFechaAMes($year."-".$i."-01");
                
                // Se comprueba que exsista el mes dentro del presupuesto tomado
                if(isset($presupuesto[$fecha_interna_mes])) {

                    $estructura_partida[$fecha_interna_mes."_precompromiso"] += $presupuesto[$fecha_interna_mes];

                    $estructura_partida["precomprometido_anual"] += $presupuesto[$fecha_interna_mes];

                    $dinero_devuelto = $presupuesto_tomado_precompromiso[$fecha_interna_mes] + $presupuesto[$fecha_interna_mes];

                    // Aqui se tiene que agregar un arreglo con el mes de donde se tomó el presupuesto y el saldo
                    $query_devolver_presupuesto .= " '".$fecha_interna_mes."', ".$dinero_devuelto.",";
                    
                }

            }

            // Se quita la última coma del query
            $query_devolver_presupuesto = substr($query_devolver_presupuesto, 0, -1);

            // Se agrega la condicional al query para que actualize el presupuesto de donde fue tomado
            $query_devolver_presupuesto .= " ) WHERE id_precompromiso_detalle = ".$value["id_precompromiso_detalle"].";";

            // $this->debugeo->imprimir_pre($query_devolver_presupuesto);

            // Se actualiza el campo del presupuesto tomado
            $this->ciclo_model->actualizar_presupuesto_tomado_precompromiso($query_devolver_presupuesto);

            // Se actualiza la partida
            $this->ciclo_model->actualizar_partida_compromiso($estructura_partida);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function devolver_dinero_cancelado_compromiso($compromiso, $fecha) {

        $this->db->trans_begin();

        $this->db->select('COLUMN_JSON(nivel) as estructura,
                            COLUMN_JSON(presupuesto_tomado) as presupuesto_tomado,
                            importe,
                            id_precompromiso_detalle')
                    ->from('mov_compromiso_detalle')
                    ->where('numero_compromiso', $compromiso);
        $query = $this->db->get();

        $resultado = $query->result_array();

        foreach ($resultado as $key => $value) {

            // Se inicializa la variable que tiene el query para poder guardar el presupuesto tomado
            $query_devolver_presupuesto = "UPDATE mov_precompromiso_detalle SET presupuesto_tomado = COLUMN_ADD( nivel,";

            // Se toma el mes de donde se quiere comprometer el dinero
            $fecha_emitido = $this->utilerias->convertirFechaAMes($fecha);

            $estructura = json_decode($value["estructura"], TRUE);
            $presupuesto = json_decode($value["presupuesto_tomado"], TRUE);

            // Se toma la estructura completa
            $query_revisar_dinero = "SELECT COLUMN_JSON(nivel) AS estructura
                                     FROM cat_niveles
                                     WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                     AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                     AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                     AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                     AND COLUMN_GET(nivel, 'partida' as char) = ?;";

            // Se tiene que comprobar si hay dinero para hacer el compromiso
            $resultado_dinero = $this->revisarDineroEstructuraCompleta(array(
                0 => $estructura["fuente_de_financiamiento"],
                1 => $estructura["programa_de_financiamiento"],
                2 => $estructura["centro_de_costos"],
                3 => $estructura["capitulo"],
                4 => $estructura["concepto"],
                5 => $estructura["partida"],), $query_revisar_dinero);

            // Se guarda la partida en una variable
            $estructura_partida = json_decode($resultado_dinero["estructura"], TRUE);

            // Se resta el importe al comprometido anual
            $estructura_partida["comprometido_anual"] -= $value["importe"];

            // Se resta el importe al mes comprometido
            $estructura_partida[$fecha_emitido."_compromiso"] -= $value["importe"];

            // Se suma el importe al total anual
            $estructura_partida["total_anual"] += $value["importe"];

            // Se suma el importe al disponible mensual
            $estructura_partida[$fecha_emitido."_saldo"] += $value["importe"];

            // Se genera el query que toma el presupuesto tomado del ID detalle del precompromiso
            $this->db->select('COLUMN_JSON(presupuesto_tomado) AS presupuesto_tomado', FALSE)
                        ->from('mov_precompromiso_detalle')
                        ->where('id_precompromiso_detalle', $value["id_precompromiso_detalle"]);
            $query_precompromiso_detalle = $this->db->get();
            $resultado_precompromiso_detalle = $query_precompromiso_detalle->row_array();

            // Se guarda el resultado del query en una variable y se transforma el JSON en un arreglo
            $presupuesto_tomado_precompromiso = json_decode($resultado_precompromiso_detalle["presupuesto_tomado"], TRUE);

            // Se crea un ciclo para recorrer todos los meses
            for ($i = 1; $i <= 12; $i++) {

                // Se toma el año en curso
                $year = date("Y");

                // Se transforma el numero del mes a texto
                $fecha_interna_mes = $this->utilerias->convertirFechaAMes($year."-".$i."-01");
                
                // Se comprueba que exsista el mes dentro del presupuesto tomado
                if(isset($presupuesto[$fecha_interna_mes])) {

                    $estructura_partida[$fecha_interna_mes."_precompromiso"] += $presupuesto[$fecha_interna_mes];

                    $estructura_partida["precomprometido_anual"] += $presupuesto[$fecha_interna_mes];

                    $dinero_devuelto = $presupuesto_tomado_precompromiso[$fecha_interna_mes] + $presupuesto[$fecha_interna_mes];

                    // Aqui se tiene que agregar un arreglo con el mes de donde se tomó el presupuesto y el saldo
                    $query_devolver_presupuesto .= " '".$fecha_interna_mes."', ".$dinero_devuelto.",";
                    
                }

            }

            // Se quita la última coma del query
            $query_devolver_presupuesto = substr($query_devolver_presupuesto, 0, -1);

            // Se agrega la condicional al query para que actualize el presupuesto de donde fue tomado
            $query_devolver_presupuesto .= " ) WHERE id_precompromiso_detalle = ".$value["id_precompromiso_detalle"].";";

            // $this->debugeo->imprimir_pre($query_devolver_presupuesto);

            // Se actualiza el campo del presupuesto tomado
            $this->ciclo_model->actualizar_presupuesto_tomado_precompromiso($query_devolver_presupuesto);

            // Se actualiza la partida
            $this->ciclo_model->actualizar_partida_compromiso($estructura_partida);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function devolver_dinero_contrarecibo_cancelado($contrarecibo, $estructura, $importe, $tipo_movimiento) {
        $anual = "";

        if($tipo_movimiento == "precompromiso") {
            $anual = "precomprometido";
        } elseif($tipo_movimiento == "compromiso") {
            $anual = "comprometido";
        }

        $mes_actual = $this->utilerias->convertirFechaAMes($fecha);

        $sql = "SELECT COLUMN_GET(nivel, '".$mes_actual."_".$tipo_movimiento."' as char) as movimiento_mes,
                COLUMN_GET(nivel, '".$mes_actual."_saldo' as char) as saldo_mes,
                COLUMN_GET(nivel, 'total_anual' as char) as saldo_anual,
                COLUMN_GET(nivel, '".$anual."_anual' as char) as movimiento_anual
                FROM cat_niveles
                    WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                        AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                        AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                        AND COLUMN_GET(nivel, 'concepto' as char) = ?
                        AND COLUMN_GET(nivel, 'partida' as char) = ?";
        $query = $this->db->query($sql, array(
            $estructura["fuente_de_financiamiento"],
            $estructura["programa_de_financiamiento"],
            $estructura["centro_de_costos"],
            $estructura["capitulo"],
            $estructura["concepto"],
            $estructura["partida"],
        ));
        $result = $query->row_array();

        $query_actualizar = FALSE;

        if($tipo_movimiento == "compromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $result["saldo_mes"] += $importe;
            $result["saldo_anual"] += $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_saldo', ?),
                            nivel = COLUMN_ADD(nivel, 'total_anual', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["saldo_mes"],
                $result["saldo_anual"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));

        } elseif($tipo_movimiento == "compromiso") {
            $result["movimiento_mes"] -= $importe;
            $result["movimiento_anual"] -= $importe;

            $sql_actualizar = "UPDATE cat_niveles SET
                            nivel = COLUMN_ADD(nivel, '".$mes_actual."_".$tipo_movimiento."', ?),
                            nivel = COLUMN_ADD(nivel, '".$anual."_anual', ?)
                                WHERE COLUMN_GET(nivel, 'fuente_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'programa_de_financiamiento' as char) = ?
                                    AND COLUMN_GET(nivel, 'centro_de_costos' as char) = ?
                                    AND COLUMN_GET(nivel, 'capitulo' as char) = ?
                                    AND COLUMN_GET(nivel, 'concepto' as char) = ?
                                    AND COLUMN_GET(nivel, 'partida' as char) = ?;";
            $query_actualizar = $this->db->query($sql_actualizar, array(
                $result["movimiento_mes"],
                $result["movimiento_anual"],
                $estructura["fuente_de_financiamiento"],
                $estructura["programa_de_financiamiento"],
                $estructura["centro_de_costos"],
                $estructura["capitulo"],
                $estructura["concepto"],
                $estructura["partida"],
            ));
        }

        if($query_actualizar) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function get_arreglo_datos($sql) {
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

}