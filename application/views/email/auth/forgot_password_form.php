<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'class'	=> 'form-control c-center',
	'maxlength'	=> 80,
	'size'	=> 30,
	'placeholder'	=> "Correo Electrónico o Usuario",
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Correo Electrónico o Usuario';
} else {
	$login_label = 'Correo Electrónico';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Armonniza</title>

    <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>">

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url("assets/templates/front/css/bootstrap.min.css") ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= base_url("assets/templates/front/css/plugins/metisMenu/metisMenu.min.css") ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= base_url("assets/templates/front/css/sb-admin-2.css") ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url("assets/templates/front/font-awesome-4.1.0/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login forgot">
<div class="login-container">
	<div class="logo">
        <img src="<?= base_url('img/logo.png') ?>">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 cont-gral-form">
                <div class="font-cont-form c-center" style="height: 23em;"></div>
                    <div class="cont-form c-center" style="top: -22em;">
                        <h2 class="center c-center" style="width:90%;">Recuperar Contraseña</h2>
                        <?php echo form_open($this->uri->uri_string()); ?>
                        <fieldset class="center c-center" style="width:90%;">
                            <div class="form-group">
                                <br/>
                                <!--<?php echo form_label($login_label, $login['id']); ?>-->
                                <?php echo form_input($login); ?>
                                <br/>
                                <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
                            </div>
                        </fieldset>
                        <div class="row c-center">
                            <div class="col-sm-8 col-md-8"></div>
                            <div class="col-sm-4 col-md-4">
                                <?php echo form_submit( array("name" => "reset", "class" => "btn btn-lg btn-default" ), 'Enviar'); ?>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row">
            <div class="col-sm-12 col-md-12 text-right">
                <img style="width: 10%;" src="<?= base_url('img/armonniza.png') ?>">
            </div>
        </div>-->
    </div>
</div>
</div>
<!-- jQuery -->
<script src="<?= base_url("assets/templates/front/js/jquery.js") ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url("assets/templates/front/js/bootstrap.min.js") ?>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url("assets/templates/front/js/plugins/metisMenu/metisMenu.min.js") ?>"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= base_url("assets/templates/front/js/sb-admin-2.js") ?>"></script>
</body>

</html>