</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="<?= base_url("assets/templates/front/js/jquery.js") ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url("assets/templates/front/js/bootstrap.min.js") ?>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url("assets/templates/front/js/plugins/metisMenu/metisMenu.min.js") ?>"></script>

<script src="<?= base_url("assets/templates/front/js/sb-admin-2.js") ?>"></script>

<script>
    // Listen for click on toggle checkbox
    $('#select_todos').click(function(event) {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });
</script>

</body>

</html>