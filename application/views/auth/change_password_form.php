<?php
$old_password = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'value' => set_value('old_password'),
    'class' => 'form-control cont',
    'placeholder' => 'Contraseña Actual',
	'size' 	=> 30,
);
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    'class' => 'form-control cont',
    'placeholder' => 'Nueva Contraseña',
	'size'	=> 30,
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    'class' => 'form-control cont',
    'placeholder' => 'Confirmar Nueva Contraseña',
	'size' 	=> 30,
);
?>
    <!-- Page Content -->
    <h3 class="page-header title center"><i class="fa fa-user"></i> Cambiar Contraseña</h3>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row perfil-usuario">
                <div class="col-lg-12">
                    <?php
                    if(isset($mensaje)) { ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $mensaje ?>
                        </div>
                    <?php } ?>

                    <?php echo form_open($this->uri->uri_string()); ?>
                        <?php echo form_password($old_password); ?>
                        <span style="color: red;"><?php echo form_error($old_password['name']); ?><?php echo isset($errors[$old_password['name']])?$errors[$old_password['name']]:''; ?></span>

                        <?php echo form_password($new_password); ?>
                        <span style="color: red;"><?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>

                        <?php echo form_password($confirm_new_password); ?></td>
                        <span style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></span>

                    <div class="btn-perfil text-center">
                        <a href="<?= base_url("usuario") ?>" class="btn btn-default back"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        <?php echo form_submit('change', 'Actualizar Contraseña'); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->