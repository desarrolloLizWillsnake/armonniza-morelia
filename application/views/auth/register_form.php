<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
        'class'	=> 'form-control',
        'placeholder'	=> "Usuario",
	);
}
$this->db->select('*')->from('grupos');

$query = $this->db->get()->result();

$this->db->select('*')->from('modulos');

$modulos = $this->db->get()->result();

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
    'class'	=> 'form-control',
    'placeholder'	=> "Correo Electrónico",
    'required'   => "required",
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    'class'	=> 'form-control',
    'placeholder'	=> "Contraseña",
    'required'   => "required",
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    'class'	=> 'form-control',
    'placeholder'	=> "Confirmar Contraseña",
    'required'   => "required",
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
    'required'   => "required",
);
$nombre = array(
    'name'	=> 'nombre',
    'id'	=> 'nombre',
    'value' => set_value('nombre'),
    'class'	=> 'form-control',
    'placeholder'	=> "Nombre(s)",
    'required'   => "required",
);

$apellido_pa = array(
    'name'	=> 'apellido_pa',
    'id'	=> 'apellido_pa',
    'value' => set_value('apellido_pa'),
    'class'	=> 'form-control',
    'placeholder'	=> "Apellido Paterno",
    'required'   => "required",
);

$apellido_ma = array(
    'name'	=> 'apellido_ma',
    'id'	=> 'apellido_ma',
    'value' => set_value('apellido_ma'),
    'class'	=> 'form-control',
    'placeholder'	=> "Apellido Materno",
    'required'   => "required",
);

$puesto = array(
    'name'	=> 'puesto',
    'id'	=> 'puesto',
    'value' => set_value('puesto'),
    'class'	=> 'form-control',
    'placeholder'	=> "Puesto",
    'required'   => "required",
);
?>
<?php
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$fecha = $meses[date('n')-1]." ".date('d');
$año = date("Y");
$hora = date('h:i:s A');
?>
<h3 class="page-header center"><i class="fa fa-laptop"></i> Panel Administrador</h3>
<?php echo form_open($this->uri->uri_string(), array("id" => "forma_registrar")); ?>
<div id="page-wrapper">
        <div class="cont-menu-admin left">
            <a class="menu-admin center" href="<?= base_url("administrador/empresa") ?>">
                <i class="fa fa-building fa-2x"></i><br><span>Empresa</span>
            </a>
            <a class="menu-admin center" href="<?= base_url("administrador/usuarios") ?>">
                <i class="fa fa-users fa-2x"></i><br><span>Usuarios</span>
            </a>
            <a class="menu-admin center" href="<?= base_url("administrador/catalogos") ?>">
                <i class="fa fa-cubes fa-2x"></i><br><span>Catálogos</span>
            </a>
            <a class="menu-admin center" href="<?= base_url("administrador/autorizaciones") ?>">
                <i class="fa fa-check-square  fa-2x"></i><br><span>Autorizaciones</span>
            </a>
            <div class="menu-admin-calendar-ic center" style="margin-left: 15.99%;">
                <i class="fa fa-calendar  fa-2x"></i><br><span><?= $año ?></span>
            </div>
            <div class="menu-admin-calendar center">
                <span style="font-size: 1.45em;"><b><?= $fecha ?></b></span><br><?= $hora ?>
            </div>
        </div>
        <div class="row add-pre">
            <?php
            if(isset($mensaje)) { ?>
            <div class="col-lg-12">
                <?= $mensaje ?>
            </div>
            <?php } ?>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <h3 class="page-header sub-page center"><i class="fa fa-user-plus"></i> Registrar Usuario</h3>
                    <div class="panel-body">
                        <div class="row register">
                            <div class="col-lg-12">
                        <?php echo form_input($nombre); ?>
                        <br />
                        <?php echo form_error($nombre['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?><?php echo isset($errors[$nombre['name']]) ? $errors[$nombre['name']]:''; ?>

                        <?php echo form_input($apellido_pa); ?>
                        <br />
                        <?php echo form_error($apellido_pa['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?><?php echo isset($errors[$apellido_pa['name']]) ? $errors[$apellido_pa['name']]:''; ?>

                        <?php echo form_input($apellido_ma); ?>
                        <br />
                        <?php echo form_error($apellido_ma['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?><?php echo isset($errors[$apellido_ma['name']])?$errors[$apellido_ma['name']]:''; ?>

                        <?php if ($use_username) { ?>
                            <?php echo form_input($username); ?>
                            <br />
                            <?php echo form_error($username['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
                        <?php } ?>

                        <?php echo form_input($puesto); ?>
                        <br />
                        <?php echo form_error($puesto['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?><?php echo isset($errors[$puesto['name']])?$errors[$puesto['name']]:''; ?>

                        <?php echo form_input($email); ?>
                        <br />
                        <?php echo form_error($email['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>

                        <?php echo form_password($password); ?>
                        <br />
                        <?php echo form_error($password['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?>

                        <?php echo form_password($confirm_password); ?>
                        <br />
                        <?php echo form_error($confirm_password['name'], '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?>

                        <select name="grupo" id="grupo"  class="form-control" style="color: #555;">
                            <option value="">Grupo</option>
                            <?php foreach($query as $fila) { ?>
                                <option value="<?= $fila->id_grupos ?>" <?php echo set_select('grupo', $fila->id_grupos); ?> ><?= $fila->nombre_grupo ?></option>
                            <?php } ?>
                        </select>

                        <br />
                        <?php echo form_error('grupo', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>'); ?>

                        <label>Módulos</label>
                        <ul style="list-style: none;">
                            <li>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="select_todos"  />Seleccionar / Quitar Todos
                                </label>
                            </li>
                        </ul>
                        <div class="panel panel-default">
                            <div class="panel-body">
                            <div class="form-group">
                                <!-- Grupo Estructuras -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input type="checkbox"name="datos[estructuras]" id="estructuras" onclick="if(this.checked)document.getElementById('ingresos_maestro').checked=true;document.getElementById('ingresos_estructura').checked=true;document.getElementById('ingresos_adecuaciones').checked=true;document.getElementById('egresos_maestro').checked=true;document.getElementById('egresos_estructura').checked=true;document.getElementById('egresos_adecuaciones').checked=true;" />Estructuras
                                        </label>
                                    </li>
                                    <li>
                                        <ul style="list-style: none;">
                                            <!-- Grupo Estructuras / Ingresos -->
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[ingresos_maestro]" type="checkbox" id="ingresos_maestro" onclick="if(this.checked)document.getElementById('ingresos_estructura').checked=true;document.getElementById('ingresos_adecuaciones').checked=true;" />Ingresos
                                                </label>
                                            </li>
                                            <li>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ingresos_estructura]" type="checkbox" id="ingresos_estructura" onclick="if(this.checked)document.getElementById('ingresos_maestro').checked=true" />Estructura Administrativa
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ingresos_adecuaciones]" type="checkbox" id="ingresos_adecuaciones" onclick="if(this.checked)document.getElementById('ingresos_maestro').checked=true" />Adecuaciones Presupuestarias
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ingresos_consulta_estructura]" type="checkbox" id="ingresos_consulta_estructura" onclick="if(this.checked)document.getElementById('ingresos_maestro').checked=true" />Consulta de partidas
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- Grupo Estructuras / Egresos -->
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[egresos_maestro]" type="checkbox" id="egresos_maestro" onclick="if(this.checked)document.getElementById('egresos_estructura').checked=true;document.getElementById('egresos_adecuaciones').checked=true;" />Egresos
                                                </label>
                                            </li>
                                            <li>
                                            <ul style="list-style: none;">
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[egresos_estructura]" type="checkbox" id="egresos_estructura" onclick="if(this.checked)document.getElementById('egresos_maestro').checked=true" />Estructura Administrativa
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[egresos_adecuaciones]" type="checkbox" id="egresos_adecuaciones" onclick="if(this.checked)document.getElementById('egresos_maestro').checked=true" />Adecuaciones Presupuestarias
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[egresos_consulta_estructura]" type="checkbox" id="egresos_consulta_estructura" onclick="if(this.checked)document.getElementById('egresos_maestro').checked=true" />Consulta de partidas
                                                    </label>
                                                </li>
                                            </ul>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Ciclo -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[ciclo_maestro]" type="checkbox" id="ciclo_maestro" onclick="if(this.checked)document.getElementById('precompromiso').checked=true;document.getElementById('terminar_precompromiso').checked=true;document.getElementById('agregar_precompromiso').checked=true;document.getElementById('editar_precompromiso').checked=true;document.getElementById('ver_precompromiso').checked=true;document.getElementById('compromiso').checked=true;document.getElementById('agregar_compromiso').checked=true;document.getElementById('editar_compromiso').checked=true;document.getElementById('ver_compromiso').checked=true;document.getElementById('contrarecibo').checked=true;document.getElementById('agregar_contrarecibo').checked=true;document.getElementById('editar_contrarecibo').checked=true;document.getElementById('ver_contrarecibo').checked=true;document.getElementById('tesoreria').checked=true;document.getElementById('movimientos_bancarios').checked=true;document.getElementById('agregar_movimientos_bancarios').checked=true;document.getElementById('editar_movimientos_bancarios').checked=true;document.getElementById('ver_movimientos_bancarios').checked=true;document.getElementById('conciliacion_bancaria').checked=true;document.getElementById('agregar_conciliacion_bancaria').checked=true;document.getElementById('editar_conciliacion_bancaria').checked=true;document.getElementById('ver_conciliacion_bancaria').checked=true;" />Ciclo
                                        </label>
                                    </li>
                                    <li>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[precompromiso]" type="checkbox" id="precompromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Precompromiso
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_precompromiso]" type="checkbox" id="agregar_precompromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Agregar Precompromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_precompromiso]" type="checkbox" id="editar_precompromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Editar Precompromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_precompromiso]" type="checkbox" id="ver_precompromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Ver Precompromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[terminar_precompromiso]" type="checkbox" id="terminar_precompromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Terminar Precompromiso
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[compromiso]" type="checkbox" id="compromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Compromiso
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_compromiso]" type="checkbox" id="agregar_compromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Agregar Compromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_compromiso]" type="checkbox" id="editar_compromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Editar Compromiso
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_compromiso]" type="checkbox" id="ver_compromiso" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Ver Compromiso
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[contrarecibo]" type="checkbox" id="contrarecibo" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Contra recibo de pago
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_contrarecibo]" type="checkbox" id="agregar_contrarecibo" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Agregar Contra Recibo de Pago
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_contrarecibo]" type="checkbox" id="editar_contrarecibo" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Editar Contra Recibo de Pago
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_contrarecibo]" type="checkbox" id="ver_contrarecibo" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Ver Contra Recibo de Pago
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[tesoreria]" type="checkbox" id="tesoreria" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Tesorería
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[movimientos_bancarios]" type="checkbox" id="movimientos_bancarios" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;document.getElementById('tesoreria').checked=true;" />Movimientos Bancarios
                                                        </label>
                                                        <ul style="list-style: none;">
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[agregar_movimientos_bancarios]" type="checkbox" id="agregar_movimientos_bancarios" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />agregar Movimiento Bancario
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[editar_movimientos_bancarios]" type="checkbox" id="editar_movimientos_bancarios" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Editar Movimiento Bancario
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[ver_movimientos_bancarios]" type="checkbox" id="ver_movimientos_bancarios" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Ver Movimiento Bancario
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[conciliacion_bancaria]" type="checkbox" id="conciliacion_bancaria" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;document.getElementById('tesoreria').checked=true;" />Conciliación Bancaria
                                                        </label>
                                                        <ul style="list-style: none;">
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[agregar_conciliacion_bancaria]" type="checkbox" id="agregar_conciliacion_bancaria" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Agregar Conciliación Bancaria
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[editar_conciliacion_bancaria]" type="checkbox" id="editar_conciliacion_bancaria" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Editar Conciliación Bancaria
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox-inline">
                                                                    <input name="datos[ver_conciliacion_bancaria]" type="checkbox" id="ver_conciliacion_bancaria" onclick="if(this.checked)document.getElementById('ciclo_maestro').checked=true;" />Ver Conciliación Bancaria
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                                <!-- Grupo Recaudación -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[recaudacion]" type="checkbox" id="recaudacion" onclick="if(this.checked)document.getElementById('devengado').checked=true;document.getElementById('agregar_devengado').checked=true;document.getElementById('recaudado').checked=true;document.getElementById('subir_devengado').checked=true;document.getElementById('editar_devengado').checked=true;document.getElementById('ver_devengado').checked=true;document.getElementById('agregar_recaudado').checked=true;document.getElementById('subir_recaudado').checked=true;document.getElementById('editar_recaudado').checked=true;document.getElementById('ver_recaudado').checked=true;" />Recaudación
                                        </label>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[devengado]" type="checkbox" id="devengado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Devengado
                                                </label>
                                            </li>
                                            <ul style="list-style: none;">
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[agregar_devengado]" type="checkbox" id="agregar_devengado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Agregar Devengado
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[subir_devengado]" type="checkbox" id="subir_devengado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Subir Devengado
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[editar_devengado]" type="checkbox" id="editar_devengado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Editar Devengado
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="checkbox-inline">
                                                        <input name="datos[ver_devengado]" type="checkbox" id="ver_devengado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Ver Devengado
                                                    </label>
                                                </li>
                                            </ul>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[recaudado]" type="checkbox" id="recaudado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Recaudado
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_recaudado]" type="checkbox" id="agregar_recaudado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Agregar Recaudado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_recaudado]" type="checkbox" id="subir_recaudado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Subir Recaudado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_recaudado]" type="checkbox" id="editar_recaudado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Editar Recaudado
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_recaudado]" type="checkbox" id="ver_recaudado" onclick="if(this.checked)document.getElementById('recaudacion').checked=true;" />Ver Recaudado
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                                <!-- Grupo Contabilidad -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[contabilidad]" type="checkbox" id="contabilidad" onclick="if(this.checked)document.getElementById('plan_cuentas').checked=true;document.getElementById('agregar_datos_cuentas').checked=true;document.getElementById('asientos_polizas').checked=true;document.getElementById('agregar_polizas').checked=true; document.getElementById('editar_polizas').checked=true; document.getElementById('ver_polizas').checked=true;document.getElementById('consulta_balanza').checked=true;document.getElementById('contabilidad_electronica').checked=true;document.getElementById('matriz_conversion').checked=true;document.getElementById('agregar_matriz').checked=true;document.getElementById('agregar_datos_matriz').checked=true;document.getElementById('auxiliar').checked=true;document.getElementById('periodos_contables').checked=true;document.getElementById('agregar_periodo').checked=true; document.getElementById('agregar_periodo_anual').checked=true;" />Contabilidad
                                        </label>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[asientos_polizas]" type="checkbox" id="asientos_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Asientos / Pólizas
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_polizas]" type="checkbox" id="agregar_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Agregar Pólizas
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_polizas]" type="checkbox" id="editar_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Editar Pólizas
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_polizas]" type="checkbox" id="ver_polizas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Ver Pólizas
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[consulta_balanza]" type="checkbox" id="consulta_balanza" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Consulta Balanza
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[auxiliar]" type="checkbox" id="auxiliar" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Auxiliar
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[matriz_conversion]" type="checkbox" id="matriz_conversion" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Matriz de Conversión
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_matriz]" type="checkbox" id="agregar_matriz" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Agregar Matriz
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_datos_matriz]" type="checkbox" id="agregar_datos_matriz" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Agregar, Editar y Eliminar Datos Tabla Matriz
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[plan_cuentas]" type="checkbox" id="plan_cuentas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Plan de cuentas
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_datos_cuentas]" type="checkbox" id="agregar_datos_cuentas" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Agregar, Editar y Eliminar Datos Tabla Cuentas
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[periodos_contables]" type="checkbox" id="periodos_contables" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Períodos Contables
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_periodo]" type="checkbox" id="agregar_periodo" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Agregar Periodos
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_periodo_anual]" type="checkbox" id="agregar_periodo_anual" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Agregar Periodo Anual
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[contabilidad_electronica]" type="checkbox" id="contabilidad_electronica" onclick="if(this.checked)document.getElementById('contabilidad').checked=true;" />Contabilidad Electrónica
                                                </label>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Patrimonio -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[patrimonio]" type="checkbox" id="patrimonio" onclick="if(this.checked)document.getElementById('nota_entrada').checked=true;document.getElementById('nota_salida').checked=true;document.getElementById('kardex').checked=true;document.getElementById('inventarios').checked=true;document.getElementById('agregar_nota_entrada').checked=true;document.getElementById('editar_nota_entrada').checked=true;document.getElementById('ver_nota_entrada').checked=true;document.getElementById('agregar_nota_salida').checked=true;document.getElementById('editar_nota_salida').checked=true;document.getElementById('ver_nota_salida').checked=true; document.getElementById('clasificador_productos').checked=true; document.getElementById('agregar_producto').checked=true; document.getElementById('editar_producto').checked=true; document.getElementById('ver_producto').checked=true; document.getElementById('eliminar_producto').checked=true;"/>Patrimonio
                                        </label>
                                    </li>
                                    <li>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[nota_entrada]" type="checkbox" id="nota_entrada" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Nota de Entrada
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_nota_entrada]" type="checkbox" id="agregar_nota_entrada" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Agregar Nota de Entrada
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_nota_entrada]" type="checkbox" id="editar_nota_entrada" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Editar Nota de Entrada
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_nota_entrada]" type="checkbox" id="ver_nota_entrada" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Ver Nota de Entrada
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[nota_salida]" type="checkbox" id="nota_salida" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Nota de Salida
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_nota_salida]" type="checkbox" id="agregar_nota_salida" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Agregar Nota de Salida
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_nota_salida]" type="checkbox" id="editar_nota_salida" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Editar Nota de Salida
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_nota_salida]" type="checkbox" id="ver_nota_salida" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Ver Nota de Salida
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[kardex]" type="checkbox" id="kardex" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Kardex de Existencias
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[inventarios]" type="checkbox" id="inventarios" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Consulta Inventarios
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[clasificador_productos]" type="checkbox" id="clasificador_productos" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Clasificador de Productos
                                                </label>
                                                <ul style="list-style: none;">
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[agregar_producto]" type="checkbox" id="agregar_producto" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Agregar Producto
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[editar_producto]" type="checkbox" id="editar_producto" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Editar Producto
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[ver_producto]" type="checkbox" id="ver_producto" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Ver Producto
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[eliminar_producto]" type="checkbox" id="eliminar_producto" onclick="if(this.checked)document.getElementById('patrimonio').checked=true;" />Eliminar Producto
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- Grupo Patrimonio -->
                                <!-- Grupo Anteproyecto -->
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[anteproyecto]" type="checkbox" id="anteproyecto" onclick="if(this.checked)document.getElementById('presupuesto_aprobado').checked=true; document.getElementById('subir_anio_anteproyecto').checked=true; document.getElementById('subir_fondos_anteproyecto').checked=true; document.getElementById('aprobado_anteproyecto').checked=true; document.getElementById('indicadores_anteproyecto').checked=true;" />Anteproyecto
                                        </label>
                                        <ul style="list-style: none;">
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[presupuesto_aprobado]" type="checkbox" id="presupuesto_aprobado" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Anteproyecto 2016
                                                </label>
                                                <ul style="list-style: none;" >
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_anio_anteproyecto]" type="checkbox" id="subir_anio_anteproyecto" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Subir Año
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[subir_fondos_anteproyecto]" type="checkbox" id="subir_fondos_anteproyecto" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Subir Fondos
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[aprobado_anteproyecto]" type="checkbox" id="aprobado_anteproyecto" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Aprobado
                                                        </label>
                                                    </li>
                                                </ul>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[indicadores_anteproyecto]" type="checkbox" id="indicadores_anteproyecto" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Indicadores
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[servicios_subrogados_anteproyecto]" type="checkbox" id="servicios_subrogados_anteproyecto" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Servicios Subrogados
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input name="datos[catalogos_anteproyecto]" type="checkbox" id="catalogos_anteproyecto" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Catalogos
                                                </label>
                                                <ul style="list-style: none;" >
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[botones_cc]" type="checkbox" id="botones_cc" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Botones CC (Nuevo/Editar/Borrar)
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="checkbox-inline">
                                                            <input name="datos[botones_partidas]" type="checkbox" id="botones_partidas" onclick="if(this.checked)document.getElementById('anteproyecto').checked=true;" />Botones Partidas (Nuevo/Editar/Borrar)
                                                        </label>
                                                    </li>
                                                </ul>
                                            </li>
                                    </li>
                                </ul>
                                </li>
                                </ul>
                                <ul style="list-style: none; ">
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[reportes]" type="checkbox" id="reportes" />Reportes
                                        </label>
                                    </li>
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[catalogos]" type="checkbox" id="catalogos" />Catálogos
                                        </label>
                                    </li>
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="datos[politicas_manuales]" type="checkbox" id="politicas_manuales" />Políticas / Manuales
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--Firmas-->
                    <label>¿El usuario firma?</label>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <input type="radio" name="firma" value="1">Firma 1<br>
                            <input type="radio" name="firma" value="2">Firma 2<br>
                            <input type="radio" name="firma" value="3">Firma 3
                        </div>
                    </div>

                    <!--Captcha-->
                    <?php if ($captcha_registration) {
                        if ($use_recaptcha) { ?>
                            <div id="recaptcha_image"></div>

                            <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                            <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                            <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>

                            <div class="recaptcha_only_if_image">Enter the words above</div>
                            <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>

                            <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
                            <p style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></p>
                            <?php echo $recaptcha_html; ?>
                        <?php } else { ?>
                            <p>Ingrese el código exactamente como aparece:</p>
                            <?php echo $captcha_html; ?>

                            <?php echo form_label('Código de Confirmación', $captcha['id']); ?>
                            <?php echo form_input($captcha); ?>
                            <p style="color: red;"><?php echo form_error($captcha['name']); ?></p>
                        <?php }
                    } ?>

                    <div class="col-lg-12 text-right" style="margin-bottom: 4%;">
                        <a href="<?= base_url("administrador/usuarios") ?>" class="btn btn-default back"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        <?php echo form_submit( array("name" => "register", "class" => "btn btn-green" ), 'Registrar'); ?>
                        <?php echo form_close(); ?>
                    </div>
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
</div><!-- /#page-wrapper -->

<script src="<?= base_url("assets/templates/front/js/jquery.js") ?>"></script>

<script>
    $('#forma_registrar').on( "submit", function(e) {

        var map = {};
        $(":input").each(function() {

            if ($(this).attr("type") == "checkbox") {

                if($(this).is(':checked')) {
                    var input = $("<input>").attr({"type":"hidden","name":$(this).attr("name")}).val(1);
                    $("#forma_registrar").append(input);
                } else {
                    var input = $("<input>").attr({"type":"hidden","name":$(this).attr("name")}).val(0);
                    $("#forma_registrar").append(input);
                }

            } else if($(this).attr("type") == "radio") {

                if($(this).is(':checked')) {
                    var input = $("<input>").attr({"type":"hidden","name":$(this).attr("name")+""+$(this).val()}).val(1);
                    $("#forma_registrar").append(input);
                } else {
                    var input = $("<input>").attr({"type":"hidden","name":$(this).attr("name")+""+$(this).val()}).val(0);
                    $("#forma_registrar").append(input);
                }
            }
            else {
                var input = $("<input>").attr({"type":"hidden","name":$(this).attr("name")}).val($(this).val());
                $("#forma_registrar").append(input);
            }
        });

        return true;
    });
</script>