<h3 class="page-header center"><i class="fa fa-file-o"></i> Presupuesto PreComprometido</h3>
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            
            <div class="panel panel-default" >
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover ajuste" id="datos_tabla">
                            <thead>
                            <tr>
                                <th width="50">No.</th>
                                <th width="100">Tipo</th>
                                <th width="100">F. Emitido</th>
                                <th width="170">PreCompromiso</th>
                                <th width="170">Compromiso</th>
                                <th width="180">Creado Por</th>
                                <th width="60">Firme</th>
                                <th width="100">Estatus</th>
                                <th width="110">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el precompromiso seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_precompromiso" id="cancelar_precompromiso" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_precompromiso">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_resultado_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_resultado_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <div id="resultado_borrar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Subir Archivo de Precompromiso</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('ciclo/subir_precompromiso', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-default',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_solicitar_terminar" tabindex="-1" role="dialog" aria-labelledby="modal_solicitar_terminar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-flag ic-modal"></i> Solicitar Terminar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_solicitud_terminar" class="control-label">¿Realmente desea dar por terminado el precompromiso seleccionado?</label>
                        <input type="hidden" value="" name="solicitar_terminar_hidden" id="solicitar_terminar_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_solicitar_terminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_terminar" tabindex="-1" role="dialog" aria-labelledby="modal_terminar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-archive ic-modal"></i> Terminar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_terminar" class="control-label">¿Realmente desea dar por terminado el precompromiso seleccionado?</label>
                        <input type="hidden" value="" name="terminar_hidden" id="terminar_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_terminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado" tabindex="-1" role="dialog" aria-labelledby="resultado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-archive ic-modal"></i> Terminar Precompromiso</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" id="mensaje_resultado" class="control-label"></label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->