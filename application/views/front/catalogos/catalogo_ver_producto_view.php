<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Editar Producto</h3>
<div id="page-wrapper">    
    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    General
                </div>
                <div class="panel-body">

                    <div class="row">
                        <!--Primer Columna-->
                        <div class="col-lg-4">

                            <!-- Input de texto Clave -->
                            <div class="form-group">
                                <?= form_label('Clave Producto', 'clave'); ?>
                                <?php
                                    if(isset($articulo)) { ?>

                                    <p class="form-control-static input_view"><?= $articulo ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                            <!-- Input de texto Código -->
                            <div class="form-group">
                                <?= form_label('C&oacute;digo', 'codigo'); ?>
                                <?php
                                if(isset($codigo)) { ?>

                                    <p class="form-control-static input_view"><?= $codigo ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input de texto Unidad de Medida -->
                            <div class="form-group">
                                <?= form_label('Unidad de Medida', 'unidad_medida'); ?>
                                <?php
                                    if(isset($unidad)) { ?>

                                    <p class="form-control-static input_view"><?= $unidad ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                            <!-- Input de texto Último Costo -->
                            <div class="form-group">
                                <?= form_label('&Uacute;ltimo costo', 'ultimo_costo'); ?>
                                <?php
                                if(isset($ultimo_costo)) { ?>

                                    <p class="form-control-static input_view">$ <?= $ultimo_costo ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                        </div>
                        <!--Fin Primer Columna-->

                        <!--Segunda Columna-->
                        <div class="col-lg-5" style="padding-left: 3%;">

                            <!-- Input de texto Tipo / Clase -->
                            <div class="form-group">
                                <?= form_label('Tipo/Clase', 'tipo_clase'); ?>
                                <?php
                                    if(isset($tipo_clase)) { ?>

                                    <p class="form-control-static input_view"><?= $tipo_clase ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                            <!-- Input de texto Partida -->
                            <div class="form-group">
                                <?= form_label('Partida', 'partida'); ?>
                                <?php
                                if(isset($partida)) { ?>

                                    <p class="form-control-static input_view"><?= $partida ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input tipo textarea Descripción principal -->
                            <div class="form-group">
                                <?= form_label('Descripción del Producto', 'descripcion'); ?>
                                <?php
                                if(isset($descripcion)) { ?>

                                    <p class="form-control-static input_view"><?= $descripcion ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                        </div>
                        <!--Fin Segunda Columna-->

                        <!--Tercera Columna-->
                        <div class="col-lg-3">

                            <!-- Input de texto Fecha de Compra -->
                            <div class="form-group">
                                <?= form_label('Fecha de Compra', 'fecha_compra'); ?>
                                <?php
                                if(isset($fecha_compra)) { ?>

                                    <p class="form-control-static input_view"><?= $fecha_compra ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input tipo upload Imagen -->
                            <?= form_label('Imagen Producto', 'imagen'); ?>
                            <div class="form-group">
                                <?php
                                    if(isset($ruta)) { ?>
                                        <img src="<?= $ruta ?>" class="input_view" title="Imagen Producto" />
                                 <?php } ?>
                            </div>
                        </div>
                        <!--Fin Tecera Columna-->
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Seccion de Activo -->
    <div class="row add-pre error-detalle">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Activo
                </div>
                <div class="panel-body">
                    <div class="row">
                        <!--Primer Columna-->
                        <div class="col-lg-4">

                            <!-- Input de texto Clave del Activo -->
                            <div class="form-group">
                                <?= form_label('Clave del Activo', 'clave_activo'); ?>
                                <?php
                                    if(isset($clave_activo)) { ?>

                                    <p class="form-control-static input_view"><?= $clave_activo ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                            <!-- Input de texto Clasificación Patrimonial -->
                            <div class="form-group">
                                <?= form_label('Clasificaci&oacute;n Patrimonial', 'clasificacion_patriomnial'); ?>
                                <?php
                                    if(isset($clasificacion_patrimonial)) { ?>

                                    <p class="form-control-static input_view"><?= $clasificacion_patrimonial ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                            <!-- Input de texto Tipo -->
                            <div class="form-group">
                                <?= form_label('Tipo', 'tipo'); ?>
                                <?php
                                if(isset($tipo)) { ?>

                                    <p class="form-control-static input_view"><?= $tipo ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                        </div>
                        <!--Fin Primer Columna-->

                        <!--Segunda Columna-->
                        <div class="col-lg-5" style="padding-left: 3%;">
                            <!-- Input de texto Marca -->
                            <div class="form-group">
                                <?= form_label('Marca', 'marca'); ?>
                                <?php
                                if(isset($marca)) { ?>

                                    <p class="form-control-static input_view"><?= $marca ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input de texto Modelo -->
                            <div class="form-group">
                                <?= form_label('Modelo', 'modelo'); ?>
                                <?php
                                    if(isset($modelo)) { ?>

                                    <p class="form-control-static input_view"><?= $modelo ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                            <!-- Input de texto Numero de Serie -->
                            <div class="form-group">
                                <?= form_label('No. Serie', 'serie'); ?>
                                <?php
                                    if(isset($serie)) { ?>

                                    <p class="form-control-static input_view"><?= $serie ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                        </div>
                        <!--Fin Segunda Columna-->

                        <!--Tercera Columna-->
                        <div class="col-lg-3">

                            <!-- Input tipo select Tipo de Unidad -->
                            <div class="form-group">
                                <?= form_label('Tipo Unidad', 'tipo_unidad'); ?>
                                <?php
                                    if(isset($tipo_unidad)) { ?>

                                    <p class="form-control-static input_view"><?= $tipo_unidad ?></p>

                                    <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                    <?php } ?>
                            </div>

                        </div>
                        <!--Fin Tecera Columna-->
                    </div>                        
                </div>
            </div>
        </div>
    </div>

    <!-- Seccion de Inventario / Existencia -->
    <div class="row add-pre error-detalle">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Inventario / Existencia
                </div>
                <div class="panel-body">
                    <div class="row">
                        <!--Primer Columna-->
                        <div class="col-lg-4">
                            <!-- Input de texto Localización -->
                            <div class="form-group">
                                <?= form_label('Localizaci&oacute;n', 'localizacion'); ?>
                                <?php
                                if(isset($localizacion)) { ?>

                                    <p class="form-control-static input_view"><?= $localizacion ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input de texto Estante -->
                            <div class="form-group">
                                <?= form_label('Estante', 'estante'); ?>
                                <?php
                                if(isset($estante)) { ?>

                                    <p class="form-control-static input_view"><?= $estante ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input de texto Anaquel -->
                            <div class="form-group">
                                <?= form_label('Anaquel', 'anaquel'); ?>
                                <?php
                                if(isset($anaquel)) { ?>

                                    <p class="form-control-static input_view"><?= $anaquel ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                        </div>
                        <!--Fin Primer Columna-->

                        <!--Segunda Columna-->
                        <div class="col-lg-5" style="padding-left: 3%;">

                            <!-- Input de texto Punto de Reorden -->
                            <div class="form-group">
                                <?= form_label('Punto de Reorden', 'punto_reorden'); ?>
                                <?php
                                if(isset($punto_reorden)) { ?>

                                    <p class="form-control-static input_view"><?= $punto_reorden ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>


                            <!-- Input de texto Area Asignado -->
                            <div class="form-group">
                                <?= form_label('Area Asignado', 'area_asignado'); ?>
                                <?php
                                if(isset($area_asignado)) { ?>

                                    <p class="form-control-static input_view"><?= $area_asignado ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input de texto Nombre Resguardante -->
                            <div class="form-group">
                                <?= form_label('Nombre Resguardante', 'nombre_resguardante'); ?>
                                <?php
                                if(isset($nombre_resguardante)) { ?>

                                    <p class="form-control-static input_view"><?= $nombre_resguardante ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                        </div>
                        <!--Fin Segunda Columna-->

                        <!--Tercera Columna-->
                        <div class="col-lg-3">

                            <!-- Input de texto Existencia -->
                            <div class="form-group">
                                <?= form_label('Existencia', 'existencia'); ?>
                                <?php
                                if(isset($existencia)) { ?>

                                    <p class="form-control-static input_view"><?= $existencia ?></p>

                                <?php } else { ?>

                                    <p class="form-control-static input_view"></p>

                                <?php } ?>
                            </div>

                            <!-- Input de texto Mínimo en Stock -->
                            <div class="form-group">
                                <?= form_label('M&iacute;nimo en Stock', 'minimo_stock'); ?>
                                <?php
                                if(isset($minimo_stock)) { ?>
                                    <p class="form-control-static input_view"><?= $minimo_stock ?></p>
                                <?php } else { ?>
                                    <p class="form-control-static input_view"></p>
                                <?php } ?>
                            </div>

                            <!-- Input de texto Máximo en Stock -->
                            <div class="form-group">
                                <?= form_label('M&aacute;ximo en Stock', 'maximo_stock'); ?>
                                <?php
                                if(isset($maximo_stock)) { ?>
                                    <p class="form-control-static input_view"><?= $maximo_stock ?></p>
                                <?php } else { ?>
                                    <p class="form-control-static input_view"></p>
                                <?php } ?>
                            </div>

                        </div>
                        <!--Fin Tecera Columna-->
                    </div>                        
                </div>
            </div>
        </div>
    </div>

    <!-- Seccion de Depreciacion / Amortizacion -->
    <!-- <div class="row add-pre error-detalle">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Depreciaci&oacute;n / Amortizaci&oacute;n
                </div>
                <div class="panel-body">
                    <div class="row">

                    </div>

                    <div class="row">

                    </div>

                    <div class="row">
                        
                    </div>

                    <div class="row">
                    
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="btns-finales text-center">
        <a href="<?= base_url("patrimonio/catalogo_inventarios") ?>" class="btn btn-default"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
    </div>
</div>