<h3 class="page-header title center"><i class="fa fa-book"></i> Catálogo de Medicamentos</h3>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <form id="forma_exportar" action="<?= base_url("catalogos/exportar_medicamentos") ?>" method="POST">
                <input class="btn btn-default" type="submit" id="exportar_medicamentos" value="Exportar Excel"/>
            </form>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>CUCoP</th>
                                <th>Clave</th>
                                <th>Descripción</th>
                                <th>Presentación</th>
<!--                                <th></th>-->
<!--                                <th>Presentación</th>-->
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->