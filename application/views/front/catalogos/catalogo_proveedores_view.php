    <h3 class="page-header title center"><i class="fa fa-book"></i> Catálogo Proveedores</h3>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <form id="forma_exportar" action="<?= base_url("catalogos/exportar_proveedores") ?>" method="POST">
                    <input class="btn btn-default" type="submit" id="exportar_proveedores" value="Exportar Excel"/>
                </form>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <table id="tabla_proveedores" class="table table-striped table-bordered table-hover datos_tabla">
                                <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Tipo</th>
                                    <th>RFC</th>
                                    <th>CURP</th>
                                    <th>Nombre</th>
                                    <th>Calle</th>
                                    <th>No. Exterior</th>
                                    <th>No. Interior</th>
                                    <th>Colonia</th>
                                    <th>C.P.</th>
                                    <th>Ciudad</th>
                                    <th>Delegación</th>
                                    <th>Estado</th>
                                    <th>No. Cuenta</th>
                                    <th>CLABE</th>
                                    <th>Banco</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->