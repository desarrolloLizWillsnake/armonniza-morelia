<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Agregar Producto</h3>
<div id="page-wrapper">
    <?php
        $forma_atributos = array(
            'class' => 'forma_producto',
            'role' => 'form',
            'id' => 'forma_principal');
        echo form_open_multipart('patrimonio/agregar_producto_inventario', $forma_atributos);
    ?>
    <!-- <form class="forma_producto" role="form" accept=""> -->
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">

                                <!-- Input de texto Clave -->
                                <div class="form-group">
                                    <!--<?= form_label('Clave', 'clave'); ?>-->
                                    <?php

                                        $input_clave = array(
                                            "class" => "form-control",
                                            "name" => "clave",
                                            "id" => "clave",
                                            "placeholder" => "Art&iacute;lo / Clave Producto",
                                            "value" => set_value("clave"),
                                             );
                                        echo form_input($input_clave);
                                    ?>
                                    <?= form_error('clave'); ?>
                                    <!-- Este pedazo lo deje comentado solo por si se necesita dar mas detalle sobre el input de arriba -->
                                    <!-- <p class="help-block">Example block-level help text here.</p> -->
                                </div>

                                <!-- Input de texto Código -->
                                <div class="form-group">
                                    <!--<?= form_label('C&oacute;digo', 'codigo'); ?>-->
                                    <?php
                                    $input_codigo = array(
                                        "class" => "form-control",
                                        "name" => "codigo",
                                        "id" => "codigo",
                                        "placeholder" => "C&oacute;digo de Barras",
                                        "value" => set_value("codigo"),
                                    );
                                    echo form_input($input_codigo);
                                    ?>
                                    <?= form_error('codigo'); ?>
                                </div>

                                <!-- Input de texto Unidad de Medida -->
                                <div class="form-group input-group">
                                    <!--<?= form_label('Unidad de Medida', 'unidad_medida'); ?>-->
                                    <?php
                                        $input_unidad_medida = array(
                                            "class" => "form-control",
                                            "name" => "unidad_medida",
                                            "id" => "unidad_medida",
                                            "placeholder" => "Unidad de Medida",
                                            "value" => set_value("unidad_medida"),
                                            "readonly" => "readonly",
                                             );
                                        echo form_input($input_unidad_medida);
                                    ?>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_unidad_medida"><i class="fa fa-search"></i></button>
                                    </span>
                                    <?= form_error('unidad_medida'); ?>
                                </div>

                                <!-- Input de texto Último Costo -->
                                <div class="form-group input-group">
                                    <!--<?= form_label('&Uacute;ltimo costo', 'ultimo_costo'); ?>-->
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <?php
                                    $input_utlimo_costo = array(
                                        "class" => "form-control dinero",
                                        "name" => "ultimo_costo",
                                        "id" => "ultimo_costo",
                                        "placeholder" => "&Uacute;ltimo Costo",
                                        "value" => set_value("ultimo_costo"),
                                        "style" => "margin-top: .5%;",
                                    );
                                    echo form_input($input_utlimo_costo);
                                    ?>
                                    <?= form_error('ultimo_costo'); ?>
                                </div>

                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">

                                <!-- Input de texto Tipo / Clase -->
                                <div class="form-group">
                                    <!--<?= form_label('Tipo/Clase', 'tipo_clase'); ?>-->
                                    <?php
                                        $input_tipo_clase = array(
                                            "class" => "form-control",
                                            "name" => "tipo_clase",
                                            "id" => "tipo_clase",
                                            "placeholder" => "Tipo / Clase",
                                            "value" => set_value("tipo_clase"),
                                             );
                                        echo form_input($input_tipo_clase);
                                    ?>
                                    <?= form_error('tipo_clase'); ?>
                                    <!-- <p class="help-block">Example block-level help text here.</p> -->
                                </div>

                                <!-- Input de texto Partida -->
                                <div class="form-group input-group">
                                    <!--<?= form_label('Partida', 'partida'); ?>-->
                                    <?php
                                    $input_partida = array(
                                        "class" => "form-control",
                                        "name" => "partida",
                                        "id" => "partida",
                                        "placeholder" => "Partida",
                                        "value" => set_value("partida"),
                                        "readonly" => "readonly",
                                    );
                                    echo form_input($input_partida);
                                    ?>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_partida"><i class="fa fa-search"></i></button>
                                    </span>
                                    <?= form_error('partida'); ?>
                                </div>

                                <!-- Input tipo textarea Descripción principal -->
                                <div class="form-group">
                                    <!--<?= form_label('Descripción', 'descripcion'); ?>-->
                                    <?php
                                    $input_descripcion = array(
                                        "class" => "form-control",
                                        "name" => "descripcion",
                                        "id" => "descripcion",
                                        "placeholder" => "Descripción del Producto",
                                        "value" => set_value("descripcion"),
                                        "style" => "height: 5em;",
                                    );
                                    echo form_textarea($input_descripcion);
                                    ?>
                                    <?= form_error('descripcion'); ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-3">

                                <!-- Input de texto Fecha de Compra -->
                                <div class="form-group">
                                    <?= form_label('Fecha de Compra', 'fecha_compra'); ?>
                                    <?php
                                    $input_fecha_compra = array(
                                        "class" => "form-control ic-calendar",
                                        "name" => "fecha_compra",
                                        "id" => "fecha_compra",
                                        "placeholder" => "Fecha de Compra",
                                        "value" => set_value("fecha_compra"),
                                    );
                                    echo form_input($input_fecha_compra);
                                    ?>
                                    <?= form_error('fecha_compra'); ?>
                                </div>

                                <!-- Input tipo upload Imagen -->
                                <div class="form-group input-group">
                                    <?= form_label('Imagen Producto', 'imagen'); ?>
                                    <?php
                                        $input_subir_imagen = array(
                                            "class" => "form-control",
                                            "name" => "imagen",
                                            "id" => "imagen",
                                            "value" => set_value("imagen"),
                                             );
                                        echo form_upload($input_subir_imagen);
                                    ?>                                    
                                    <?= form_error('imagen'); ?>
                                </div>

                                <div class="form-group">
                                    <?php
                                        if(isset($ruta)) { ?>
                                        <img src="<?= $ruta ?>" title="Imagen Producto" />
                                        <?php }
                                    ?>
                                </div>

                                <div class="form-group">
                                    <?php
                                        if(isset($error_imagen)) {
                                            echo($error_imagen);
                                        }
                                    ?>
                                </div>

                            </div>
                            <!--Fin Tecera Columna-->
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Seccion de Activo -->
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Activo
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">

                                <!-- Input de texto Clave del Activo -->
                                <div class="form-group">
                                    <!--<?= form_label('Clave del Activo', 'clave_activo'); ?>-->
                                    <?php
                                        $input_clave_activo = array(
                                            "class" => "form-control",
                                            "name" => "clave_activo",
                                            "id" => "clave_activo",
                                            "placeholder" => "Clave del Activo",
                                            "value" => set_value("clave_activo"),
                                             );
                                        echo form_input($input_clave_activo);
                                    ?>
                                    <?= form_error('clave_activo'); ?>
                                </div>

                                <!-- Input de texto Clasificación Patrimonial -->
                                <div class="form-group">
                                    <!--<?= form_label('Clasificaci&oacute;n Patrimonial', 'clasificacion_patriomnial'); ?>-->
                                    <?php
                                        $input_clasificacion_patriomnial = array(
                                            "class" => "form-control",
                                            "name" => "clasificacion_patriomnial",
                                            "id" => "clasificacion_patriomnial",
                                            "placeholder" => "Clasificaci&oacute;n Patrimonial",
                                            "value" => set_value("clasificacion_patriomnial"),
                                             );
                                        echo form_input($input_clasificacion_patriomnial);
                                    ?>
                                    <?= form_error('clasificacion_patriomnial'); ?>
                                </div>

                                <!-- Input de texto Tipo -->
                                <div class="form-group">
                                    <!--<?= form_label('Tipo', 'tipo'); ?>-->
                                    <?php
                                    $input_tipo = array(
                                        "class" => "form-control",
                                        "name" => "tipo",
                                        "id" => "tipo",
                                        "placeholder" => "Tipo",
                                        "value" => set_value("tipo"),
                                    );
                                    echo form_input($input_tipo);
                                    ?>
                                    <?= form_error('tipo'); ?>
                                </div>

                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!-- Input de texto Marca -->
                                <div class="form-group">
                                    <!--<?= form_label('Marca', 'marca'); ?>-->
                                    <?php
                                    $input_marca = array(
                                        "class" => "form-control",
                                        "name" => "marca",
                                        "id" => "marca",
                                        "placeholder" => "Marca",
                                        "value" => set_value("marca"),
                                    );
                                    echo form_input($input_marca);
                                    ?>
                                    <?= form_error('marca'); ?>
                                </div>

                                <!-- Input de texto Modelo -->
                                <div class="form-group">
                                    <!--<?= form_label('Modelo', 'modelo'); ?>-->
                                    <?php
                                        $input_modelo = array(
                                            "class" => "form-control",
                                            "name" => "modelo",
                                            "id" => "modelo",
                                            "placeholder" => "Modelo",
                                            "value" => set_value("modelo"),
                                             );
                                        echo form_input($input_modelo);
                                    ?>
                                    <?= form_error('modelo'); ?>
                                </div>

                                <!-- Input de texto Numero de Serie -->
                                <div class="form-group">
                                    <!--<?= form_label('No. Serie', 'serie'); ?>-->
                                    <?php
                                        $input_serie = array(
                                            "class" => "form-control",
                                            "name" => "serie",
                                            "id" => "serie",
                                            "placeholder" => "No. Serie",
                                            "value" => set_value("serie"),
                                             );
                                        echo form_input($input_serie);
                                    ?>
                                    <?= form_error('serie'); ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-3">

                                <!-- Input tipo select Tipo de Unidad -->
                                <div class="form-group">
                                    <!--<?= form_label('Tipo Unidad', 'tipo_unidad'); ?>-->
                                    <select class="form-control" name="tipo_unidad">
                                        <option value="">Tipo Unidad</option>
                                        <option value="inventariada" <?= set_select('tipo_unidad', 'inventariada'); ?> >Inventariada</option>
                                        <option value="nueva" <?= set_select('tipo_unidad', 'nueva'); ?> >Nueva</option>
                                    </select>
                                </div>

                            </div>
                            <!--Fin Tecera Columna-->
                        </div>                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Seccion de Inventario / Existencia -->
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Inventario / Existencia
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!-- Input de texto Localización -->
                                <div class="form-group">
                                    <!--<?= form_label('Localizaci&oacute;n', 'localizacion'); ?>-->
                                    <?php
                                    $input_localizacion = array(
                                        "class" => "form-control",
                                        "name" => "localizacion",
                                        "id" => "localizacion",
                                        "placeholder" => "Localizaci&oacute;n",
                                        "value" => set_value("localizacion"),
                                    );
                                    echo form_input($input_localizacion);
                                    ?>
                                    <?= form_error('localizacion'); ?>
                                </div>

                                <!-- Input de texto Estante -->
                                <div class="form-group">
                                    <!--<?= form_label('Estante', 'estante'); ?>-->
                                    <?php
                                    $input_estante = array(
                                        "class" => "form-control",
                                        "name" => "estante",
                                        "id" => "estante",
                                        "placeholder" => "Estante",
                                        "value" => set_value("estante"),
                                    );
                                    echo form_input($input_estante);
                                    ?>
                                </div>

                                <!-- Input de texto Anaquel -->
                                <div class="form-group">
                                    <!--<?= form_label('Anaquel', 'anaquel'); ?>-->
                                    <?php
                                    $input_serie = array(
                                        "class" => "form-control",
                                        "name" => "anaquel",
                                        "id" => "anaquel",
                                        "placeholder" => "Anaquel",
                                        "value" => set_value("anaquel"),
                                    );
                                    echo form_input($input_serie);
                                    ?>
                                    <?= form_error('anaquel'); ?>
                                </div>

                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!-- Input de texto Punto de Reorden -->
                                <div class="form-group">
                                    <!--<?= form_label('Punto de Reorden', 'punto_reorden'); ?>-->
                                    <?php
                                    $input_punto_reorden = array(
                                        "class" => "form-control dinero",
                                        "name" => "punto_reorden",
                                        "id" => "punto_reorden",
                                        "placeholder" => "Punto de Reorden",
                                        "value" => set_value("punto_reorden"),
                                    );
                                    echo form_input($input_punto_reorden);
                                    ?>
                                    <?= form_error('punto_reorden'); ?>
                                </div>

                                <!-- Input de texto Area Asignado -->
                                <div class="form-group">
                                    <!--<?= form_label('Area Asignado', 'area_asignado'); ?>-->
                                    <?php
                                    $input_area_asignado = array(
                                        "class" => "form-control",
                                        "name" => "area_asignado",
                                        "id" => "area_asignado",
                                        "placeholder" => "Area Asignado",
                                        "value" => set_value("area_asignado"),
                                    );
                                    echo form_input($input_area_asignado);
                                    ?>
                                    <?= form_error('area_asignado'); ?>
                                </div>

                                <!-- Input de texto Nombre Resguardante -->
                                <div class="form-group">
                                    <!--<?= form_label('Nombre Resguardante', 'nombre_resguardante'); ?>-->
                                    <?php
                                    $input_nombre_resguardante = array(
                                        "class" => "form-control",
                                        "name" => "nombre_resguardante",
                                        "id" => "nombre_resguardante",
                                        "placeholder" => "Nombre Resguardante",
                                        "value" => set_value("nombre_resguardante"),
                                    );
                                    echo form_input($input_nombre_resguardante);
                                    ?>
                                    <?= form_error('nombre_resguardante'); ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <!-- Input de texto Existencia -->
                                <div class="form-group">
                                    <!--<?= form_label('Existencia', 'existencia'); ?>-->
                                    <?php
                                    $input_existencia = array(
                                        "class" => "form-control dinero",
                                        "name" => "existencia",
                                        "id" => "existencia",
                                        "placeholder" => "Existencia",
                                        "value" => set_value("existencia"),
                                    );
                                    echo form_input($input_existencia);
                                    ?>
                                    <?= form_error('existencia'); ?>
                                </div>

                                <!-- Input de texto Mínimo en Stock -->
                                <div class="form-group">
                                    <!--<?= form_label('M&iacute;nimo en Stock', 'minimo_stock'); ?>-->
                                    <?php
                                    $input_minimo_stock = array(
                                        "class" => "form-control dinero",
                                        "name" => "minimo_stock",
                                        "id" => "minimo_stock",
                                        "placeholder" => "M&iacute;nimo en Stock",
                                        "value" => set_value("minimo_stock"),
                                    );
                                    echo form_input($input_minimo_stock);
                                    ?>
                                    <?= form_error('minimo_stock'); ?>
                                </div>

                                <!-- Input de texto Máximo en Stock -->
                                <div class="form-group">
                                    <!--<?= form_label('M&aacute;ximo en Stock', 'maximo_stock'); ?>-->
                                    <?php
                                    $input_maximo_stock = array(
                                        "class" => "form-control dinero",
                                        "name" => "maximo_stock",
                                        "id" => "maximo_stock",
                                        "placeholder" => "M&aacute;ximo en Stock",
                                        "value" => set_value("maximo_stock"),
                                    );
                                    echo form_input($input_maximo_stock);
                                    ?>
                                    <?= form_error('maximo_stock'); ?>
                                </div>

                            </div>
                            <!--Fin Tecera Columna-->
                        </div>                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Seccion de Depreciacion / Amortizacion -->
        <!-- <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Depreciaci&oacute;n / Amortizaci&oacute;n
                    </div>
                    <div class="panel-body">
                        <div class="row">

                        </div>

                        <div class="row">

                        </div>

                        <div class="row">
                            
                        </div>

                        <div class="row">
                        
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="btns-finales text-center">
            <?php
            if(isset($mensaje)) { ?>
                <div class="col-lg-12 text-center">
                    <div class="text-center" id="resultado_insertar_caratula">
                        <?= $mensaje ?>
                    </div>
                </div>
            <?php }
            ?>
            <a href="<?= base_url("patrimonio/catalogo_inventarios") ?>" class="btn btn-default"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
            <input type="submit" id="guardar_producto" name="guardar_producto" class="btn btn-green" style="border:none;" value="Guardar Producto"/>
        </div>
    </form>
</div>

<!-- Modal Partidas -->
<div class="modal fade" id="modal_partida" tabindex="-1" role="dialog" aria-labelledby="modal_partida" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span> Partidas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_partidas">
                        <thead>
                        <tr>
                            <th>Partida</th>
                            <th>Descripci&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Unidad de Medida -->
<div class="modal fade" id="modal_unidad_medida" tabindex="-1" role="dialog" aria-labelledby="modal_unidad_medida" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-book ic-color"></i> Unidades de Medida</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_unidad_medida">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Descripci&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>