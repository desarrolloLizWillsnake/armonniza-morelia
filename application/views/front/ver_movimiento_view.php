<h3 class="page-header center"><i class="fa fa-eye"></i> Ver Movimiento Bancario</h3>
<div id="page-wrapper">
    <form class="forma_movimiento" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <!---No. Movimiento-->
                                <div class="form-group">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Movimiento</label></div>
                                            <div class="col-lg-6">
                                                <?php if(isset($movimiento)) { ?>
                                                    <p class="form-control-static input_view"><?= $movimiento ?></p>
                                                <?php } else { ?>
                                                    <p class="form-control-static input_view"></p>
                                                <?php }  ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---Cuenta-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label>No. Cuenta</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($cuenta)) { ?>
                                                <p class="form-control-static input_view"><?= $cuenta ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!---Tipo Movimiento-->
                                <div class="form-group">
                                    <label>Tipo Movimiento</label>
                                    <?php if(isset($tipo)) { ?>
                                        <p class="form-control-static input_view"><?= $tipo ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Concepto Bancario-->
                                <label>Concepto Bancario</label>
                                <div class="form-group">
                                    <?php if(isset($concepto)) { ?>
                                        <p class="form-control-static input_view"><?= $concepto ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--No. Cheque-->
                                <label>No. Cheque</label>
                                <div class="form-group">
                                    <?php if(isset($cheque)) { ?>
                                        <p class="form-control-static input_view"><?= $cheque ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--No. Movimiento Bancario-->
                                <label>No. Movimiento Bancario</label>
                                <div class="form-group">
                                    <?php if(isset($movimiento_bancario)) { ?>
                                        <p class="form-control-static input_view"><?= $movimiento_bancario ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>

                            <div class="col-lg-4">

                                <!-- Proveedor-->
                                <div class="form-group">
                                    <label>Proveedor</label>
                                    <?php if(isset($proveedor)) { ?>
                                        <p class="form-control-static input_view"><?= $proveedor ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!-- Importe -->
                                <div class="form-group">
                                    <label>Importe</label>
                                    <?php if(isset($importe)) { ?>
                                        <p class="form-control-static input_view"><?= "$".$this->cart->format_number($importe) ?></p>
                                    <?php } else { ?>
                                        $<p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!-- Descripción-->
                                <div class="form-group">
                                    <label>Concepto Específico</label>
                                    <?php if(isset($concepto_especifico)) { ?>
                                        <p class="form-control-static input_view"><?= $concepto_especifico ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!-- Concepto Específico-->
                                <div class="form-group">
                                    <label>Descripción Movimiento</label>
                                    <?php if(isset($descripcion)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <!--Fecha de emisión-->
                                <div class="form-group">
                                    <label>Fecha Emitido</label>
                                    <?php if(isset($fecha_emision)) { ?>
                                        <p class="form-control-static input_view"><?= $fecha_emision ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Fecha de pago -->
                                <div class="form-group">
                                    <label>Fecha Pago</label>
                                    <?php if(isset($fecha_pago)) { ?>
                                        <p class="form-control-static input_view"><?= $fecha_pago ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Precompromiso en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>¿Movimiento en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row add-pre error-gral">
                <div class="col-lg-12">
                    <div class="btns-finales text-center">
                        <a class="btn btn-default" onclick="window.history.back();"><i class="fa fa-reply ic-color"></i> Regresar</a>
                    </div>
                </div>
            </div>
    </form>
</div>

</div>
<!-- /#page-wrapper -->