<h3 class="page-header title center"><i class="fa fa-tasks"></i> Clasificador de Productos</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
           <?php if($this->utilerias->get_permisos("agregar_producto") || $this->utilerias->get_grupo() == 1){ ?>
                <a href="<?= base_url("patrimonio/agregar_producto_inventario")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Producto</a>
            <?php } ?>

            <a href="<?= base_url("patrimonio/exportar_clasificador_productos") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Clasificador</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="10%">Art&iacute;culo</th>
                                <!-- <th width="10%">C&oacute;digo</th> -->
                                <th width="57%">Descripci&oacute;n</th>
                                <th width="5%">U/M</th>
                                <th width="9%">Existencia</th>
                                <th width="10%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Borrar Producto</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea borrar el producto seleccionado?</label>
                        <input type="hidden" value="" name="producto_borrar" id="producto_borrar" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_producto_borrar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_resultado" tabindex="-1" role="dialog" aria-labelledby="modal_resultado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Resultado Opreaci&oacute;n</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <div id="resultado_operacion"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>