<h3 class="page-header title center"><i class="fa fa-tasks"></i> Nota de Salida</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <?php if($this->utilerias->get_permisos("agregar_nota_salida") || $this->utilerias->get_grupo() == 1){ ?>
                <a href="<?= base_url("patrimonio/agregar_nota_salida")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Nota de Salida</a>
                <a href="<?= base_url("plantillas/plantilla_nota_salida.xls") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Descargar Plantilla</a>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload ic-color"></i> Subir Archivo</button>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="6%">No.</th>
                                <th width="12%">Concepto Salida</th>
                                <th width="10.5%">F. Emisión</th>
                                <th width="11%">Importe Total</th>
                                <th>Creado por</th>
                                <th width="7%">Firme</th>
                                <th width="8%">Estatus</th>
                                <th width="16%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal Borrar -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Nota de Salida</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">&iquest;Realmente desea cancelar la Nota de Salida seleccionada?</label>
                        <input type="hidden" value="" name="cancelar_nota_salida" id="cancelar_nota_salida" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_nota_salida">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subir Archivo -->
<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color ic-modal"></i> Subir Archivo Notas de Salida</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('patrimonio/subir_nota_salida', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>

