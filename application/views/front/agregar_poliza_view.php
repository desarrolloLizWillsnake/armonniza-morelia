<h3 class="page-header title center"><i class="fa fa-plus-circle"></i> Agregar Asiento / Póliza</h3>
<div id="page-wrapper">
    <form class="" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <!--<input type="hidden" name="" id="" value="<?= $ultimo ?>">-->
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!--Tipo de Asiento-->
                                <select class="form-control" id="" name="">
                                    <option value="">Tipo de Póliza</option>
                                    <option value="diario">Diario</option>
                                    <option value="ingresos">Ingresos</option>
                                    <option value="egresos">Egresos</option>
                                </select>

                                <!--Concepto-->
                                <div class="form-group input-group">
                                    <input type="hidden" name="" id="" value=""/>
                                    <input type="text" class="form-control" name="" id="" placeholder="Concepto" disabled />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target=""><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!-- Proveedor -->
                                <div class="form-group input-group">
                                    <input type="hidden" name="id_proveedor" id="id_proveedor" />
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor / Beneficiario" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-3">
                                <!--<label>No. Partida</label>-->
                                <input type="text" class="form-control" name="" id="" placeholder="No. Partida">
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!--<label>Fecha de Solicitud</label>-->
                                <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" >

                                <!--Firmas-->
                                <div class="form-group">
                                    <input type="hidden" name="firma1" id="firma1" value="0" />
                                    <input type="hidden" name="firma2" id="firma2" value="0" />
                                    <input type="hidden" name="firma3" id="firma3" value="0" />
                                    <label style="margin-top:6%;"> Firmas de Autorización</label>
                                    <?php if($this->utilerias->get_firmas() == 1 || $this->utilerias->get_grupo() == 1) { ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma1_check" id="firma1_check">Angélica Gandara</label>
                                        </div>
                                    <?php } ?>
                                    <?php if($this->utilerias->get_firmas() == 2 || $this->utilerias->get_grupo() == 1) { ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma2_check" id="firma2_check">Alberto Medina</label>
                                        </div>
                                    <?php } ?>
                                    <?php if($this->utilerias->get_firmas() == 3 || $this->utilerias->get_grupo() == 1) { ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="firma3_check" id="firma3_check">Rodrigo Soto</label>
                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                            <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <!--Subsidio-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="" id="" placeholder="Subsidio" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>

                                <!--Centro de Costos-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="" id="" placeholder="Centro de Costos" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>

                                <!--Concepto-->
                                <input type="text" class="form-control" name="" id="" placeholder="Concepto">

                                <!--Partida-->
                                <input type="text" class="form-control" name="" id="" placeholder="Partida">
                            </div>
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                               <!--Fecha -->
                                <input type="text" class="form-control ic-calendar hasDatepicker" name="" id="" placeholder="Fecha">

                                <!--No. Cuenta-->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="" id="" placeholder="Cuenta" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>

                                <!--Nombre Cuenta-->
                                <input type="text" class="form-control" name="" id="" placeholder="Nombre Cuenta" disabled>

                                <!--Debe-->
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: -.5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control" name="" id="" placeholder="Debe">
                                </div>

                                <!--Haber-->
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control" name="" id="" placeholder="Haber">
                                </div>



                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_precompromiso_detalle">Guardar</button>
                                <div id="resutado_insertar_detalle"></div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_length" id="dataTables-example_length"><label>Mostrar <select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> registros</label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"><label>Buscar  <input type="search" class="form-control input-sm" aria-controls="dataTables-example"></label></div></div></div><table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" aria-describedby="dataTables-example_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th style="width: 11%;" class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending">No. Cuenta</th>
                                                        <th style="width: 14%;" class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 212px;">Nombre Cuenta</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 195px;">Subsidio</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 136px;">CC</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 136px;">Concepto</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 136px;">Partida</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 136px;">Fecha</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 136px;">Debe</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 136px;">Haber</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 98px;">Acciones</th></tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="gradeA odd">
                                                        <td class="sorting_1"> </td>
                                                        <td class="center"> </td>
                                                        <td class="center "> </td>
                                                        <td class="center "> </td>
                                                        <td class="center "> </td>
                                                        <td class="center"> </td>
                                                        <td class="center "> </td>
                                                        <td class="center "> </td>
                                                        <td class="center "> </td>
                                                        <td class="center "> </td>
                                                    </tr>
                                                    </tbody>
                                                </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">Most0 rando registros del al 0 de un total de 0 registros</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Anterior</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">2</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Siguiente</a></li></ul></div></div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!---Tabla-->
                        <!--
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="subtotal_hidden" id="subtotal_hidden" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_precompromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Subsidio</th>
                                                    <th>C.C.</th>
                                                    <th>Concepto</th>
                                                    <th>Partida</th>
                                                    <th>No. Cuenta</th>
                                                    <th>Nombre Cuenta</th>
                                                    <th>Fecha</th>
                                                    <th>Debe</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <input type="hidden" value="" name="" id="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="form-group c-firme">
                    <h3>¿Asiento en Firme?</h3>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" />Sí
                    </label>
                </div>
            </div>
        </div>

        <div class="btns-finales text-center">
            <a id="" href="<?= base_url("contabilidad/polizas") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
            <a id="" class="btn btn-green" style="border:none;">Guardar</a>
        </div>

    </form>
</div>

<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Nombre Fiscal</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
