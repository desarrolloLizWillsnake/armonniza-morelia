<?php
$this->db->select('*')->from('cat_personas_firma');
$query = $this->db->get();
$resultados = $query->result();
?>
<h3 class="page-header title center" xmlns="http://www.w3.org/1999/html"><i class="fa fa-print"></i> Impresión Precompromiso</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group error-completar">
                <?php if(isset($mensaje)) { ?>
                    <div class="alert alert-danger">
                        <?= $mensaje ?>
                    </div>
                    <div class="text-center">
                        <div class="btns-finales">
                            <a class="btn btn-default" href="<?= base_url("ciclo/precompromiso") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        </div>
                    </div>
                <?php } elseif($tipo_requisicion == "Bienes" || $tipo_requisicion == "Servicio" || $tipo_requisicion == "Obra") { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default datos-requeridos">
                            <div class="panel-body">
                                <form class="" action="<?= base_url("ciclo/imprimir_precompromiso_formato") ?>" method="POST" role="form">
                                    <input type="hidden" name="precompromiso" value="<?= $precompromiso ?>" id="precompromiso" />
                                    <div class="form-group">
                                        <label>Persona que elaboró</label>
                                        <select class="form-control" name="persona" required>
                                        <?php
                                        foreach($resultados as $row) { ?>
                                            <option value="<?= $row->id_personas_firma ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                    <div class="text-center">
                                        <div class="btns-finales text-center">
                                            <button type="submit" class="btn btn-green">Continuar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } else { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default datos-requeridos">
                            <div class="panel-body">
                                <form class="" action="<?= base_url("ciclo/imprimir_precompromiso_formato") ?>" method="POST" role="form">
                                    <input type="hidden" name="precompromiso" value="<?= $precompromiso ?>" id="precompromiso" />
                                    <div class="form-group">
                                        <label>Vo. Bo.</label>
                                        <select class="form-control" name="persona">
                                            <?php
                                            foreach($resultados as $row) { ?>
                                                <option value="<?= $row->id_personas_firma ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="text-center">
                                        <div class="btns-finales text-center">
                                            <button type="submit" class="btn btn-green">Continuar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>



</div>