<?php
$this->db->select('*')->from('cat_polizas_firmas');
$query = $this->db->get();
$resultados = $query->result();
?>
<h3 class="page-header title center"><i class="fa fa-print"></i> Impresión Póliza</h3>
<div id="page-wrapper">
    <div class="row center">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group error-completar">
                <?php if(isset($mensaje)) { ?>
                    <div class="alert alert-danger">
                        <?= $mensaje ?>
                    </div>
                    <div class="text-center">
                        <div class="btns-finales">
                            <a class="btn btn-default" href="<?= base_url("contabilidad/polizas") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default datos-requeridos">
                                <div class="panel-body">
                                    <form class="" action="<?= base_url("contabilidad/imprimir_poliza_formato") ?>" method="POST" role="form">
                                        <input type="hidden" name="poliza" value="<?= $poliza ?>" id="poliza" />
                                        <div class="form-group">
                                            <label>Elaboró</label>
                                            <select class="form-control" name="persona">
                                                <?php
                                                foreach($resultados as $row) { ?>
                                                    <option value="<?= $row->id_persona ?>"><?= $row->grado_estudio ?> <?= $row->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="text-center">
                                            <div class="btns-finales text-center">
                                                <button type="submit" class="btn btn-green">Continuar</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>



</div>