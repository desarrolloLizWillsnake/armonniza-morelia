<?php

$this->db->select('*')->from('facturas_adjuntas')->where('no_mov', $ticket);
$query = $this->db->get();
$resultado_egresos = $query->result_array();
//$this->debugeo->imprimir_pre($resultado_egresos);

?>
<h3 class="page-header title center"><i class="fa fa-eye"></i> Ver Devengado</h3>
<div id="page-wrapper">
    <form class="" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="ultimo" id="ultimo" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!---No. Devengado-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Devengado</label></div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php if(isset($numero)) { ?>
                                                <p class="form-control-static input_view"><?= $numero ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>

                                    </div>
                                </div>
                                <!--Clasificación-->
                                <div class="form-group">
                                    <label>Clasificación</label>
                                    <?php if(isset($clasificacion)) { ?>
                                        <p class="form-control-static input_view"><?= $clasificacion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--No. Factura-->
                                <div class="form-group">
                                    <label>No. Factura</label>
                                    <?php if(isset($no_movimiento)) { ?>
                                        <p class="form-control-static input_view"><?= $no_movimiento ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Primer Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <!---Clave Cliente-->
                                <div class="form-group">
                                    <label>Clave Cliente</label>
                                    <?php if(isset($clave_cliente)) { ?>
                                        <p class="form-control-static input_view"><?= $clave_cliente ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!---Cliente-->
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <?php if(isset($cliente)) { ?>
                                        <p class="form-control-static input_view"><?= $cliente ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-4">
                                <!---Fecha de Solicitud-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Solicitud</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_solicitud)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_solicitud ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- Descripción General-->
                                <div class="form-group">
                                    <label>Descripción General</label>
                                    <?php if(isset($descripcion)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Precompromiso en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>¿Devengado en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div
                            <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if($resultado_egresos) { ?>
                <div class="row add-pre error-gral">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Facturas de Egresos
                            </div>
                            <div class="panel-body">
                                <?php foreach($resultado_egresos as $key => $value) { ?>
                                <div class="row">
                                    <!--Primer Columna-->
                                    <div class="col-lg-4">
                                        <!--- Ticket de Venta -->
                                        <div class="row">
                                            <div class="col-lg-6"><label>Ticket de Venta</label></div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <p class="form-control-static input_view"><?= $value["no_mov"] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Fecha -->
                                        <div class="form-group">
                                            <label>Factura</label>
                                            <p class="form-control-static input_view"><?= $value["folio"] ?></p>
                                        </div>
                                    </div>
                                    <!--Fin Primer Columna-->
                                    <!--Segunda Columna-->
                                    <div class="col-lg-4">
                                        <!---Clave Cliente-->
                                        <div class="form-group">
                                            <label>Efecto</label>
                                            <?php
                                            if($value["efecto"] == "E") {
                                                $value["efecto"] = "Egresos";
                                            } elseif($efecto == "I") {
                                                $value["efecto"] = "Ingresos";
                                            }
                                            ?>
                                            <p class="form-control-static input_view"><?= $value["efecto"] ?></p>
                                        </div>
                                        <!-- Fecha -->
                                        <div class="form-group">
                                            <label>Fecha</label>
                                            <p class="form-control-static input_view"><?= $value["fecha"] ?></p>
                                        </div>
                                    </div>
                                    <!--Fin Segunda Columna-->
                                    <!--Tercera Columna-->
                                    <div class="col-lg-4">
                                        <?php if($value["mov"]) {?>
                                        <!---Fecha de Solicitud-->
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-6"><label class="label-f">Tipo de Movimiento</label>
                                                </div>
                                                <?php
                                                if($value["mov"] == "R") {
                                                    $value["mov"] = "Refacturaci&oacute;n";
                                                } elseif($value["mov"] == "D") {
                                                    $value["mov"] = "Devoluci&oacute;n";
                                                } elseif($value["mov"] == "C") {
                                                    $value["mov"] = "Cancelaci&oacute;n";
                                                }
                                                ?>
                                                <div class="col-lg-6">
                                                    <p class="form-control-static input_view"><?= $value["mov"] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <!-- Importe -->
                                        <div class="form-group">
                                            <label>Importe</label>
                                            <p class="form-control-static input_view">$<?= number_format($value["impbru"], 2) ?></p>
                                        </div>
                                        <!-- Descuento -->
                                        <div class="form-group">
                                            <label>Descuento</label>
                                            <p class="form-control-static input_view">$<?= number_format($value["descto"], 2) ?></p>
                                        </div>
                                        <!-- I.V.A. -->
                                        <div class="form-group">
                                            <label>I.V.A.</label>
                                            <p class="form-control-static input_view">$<?= number_format($value["iva"], 2) ?></p>
                                        </div>
                                        <!-- Total -->
                                        <div class="form-group">
                                            <label>Total</label>
                                            <p class="form-control-static input_view">$<?= number_format($value["total"], 2) ?></p>
                                        </div>
                                    </div
                                        <!--Fin Tecera Columna-->
                                </div>
                                <?php } ?>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
        <?php } ?>

        <!-- Tabla Detalle -->
        <!--
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <h4 id="suma_total" class="text-center"></h4>
                            <input type="hidden" value="" name="importe_total" id="importe_total" />
                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_devengado">
                                <thead>
                                <th>ID</th>
                                <th width="8%">Gerencia</th>
                                <th width="5%">C.C.</th>
                                <th width="6%">Rubro</th>
                                <th width="5%">Tipo</th>
                                <th width="7%">Clase</th>
                                <th width="9%">F. Aplicada</th>
                                <th width="10%">Forma Pago</th>
                                <th width="11%">Subtotal No Gravado</th>
                                <th width="11%">Descuento No Gravado</th>
                                <th width="11%">Subtotal Gravado</th>
                                <th width="11%">Descuento Gravado</th>
                                <th width="11%">IVA</th>
                                <th width="12%">Importe</th>
                                <th width="9%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        -->

        <!-- Tabla Detalle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <h4 id="suma_total" class="text-center"></h4>
                            <input type="hidden" value="" name="importe_total" id="importe_total" />
                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_devengado">
                                <thead>
                                <th>ID</th>
                                <th width="8%">Gerencia</th>
                                <th width="5%">C.R.</th>
                                <th width="6%">Rubro</th>
                                <th width="5%">Tipo</th>
                                <th width="6%">Clase</th>
                                <th width="9%">F. Aplicada</th>
                                <th width="11%">Forma Pago</th>
                                <th width="5.5%">Cant.</th>
                                <th width="11%">Importe</th>
                                <th width="11%">Descuento</th>
                                <th width="11%">IVA</th>
                                <th width="11%">Total</th>
                                <th width="9%">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <input type="hidden" value="" name="borrar_devengado_hidden" id="borrar_devengado_hidden" />
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="btns-finales text-center">
            <a id="" href="<?= base_url("recaudacion/devengado") ?>" class="btn btn-default"><i class="fa fa-reply ic-color"></i> Regresar</a>
        </div>

    </form>
</div>
