<h3 class="page-header title center"><i class="fa fa-shopping-cart fa-fw"></i> Devengado </h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <a href="<?= base_url("recaudacion/agregar_devengado")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Generar Devengado</a>
            <!--<a href="<?= base_url("recaudacion/exportar_devengado_por_recaudar")?>" class="btn btn-default"><i class="fa fa-plus-circle circle" style="color: #B6CE33;" ></i> Generar Devengado por Recaudar</a>-->
            <?php if($this->utilerias->get_permisos("subir_devengado") || $this->utilerias->get_grupo() == 1) { ?>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload ic-color"></i> Subir Archivo</button>
            <?php } ?>
            <a href="<?= base_url("recaudacion/imprimir_devengados") ?>" class="btn btn-default"><i class="fa fa-print ic-color"></i> Imprimir Movimientos</a>
            <a href="<?= base_url("recaudacion/exportar_devengado") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Movimientos</a>
            <a href="<?= base_url("recaudacion/exportar_devengado_recaudacion_filtro") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Movimientos para Recaudar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <!--<table class="table table-bordered table-hover table-striped datos_tabla">-->
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="5%">No.</th>
                                <th width="11%">Clasificación</th>
                                <th width="8%">Factura</th>
                                <th width="8%">C.R.</th>
                                <th width="10%">F. Solicitud</th>
                                <th width="11%">Importe</th>
                                <th  width="10%">Creado por</th>
                                <th width="6%">Firme</th>
                                <th width="8%">Estatus</th>
                                <th width="17%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar Devengado</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el devengado seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_devengado" id="cancelar_devengado" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_devengado">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_recaudar" tabindex="-1" role="dialog" aria-labelledby="modal_recaudar" aria-hidden="true" style="z-index: 1; margin-top: 5%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus ic-modal"></i> Recaudar</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <h5 for="message-text" class="control-label center">Fecha en que se generará el recaudado</h5>
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6"><input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud"></div>
                            <div class="col-lg-3"></div>
                        </div>
                        <input type="hidden" value="" name="id_recaudar_hidden" id="id_recaudar_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_recaudar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado_poliza" tabindex="-1" role="dialog" aria-labelledby="resultado_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus ic-modal"></i> Resultado Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><p id="mensaje_resultado_poliza"></p></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado_recaudado" tabindex="-1" role="dialog" aria-labelledby="resultado_recaudado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus ic-modal"></i> Resultado Recaudado</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><p id="mensaje_resultado_recaudado"></p></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_poliza" tabindex="-1" role="dialog" aria-labelledby="modal_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-dot-circle-o ic-modal"></i> Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se generara Póliza de Diario correspondiente al Devengado No. <span id="numero_devengado_poliza"></span></label>
                        <input type="hidden" value="" name="numero_devengado_poliza_hidden" id="numero_devengado_poliza_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_generar_poliza">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-indent-left" style="color: #25B7BC;"></span> Subir Archivo(s) de Devengado</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('recaudacion/subir_devengado', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo(s)</span>
                        <?php
                        $data = array(
                            'name'        => 'archivoSubir[]',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                            'multiple' => 'multiple',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo(s) a subir debe(n) de tener la extensión .XLS</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo(s)'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>