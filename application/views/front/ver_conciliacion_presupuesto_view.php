<h3 class="page-header center"><i class="fa fa-eye"></i> Ver Conciliación Bancaria</h3>
<div id="page-wrapper">
    <form class="forma_movimiento" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="movimiento" id="movimiento" value="<?= $movimiento ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                               <!---No. Conciliación-->
                               <div class="row">
                                   <div class="form-group">
                                       <div class="col-lg-8">
                                           <label>No. Conciliación Bancaria</label>
                                       </div>
                                       <div class="col-lg-4">
                                           <?php if(isset($movimiento)) { ?>
                                               <p class="form-control-static input_view"><?= $movimiento ?></p>
                                           <?php } else { ?>
                                               <p class="form-control-static input_view"></p>
                                           <?php }  ?>
                                       </div>
                                   </div>
                               </div>
                                <div class="row">
                                <!---No. Movimiento-->
                                    <div class="form-group">
                                        <div class="col-lg-8">
                                            <label>No. Movimiento Bancario</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <?php if(isset($movimiento)) { ?>
                                                <p class="form-control-static input_view"><?= $movimiento ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!---Cuenta-->
                                <div class="form-group">
                                    <label>Cuenta</label>
                                    <?php if(isset($cuenta)) { ?>
                                        <p class="form-control-static input_view"><?= $cuenta ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!---Tipo Movimiento-->
                                <div class="form-group">
                                    <label>Tipo Movimiento</label>
                                    <?php if(isset($tipo)) { ?>
                                        <p class="form-control-static input_view"><?= $tipo ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Concepto Bancario-->
                                <div class="row">
                                    <label style="width: 100%; padding-left: 4.5%;">Concepto Bancario</label>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <?php if(isset($clave)) { ?>
                                                <p class="form-control-static input_view"><?= trim($clave) ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <?php if(isset($concepto)) { ?>
                                                <p class="form-control-static input_view"><?= $concepto ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">

                                <!-- Proveedor-->
                                <div class="form-group">
                                    <label>Proveedor</label>
                                    <?php if(isset($proveedor)) { ?>
                                        <p class="form-control-static input_view"><?= $proveedor ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Importe -->
                                <div class="form-group">
                                    <label>Importe</label>
                                    <?php if(isset($importe)) { ?>
                                        <p class="form-control-static input_view"><?= "$".$this->cart->format_number($importe) ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Neto -->
                                <div class="form-group">
                                    <label>Neto</label>
                                    <?php if(isset($neto)) { ?>
                                        <p class="form-control-static input_view"><?= "$".$this->cart->format_number($neto) ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>

                            <div class="col-lg-4">
                                <!--Fecha de emisión-->
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-lg-6">
                                            <label>Fecha Emisión</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_emision)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_emision ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fecha de pago -->
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-lg-6">
                                            <label>Fecha Pago</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_pago)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_pago ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Precompromiso en Firme-->
                                <div class="form-group c-firme" style="margin-top: 5%;">
                                    <div class="form-group">
                                        <label>¿Movimiento en Firme?</label>
                                        <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row add-pre error-gral">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Contra Recibos de Pago
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-striped table-bordered table-hover" id="tabla_detalle">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th width="17%">No. Contrarecibo</th>
                                                <th width="50%">Proveedor / Beneficiario</th>
                                                <th>Importe</th>
                                                <th width="12%">Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row add-pre error-gral">
                <div class="col-lg-12">
                    <div class="btns-finales text-center">
                        <a class="btn btn-default" onclick="window.history.back();"><i class="fa fa-reply ic-color"></i> Regresar</a>
                    </div>
                </div>
            </div>
    </form>
</div>

</div>
<!-- /#page-wrapper -->