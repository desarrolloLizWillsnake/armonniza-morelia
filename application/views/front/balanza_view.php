<h3 class="page-header title center"><i class="fa fa-plus-circle"></i> Consulta de Balanza</h3>

<input type="hidden" name="subsidio" value="<?= $subsidio ?>" />
<input type="hidden" name="centro_costos" value="<?= $centro_costos ?>" />
<input type="hidden" name="fecha_inicial" value="<?= $fecha_inicial ?>" />
<input type="hidden" name="fecha_final" value="<?= $fecha_final ?>" />
<input type="hidden" name="cuenta_inicial" value="<?= $cuenta_inicial ?>" />
<input type="hidden" name="cuenta_final" value="<?= $cuenta_final ?>" />
<input type="hidden" name="nivel_cuenta" value="<?= $nivel_cuenta ?>" />

<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <form id="forma_exportar" action="<?= base_url("contabilidad/exportar_consulta_balanza") ?>" method="POST">
                <input type="hidden" name="fecha_inicial" value="<?= $fecha_inicial ?>" />
                <input type="hidden" name="fecha_final" value="<?= $fecha_final ?>" />
                <!--<i class="fa fa-file-excel-o" style="color: #B6CE33;"></i> Exportar Excel</a>-->
                <input class="btn btn-default" type="submit" id="exportar_consulta_balanza" value="Exportar Excel"/>
                <!--<a href="<?= base_url("contabilidad/imprimir_reporte_consulta_balanza") ?>" class="btn btn-default"><i class="fa fa-print circle" style="color: #B6CE33;"></i> Imprimir</a>-->
            </form>
            <br />
            <br />
            <div id="espera"></div>
        </div>
    </div>
    <div class="row add-pre">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>Cuenta</th>
                                <th>Nombre Cuenta</th>
                                <th>Saldo Inicial</th>
                                <th>Cargos</th>
                                <th>Abonos</th>
                                <th>Saldo Final</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>