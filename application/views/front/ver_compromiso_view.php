<h3 class="page-header center"><i class="fa fa-eye"></i> Ver Compromiso</h3>
<div id="page-wrapper">
    <form class="forma_compromiso" role="form">
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_compromiso" id="ultimo_compromiso" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!---No. Compromiso-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Compromiso</label></div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php if(isset($numero_compromiso)) { ?>
                                                <p class="form-control-static input_view"><?= $numero_compromiso ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view2"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!---No. Precompromiso-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Precompromiso</label></div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <?php if(isset($num_precompromiso)) { ?>
                                                <p class="form-control-static input_view"><?= $num_precompromiso ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Tipo de Precompromiso-->
                                <div class="form-group">
                                    <label>Tipo de Compromiso</label>
                                    <?php if(isset($tipo_compromiso)) { ?>
                                        <p id="tipo_compromiso" class="form-control-static input_view"><?= $tipo_compromiso ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!---Concepto Específico -->
                                <div class="form-group">
                                    <label class="label-f">Concepto Específico </label>
                                    <?php if(isset($concepto_especifico)) { ?>
                                        <p class="form-control-static input_view"><?= $concepto_especifico ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!-- Descripción Compromiso-->
                                <div class="form-group">
                                    <label>Justificación</label>
                                    <?php if(isset($descripcion_general)) { ?>
                                        <p class="form-control-static input_view"><?= $descripcion_general ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <!--Proveedor-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Proveedor</label>
                                    <?php if(isset($nombre_proveedor)) { ?>
                                        <p class="form-control-static input_view" ><?= $nombre_proveedor ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>

                                <!--Lugar de Entrega-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Lugar de Entrega</label>
                                    <?php if(isset($lugar_entrega)) { ?>
                                        <p class="form-control-static input_view" ><?= $lugar_entrega ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view" ></p>
                                    <?php }  ?>
                                </div>

                                <!--Condición de Entrega-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Condición de Entrega</label>
                                    <?php if(isset($condicion_entrega)) { ?>
                                        <p class="form-control-static input_view"><?= $condicion_entrega ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Condición de Entrega-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Tipo de Formato Impresión</label>
                                    <?php if(isset($condicion_entrega)) { ?>
                                        <p class="form-control-static input_view"><?= $tipo_impresion ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Tipo Gastos-->
                                <div class="form-group forma_compromis_dato">
                                    <label>Tipo de Gasto</label>
                                    <?php if(isset($tipo_gasto)) { ?>
                                        <p class="form-control-static input_view"><?= $tipo_gasto ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-4">

                                <!--Fecha Solicitud-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Solicitud</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_emision)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_emision ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Fecha Autorización-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Autorizada</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_autoriza)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_autoriza ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Fecha Entrega-->
                                <div class="form-group forma_compromis_dato">
                                    <div class="row">
                                        <div class="col-lg-6"><label class="label-f">Fecha Entrega</label></div>
                                        <div class="col-lg-6">
                                            <?php if(isset($fecha_programada)) { ?>
                                                <p class="form-control-static input_view"><?= $fecha_programada ?></p>
                                            <?php } else { ?>
                                                <p class="form-control-static input_view"></p>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>

                                <!--Compromiso en Firme-->
                                <div class="form-group c-firme" style="margin-top: 8%;">
                                    <label>¿Compromiso en Firme?</label>
                                    <?php if(isset($enfirme) && $enfirme == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-circle i-firmesi"></i></p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-times-circle i-firmeno"></i></p>
                                    <?php }  ?>
                                </div>
                                <!---Firmas de Autorización-->
                                <div class="form-group">
                                    <label style="margin-top:6%;"> Firmas de Autorización</label>
                                    <?php if(isset($firma1) && $firma1 == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-square-o"></i> Autorizado por Programación y Evaluación</p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-square-o"></i> Por autorizar de Programación y Evaluación</p>
                                    <?php }  ?>

                                    <?php if(isset($firma2) && $firma2 == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-square-o"></i> Autorizado por Recursos Materiales</p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-square-o"></i> Por autorizar de Recursos Materiales</p>
                                    <?php }  ?>

                                    <?php if(isset($firma3) && $firma3 == 1) { ?>
                                        <p class="form-control-static"><i class="fa fa-check-square-o"></i> Autorizado por Gerencia Técnica Administrativa</p>
                                    <?php } else { ?>
                                        <p class="form-control-static"><i class="fa fa-square-o"></i> Por autorizar de Gerencia Técnica Administrativa</p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <!--Fin Tercer Columna-->
                        </div>

                        <hr class="forma_fondo" style="display: none;"/>
                        <!--Apartado Datos del Comisionado-->
                        <div class="row forma_fondo" style="display: none;">
                            <div class="col-lg-12 c-firme center">
                                <label>Datos del Comisionado o Beneficiario</label>
                            </div>
                            <div class="col-lg-4">
                                <!---Nombre del Comisionado ó Beneficiario-->
                                <div class="form-group">
                                    <label class="label-f">Nombre Comisionado / Beneficiario</label>
                                    <?php if(isset($nombre_completo)) { ?>
                                        <p class="form-control-static input_view"><?= $nombre_completo ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Número de Empleado-->
                                <div class="form-group">
                                    <label class="label-f">No. Empleado</label>
                                    <?php if(isset($no_empleado)) { ?>
                                        <p class="form-control-static input_view"><?= $no_empleado ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!--Puesto o Categoría-->
                                <div class="form-group">
                                    <label class="label-f">Puesto o Categoría</label>
                                    <?php if(isset($puesto)) { ?>
                                        <p class="form-control-static input_view"><?= $puesto?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                                <!--Área de Adscripción-->
                                <div class="form-group">
                                    <label >Área de Adscripción</label>
                                    <?php if(isset($area)) { ?>
                                        <p class="form-control-static input_view"><?= $area ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!--Clave-->
                                <div class="form-group">
                                    <label>Centro de Costos</label>
                                    <?php if(isset($clave)) { ?>
                                        <p class="form-control-static input_view"><?= $clave ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                        </div>
                        <hr class="forma_fondo" style="display: none;"/>
                        <!--Apartado Características de viáticos-->
                        <div class="row forma_viaticos" style="display: none;">
                            <div class="col-lg-4">
                                <!--Forma de pago-->
                                <div class="form-group">
                                    <label >Forma de pago</label>
                                    <?php if(isset($forma_pago)) { ?>
                                        <p class="form-control-static input_view"><?= $forma_pago ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Medio de Transporte-->
                                <div class="form-group">
                                    <label >Medio de Transporte</label>
                                    <?php if(isset($transporte)) { ?>
                                        <p class="form-control-static input_view"><?= $transporte ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>

                                <!--Grupos Jerárquicos-->
                                <div class="form-group">
                                    <label >Grupos Jerárquicos</label>
                                    <?php if(isset($grupo_jerarquico)) { ?>
                                        <p class="form-control-static input_view"><?= $grupo_jerarquico ?></p>
                                    <?php } else { ?>
                                        <p class="form-control-static input_view"></p>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <!--Tipo de Zona-->
                                <div class="form-group">
                                    <div class="form-group">
                                        <label style="margin-top:6%;">Tipo de Zona</label>
                                        <?php if(isset($zona_marginada) && $zona_marginada == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-square-o"></i> Zona Marginada</p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-square-o"></i> Zona Marginada</p>
                                        <?php }  ?>

                                        <?php if(isset($zona_mas_economica) && $zona_mas_economica == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-square-o"></i> Zona más Económica</p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-square-o"></i> Zona más Económica</p>
                                        <?php }  ?>

                                        <?php if(isset($zona_menos_economica) && $zona_menos_economica == 1) { ?>
                                            <p class="form-control-static"><i class="fa fa-check-square-o"></i> Zona menos Económica</p>
                                        <?php } else { ?>
                                            <p class="form-control-static"><i class="fa fa-square-o"></i> Zona menos Económica</p>
                                        <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="forma_viaticos" style="display: none;"/>

                    </div>
                </div>
            </div>
        </div>

        <!--Tabla Detalle-->
        <div class="row add-pre error-detalle forma_viaticos">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle Viáticos
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total_viaticos" class="text-center"></h4>
                                            <input type="hidden" value="" name="total_viaticos_hidden" id="total_viaticos_hidden" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_viaticos_compromiso">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Lugar de Comisión</th>
                                                    <th width="12%">Fecha Inicio</th>
                                                    <th width="13%">Fecha Término</th>
                                                    <th>Días</th>
                                                    <th width="16%">Cuota Diaria</th>
                                                    <th width="16%">Importe Total</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalles
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">

                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">
                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="subtotal_hidden" id="subtotal_hidden" />
                                            <input type="hidden" value="" name="total_hidden" id="total_hidden" />
                                            <table class="table table-striped table-bordered table-hover" id="tabla_datos_compromiso">
                                                <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>F.F.</th>
                                                    <th>P.F.</th>
                                                    <th>C.C.</th>
                                                    <th>Capítulo</th>
                                                    <th>Concepto</th>
                                                    <th>Partida</th>
                                                    <th>Gasto</th>
                                                    <th>U/M</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio U.</th>
                                                    <th>Subtotal</th>
                                                    <th>IVA</th>
                                                    <th>Importe</th>
                                                    <th>Título</th>
                                                    <th>Descripción</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="btns-finales text-center">
            <a class="btn btn-default" href="<?= base_url("ciclo/compromiso") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
        </div>
    </form>


</div>

</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->