<h3 class="page-header title center"><i class="fa fa-files-o"></i> Estado Anal&iacute;tico del Ejercido Egresos</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("reportes/exportar_reporte_analiticoregresosgasto") ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 3%; width: 50%;">
                    <div class="panel-body">
                        <h4 class="text-center">Cap&iacute;tulo del Gasto</h4>

                        <div class="row" style="margin-top: 5%;">
                            <div class="col-lg-6">
                                <select class="form-control" id="mes_inicial" name="mes_inicial">
                                    <option value="1">Enero</option>
                                    <option value="2">Febrero</option>
                                    <option value="3">Marzo</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Mayo</option>
                                    <option value="6">Junio</option>
                                    <option value="7">Julio</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <select class="form-control" id="mes_final" name="mes_final">
                                    <option value="1">Enero</option>
                                    <option value="2">Febrero</option>
                                    <option value="3">Marzo</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Mayo</option>
                                    <option value="6">Junio</option>
                                    <option value="7">Julio</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                        </div>

                        <!--Tipo Cap�tulo-->
                        <select class="form-control" id="tipo_gasto" name="tipo_gasto" required>
                            <option value="">Selecciona Cap&iacute;tulo</option>
                            <option value="1">1000  Servicios Personales</option>
                            <option value="2">2000  Materiales y Suministros</option>
                            <option value="3">3000  Servicios Generales</option>
                            <option value="4">4000  Transferencias, Asignaciones, Subsidios y Otras Ayudas</option>
                            <option value="5">5000  Bienes Muebles, Inmuebles e Intangibles</option>
                            <option value="6">6000  Inversi&oacute;n P&uacute;blica</option>
                            <option value="7">7000  Inversiones Financieras y Otras Provisiones</option>
                            <option value="9">9000  Deuda P&uacute;blica</option>
                            <option value="10">Todos</option>
                        </select>

                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesPresupuestos") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
