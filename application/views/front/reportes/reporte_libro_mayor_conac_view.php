<h3 class="page-header title center"><i class="fa fa-files-o"></i> Libro Mayor </h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("reportes/imprimir_reporte_libroMayor") ?>" method="POST" id="datos_impresion"  role="form">
        <div class="row add-pre error-gral text-center">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Período Tiempo -->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>
                        <!-- Cuenta -->
                        <div class="form-group input-group">
                            <input type="text" class="form-control" name="cuenta" id="cuenta" placeholder="Cuenta" required/>
                            <span class="input-group-btn ic-buscar-btn">
                                <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesContabilidad") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Modal Cuenta -->
    <div class="modal fade" id="modal_cuenta" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Cuentas Contables</h4>
                </div>
                <div class="modal-body table-gral modal-action modal-prove">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="tabla_cuentas">
                            <thead>
                            <tr>
                                <th>Cuenta</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>