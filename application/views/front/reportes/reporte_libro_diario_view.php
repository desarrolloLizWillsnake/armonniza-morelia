<h3 class="page-header title center"><i class="fa fa-files-o"></i> Libro Diario [CONAC]</h3>
<div id="page-wrapper">
    <form class="" action="" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Tipo Adeacuación-->
                        <select class="form-control" id="tipo_poliza" name="tipo_adecuacion">
                            <option value="">Tipo de Pólizas</option>
                            <option value="Diario">Diario</option>
                            <option value="Ingresos">Ingresos</option>
                            <option value="Egresos">Egresos</option>
                            <option value="Egresos">Todas</option>
                        </select>
                        <!--Rango Fechas-->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>
                        <div class="btns-finales text-center">
                            <input class="btn btn-green" type="submit" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>