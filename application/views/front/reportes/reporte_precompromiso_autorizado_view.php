<h3 class="page-header title center"><i class="fa fa-files-o"></i> Presupuesto Precompromiso vs. Compromiso</h3>
<div id="page-wrapper">
    <form class="" action="<?= base_url("reportes/imprimir_reporte_precompromisosAutorizados") ?>" method="POST" id="datos_impresion" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_inicial" id="fecha_inicial" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="fecha_final" id="fecha_final" placeholder="Fecha Final" >
                            </div>
                        </div>

                        <div class="btns-finales text-center">
                            <a class="btn btn-default" href="<?= base_url("reportes/reportesPresupuestos") ?>"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Continuar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>