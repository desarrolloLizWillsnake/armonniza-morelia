<h3 class="page-header title center"><i class="fa fa-files-o"></i> Libro Mayor [CONAC]</h3>
<div id="page-wrapper">
    <form class="" role="form">
        <div class="row add-pre error-gral text-center">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--<input type="hidden" name="ultimo_pre" id="ultimo_pre" value="<?= $ultimo ?>">-->
                        <!--Rango Cuentas -->

                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="hidden" name="" id="" value=""/>
                                    <input type="text" class="form-control" name="" id="" style="margin-top: -1%;"  placeholder="No. Cuenta Inicial" disabled />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="hidden" name="" id="" value=""/>
                                    <input type="text" class="form-control" name="" id="" style="margin-top: -1%;" placeholder="No. Cuenta Final" disabled />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <!--Período Tiempo -->
                        <div class="row" style="margin-top: 1%;">
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="" id="" placeholder="Fecha Inicial" >
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control ic-calendar" name="" id="" placeholder="Fecha Final" >
                            </div>
                        </div>

                        <!--Importes-->
                        <!--<div class="row text-left">
                            <div class="col-lg-12" style="margin-top: 1%;">
                                <label>Importes</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo_radio" id="optionsRadios2" value="2">Solo presenta importes Debe o Haber con cantidades o saldos
                                    </label>
                                </div>
                            </div>
                        </div>-->

                        <div class="btns-finales text-center">
                            <a href="<?= base_url("contabilidad/consulta_balanza")?>" class="btn btn-green" style="border:none;">Aceptar</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>