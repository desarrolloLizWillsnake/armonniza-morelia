<h3 class="page-header title center"><i class="fa fa-book"></i> Editar Póliza</h3>
<div id="page-wrapper">

    <form class="forma_poliza_general" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">General</div>
                    <div class="panel-body">
                        <input type="hidden" name="ultima_poliza" id="ultima_poliza" value="<?= $numero_poliza ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <!---No. Poliza-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Póliza</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $numero_poliza ?></p></div>
                                        </div>
                                    </div>
                                    <!---No. Poliza Diario-->
                                    <?php if($poliza_contrarecibo != 0) { ?>
                                        <!---No. Movimiento Bancario-->
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-6"><label>No. Póliza de Diario</label></div>
                                                <div class="col-lg-6"><p class="form-control-static input_view"><?= $poliza_contrarecibo ?></p></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!---No. Contra Recibo de Pago-->
                                    <?php if($contrarecibo != 0) { ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Contra Recibo de Pago</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $contrarecibo ?></p></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if($movimiento != 0) { ?>
                                    <!---No. Movimiento Bancario-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Movimiento Bancario</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $movimiento ?></p></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <!---No. Póliza Diario Ingresos-->
                                    <?php if($poliza_devengado != 0) { ?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-6"><label>No. Póliza Diario</label></div>
                                                <div class="col-lg-6"><p class="form-control-static input_view"><?= $poliza_devengado ?></p></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!---No. Devengado-->
                                    <?php if($no_devengado != 0) { ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Devengado</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $no_devengado ?></p></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <!---No. Recaudado-->
                                    <?php if($no_recaudado != 0) { ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6"><label>No. Recaudado</label></div>
                                            <div class="col-lg-6"><p class="form-control-static input_view"><?= $no_recaudado ?></p></div>
                                        </div>
                                    </div>
                                     <?php } ?>
                                    <!-- Tipo de Póliza -->
                                    <select class="form-control" id="tipo_poliza" name="tipo_poliza" style="margin-top: 2.5%;" required>
                                        <option value="">Tipo de Póliza</option>
                                        <option value="Diario"<?= $tipo_poliza == 'Diario' ? ' selected="selected"' : '';?>>Diario</option>
                                        <option value="Ingresos"<?= $tipo_poliza == 'Ingresos' ? ' selected="selected"' : '';?>>Ingresos</option>
                                        <option value="Egresos"<?= $tipo_poliza == 'Egresos' ? ' selected="selected"' : '';?>>Egresos</option>
                                    </select>
                                </div>
                            </div>
                            <!--Fin Primera Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-6">
                                <?php if($proveedor != NULL) { ?>
                                <!-- Proveedor -->
                                <div class="form-group">
                                    <input type="text" class="form-control" name="id_proveedor" id="id_proveedor" placeholder="Clave Proveedor / Cliente" <?= $id_proveedor != NULL ? ' value="'.$id_proveedor.'"' : '';?> />
                                </div>
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor / Cliente" <?= $proveedor != NULL ? ' value="'.$proveedor.'"' : '';?> />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <?php } elseif($cliente != NULL) {?>
                                <!-- Proveedor -->
                                <div class="form-group">
                                    <input type="text" class="form-control" name="id_proveedor" id="id_proveedor" placeholder="Clave Proveedor / Cliente" <?= $clave_cliente != NULL ? ' value="'.$clave_cliente.'"' : '';?> />
                                </div>
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor / Cliente" <?= $cliente != NULL ? ' value="'.$cliente.'"' : '';?> />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <?php } else { ?>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="id_proveedor" id="id_proveedor" placeholder="Clave Proveedor / Cliente"/>
                                    </div>
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor / Cliente" />
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i></button>
                                    </span>
                                    </div>
                                <?php }?>

                                <!-- Importe -->
                                <label>Importe</label>
                                <div class="form-group input-group">
                                <span class="input-group-btn ic-peso-btn">
                                        <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i></button>
                                    </span>
                                    <input style="margin-top: .5%;" type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="importe" id="importe"  placeholder="Importe" value="<?= $importe ?>" required/>
                                </div>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <input ype="text" class="form-control ic-calendar" name="fecha" id="fecha" placeholder="Fecha" <?= $fecha != NULL ? ' value="'.$fecha.'"' : '';?> required />
                                <!---No. Factura-->
                                <div class="form-group">
                                    <label>No. Factura</label>
                                    <input type="text" class="form-control" name="no_movimiento" id="no_movimiento" placeholder="No. Factura" <?= $no_movimiento != NULL ? ' value="'.$no_movimiento.'"' : '';?> required />
                                </div>
                            </div>
                            <!--Fin Tercera Columna-->
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <!--Concepto Específico-->
                                <input type="text" class="form-control" name="concepto_especifico" id="concepto_especifico" placeholder="Concepto Específico" <?= $concepto_especifico != NULL ? ' value="'.$concepto_especifico.'"' : '';?> maxlength="120" required />
                                <!-- Concepto -->
                                <textarea style="height: 9em;" class="form-control" id="concepto" name="concepto" placeholder="Concepto" required <?= $concepto != NULL ? '> '.$concepto.'</textarea>' : '> </textarea>' ;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="row add-pre error-gral">
        <form class="forma_poliza_detalle" role="form">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle de Póliza
                    </div>
                    <div class="panel-body">
                        <!-- Primera Columna -->
                        <div class="row">
                            <div class="col-lg-4">
                                <!-- Cuenta -->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="cuenta" id="cuenta" placeholder="Cuenta" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_cuenta"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!-- Descripción Cuenta -->
                                <input type="text" class="form-control" name="descripcion_cuenta" id="descripcion_cuenta" placeholder="Descripción Cuenta"/>

                                <!-- Debe -->
                                <label style="width: 100%;" class="center">Debe</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-dollar"></i>
                                        </button>
                                    </span>
                                    <input type="text" class="form-control dinero" name="debe" id="debe" placeholder="Debe" required/>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!-- Centro de Costos -->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="centro_costos" id="centro_costos" placeholder="Centro de Costos" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_centro_costo"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- Subsidio -->
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="subsidio" id="subsidio" placeholder="Subsidio" required/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_subsidio"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <!-- Haber -->
                                <label style="width: 100%;" class="center">Haber</label>
                                <div class="form-group input-group">
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-dollar"></i>
                                        </button>
                                    </span>
                                    <input type="text" class="form-control dinero" name="haber" id="haber" placeholder="Haber" required/>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <!-- Concepto Detalle -->
                                <textarea style="height: 10em;" class="form-control" id="concepto_detalle" name="concepto_detalle" placeholder="Concepto Cuenta" required></textarea>
                            </div>
                        </div>
                        <!-- Fin de Primera Columna -->
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_poliza_detalle">Guardar Detalle Póliza</button>
                                <div id="resultado_insertar_detalle"></div>
                            </div>
                        </div>

                        <div class="row center">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-upload ic-color"></i> Subir Archivo</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cuenta</th>
                                <th width="6%">C.C.</th>
                                <th>Partida</th>
                                <th>Subsidio</th>
                                <th>Descripción Cuenta</th>
                                <th width="14%">Debe</th>
                                <th width="14%">Haber</th>
                                <th width="7%">No. Póliza</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <h5 id="sumas_totales" class="text-center"></h5>
                    <input name="debe_hidden" id="debe_hidden" type="hidden" value="0" />
                    <input name="haber_hidden" id="haber_hidden" type="hidden" value="0" />
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body text-center">
            <div class="form-group c-firme">
                <h3>¿Póliza en Firme?</h3>
                <label class="checkbox-inline">
                    <input type="checkbox" id="check_firme" name="check_firme" <?= $enfirme == 1 ? ' checked="checked" disabled' : '';?> />Sí
                </label>
            </div>
        </div>
    </div>
    <div class="btns-finales text-center">
        <div id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("contabilidad/polizas") ?>" ><i class="fa fa-reply ic-color"></i> Regresar</a>
        <input type="submit" id="guardar_poliza" class="btn btn-green" name="guardar_poliza" value="Guardar Póliza"/>
    </div>
    </form>


</div>
</div>


<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cuenta -->
<div class="modal fade" id="modal_cuenta" tabindex="-1" role="dialog" aria-labelledby="modal_cuenta" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Cuentas Contables</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_cuentas">
                        <thead>
                        <tr>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Centros de Costo -->
<div class="modal fade" id="modal_centro_costo" tabindex="-1" role="dialog" aria-labelledby="modal_centro_costo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Centros de Costo</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_centro_costos">
                        <thead>
                        <tr>
                            <th>Centro de Costo</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Subsidio -->
<div class="modal fade" id="modal_subsidio" tabindex="-1" role="dialog" aria-labelledby="modal_subsidio" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Subsidio</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_subsidio">
                        <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Editar Detalle -->
<div class="modal fade modal_editar" tabindex="-1" role="dialog" aria-labelledby="modal_editar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit ic-modal"></i> Editar Detalle</h4>
            </div>
            <div class="modal-body modal_edit_detalle">
                <form role="form">
                    <div class="form-group" style="margin: 0 auto; width: 60%;">
                        <input type="hidden" name="editar_id_detalle_poliza" id="editar_id_detalle_poliza" />
                        <label for="message-text" class="control-label">Cuenta</label>
                        <input class="form-control" type="text" name="editar_cuenta_poliza" id="editar_cuenta_poliza" />
                        <label for="message-text" class="control-label">Centro de Costos</label>
                        <input class="form-control" type="text" value="" name="editar_centro_costo_poliza" id="editar_centro_costo_poliza" />
                        <label for="message-text" class="control-label">Subsidio</label>
                        <input class="form-control" type="text" value="" name="editar_subsidio_poliza" id="editar_subsidio_poliza" />
                        <label for="message-text" class="control-label">Concepto</label>
                        <input class="form-control" type="text" value="" name="editar_concepto_poliza" id="editar_concepto_poliza" />
                        <label for="message-text" class="control-label">Debe</label>
                        <input class="form-control dinero" type="text" value="" name="editar_debe_poliza" id="editar_debe_poliza" />
                        <label for="message-text" class="control-label">Haber</label>
                        <input class="form-control dinero" type="text" value="" name="editar_haber_poliza" id="editar_haber_poliza" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_editar_detalle">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash ic-modal"></i> Eliminar Movimiento</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea eliminar el movimiento seleccionado?</label>
                        <input type="hidden" value="" name="borrar_detalle_poliza" id="borrar_detalle_poliza" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_borrar_movimiento">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subirArchivo" tabindex="-1" role="dialog" aria-labelledby="subirArchivo" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-color"></i> Subir Detalle de Póliza</h4>
            </div>
            <div class="modal-body">
                <form method="POST" class="forma_poliza_detalle_archivo" id="forma_poliza_detalle_archivo" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="form-group center">
                        <div class="fileUpload btn btn-default btn-size">
                            <span>Selecciona Archivo(s)</span>
                            <input type="file" name="archivoSubir" id="archivoSubir" class="upload" accept="application/vnd.ms-excel" required="required" multiple="multiple"  required/>
                        </div>
                        <p class="help-block">El archivo(s) a subir debe(n) de tener la extensión .XLS</p>
                        <div id="mensaje_resultado_detalle_poliza"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                        <input type="button" value="Subir Archivo" class="btn btn-green" required="required" onclick="submitFile();"  />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->