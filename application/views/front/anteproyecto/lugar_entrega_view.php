<h3 class="page-header title center"><i class="fa fa-book"></i> Lugar de Entrega</h3>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="display" id="datos_tabla">
                            <thead>
                            <tr>
                                <th width="15%">Clave</th>
                                <th>Nombre</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
