<h3 class="page-header title center"><i class="fa fa-book"></i> Servicios Subrogados </h3>
<div id="page-wrapper">

    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <form action="<?= base_url("anteproyecto/exportar_subrogados_excel") ?>" name="form" id="datos_subrogados" role="form">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivoSolicitado" data-whatever="SubirArchivo"><i class="fa fa-upload ic-color"></i> Subir Solicitado</button>
                <input clas="btn-subrogados" type="submit" value="Exportar" class="btn btn-default">
            </form>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="display" id="datos_tabla">
                            <thead>
                            <tr>
                                <th style="border-left: 1px dotted #959496" class="border">CC</th>
                                <th class="border">Descripcion CC</th>
                                <th class="border">Partida</th>
                                <th class="border">Descripcion partida</th>
                                <th class="border">Enero</th>
                                <th class="border">Febrero</th>
                                <th class="border">Marzo</th>
                                <th class="border">Abril</th>
                                <th class="border">Mayo</th>
                                <th class="border">Junio</th>
                                <th class="border">Julio</th>
                                <th class="border">Agosto</th>
                                <th class="border">Septiembre</th>
                                <th class="border">Octubre</th>
                                <th class="border">Noviembre</th>
                                <th class="border">Diciembre</th>
                                <th class="border">Total</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>


</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Modal Solicitado -->
<div class="modal fade" id="subirArchivoSolicitado" tabindex="-1" role="dialog" aria-labelledby="subirArchivoSolicitado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload ic-modal"></i> Subir Archivo Solicitado</h4>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array(
                    'role' => 'form',
                );

                echo(form_open_multipart('anteproyecto/subir_solicitado', $attributes));
                ?>
                <div class="form-group center">
                    <div class="fileUpload btn btn-default btn-size">
                        <span>Selecciona Archivo</span>
                        <?php
                        //echo(form_label('', 'cammpoArchivo'));
                        $data = array(
                            'name'        => 'archivoSubir',
                            'id'          => 'archivoSubir',
                            'class'       => 'upload',
                            'accept' => 'application/vnd.ms-excel',
                            'required' => 'required',
                        );

                        echo(form_upload($data));
                        ?>
                    </div>
                    <p class="help-block">El archivo a subir debe de tener la extensi&oacute;n .XLS</p>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <?php
                $atributos_submit = array(
                    'class' => 'btn btn-green',
                    'required' => 'required',
                );

                echo(form_submit($atributos_submit, 'Subir Archivo'));
                ?>
                <?php echo(form_close()); ?>
            </div>
        </div>
    </div>
</div>
<!-- Modal Ver Detalle -->
<div class="modal fade modal_ver_detalle" tabindex="-1" role="dialog" aria-labelledby="modal_ver_detalle" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye ic-modal"></i> Detalle de Fila</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <form role="form">
                            <div class="form-group">
                                <label>Centro de Costos</label>
                                <p class="input_view" id="detalle_centro_costos"></p>
                                <label>Partida</label>
                                <p class="input_view" id="no_partida"></p>
                                <p class="input_view" id="detalle_partida"></p>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-gray" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
            </div>
        </div>
    </div>
</div>








