<h3 class="page-header center"><i class="fa fa-random"></i> Movimientos bancarios</h3>
<input type="hidden" name="cuenta" id="cuenta" value="<?= $id_cuenta ?>" />
<div id="page-wrapper">
    <div class="row cont-btns-c center"  style="margin-top: 1%;">
        <div class="col-lg-12">
            <h3 class="text-center"><?= $banco ?></h3>
            <h4 class="text-center" style="margin-bottom: 3%; color: #848484;"><i class="fa fa-credit-card" style="font-size: 16px;"></i> <?= $cuenta ?></h4>
            <?php if($this->utilerias->get_permisos("agregar_movimientos_bancarios") || $this->utilerias->get_grupo() == 1) {?>
                <a href="" data-toggle="modal" data-target="#modal_tipo" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Agregar Movimiento</a>
            <?php } ?>
            <a href="<?= base_url("ciclo/imprimir_movimiento/".$id_cuenta) ?>" class="btn btn-default"><i class="fa fa-print ic-color"></i> Imprimir Movimientos</a>
            <a href="<?= base_url("ciclo/exportar_movimiento/".$id_cuenta) ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Movimientos</a>
            <a class="btn btn-default" onclick="window.history.back();"><i class="fa fa-reply ic-color"></i> Regresar</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th width="6%">No.</th>
                                <th width="7%">Contra Recibo</th>
                                <th width="20%">Proveedor</th>
                                <th width="10%">Tipo</th>
                                <th width="11%">Concepto</th>
                                <th width="9.5%">F. Emisión</th>
                                <th width="9%">F. Pago</th>
                                <th width="10%">No. Cuenta</th>
                                <th width="6.5%">Firme</th>
                                <th width="16%">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Cancelar movimiento</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el movimiento seleccionado?</label>
                        <input type="hidden" value="" name="cancelar_movimiento" id="cancelar_movimiento" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_movimiento">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_conciliar" tabindex="-1" role="dialog" aria-labelledby="modal_conciliar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Conciliar Movimiento</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se va a conciliar el movimiento Seleccionado</label>
                        <input type="hidden" value="" name="conciliar_movimiento" id="conciliar_movimiento" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_conciliar_movimiento">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Tipo de Movimiento -->
<div class="modal fade" id="modal_tipo" tabindex="-1" role="dialog" aria-labelledby="modal_tipo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="margin-top: 20%; width: 80%; margin: 0 auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-random ic-modal"></i> Tipo de Movimiento</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-pre">
                <form id="forma_tipo_movimiento" action="<?= base_url("ciclo/agregar_movimiento") ?>" method="POST">
                    <input type="hidden" name="hidden_id_cuenta" id="hidden_id_cuenta" value="<?= $id_cuenta ?>" />
                    <h5 style="padding-left: 10%;">¿Que tipo de movimiento va a realizar?</h5>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="tipo_movimiento" id="presupuestario" value="presupuesto" checked>Presupuestario
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="tipo_movimiento" id="directo" value="extra_presupuesto">Extra Presupuestario
                            </label>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" id="crear_movimientos">Crear Movimiento</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modal_poliza" tabindex="-1" role="dialog" aria-labelledby="modal_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-leanpub ic-modal"></i> Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Se generara Póliza de Egresos correspondiente al Movimiento Bancario No. <span id="numero_movimiento_poliza"></span></label>
                        <input type="hidden" value="" name="numero_movimiento_poliza_hidden" id="numero_movimiento_poliza_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_generar_poliza">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade resultado_poliza" tabindex="-1" role="dialog" aria-labelledby="resultado_poliza" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-leanpub ic-modal"></i> Resultado Póliza</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label"><span id="mensaje_resultado_poliza"></span></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->