<h3 class="page-header center"><i class="fa fa-edit"></i> Editar Contra Recibo</h3>
<div id="page-wrapper">
    <form class="forma_contrarecibo" role="form">
        <!--Contenido General-->
        <div class="row add-pre">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        General
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <input type="hidden" name="ultimo_contrarecibo" id="ultimo_contrarecibo" value="<?= $ultimo ?>">
                        <div class="row">
                            <!--Primera Columna-->
                            <div class="col-lg-4">
                                <!--No. Contra Recibo-->
                                <div class="row">
                                    <div class="col-lg-6"><label>No. Contra Recibo</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver"><?= $ultimo ?></p></div>
                                </div>
                                <div class="form-group input-group">
                                    <?php if(isset($numero_compromiso)) { ?>
                                        <input type="text" class="form-control" name="no_compromiso" id="no_compromiso" placeholder="No. Compromiso" value="<?= $numero_compromiso ?>"/>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" name="no_compromiso" id="no_compromiso" placeholder="No. Compromiso"/>
                                    <?php  } ?>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_compromiso"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <?php if(isset($destino)) { ?>
                                    <input type="text" class="form-control" name="destino" id="destino" placeholder="Destino" value="<?= $destino ?>" disabled/>
                                <?php } else { ?>
                                    <input type="text" class="form-control" name="destino" id="destino" placeholder="Destino" disabled/>
                                <?php  } ?>
                                <!-- Concepto Específico -->
                                <?php if(isset($concepto)) { ?>
                                    <textarea style="height: 6.8em;" class="form-control" id="concepto_especifico" name="concepto_especifico" placeholder="Concepto Específico" maxlength="120" disabled><?= $concepto_especifico ?></textarea>
                                <?php } else { ?>
                                    <textarea style="height: 6.8em;" class="form-control" id="concepto_especifico" name="concepto_especifico" placeholder="Concepto Específico" disabled></textarea>
                                <?php  } ?>

                                <?php if(isset($concepto)) { ?>
                                    <textarea style="height: 8em;" class="form-control" id="concepto" name="concepto" placeholder="Concepto" disabled><?= $concepto ?></textarea>
                                <?php } else { ?>
                                    <textarea style="height: 8em;" class="form-control" id="concepto" name="concepto" placeholder="Concepto" disabled></textarea>
                                <?php  } ?>

                            </div>
                            <!--Fin Primera Columna-->
                            <!--Segunda Columna-->
                            <div class="col-lg-4">
                                <div class="form-group input-group forma_normal">
                                    <?php if(isset($id_proveedor)) { ?>
                                        <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="<?= $id_proveedor ?>"/>
                                    <?php } else { ?>
                                        <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="0" />
                                    <?php  } ?>

                                    <?php if(isset($proveedor)) { ?>
                                        <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" value="<?= $proveedor ?>" required/>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" required/>
                                    <?php  } ?>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores">
                                            <i class="fa fa-search"></i></button>
                                    </span>
                                </div>

                                <div class="form-group forma_diferente" style="display: none;">
                                    <?php if(isset($id_persona)) { ?>
                                        <input type="hidden" name="hidden_id_persona" id="hidden_id_persona" value="<?= $id_persona ?>"/>
                                        <input type="hidden" name="hidden_tipo_impresion" id="hidden_tipo_impresion" value="<?= $tipo_impresion ?>"/>
                                    <?php } else { ?>
                                        <input type="hidden" name="hidden_id_persona" id="hidden_id_persona" value="0"/>
                                        <input type="hidden" name="hidden_tipo_impresion" id="hidden_tipo_impresion" />
                                    <?php  } ?>

                                    <?php if(isset($nombre_completo)) { ?>
                                        <input type="text" class="form-control" name="nombre_completo" id="nombre_completo" placeholder="Beneficiario" value="<?= $nombre_completo ?>"/>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" name="nombre_completo" id="nombre_completo" placeholder="Beneficiario"/>
                                    <?php  } ?>
                                </div>

                                <select class="form-control" id="tipo_documento" name="tipo_documento">
                                    <option value="">Comprobante Fiscal</option>
                                    <?php if(isset($tipo_documento) && $tipo_documento == "Factura") { ?>
                                        <option value="Factura" selected>Factura</option>
                                    <?php } else { ?>
                                        <option value="Factura">Factura</option>
                                    <?php  } ?>
                                    <?php if(isset($tipo_documento) && $tipo_documento == "Honorario") { ?>
                                        <option value="Honorario" selected>Honorario</option>
                                    <?php } else { ?>
                                        <option value="Honorario">Honorario</option>
                                    <?php  } ?>
                                    <?php if(isset($tipo_documento) && $tipo_documento == "Nota de Crédito") { ?>
                                        <option value="Nota de Crédito" selected>Nota de Crédito</option>
                                    <?php } else { ?>
                                        <option value="Nota de Crédito">Nota de Crédito</option>
                                    <?php  } ?>
                                    <?php if(isset($tipo_documento) && $tipo_documento == "Nómina") { ?>
                                        <option value="Nómina" selected>Nómina</option>
                                    <?php } else { ?>
                                        <option value="Nómina">Nómina</option>
                                    <?php  } ?>
                                </select>
                                <?php if(isset($documento)) { ?>
                                    <input type="text" class="form-control" name="documento" id="documento" placeholder="No. Comprobante Fiscal" value="<?= $documento ?>" />
                                <?php } else { ?>
                                    <input type="text" class="form-control" name="documento" id="documento" placeholder="No.Comprobante Fiscal" />
                                <?php  } ?>

                                <?php if(isset($descripcion)) { ?>
                                    <textarea style="height: 15.2em;" class="form-control" id="descripcion_general" name="descripcion_general" placeholder="Descripción del Contrarecibo" ><?= $descripcion ?></textarea>
                                <?php } else { ?>
                                    <textarea style="height: 15.2em;" class="form-control" id="descripcion_general" name="descripcion_general" placeholder="Descripción del Contrarecibo" ></textarea>
                                <?php  } ?>
                            </div>
                            <!--Fin Segunda Columna-->
                            <!--Tercer Columna-->
                            <div class="col-lg-4">
                                <?php if(isset($fecha_emision)) { ?>
                                    <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" value="<?= $fecha_emision ?>" />
                                <?php } else { ?>
                                    <input type="text" class="form-control ic-calendar" name="fecha_solicitud" id="fecha_solicitud" placeholder="Fecha de Solicitud" />
                                <?php  } ?>

                                <?php if(isset($fecha_pago)) { ?>
                                    <input type="text" class="form-control ic-calendar" name="fecha_p_pago" id="fecha_p_pago" placeholder="Fecha Probable de Pago" value="<?= $fecha_pago ?>">
                                <?php } else { ?>
                                    <input type="text" class="form-control ic-calendar" name="fecha_p_pago" id="fecha_p_pago" placeholder="Fecha Probable de Pago">
                                <?php  } ?>
                                <?php if(isset($documentacion_anexa)) { ?>
                                    <textarea style="height: 5em;" class="form-control" id="documentacion_anexa" name="documentacion_anexa" placeholder="Documentación Anexa" required><?= $documentacion_anexa ?></textarea>
                                <?php } else { ?>
                                    <textarea style="height: 5em;" class="form-control" id="documentacion_anexa" name="documentacion_anexa" placeholder="Documentación Anexa" required></textarea>
                                <?php  } ?>
                            </div>
                            <!--Fin Tercera Columna-->
                    </div>

                </div>
            </div>

                <div class="row add-pre" id="desglose_honorarios">

                </div>

<div class="row add-pre">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Detalles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-gral">
                                <div class="table-responsive">
                                    <h4 id="suma_total" class="text-center"></h4>
                                    <input type="hidden" value="" name="total_hidden" id="total_hidden" />
                                    <table class="table table-striped table-bordered table-hover" id="tabla_datos_contrarecibo">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th width="15%">No. Compromiso</th>
                                            <th width="18%">Tipo de Compromiso</th>
                                            <th width="13%">Subtotal</th>
                                            <th width="13%">IVA</th>
                                            <th width="13%">Total</th>
                                            <th>Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    <div class="panel panel-default">
        <div class="panel-body text-center">
            <div class="form-group c-firme">
                <h3>¿Contra Recibo en Firme?</h3>
                <?php if(isset($enfirme) && $enfirme == 1) { ?>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" checked disabled/>Sí
                    </label>
                <?php } else { ?>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_firme" name="check_firme" />Sí
                    </label>
                <?php  } ?>
            </div>
        </div>
    </div>
    <div class="btns-finales text-center">
        <div class="text-center" id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("ciclo/contrarecibos") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
        <input type="submit" name="guardar_contrarecibo" id="guardar_contrarecibo" class="btn btn-green" value="Guardar Contra Recibo" />
    </div>

</form>


</div>

</div>
<!-- /.row -->

<!-- Modal Compromiso -->
<div class="modal fade" id="modal_compromiso" tabindex="-1" role="dialog" aria-labelledby="modal_compromiso" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-text-o ic-modal"></i> Compromisos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-10">
                <input type="hidden" name="hidden_no_compromiso" id="hidden_no_compromiso" />
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tabla_compromiso">
                        <thead>
                        <tr>
                            <th width="6%">No.</th>
                            <th width="13%">Precompromiso</th>
                            <th width="12%">F. Autorizado</th>
                            <th width="10%">Tipo</th>
                            <th width="11%">Proveedor</th>
                            <th width="11%">Total</th>
                            <th width="12%">Creado Por</th>
                            <th width="7%">Firme</th>
                            <th width="9%">Estatus</th>
                            <th width="9%">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Proveedores -->
<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Nombre Comercial</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Borrar Detalle -->
<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Cancelar Contrarecibo</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea cancelar el contrarecibo seleccionado?</label>
                        <input type="hidden" value="" name="borrar_contrarecibo_hidden" id="borrar_contrarecibo_hidden" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_cancelar_contrarecibo">Aceptar</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- /#page-wrapper -->