<h3 class="page-header title center"><i class="fa fa-plus-circle"></i>Ventanilla General</h3>
<div id="page-wrapper">
    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Proveedor
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <!--Primera Columna-->
                        <div class="col-lg-3">

                            <div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
                                        </div>
                                        <div class="modal-body table-gral modal-action modal-prove">
                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-4 center">
                                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_proveedores_contrato">Proveedores Con Contrato</button>
                                                </div>
                                                <div class="col-md-4 center">
                                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_proveedores_sincontrato">Proveedores Sin Contrato</button>
                                                </div>
                                                <div class="col-md-2"></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modal_proveedores_contrato" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores_contrato" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores con Contrato</h4>
                                        </div>
                                        <div class="modal-body table-gral modal-action modal-3">
                                            <div class="table-responsive">
                                                <input type="hidden" name="hidden_clave_proveedor" id="hidden_clave_proveedor" value="" />
                                                <table class="table table-striped table-bordered table-hover" id="tabla_proveedores_extranet">
                                                    <thead>
                                                        <tr>
                                                            <th>RCF</th>
                                                            <th>Nombre Comercial</th>
                                                            <th>Contrato</th>
                                                            <th>Fecha de Ingreso</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modal_proveedores_sincontrato" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores_sincontrato" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores sin Contrato</h4>
                                        </div>
                                        <div class="modal-body table-gral modal-action modal-3">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="tabla_proveedores_extranet2">
                                                    <thead>
                                                        <tr>
                                                            <th>RCF</th>
                                                            <th>Nombre Comercial</th>
                                                            <th>Fecha de Ingreso</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="col-lg-9">
                                <!-- Nombre Proveedor -->
                                <div class="form-group input-group">
                                    <input type="hidden" name="rfc_proveedor" id="rfc_proveedor" />
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" disabled/>
                                <span class="input-group-btn ic-buscar-btn">
                                    <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                    </button>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <hr/>
            </div>
        </div>
    </div>

    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Facturas por Autorizar del Proveedor
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body table-gral">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="tabla_facturas">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Folio</th>
                                                    <th>Fecha Emitida</th>
                                                    <th>No. Precompromiso</th>
                                                    <th>Importe Total Factura</th>
                                                    <th>Estatus</th>
                                                    <th>Descripcion</th>
                                                    <th>Clase</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            <thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Datos de Factura
                </div>
                <div class="panel-body table-gral">
                    <input type="hidden" name="id_factura" id="id_factura" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Fecha</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="fecha" id="fecha" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">RFC</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="rfc" id="rfc" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Contrato</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="contrato" id="contrato" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Anexo</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="anexo" id="anexo" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Localidad</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="localidad" id="localidad" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Folio</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="seriefolio" id="folio" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Unidad</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="unidad" id="unidad" disabled/>
                                </div>
                            </div>
<!--                                <div class="row">-->
<!--                                    <div class="col-md-4">-->
<!--                                        <label class="cont-mes">Estado</label>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-8">-->
<!--                                        <input type="text" class="form-control disabled-color" name="estado" id="estado" disabled/>-->
<!--                                    </div>-->
<!--                                </div>-->
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Zona</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="zona" id="zona" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Remisión</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="remision" id="remision" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Importe</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="importe" id="importe" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Clave</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="clave_proveedor" id="clave_proveedor" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Jurisdicción</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="jurisdiccion" id="jurisdiccion" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="cont-mes">Forma de Pago</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control disabled-color" name="formapago" id="formapago" disabled/>
                                </div>
                            </div>
<!--                                <div class="row">-->
<!--                                    <div class="col-md-4">-->
<!--                                        <label class="cont-mes">Colonia</label>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-8">-->
<!--                                        <input type="text" class="form-control disabled-color" name="colonia" id="colonia" disabled/>-->
<!--                                    </div>-->
<!--                                </div>-->
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Enviar Comentarios por E-mail</label>
                                    <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Titulo">
                                    <textarea class="form-control" id="descripcion_archivo" name="descripcion_archivo" placeholder="Descripción" required></textarea>
                                </div>
<!--                                <div class="col-md-6">-->
<!--                                    <label>Observaciones Proveedor</label>-->
<!--                                    <textarea style="height: 8em; margin-top: .5%;" class="form-control" id="observaciones" name="observaciones" disabled></textarea>-->
<!--                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <div class="form-group c-firme">
                        <h3>¿Autorizar Factura?</h3>
                        <div class="radio forma_caratula_normal">
                            <label>
                                <input type="radio" name="radio_autorizar" id="autorizar_si" value="si" checked>Sí
                            </label>
                        </div>
                        <div class="radio forma_caratula_normal">
                            <label>
                                <input type="radio" name="radio_autorizar" id="autorizar_no" value="no">No
                            </label>
                        </div>
                        <div class="radio forma_caratula_normal">
                            <label>
                                <input type="text" class="form-control" name="fecha" id="usuario" placeholder="Usuario" value="<?= $usuario_autoriza ?>"  disabled="disabled">
                            </label>
                        </div>
                        <div class="btns-finales text-center">
                            <div class="text-center" id="mensaje_guardar"></div>
                            <a id="guardar_nota" class="btn btn-green">Guardar Autorización</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Facturas Autorizadas
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body table-gral">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="tabla_facturas_glosa">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Folio</th>
                                                    <th>Fecha Emitida</th>
                                                    <th>No. Precompromiso</th>
                                                    <th>Importe Total Factura</th>
                                                    <th>Estatus</th>
                                                    <th>Descripcion</th>
                                                    <th>Clase</th>
                                                    <th>Autorizado Por</th>
                                                    <th>Fecha / Hora Autorización</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ver Detalle -->
<div class="modal fade modal_ver" tabindex="-1" role="dialog" aria-labelledby="modal_ver" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye ic-modal"></i> Detalle de Factura</h4>
            </div>
            <div class="modal-body">
                <h3>Datos del Emisor</h3>

                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Razón Social</label>
                            <p class="form-control-static input_view" id="nombre_cliente_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>RFC</label>
                            <p class="form-control-static input_view" id="rfc_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Calle</label>
                            <p class="form-control-static input_view" id="calle_emisor_ver"></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Exterior</label>
                            <p class="form-control-static input_view" id="no_exterior_emisor_ver"></p>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Interior</label>
                            <p class="form-control-static input_view" id="no_interior_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Colonia</label>
                            <p class="form-control-static input_view" id="colonia_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Municipio / Delegación</label>
                            <p class="form-control-static input_view" id="municipio_emisor_ver"></p>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <p class="form-control-static input_view" id="estado_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>País</label>
                            <p class="form-control-static input_view" id="pais_emisor_ver"></p>
                        </div>
                    </div>

<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Régimen Fiscal</label>-->
<!--                            <p class="form-control-static input_view" id="regimen_fiscal_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>

                <hr />

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha</label>
                            <p class="form-control-static input_view" id="fecha_emision_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de Comprobante</label>
                            <p class="form-control-static input_view" id="tipo_comprobante_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Número de Comprobante</label>
                            <p class="form-control-static input_view" id="numero_comprobante_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Folio</label>
                            <p class="form-control-static input_view" id="folio_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Método de Pago</label>
                            <p class="form-control-static input_view" id="metodo_pago_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>I.V.A.</label>
                            <p class="form-control-static input_view" id="iva_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tasa de I.V.A</label>
                            <p class="form-control-static input_view" id="tasa_iva_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Subtotal</label>
                            <p class="form-control-static input_view" id="subtotal_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Total</label>
                            <p class="form-control-static input_view" id="total_ver"></p>
                        </div>
                    </div>

                </div>
                
                <hr />
                <h3>Datos del Receptor</h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Razón Social</label>
                            <p class="form-control-static input_view" id="nombre_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>RFC</label>
                            <p class="form-control-static input_view" id="rfc_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Calle</label>
                            <p class="form-control-static input_view" id="calle_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Exterior</label>
                            <p class="form-control-static input_view" id="no_exterior_receptor_ver"></p>
                        </div>
                    </div>

                </div>
                <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Interior</label>
                            <p class="form-control-static input_view" id="no_interior_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Colonia</label>
                            <p class="form-control-static input_view" id="colonia_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Municipio / Delegación</label>
                            <p class="form-control-static input_view" id="municipio_receptor_ver"></p>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <p class="form-control-static input_view" id="estado_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>País</label>
                            <p class="form-control-static input_view" id="pais_receptor_ver"></p>
                        </div>
                    </div>

<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Régimen Fiscal</label>-->
<!--                            <p class="form-control-static input_view" id="regimen_receptor_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>

<!---->
<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>País</label>-->
<!--                            <p class="form-control-static input_view" id="pais_receptor_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <hr />
                <h3>Sellos Digitales</h3>

<!--                <div class="row">-->
<!--                    <div class="col-md-8">-->
<!--                        <div class="form-group">-->
<!--                            <label>No. Certificado</label>-->
<!--                            <p class="form-control-static input_view" id="no_certificado_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>Certificado</label>-->
<!--                            <p class="form-control-static input_view" id="certificado_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>Sello</label>-->
<!--                            <p class="form-control-static input_view" id="sello_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Folio Fiscal</label>
                            <p class="form-control-static input_view" id="uuid_ver"></p>
                        </div>
                    </div>
<!--                    <div class="col-md-8">-->
<!--                        <div class="form-group">-->
<!--                            <label>No. Certificado SAT</label>-->
<!--                            <p class="form-control-static input_view" id="no_certificado_sat_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>Sello SAT</label>-->
<!--                            <p class="form-control-static input_view" id="sello_sat_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>Observaciones del Proveedor</label>-->
<!--                            <p class="form-control-static input_view" id="observaciones_proveedor_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>