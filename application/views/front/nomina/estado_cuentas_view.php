<h3 class="page-header title center"><i class="fa fa-plus-circle"></i>Estado de Cuenta</h3>
<div id="page-wrapper">

    <div class="row cont-btns-c center">
        <button type="button" class="btn btn-default" id="boton_exportar" ><i class="fa fa-download ic-color"></i> Exportar a Excel</button>
    </div>

    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Buscar Proveedor
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="col-lg-9">
                                <div class="form-group input-group">
                                    <input type="hidden" name="rfc_proveedor" id="rfc_proveedor" />
                                    <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Proveedor" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Facturas
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body table-gral">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="tabla_facturas">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Folio</th>
                                                    <th>Fecha</th>
                                                    <th>No. Precompro</th>
                                                    <th>Importe Total Factura</th>
                                                    <th>Estatus</th>
                                                    <th>Descripción</th>
                                                    <th>Clase</th>
                                                </tr>
                                        </table>
                                        <tbody>
                                        </tbody>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-prove">
                <div class="modal-body table-gral modal-action modal-3">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="tabla_proveedores">
                            <thead>
                                <tr>
                                    <th>RFC</th>
                                    <th>Nombre Comercial</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>