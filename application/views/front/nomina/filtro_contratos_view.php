<h3 class="page-header center"><i class="fa fa-users"></i> Consulta de Contratos</h3>

<div id="page-wrapper">
    <form method="POST" id="daros_persona" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">

                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="form-group input-group">
                        <input type="hidden" name="rfc_proveedor" id="rfc_proveedor" />
                        <input type="text" class="form-control" name="rfcproveedor" id="rfcproveedor" placeholder="RFC del Proveedor" disabled/>
                                    <span class="input-group-btn ic-buscar-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                    </div>
<!--                    <div class="form-group input-group">-->
<!--                        <input type="hidden" name="contrato_proveedor" id="contrato_proveedor" />-->
<!--                        <input type="text" class="form-control" name="proveedor" id="proveedor" placeholder="Contrato"/>-->
<!--                                    <span class="input-group-btn ic-buscar-btn">-->
<!--                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_proveedores1"><i class="fa fa-search"></i>-->
<!--                                        </button>-->
<!--                                    </span>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </form>
<br>
    <br>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-gral">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="datos_tabla_contratos">
                                    <thead>
                                    <tr>
                                        <th >RFC</th>
                                        <th >Proveedor</th>
                                        <th >Num. Contrato</th>
                                        <th >Fecha de Contrato</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div class="modal fade" id="modal_proveedores" tabindex="-1" role="dialog" aria-labelledby="modal_proveedores" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i> Proveedores</h4>
                    </div>
                        <div class="modal-body table-gral modal-action modal-prove">
                            <div class="modal-body table-gral modal-action modal-3">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tabla_proveedores_rcf">
                                        <thead>
                                        <tr>
                                            <th>RFC</th>
                                            <th>Nombre Comercial</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
