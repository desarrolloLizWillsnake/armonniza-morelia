<h3 class="page-header center"><i class="fa fa-users"></i>Busqueda de Proveedores</h3>
<div id="page-wrapper">
    <form method="POST" id="daros_persona" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">
                        <!--Fecha de Entrada-->
                        <input type="text" class="form-control ic-calendar" name="fecha_registro" id="fecha_registro" placeholder="Fecha de Entrada" >

                        <!--No. Contrato-->
                        <input type="text" class="form-control" name="no_registro" id="no_registro" placeholder="No. Contrato" >

                        <!--Nombre Proveedor-->
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del Proveedor" >

                        <!--RFC-->
                        <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" placeholder="RFC" >

                        <input type="text" class="form-control" name="id_proveedor" id="id_proveedor" placeholder="Clave Proveedor">

                        <div class="btns-finales text-center">
                            <input class="btn btn-green" type="submit" id="consultar_reporte" value="Buscar"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <br>
    <br>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body table-gral">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="datos_tabla">
                                <thead>
                                <tr>
                                    <th >No.</th>
                                    <th >Fecha</th>
                                    <th >Num. Contrato</th>
                                    <th >Proveedor</th>
                                    <th >Fecha de Emisión</th>
                                    <th >Importe</th>
                                    <th >Estatus</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


