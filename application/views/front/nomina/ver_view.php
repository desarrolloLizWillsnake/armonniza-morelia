<h3 class="page-header title center"><i class="fa fa-plus-circle"></i>Buzón de Sugerencias</h3>
<div id="page-wrapper">
    <form class="forma_nota" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">

        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Registro de Mensajes Proveedores
                    </div>
                    <div class="panel-body">
<!--                        <div class="text-center">-->
<!--                            <a id="btn_detalle" class="btn btn-default"><i class="fa fa-plus-square ic-color"></i> Agregar Detalle</a>-->
<!--                        </div>-->
                        <br>
                        <div id="detalle" style="display: none;">
                            <div class="row">
                                <div class="col-lg-3 niveles-pc">
                                    <!--Estructura Administrativa de Egresos-->
                                    <?= $niveles ?>
                                    <a href="#modal_estructura" class="btn btn-default" data-toggle="modal" data-target="#modal_estructura">¿No conoces la
                                        <br/>estructura?</a>
                                </div>
                                <div class="col-lg-5"></div>
                                <div class="col-lg-4">
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control" style="margin-top: -.5%;" name="gasto" id="gasto" placeholder="Gasto" required/>
                                        <span class="input-group-btn ic-buscar-btn" >
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>

                                    <input id="titulo_gasto" class="form-control" name="titulo_gasto" placeholder="Título" required />

                                    <input type="text" class="form-control" name="u_medida" id="u_medida" placeholder="Unidad de Medida" required />

                                    <input type="text" pattern="[0-9]+" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad" required>

                                    <div class="form-group input-group">
                                        <span class="input-group-btn ic-peso-btn">
                                            <button class="btn btn-default sg-dollar" ><i class="fa fa-dollar"></i>
                                            </button>
                                        </span>
                                        <input type="text" pattern="[0-9]+([\.|,][0-9]+)?" class="form-control dinero" name="precio" id="precio" placeholder="Precio Unitario" required />
                                    </div>

                                    <textarea class="form-control" id="descripcion_detalle" name="descripcion_detalle" placeholder="Descripción" required></textarea>

                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-green btn-block btn-gd" id="guardar_nota_detalle">Guardar Detalle de Nota</button>
                                    <div id="resultado_insertar_detalle"></div>
                                </div>
                            </div>
                        </div>
                        </form>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body table-gral">
                                        <div class="table-responsive">

                                            <h4 id="suma_total" class="text-center"></h4>
                                            <input type="hidden" value="" name="importe_total" id="importe_total" />

                                            <table class="table table-striped table-bordered table-hover" id="tabla_detalle">
                                                <thead>
                                                <tr>
                                                    <th>No. Registro</th>
                                                    <th>Fecha Registro</th>
                                                    <th>Proveedor</th>
                                                    <th>Aviso</th>
                                                </tr>

                                                <?php
                                                if($enlaces != FALSE){
                                                    foreach ($enlaces->result() as $row) {
                                                        echo "<tr>";
                                                        echo "<td>" . $row->clave_prov . "</td>";
                                                        echo "<td>" . $row->nombre . "</td>";
                                                        echo "</tr>";
                                                    }
                                                }else{
                                                        echo "no hay datos en las tablas usudas";
                                                        echo "no hay datos en la base de datos";
                                                    }
                                                ?>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
