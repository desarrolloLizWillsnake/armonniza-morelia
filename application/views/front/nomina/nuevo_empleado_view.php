<h3 class="page-header center"><i class="fa fa-plus-circle circle ic-color"></i> Nuevo Registro</h3>
<div id="page-wrapper">

    <center>
        <div class="">
            <div class="row">
                <div class="">
                    <!--Estructura Administrativa de Egresos-->
                    <div class="col-lg-12">
                        <!--                                    <a href="--><?//= base_url("nomina/nueva_")?><!--" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Nuevo Registro</a>-->
                        <!--                                    <a href="--><?//= base_url("nomina/exportar_empleados") ?><!--" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Datos</a>-->
                        <a href="#modal_nuevo_emp" class="btn btn-default" data-toggle="modal" data-target="#modal_nuevo_emp">Datos Extras</a>
                        <a href="#modal_nuevo_emp2" class="btn btn-default" data-toggle="modal" data-target="#modal_nuevo_emp2">Datos Extras 2</a>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div id="usuario" class="text-center text-muted"></div>
                    <div id="partida"></div>
                </div>
            </div>
        </div>
    </center>

    <form class="forma_empleado" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">

                                <div class="row">
                                    <div class="col-lg-6"><label>No. Registro</label></div>
                                    <div class="col-lg-6"><p class="form-control-static input_ver">11</p></div>
                                </div>

                                <!-- Nombre -->
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre(s)">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Apellido Paterno-->
                                        <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" placeholder="Apellido Paterno">
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Apellido Materno-->
                                        <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" placeholder="Apellido Materno">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--RFC-->
                                        <input type="text" class="form-control" name="rfc" id="rfc" placeholder="R.F.C.">
                                    </div>

                                    <div class="col-lg-6">
                                        <!--CURP-->
                                        <input type="text" class="form-control" name="curp" id="curp" placeholder="Curp">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Teléfono Contacto-->
                                        <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono Contacto">
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Estado del Emepleado-->
                                        <select class="form-control" id="estado_empleado" name="estado_empleado" required>
                                            <option value="">Estado Empleado</option>
                                            <option value="Activo">Activo</option>
                                            <option value="Inactivo">Inactivo</option>
                                            <option value="Pr&aacute;cticas">Pr&aacute;cticas</option>
                                            <option value="Maternidad">Maternidad</option>
                                            <option value="Permiso">Permiso</option>
                                            <option value="Baja">Baja</option>
                                            <option value="Otro">Otro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--Sexo-->
                                        <select class="form-control" id="estado_empleado" name="estado_empleado" required>
                                            <option value="">Sexo</option>
                                            <option value="Femenino">M</option>
                                            <option value="Masculino">H</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <!--Edad-->
                                        <input type="text" class="form-control" name="edad" id="edad" placeholder="Edad">
                                        <input type="text" class="form-control" name="no_emple" id="no_emple" placeholder="Número de Empleado">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 center">
                                <!-- Cargar Fotografía Emepleado -->
                                <script>
                                    $(function() {
                                        // Botón para subir la imagen
                                        var btn_firma = $('#addImage'), interval;
                                        new AjaxUpload('#addImage', {
                                            action: 'includes/uploadFile.php',
                                            onSubmit : function(file , ext){
                                                if (! (ext && /^(jpg|png)$/.test(ext))){
                                                    // extensiones permitidas
                                                    alert('Sólo se permiten Imagenes .jpg o .png');
                                                    // cancela upload
                                                    return false;
                                                } else {
                                                    $('#loaderAjax').show();
                                                    btn_firma.text('Espere por favor');
                                                    this.disable();
                                                }
                                            },
                                        : function(file, response){
                                            // alert(response);
                                            btn_firma.text('Cambiar Imagen');

                                            respuesta = $.parseJSON(response);

                                            if(respuesta.respuesta == 'done'){
                                                $('#fotografia').removeAttr('scr');
                                                $('#fotografia').attr('src','images/' + respuesta.fileName);
                                                $('#loaderAjax').show();
                                                // alert(respuesta.mensaje);
                                            }
                                            else{
                                                alert(respuesta.mensaje);
                                            }

                                            $('#loaderAjax').hide();
                                            this.enable();
                                        }
                                    });
                                    });
                                </script>

                                <header class="headerLayout">
                                    <h4>Fotografía</h4>
                                </header>

                                <section class="contentLayout" id="contentLayout">
                                    <div id="contenedorImagen">
                                        <img id="fotografia" class="fotografia" src="<?= base_url("assets/templates/front/nomina/images/nofoto.jpg") ?>">
                                    </div>
                                    <div style="margin: 5%;">
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#subirArchivo" data-whatever="Subir"><i class="fa fa-refresh ic-color"></i> Actualizar Imagen</button>
                                        <div class="loaderAjax" id="loaderAjax" style="margin: 3%;">
                                            <img src="<?= base_url("assets/templates/front/nomina/images/default-loader.gif") ?>">
                                            <span>Subiendo Imagen...</span>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <h4 class="center"><b>Dirección</b></h4>
                            <div class="col-lg-6">
                                <!-- Calle -->
                                <input type="text" class="form-control" name="calle" id="calle" placeholder="Calle">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- No. Ext. -->
                                        <input type="text" class="form-control" name="no_exterior" id="no_exterior" placeholder="No. Interior">
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- No. Int. -->
                                        <input type="text" class="form-control" name="no_interior" id="no_interior" placeholder="No. Exterior">
                                    </div>
                                </div>

                                <!-- Colonia -->
                                <input type="text" class="form-control" name="colonia" id="colonia" placeholder="Colonia">

                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- País -->
                                        <input type="text" class="form-control" name="pais" id="pais" placeholder="País">
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- Ciudad/Estado -->
                                        <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad/Estado">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- Código Postal -->
                                        <input type="text" class="form-control" name="cp" id="cp" placeholder="Código Postal">
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- Delegación/Municipio -->
                                        <input type="text" class="form-control" name="delegacion" id="delegacion" placeholder="Delegación/Municipio">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-lg-6">

                                <!-- Lugar de Nacimiento -->
                                <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Lugar de Nacimiento">
                                <!-- Fecha de Nacimiento -->
                                <input type="text" class="form-control" name="fechadenacimiento" id="fechadenacimiento" placeholder="Fecha de Nacimiento">
                                <!-- Email -->
                                <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                <!-- Estado Civil -->
                                <select class="form-control" id="edo_civil" name="edo_civil" required>
                                    <option value="">Estado Civil</option>
                                    <option value="Soltero/a">Soltero/a</option>
                                    <option value="Casado/a">Casado/a</option>
                                    <option value="Viudo/a">Viudo/a</option>
                                </select>
                                <!-- Puesto -->
                                <select class="form-control" id="edo_civil" name="edo_civil" required>
                                    <option value="">Puesto</option>
                                    <option value="ADMON. PATRONATO DE LA BENEFICIENCIA PUBLICA">ADMON. PATRONATO DE LA BENEFICIENCIA PUBLICA</option>
                                    <option value="C.S. LA JOYA (PSC), ZITACUARO">C.S. LA JOYA (PSC), ZITACUARO</option>
                                    <option value="CENTRO ESTATAL DE ATENCION ONCOLOGICA">CENTRO ESTATAL DE ATENCION ONCOLOGICA</option>
                                    <option value="CENTRO ESTATAL DE LA TRANSFUSION SANGUINEA">CENTRO ESTATAL DE LA TRANSFUSION SANGUINEA</option>
                                    <option value="CENTRO MICHOACANO DE SALUD MENTAL">CENTRO MICHOACANO DE SALUD MENTAL</option>
                                    <option value="CENTRO REGIONAL DE DRLLO INFANTIL Y ESTIMULACION TEMPRANA">CENTRO REGIONAL DE DRLLO INFANTIL Y ESTIMULACION TEMPRANA</option>
                                    <option value="COORDINACION DE CALIDAD DE LA ATENCION MEDICA">COORDINACION DE CALIDAD DE LA ATENCION MEDICA</option>
                                    <option value="COORDINACION DE INFRAESTRUCTURA HOSPITALARIA">COORDINACION DE INFRAESTRUCTURA HOSPITALARIA</option>
                                    <option value="COORDINACION DE PROGRAMAS DE GRATUIDAD">COORDINACION DE PROGRAMAS DE GRATUIDAD</option>
                                    <option value="CS/H-07 EN COAHUAYANA DE HIDALGO, COAHUAYANA">CS/H-07 EN COAHUAYANA DE HIDALGO, COAHUAYANA</option>
                                    <option value="CS/H-08 EN ARTEAGA, ARTEAGA">CS/H-08 EN ARTEAGA, ARTEAGA</option>
                                </select>
                            </div>

                            <div class="col-lg-6">
                                <!-- N&uacute;mero Seguridad Social -->
                                <input type="text" class="form-control" name="num_seguro_social" id="num_seguro_social" placeholder="N&uacute;mero Seguridad Social">
                                <!-- Descripción -->
                                <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción">

                                <!-- CRESPO -->
                                <input type="text" class="form-control" name="crespo" id="crespo" placeholder="CRESPO">
                                <!-- FEcha Alta -->
                                <input type="text" class="form-control" name="fecha_alta" id="fecha_alta" placeholder="Fecha de Alta" step="1" min="2000-01-01" max="2099-12-31" value="Fecha de Alta">
                                <!-- Fecha Baja -->
                                <input type="text" class="form-control" name="fecha_baja" id="fecha_baja" placeholder="Fecha Baja" step="1" min="2000-01-01" max="2099-12-31" value="Fecha Baja">
                                <!-- Lugar -->
<!--                                <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Lugar">-->
                                <!-- Unidad -->
                                <input type="text" class="form-control" name="unidad" id="unidad" placeholder="Unidad">
                                <!-- Percepcion -->
                                <input type="text" class="form-control" name="percepcion" id="percepcion" placeholder="Percepciones">
                                <!-- Deducciones -->
                                <input type="text" class="form-control" name="deducciones" id="deducciones" placeholder="Deducciones">
                                <!-- Total Neto -->
                                <input type="text" class="form-control" name="total_neto" id="total_neto" placeholder="Total Neto">
                                <!-- jurisdicción -->
<!--                                <input type="text" class="form-control" name="deducciones" id="jurisdiccion" placeholder="Jurisdicción">-->
                                <!-- Total Neto -->
<!--                                <input type="text" class="form-control" name="municipio" id="total_neto" placeholder="Municipio">-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="btns-finales text-center">
        <div class="text-center" id="resultado_insertar_caratula"></div>
        <a class="btn btn-default" href="<?= base_url("nomina/empleados") ?>"><i class="fa fa-reply ic-color"></i> Regresar</a>
        <a id="guardar_nota" class="btn btn-green">Guardar Registro</a>
    </div>

</div>


                                <div class="modal fade" id="modal_nuevo_emp" tabindex="-1" role="dialog" aria-labelledby="modal_nuevo_emp" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Salario</h4>
                                            </div>
                                            <div class="modal-body text-center">

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Prog. Subp.-->
                                                        <input type="text" class="form-control" name="Prog_Subp" id="Prog_Subp" placeholder="Prog. Subp.">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <!--Partida-->
                                                        <input type="text" class="form-control" name="partida" id="partida" placeholder="Partida">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Proy-->
                                                        <input type="text" class="form-control" name="proy" id="proy" placeholder="Proy">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <!--Número de Plaza-->
                                                        <input type="text" class="form-control" name="numero_plaza" id="numero_plaza" placeholder="N&uacute;mero Plaza">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Descripción del Puesto-->
                                                        <input type="text" class="form-control" name="descripcion_puesto" id="descripcion_puesto" placeholder="Descripción del Puesto">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <!--Apellido Materno-->
                                                        <input type="text" class="form-control" name="rama" id="rama" placeholder="Rama">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Fuente de Financiamiento-->
                                                        <input type="text" class="form-control" name="fuente_finan" id="fuente_finan" placeholder="Fuente de Financiamiento">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <!--Tipo de Contrato-->
                                                        <input type="text" class="form-control" name="tipo_cont" id="tipo_cont" placeholder="Tipo de Contrato">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Juridicción-->
                                                        <input type="text" class="form-control" name="juris" id="juris" placeholder="Juridicción">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <!--Tipo de Plaza-->
                                                        <input type="text" class="form-control" name="tipo_plaza" id="tipo_plaza" placeholder="Tipo de Plaza">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Nacionalidad-->
                                                        <input type="text" class="form-control" name="nacional" id="nacional" placeholder="Nacionalidad">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <!--Días Laborales-->
                                                        <input type="text" class="form-control" name="dias_lab" id="dias_lab" placeholder="Días Laborales">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Actividad Principal Desempeño-->
                                                        <input type="text" class="form-control" name="actividad_prin" id="actividad_prin" placeholder="Actividad Principal Desempeño">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <!--Jornada-->
                                                        <input type="text" class="form-control" name="jornada" id="jornada" placeholder="Jornada">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--TU-->
                                                        <input type="text" class="form-control" name="tu" id="tu" placeholder="TU">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <!--Area/Servicio de Trabajo2-->
                                                        <input type="text" class="form-control" name="area_serv" id="area_serv" placeholder="Area/Servicio de Trabajo2">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--CLUES Adscripción Real-->
                                                        <input type="text" class="form-control" name="clues_adscrip" id="clues_adscrip" placeholder="CLUES Adscripción Real">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <!--CLUES Adscripción en Nómina-->
                                                        <input type="text" class="form-control" name="clues_nomina" id="clues_nomina" placeholder="CLUES Adscripción en Nómina">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <!--Entidad Federativa de la Plaza-->
                                                        <input type="text" class="form-control" name="entidad_federal" id="entidad_federal" placeholder="Entidad Federativa de la Plaza">
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <!--Descripción CLUES Real2-->
                                                        <input type="text" class="form-control" name="descripcion_real2" id="descripcion_real2" placeholder="Descripción CLUES Real2">
                                                    </div>
                                                </div>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="datos_guardar">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                                </div>



                                    <div class="modal fade" id="modal_nuevo_emp2" tabindex="-1" role="dialog" aria-labelledby="modal_nuevo_emp2" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span></h4>
                                                </div>
                                                <div class="modal-body text-center">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Descripción CRESPO 2-->
                                                            <input type="text" class="form-control" name="desc_crespo" id="desc_crespo" placeholder="Descripción CRESPO 2">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <!--Actividad Principal Desempeño-->
                                                            <input type="text" class="form-control" name="acti_desemp" id="acti_desemp" placeholder="Actividad Principal Desempeño">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--tipo de recurso2-->
                                                            <input type="text" class="form-control" name="tipo_recurso2" id="tipo_recurso2" placeholder="Tipo de Recurso 2">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <!--Institución de Pertenencia de la Plaza-->
                                                            <input type="text" class="form-control" name="inst_plaza" id="inst_plaza" placeholder="Institución de Pertenencia de la Plaza">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Cédula Profesional-->
                                                            <input type="text" class="form-control" name="cedula_pro" id="cedula_pro" placeholder="Cédula Profesional">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <!--Hora de Entrada-->
                                                            <input type="text" class="form-control" name="hora_ent" id="hora_ent" placeholder="Hora de Entrada">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Hora de Salida-->
                                                            <input type="text" class="form-control" name="hora_sal" id="hora_sal" placeholder="Hora de Salida">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <!--Escolaridad 2-->
                                                            <input type="text" class="form-control" name="escolare2" id="escolar2" placeholder="Escolaridad 2">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Escolaridad 3-->
                                                            <input type="text" class="form-control" name="escolar3" id="escolar3" placeholder="Escolaridad 3">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <!--Cédula Profesional 2-->
                                                            <input type="text" class="form-control" name="cedula_pro2" id="cedula_pro2" placeholder="Cédula Profesional 2">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Título Profesional 2-->
                                                            <input type="text" class="form-control" name="titulo_pro2" id="titulo_pro2" placeholder="Título Profesional 2">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <!--Días Laborales-->
                                                            <input type="text" class="form-control" name="dias_lab" id="dias_lab" placeholder="Días Laborales">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Título Profesional 3-->
                                                            <input type="text" class="form-control" name="titulo_pro3" id="titulo_pro3" placeholder="Título Profesional 3">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <!--Residencia Médica-->
                                                            <input type="text" class="form-control" name="residen_medica" id="residen_medica" placeholder="Residencia Médica">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--RFC-->
                                                            <input type="text" class="form-control" name="ingreso_residen" id="ingreso_residen" placeholder="A&ntilde;o de la Residencia">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <!--Especialidad-->
                                                            <input type="text" class="form-control" name="especialidad" id="especialidad" placeholder="Especialidad">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Certificado Por-->
                                                            <input type="text" class="form-control" name="cert_por" id="cert_por" placeholder="Certificado Por">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <!--Cuenta con FIEL-->
                                                            <input type="text" class="form-control" name="cuenta_fiel" id="cuenta_fiel" placeholder="Cuenta con FIEL">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Vigencia FIEL-->
                                                            <input type="text" class="form-control" name="vigencia_fiel" id="vigencia_fiel" placeholder="Vigencia FIEL">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <!--Estatus del Registro-->
                                                            <input type="text" class="form-control" name="estatus_reg" id="estatus_reg" placeholder="Estatus del Registro">
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <!--Fecha de Ingreso del Registro-->
                                                            <input type="text" class="form-control" name="fecha_ingreso_reg" id="fecha_ingreso_reg" placeholder="Fecha de Ingreso del Registro">
                                                        </div>

                                                        <div class="col-lg-6">

                                                </div>
                                            </div>

                                                <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="datos_guardar1">Guardar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<!---->
<!--<!---->-->
<!--                                    <div class="modal fade" id="modal_nuevo_emp2" tabindex="-1" role="dialog" aria-labelledby="modal_nuevo_emp2" aria-hidden="true">-->
<!--                                        <div class="modal-dialog modal-lg">-->
<!--                                            <div class="modal-content">-->
<!--                                                <div class="modal-header">-->
<!--                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>-->
<!--                                                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-indent-left"></span>Salario</h4>-->
<!--                                                </div>-->
<!--                                                <div class="modal-body text-center">-->
<!---->

<!---->
<!---->
<!--                                                    <div class="modal-footer">-->
<!--                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>-->
<!--                                                        <button type="button" class="btn btn-green" data-dismiss="modal" disabled id="elegir_ultimoNivel">Aceptar</button>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->


