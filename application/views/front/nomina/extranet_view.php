<h3 class="page-header title center"><i class="fa fa-users ic-color"></i>Datos Extranet</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">
        <div class="col-lg-12">
            <a href="<?= base_url("nomina/nuevo_empleado")?>" class="btn btn-default"><i class="fa fa-plus-circle circle ic-color"></i> Nuevo Registro</a>
            <a href="<?= base_url("nomina/exportar_empleados") ?>" class="btn btn-default"><i class="fa fa-download ic-color"></i> Exportar Datos</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datos_tabla">
                            <thead>
                            <tr>
                                <th >Num. de Usuarios</th>
                                <th >RFC</th>
                                <th >Apellido Paterno</th>
                                <th >Apellido Materno</th>
                                <th >Nombre</th>
                                <th >CURP</th>
                                <th >Sexo</th>
                                <th >Fecha Ingreso</th>
                                <th >CRESPO</th>
<!--                                <th >Descripci&oacute;n</th>-->
<!--                                <th >Clave CLUES</th>-->
                                <th >Prog. Subp.</th>
                                <th >Unidad</th>
                                <th >Partida</th>
                                <th >Puesto</th>
                                <th >Proy</th>
                                <th >N&uacute;mero Plaza</th>
                                <th >Descripci&oacute;n del Puesto</th>
                                <th >Rama</th>
                                <th >Fuente de Financiamiento</th>
                                <th >Tipo de Contrato</th>
                                <th >Juridicción</th>
                                <th >Municipio</th>
                                <th >Tipo de Plaza</th>
                                <th >Nacionalidad</th>
                                <th >Días Laborales</th>
                                <th >E-mail</th>
                                <th >Escolaridad</th>
                                <th >Jornada</th>
                                <th >TU</th>
                                <th >CLUES Adscripción Real</th>
                                <th >CLUES Adscripción en Nómina</th>
                                <th >Entidad Federativa de la Plaza</th>
                                <th >Descripción CLUES Real2</th>
                                <th >Descripción CRESPO 2</th>
                                <th >Actividad Principal Desempeño</th>
                                <th >Area/Servicio de Trabajo2</th>
                                <th >Institución de Pertenencia de la Plaza</th>
                                <th >Cédula Profesional</th>
                                <th >Hora de Entrada</th>
                                <th >Hora de Salida</th>
                                <th >Escolaridad 2</th>
                                <th >Escolaridad 3</th>
                                <th >Cédula Profesional 2</th>
                                <th >Título Profesional 2</th>
                                <th >Título Profesional 3</th>
                                <th >Residencia Médica</th>
                                <th >A&ntilde;o de la Residencia</th>
                                <th >Especialidad</th>
                                <th >Certificado Por</th>
                                <th >Cuenta con FIEL</th>
                                <th >Vigencia FIEL</th>
                                <th >Estatus del Registro</th>
                                <th >Fecha de Ingreso del Registro</th>
                                <th >Percepciones </th>
                                <th >Deducciones</th>
                                <th >Total Neto</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


            </div>
        </div>
    </div>
</div>