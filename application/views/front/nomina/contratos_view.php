<h3 class="page-header title center"><i class="fa fa-users ic-color"></i>Contratos</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="container_12">
                    <div class="grid_12" id="buscador_multipe">
                        <h2>Buscador múltiples criterios</h2>
                        <?php $atributos = array('class' => 'formulario') ?>
                        <?php echo form_open('buscador',$atributos) ?>

                        <?php echo form_label('A buscar') ?>
                        <input type="text" name="descripcion" class="descripcion" id="descripcion" placeholder="Tus palabras clave" />

                        <?php echo form_label('Sector') ?>
                        <select name="sector" class="sector" id="select">
                            <option value="" selected="selected">Sector</option>
                            <option value="Programación">Programación</option>
                            <option value="Medicina">Medicina</option>
                            <option value="Comerciales">Comerciales</option>
                            <option value="Almacén">Almacén</option>
                        </select>

                        <!--este es nuestro autocompletado-->
                        <input type="text" autocomplete="off" onpaste="return false" name="poblacion"
                               id="poblacion" class="poblacion" placeholder="Busca tu población" />

                        <div class="muestra_poblaciones"></div>

                        <?php echo form_submit('buscar','Buscar') ?>

                        <?php echo form_close() ?>

                    </div>






                </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datos_tabla">
                            <thead>
                            <tr>
                                <th >No.</th>
                                <th >Fecha</th>
                                <th >Num. Contrato</th>
                                <th >Proveedor</th>
                                <th >Fecha de Emisión</th>
                                <th >Importe</th>
                                <th >Estatus</th>
                                <th>Acciones</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

