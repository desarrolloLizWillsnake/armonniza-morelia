<h3 class="page-header title center"><i class="fa fa-users ic-color"></i>Estatus Proveedores</h3>
<div id="page-wrapper">
    <div class="row cont-btns-c center">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                        <li><a href="">Contratos</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Seguimiento de Facturas<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        <li><a href="#">Estado de Cuenta</a></li>
                        <li><a href="#">Inventario</a></li>
                        <li><a href="#">Buzón de Sugerencias</a></li>
                        <li><a href="#">Noticias</a></li>
                        </li>

                    </ul>


<!--                        <ul class="nav navbar-nav navbar-right">-->
<!--                            <li><a href="#">Link</a></li>-->
<!--                            <li class="dropdown">-->
<!--                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"-->
<!---->
<!--                        </ul>-->

<!--                    <form class="navbar-form navbar-left" role="search">-->
<!--                        <div class="form-group">-->
<!--                            <input type="text" class="form-control" placeholder="Search"> <button type="submit" class="btn btn-default">Submit</button>-->
<!--                        </div>-->
<!--                    </form>-->

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datos_tabla">
                            <thead>
                            <tr>
                                <th >Proveedor</th>
                                <th >Num. Contrato</th>
                                <th >Folio</th>
<!--                                <th >Fecha y Hora Entrada Factura</th>-->
<!--                                <th >Fecha y Hora Salida Factura</th>-->
<!--                                <th >Fecha de Autorización de Facturas</th>-->
<!--                                <th >Num. Facturas</th>-->
<!--                                <th >Facturas Ingresadas</th>-->
<!--                                <th >Facturas Por pagar</th>-->
<!--                                <th >Total del Contrato</th>-->
<!--                                <th >Estatus</th>-->


                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

