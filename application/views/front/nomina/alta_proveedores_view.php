<h3 class="page-header center"><i class="fa fa-plus-circle"></i> Agregar Proveedor</h3>
<div id="page-wrapper">
    <form class="forma_compromiso" role="form">
        <div class="row add-pre error-gral">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos Proveedor
                    </div>

                    <div class="row add-pre error-detalle">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">

                    <!-- Tipo de Gastos -->
                    <label class="forma_caratula_normal">PyME</label>
                    <div class="radio forma_caratula_normal">
                        <label>
                            <input type="radio" name="tipo_radio" id="optionsRadios1" value="1" checked>Si
                        </label>
                    </div>
                    <div class="radio forma_caratula_normal">
                        <label>
                            <input type="radio" name="tipo_radio" id="optionsRadios2" value="2">No
                        </label>
                    </div>
                    <div>
                        <script type="text/javascript">
                            function showContent() {
                                element = document.getElementById("content");
                                check = document.getElementById("check");
                                if (check.checked) {
                                    element.style.display='block';
                                }
                                else {
                                    element.style.display='none';
                                }
                            }
                        </script>
                        <div class="page-header center">
                            <div class="row">
                                <div class="col-lg-6"><label>Persona Física <input type="checkbox" name="check" id="check" value="1" onchange="javascript:showContent()" /></label> </div></h4>

                            </div>
                        </div>

                        <div id="content" style="display: none;">

                            <div class="col-lg-4">
                                <input type="text" class="form-control forma_caratula_normal" name="nombre" id="nombre" placeholder="Nombres">
                                <input type="text" class="form-control forma_caratula_normal" name="rfc" id="rfc" placeholder="RFC">
                                <!--                                <input type="text" class="form-control forma_caratula_normal" name="lugar_entrega" id="lugar_entrega" placeholder="Lugar de Entrega">-->
                            </div>

                            <div class="col-lg-4">

                                <input type="text" class="form-control forma_caratula_normal" name="a_paterno" id="a_parteno" placeholder="Apellido Parterno">
                                <input type="text" class="form-control forma_caratula_normal" name="comprobante_rfc" id="comprobante_rfc" placeholder="Comprobante RFC">
                                <!--                                    <input type="text" class="form-control forma_caratula_normal" name="condicion_entrega" id="condicion_entrega" placeholder="Condición de Entrega">-->
                            </div>
                            <div class="col-lg-4">

                                <input type="text" class="form-control forma_caratula_normal" name="a_materno" id="a_materno" placeholder="Apellido Materno">
                                <!--                                <input type="" class="" name="" id="" placeholder="">-->
                                <!--                                <input type="text" class="form-control forma_caratula_normal" name="condicion_entrega" id="condicion_entrega" placeholder="Condición de Entrega">-->
                            </div>
                        </div>
                    </div>

                    <!-- /.panel-heading -->
                        <div class="panel-body"><br><br><br><br><br><br><br>
<!--                            <input type="hidden" name="ultimo_compromiso" id="ultimo_compromiso" value="--><?//= $ultimo ?><!--" />-->
                            <div class="row add-pre error-detalle">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Empresa Moral
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <!--Primer Columna-->
                                                <div class="col-lg-4">

                                                    <!-- Input de texto Clave del Activo -->
                                                    <div class="form-group">
                                                        <!--<?= form_label('nombre_usuario', 'nombre_usuario'); ?>-->
                                                        <?php
                                                        $input_nombre_usuario = array(
                                                            "class" => "form-control",
                                                            "name" => "nombre_usuario",
                                                            "id" => "nombre_usuario",
                                                            "placeholder" => "Nombre (Usuario del portal)",
                                                            "value" => set_value("nombre_usuario"),
                                                        );
                                                        echo form_input($input__nombre_usuario);
                                                        ?>
                                                        <?= form_error('nombre_usuario'); ?>
                                                    </div>

                                                </div>
                                                <!--Fin Primer Columna-->

                                                <!--Segunda Columna-->
                                                <div class="col-lg-5" style="padding-left: 3%;">
                                                    <!-- Input de texto Marca -->
                                                    <div class="form-group">
                                                        <!--<?= form_label('Email', 'email'); ?>-->
                                                        <?php
                                                        $input_email= array(
                                                            "class" => "form-control",
                                                            "name" => "e_mail",
                                                            "id" => "e_mail",
                                                            "placeholder" => " E-Mail(usuario del portal)",
                                                            "value" => set_value("e_mail"),
                                                        );
                                                        echo form_input($input_email);
                                                        ?>
                                                        <?= form_error('e_mail'); ?>
                                                    </div>

                                                    <!-- Input de texto Modelo -->

                                                    <!-- Input de texto Numero de Serie -->
                                                </div>
                                                <!--Fin Segunda Columna-->

                                                <!--Tercera Columna-->
                                                <div class="col-lg-3">

                                                    <input type="text" class="form-control forma_caratula_normal" name="lugar_entrega" id="lugar_entrega" placeholder="Télefono (Usuario Portal)">

                                                </div>
                                                <!--Fin Tecera Columna-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

        <!-- Seccion de Inventario / Existencia -->
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Domicilio Fiscal
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!-- Input de texto Localización -->
                                <div class="form-group">
                                    <!--<?= form_label('Calle', 'calle'); ?>-->
                                    <?php
                                    $input_calle = array(
                                        "class" => "form-control",
                                        "name" => "calle",
                                        "id" => "calle",
                                        "placeholder" => "Calle",
                                        "value" => set_value("calle"),
                                    );
                                    echo form_input($input_calle);
                                    ?>
                                    <?= form_error('calle'); ?>
                                </div>

                                <!-- Input de texto Estante -->
                                <div class="form-group">
                                    <!--<?= form_label('Colonia', 'colonia'); ?>-->
                                    <?php
                                    $input_colonia = array(
                                        "class" => "form-control",
                                        "name" => "colonia",
                                        "id" => "colonia",
                                        "placeholder" => "Colonia",
                                        "value" => set_value("colonia"),
                                    );
                                    echo form_input($input_colonia);
                                    ?>
                                </div>

                                <!-- Input de texto Anaquel -->
                                <div class="form-group">
                                    <!--<?= form_label('Estado', 'estado'); ?>-->
                                    <?php
                                    $input_estado = array(
                                        "class" => "form-control",
                                        "name" => "estado",
                                        "id" => "estado",
                                        "placeholder" => "Estado",
                                        "value" => set_value("estado"),
                                    );
                                    echo form_input($input_estado);
                                    ?>
                                    <?= form_error('estado'); ?>
                                </div>

                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!-- Input de texto Punto de Reorden -->
                                <div class="form-group">
                                    <!--<?= form_label('Numero exterior', 'numero_exterior'); ?>-->
                                    <?php
                                    $input_numero_ext = array(
                                        "class" => "form-control dinero",
                                        "name" => "numero_exterior",
                                        "id" => "numero_exterior",
                                        "placeholder" => "Número Exterior",
                                        "value" => set_value("numero_exterior"),
                                    );
                                    echo form_input($input_numero_ext);
                                    ?>
                                    <?= form_error('numero_exterior'); ?>
                                </div>

                                <!-- Input de texto Area Asignado -->
                                <div class="form-group">
                                    <!--<?= form_label('Codigo postal', 'codigo_postal'); ?>-->
                                    <?php
                                    $input_codigo_postal = array(
                                        "class" => "form-control",
                                        "name" => "codigo_postal",
                                        "id" => "codigo_postal",
                                        "placeholder" => "Código Postal",
                                        "value" => set_value("codigo_postal"),
                                    );
                                    echo form_input($input_codigo_postal);
                                    ?>
                                    <?= form_error('codigo_postal'); ?>
                                </div>

                                <!-- Input de texto Nombre Resguardante -->
                                <div class="form-group">
                                    <!--<?= form_label('Pais', 'pais'); ?>-->
                                    <?php
                                    $input_pais = array(
                                        "class" => "form-control",
                                        "name" => "pais",
                                        "id" => "pais",
                                        "placeholder" => "País",
                                        "value" => set_value("pais"),
                                    );
                                    echo form_input($input_pais);
                                    ?>
                                    <?= form_error('nombre_resguardante'); ?>
                                </div>

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <!-- Input de texto Existencia -->
                                <div class="form-group">
                                    <!--<?= form_label('Numero interior', 'numero_interior'); ?>-->
                                    <?php
                                    $input_numero_int = array(
                                        "class" => "form-control dinero",
                                        "name" => "numero_interior",
                                        "id" => "numero_interior",
                                        "placeholder" => "Número Interior",
                                        "value" => set_value("numero_interior"),
                                    );
                                    echo form_input($input_numero_int);
                                    ?>
                                    <?= form_error('numero_interior'); ?>
                                </div>

                                <!-- Input de texto Mínimo en Stock -->
                                <div class="form-group">
                                    <!--<?= form_label('Ciudad', 'ciudad'); ?>-->
                                    <?php
                                    $input_minimo_stock = array(
                                        "class" => "form-control dinero",
                                        "name" => "ciudad",
                                        "id" => "ciudad",
                                        "placeholder" => "Ciudad",
                                        "value" => set_value("ciudad"),
                                    );
                                    echo form_input($input_minimo_stock);
                                    ?>
                                    <?= form_error('ciudad'); ?>
                                </div>

                            <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                <!-- Seccion de Activo -->
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Contacto Usuario del Sistema
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">

                                <!-- Input de texto Clave del Activo -->
                                <div class="form-group">
                                    <!--<?= form_label('Nombre Portal', 'nombre_portal'); ?>-->
                                    <?php
                                        $input_nombre_portal = array(
                                            "class" => "form-control",
                                            "name" => "nombre_portal",
                                            "id" => "nombre_portal",
                                            "placeholder" => "Nombre (Usuario del portal)",
                                            "value" => set_value("nombre_portal"),
                                             );
                                        echo form_input($input_nombre_portal);
                                    ?>
                                    <?= form_error('nombre_portal'); ?>
                                </div>

                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!-- Input de texto Marca -->
                                <div class="form-group">
                                    <!--<?= form_label('E mail portal', 'email_portal'); ?>-->
                                    <?php
                                    $input_marca = array(
                                        "class" => "form-control",
                                        "name" => "email_portal",
                                        "id" => "marca",
                                        "placeholder" => " E-Mail(usuario del portal)",
                                        "value" => set_value("email_portal"),
                                    );
                                    echo form_input($input_marca);
                                    ?>
                                    <?= form_error('email_portal'); ?>
                                </div>

                                <!-- Input de texto Modelo -->

                                <!-- Input de texto Numero de Serie -->

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-3">

                                <!-- Input tipo select Tipo de Unidad -->
<!--                                <div class="form-group">-->
<!--                                    <!----><?//= form_label('Tipo Unidad', 'tipo_unidad'); ?><!---->
<!--                                    <select class="form-control" name="tipo_unidad">-->
                                        <input type="text" class="form-control forma_caratula_normal" name="lugar_entrega" id="lugar_entrega" placeholder="Télefono (Usuario Portal)">
<!--                                        <option value="">Télefono (Usuario Portal)</option>-->

<!--                                    </select>-->
<!--                                </div>-->
                            </div>
                            <!--Fin Tecera Columna-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Seccion de Inventario / Existencia -->
        <div class="row add-pre error-detalle">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Relación
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--Primer Columna-->
                            <div class="col-lg-4">
                                <!-- Input de texto Localización -->
                                <div class="form-group">
                                    <!--<?= form_label('Servicios presta', 'servicios_presta'); ?>-->
                                    <?php
                                    $input_servicios = array(
                                        "class" => "form-control",
                                        "name" => "servicios_presta",
                                        "id" => "servicios_presta",
                                        "placeholder" => "Servicios que Presta (Ejemplo: Consultoria,)",
                                        "value" => set_value("servicios_presta"),
                                    );
                                    echo form_input($input_servicios);
                                    ?>
                                    <?= form_error('servicios_presta'); ?>
                                </div>

                                <!-- Input de texto Estante -->
                                <div class="form-group">
                                    <!--<?= form_label('Email contacto', 'email_contacto'); ?>-->
                                    <?php
                                    $input_estante = array(
                                        "class" => "form-control",
                                        "name" => "email_contacto",
                                        "id" => "email_contacto",
                                        "placeholder" => "E-Mail Contacto",
                                        "value" => set_value("email_contacto"),
                                    );
                                    echo form_input($input_estante);
                                    ?>
                                </div>
                                <!-- Input de texto Anaquel -->
                            </div>
                            <!--Fin Primer Columna-->

                            <!--Segunda Columna-->
                            <div class="col-lg-5" style="padding-left: 3%;">
                                <!-- Input de texto Punto de Reorden -->
                                <div class="form-group">
                                    <!--<?= form_label('Contacto vendedor', 'contacto_vendedor'); ?>-->
                                    <?php
                                    $input_punto_reorden = array(
                                        "class" => "form-control dinero",
                                        "name" => "contacto_vendedor",
                                        "id" => "contacto_vendedor",
                                        "placeholder" => "Contacto (Vendedor)",
                                        "value" => set_value("contacto_vendedor"),
                                    );
                                    echo form_input($input_punto_reorden);
                                    ?>
                                    <?= form_error('contacto_vendedor'); ?>
                                </div>

                                <!-- Input de texto Area Asignado -->

                                <!-- Input de texto Nombre Resguardante -->

                            </div>
                            <!--Fin Segunda Columna-->

                            <!--Tercera Columna-->
                            <div class="col-lg-3">
                                <!-- Input de texto Existencia -->
                                <div class="form-group">
                                    <!--<?= form_label('Telefono contacto', 'tel_contacto'); ?>-->
                                    <?php
                                    $input_tel_contacto = array(
                                        "class" => "form-control dinero",
                                        "name" => "tel_contacto",
                                        "id" => "tel_contacto",
                                        "placeholder" => "Télefono Contacto",
                                        "value" => set_value("tel_contacto"),
                                    );
                                    echo form_input($input_tel_contacto);
                                    ?>
                                    <?= form_error('tel_contacto'); ?>
                                </div>

                                <!-- Input de texto Mínimo en Stock -->


                                <!-- Input de texto Máximo en Stock -->

                            </div>
                            <!--Fin Tecera Columna-->
                        </div>                        
                    </div>
                </div>
            </div>
        </div>

            <!-- Seccion de Inventario / Existencia -->
            <div class="row add-pre error-detalle">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Forma de Pago
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <!--Primer Columna-->
                                <div class="col-lg-4">
                                    <!-- Input de texto Localización -->
                                    <div class="form-group">
                                        <!--<?= form_label('Peso mexicano', 'mxp'); ?>-->
                                        <?php
                                        $input_localizacion = array(
                                            "class" => "form-control",
                                            "name" => "mxp",
                                            "id" => "mxp",
                                            "placeholder" => "MXP",
                                            "value" => set_value("mxp"),
                                        );
                                        echo form_input($input_localizacion);
                                        ?>
                                        <?= form_error('mxp'); ?>
                                    </div>

                                    <!-- Input de texto Estante -->


                                    <!-- Input de texto Anaquel -->


                                </div>
                                <!--Fin Primer Columna-->

                                <!--Segunda Columna-->
                                <div class="col-lg-5" style="padding-left: 3%;">
                                    <!-- Input de texto Punto de Reorden -->
                                    <div class="form-group">
                                        <!--<?= form_label('Banco', 'banco'); ?>-->
                                        <?php
                                        $input_banco = array(
                                            "class" => "form-control dinero",
                                            "name" => "banco",
                                            "id" => "banco",
                                            "placeholder" => "Banco",
                                            "value" => set_value("banco"),
                                        );
                                        echo form_input($input_banco);
                                        ?>
                                        <?= form_error('banco'); ?>
                                    </div>

                                    <!-- Input de texto Area Asignado -->


                                    <!-- Input de texto Nombre Resguardante -->


                                </div>
                                <!--Fin Segunda Columna-->

                                <!--Tercera Columna-->
                                <div class="col-lg-3">
                                    <!-- Input de texto Existencia -->
                                    <div class="form-group">
                                        <!--<?= form_label('Clabe', 'clabe'); ?>-->
                                        <?php
                                        $input_existencia = array(
                                            "class" => "form-control dinero",
                                            "name" => "clabe",
                                            "id" => "clabe",
                                            "placeholder" => "CLABE (18 Digitos)",
                                            "value" => set_value("clabe"),
                                        );
                                        echo form_input($input_existencia);
                                        ?>
                                        <?= form_error('clabe'); ?>
                                    </div>

                                    <!-- Input de texto Mínimo en Stock -->
                                    <div class="form-group input-group">
                                        <?= form_label('Comprobante de Pago', 'archivo'); ?>
                                        <?php
                                        $input_subir_imagen = array(
                                            "class" => "form-control",
                                            "name" => "archivo",
                                            "id" => "archivo",
                                            "value" => set_value("archivo"),
                                        );
                                        echo form_upload($input_subir_imagen);
                                        ?>
                                        <?= form_error('archivo'); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php
                                        if(isset($ruta)) { ?>
                                            <img src="<?= $ruta ?>" title="Imagen Producto" />
                                        <?php }
                                        ?>
                                    </div>

                                    <div class="form-group">
                                        <?php
                                        if(isset($error_imagen)) {
                                            echo($error_imagen);
                                        }
                                        ?>
                                    </div>

                                </div>

                                    <!-- Input de texto Máximo en Stock -->

                                </div>
                                <!--Fin Tecera Columna-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="btns-finales text-center">
            <?php
            if(isset($mensaje)) { ?>
                <div class="col-lg-12 text-center">
                    <div class="text-center" id="resultado_insertar_caratula">
                        <?= $mensaje ?>
                    </div>
                </div>
            <?php }
            ?>
<!--            <a href="--><?//= base_url("patrimonio/catalogo_inventarios") ?><!--" class="btn btn-default"><i class="fa fa-reply" style="color: #B6CE33;"></i> Regresar</a>-->
            <input type="submit" id="guardar_producto" name="guardar_producto" class="btn btn-green" style="border:none;" value="Guardar Proveedor"/>
        </div>
    </form>
</div>

