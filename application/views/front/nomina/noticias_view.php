<h3 class="page-header title center"><i class="fa fa-plus-circle"></i> Agregar Noticia</h3>
<div id="page-wrapper">
    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    General
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <!--Primera Columna-->
                        <div class="col-lg-3">
                            <!--Tipo de Entrada-->
                            <select class="form-control" id="tipo_mensaje" name="tipo_mensaje" required>
                                <option value="">Asunto</option>
                                <option value="Aviso">Aviso </option>
                                <option value="Urgente">Urgente</option>
                            </select>
                        </div>
                        <div class="col-lg-9">
                            <textarea style="height: 8em; margin-top: .5%;" class="form-control" id="observaciones" name="observaciones" placeholder="Mensaje/Aviso"></textarea>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>

    <div class="btns-finales text-center">
        <div class="text-center" id="mensaje_guardar"></div>
        <a id="guardar_noticia" class="btn btn-green">Guardar y Enviar Noticia</a>
    </div>
    <div class="row add-pre error-gral">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Historial de Noticias
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body table-gral">
                                <div class="table-responsive">
                                    <h4 id="suma_total" class="text-center"></h4>
                                    <table class="table table-striped table-bordered table-hover" id="tabla_detalle_noticia">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Fecha / Hora</th>
                                                <th>Asunto</th>
                                                <th>Mensaje</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
