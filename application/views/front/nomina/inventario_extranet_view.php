<h3 class="page-header title center"><i class="fa fa-tasks"></i> Consulta Inventario</h3>
<div id="page-wrapper">
    <?php
        $forma_atributos = array(
            'class' => 'forma_producto',
            'role' => 'form',
            'id' => 'forma_principal');
        echo form_open('patrimonio/imprimir_consulta_inventarios', $forma_atributos);
    ?>
        <div class="row add-pre error-gral text-center">
            <div class="col-lg-12">
                <div class="panel panel-default" style="margin: 0 auto; margin-top: 2%; width: 50%;">
                    <div class="panel-body">

                        <!--Rango Cuentas -->
                        <div class="row" style="margin-top: 2%;">
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="gasto_inicial" id="gasto_inicial" style="margin-top: -.5%;" placeholder="Contratos" readonly="readonly" />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto_inicial"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" name="gasto_final" id="gasto_final" style="margin-top: -.5%;" placeholder="Facturas" readonly="readonly" />
                                        <span class="input-group-btn ic-buscar-btn">
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal_gasto_final"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>

        <!-- Modal Gasto Inicial -->
<div class="modal fade" id="modal_gasto_inicial" tabindex="-1" role="dialog" aria-labelledby="modal_gasto_inicial" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i>No. de Contratos</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="rango_inicial">
                        <thead>
                        <tr>

                            <th>Clave/Producto</th>
                            <th>Descripción</th>
                            <th>Unidad de Medida</th>
                            <th>Cantidad</th>
                            <th>Precio</th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Gasto Inicial -->
<div class="modal fade" id="modal_gasto_final" tabindex="-1" role="dialog" aria-labelledby="modal_gasto_final" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-users ic-modal"></i>Facturas Entregadas</h4>
            </div>
            <div class="modal-body table-gral modal-action modal-3">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="rango_final">
                        <thead>
                        <tr>
                            <th>No. Factura</th>
                            <th>Proveedor</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>