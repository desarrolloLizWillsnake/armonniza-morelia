<h3 class="page-header center"><i class="fa fa-users"></i>Proveedores por Aprobar</h3>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datos_tabla">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Clave</th>
                                <th>RFC</th>
                                <th>Nombre</th>
                                <th>No. Cuenta</th>
                                <th>CLABE</th>
                                <th>Estatus</th>
                                <th>ID Extranet</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <h3 class="center"><i class="fa fa-users"></i> Proveedores Aprobados</h3>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body table-gral">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datos_tabla_aprobado">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Clave</th>
                                <th>RFC</th>
                                <th>Nombre</th>
                                <th>No. Cuenta</th>
                                <th>CLABE</th>
                                <th>Estatus</th>
                                <th>Aprobado Por</th>
                                <th>Fecha Aprobado</th>
                                <th>ID Extranet</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_activar" tabindex="-1" role="dialog" aria-labelledby="modal_activar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-check ic-modal"></i> Aprobar Proveedor</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea aprobar el proveedor seleccionado?</label>
                        <input type="hidden" value="" name="aprobar_proveedor" id="aprobar_proveedor" />
                        <input type="hidden" value="" name="id_proveedor" id="id_proveedor" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_activar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_desactivar" tabindex="-1" role="dialog" aria-labelledby="modal_desactivar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-check ic-modal"></i> Desaprobar Proveedor</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea desaprobar el proveedor seleccionado?</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_desactivar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_ver" tabindex="-1" role="dialog" aria-labelledby="modal_ver" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa ic-modal"></i> Datos del Proveedor</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>RFC</label>
                            <p class="form-control-static input_view" id="rfc_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label>Razón Social</label>
                            <p class="form-control-static input_view" id="nombre_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>PyME</label>
                            <p class="form-control-static input_view" id="pyme_ver"></p>
                        </div>
                    </div>
                    <div class=tas "col-lg-4">
                        <div class="form-group">
                            <label>Calle</label>
                            <p class="form-control-static input_view" id="calle_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Número Interior</label>
                            <p class="form-control-static input_view" id="num_interior_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Número Exterior</label>
                            <p class="form-control-static input_view" id="num_exterior_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Colonia</label>
                            <p class="form-control-static input_view" id="colonia_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Ciudad</label>
                            <p class="form-control-static input_view" id="ciudad_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <p class="form-control-static input_view" id="estado_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>País</label>
                            <p class="form-control-static input_view" id="pais_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>C.P.</label>
                            <p class="form-control-static input_view" id="cp_ver"></p>
                        </div>
                    </div>
                </div>

                <hr />
                <h3>Contacto Facturas</h3>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Nombre</label>
                            <p class="form-control-static input_view" id="nombre_facturas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <p class="form-control-static input_view" id="a_paterno_facturas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <p class="form-control-static input_view" id="a_materno_facturas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <p class="form-control-static input_view" id="telefono_facturas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Correo</label>
                            <p class="form-control-static input_view" id="correo_facturas_ver"></p>
                        </div>
                    </div>
                </div>

                <hr />
                <h3>Contacto Ventas</h3>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Razón Social</label>
                            <p class="form-control-static input_view" id="nombre_ventas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <p class="form-control-static input_view" id="a_paterno_ventas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <p class="form-control-static input_view" id="a_materno_ventas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <p class="form-control-static input_view" id="telefono_ventas_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Correo</label>
                            <p class="form-control-static input_view" id="correo_ventas_ver"></p>
                        </div>
                    </div>
                </div>
                <hr />
                <h3>Datos Bancarios</h3>

                <div class="row">
                    <!-- <div class="col-lg-4">
                        <div class="form-group">
                            <label>Moneda</label>
                            <p class="form-control-static input_view" id="moneda_ver"></p>
                        </div>
                    </div> -->
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Banco</label>
                            <p class="form-control-static input_view" id="banco_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>No. de Cuenta</label>
                            <p class="form-control-static input_view" id="no_cuenta_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>CLABE</label>
                            <p class="form-control-static input_view" id="clabe_ver"></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Comprobante de Pago</label>
                            <br />
                            <img src="" id="comprobante_pago_ver" alt="Comprobante de Pago" height="42" width="42">
                            <a href="" class="btn btn-default" id="descargar_imagen" download="" />Descargar</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_resultado" tabindex="-1" role="dialog" aria-labelledby="modal_resultado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa ic-modal"></i> Resultado</h4>
            </div>
            <div class="modal-body">
                <div id="mensaje_resultado">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
