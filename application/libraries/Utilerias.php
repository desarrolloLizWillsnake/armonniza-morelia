<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilerias
{

    function __construct()
    {
        $this->ci =& get_instance();
    }

    function convertirFechaAMes($fecha)
    {
        $mes = "";

        $fecha_mes = "";

        if (strpos($fecha, '/') !== FALSE) {
            $fecha_mes = explode("/", $fecha);

            $fecha_mes[1] = $fecha_mes[0];

        } elseif (strpos($fecha, '-') !== FALSE) {
            $fecha_mes = explode("-", $fecha);
        }

        switch ($fecha_mes[1]) {
            case 1:
                $mes = 'enero';
                break;

            case 2:
                $mes = 'febrero';
                break;

            case 3:
                $mes = 'marzo';
                break;

            case 4:
                $mes = 'abril';
                break;

            case 5:
                $mes = 'mayo';
                break;

            case 6:
                $mes = 'junio';
                break;

            case 7:
                $mes = 'julio';
                break;

            case 8:
                $mes = 'agosto';
                break;

            case 9:
                $mes = 'septiembre';
                break;

            case 10:
                $mes = 'octubre';
                break;

            case 11:
                $mes = 'noviembre';
                break;

            case 12:
                $mes = 'diciembre';
                break;
        }

        return $mes;
    }

    function traducir_mes($mes)
    {

        switch ($mes) {
            case "January":
                $mes = 'enero';
                break;

            case "February":
                $mes = 'febrero';
                break;

            case "March":
                $mes = 'marzo';
                break;

            case "April":
                $mes = 'abril';
                break;

            case "May":
                $mes = 'mayo';
                break;

            case "June":
                $mes = 'junio';
                break;

            case "July":
                $mes = 'julio';
                break;

            case "August":
                $mes = 'agosto';
                break;

            case "September":
                $mes = 'septiembre';
                break;

            case "October":
                $mes = 'octubre';
                break;

            case "November":
                $mes = 'noviembre';
                break;

            case "December":
                $mes = 'diciembre';
                break;
        }

        return $mes;

    }

    function preparar_mes_actual()
    {
        $mes = date("F");

        switch ($mes) {
            case "January":
                $mes = 'enero';
                break;

            case "February":
                $mes = 'febrero';
                break;

            case "March":
                $mes = 'marzo';
                break;

            case "April":
                $mes = 'abril';
                break;

            case "May":
                $mes = 'mayo';
                break;

            case "June":
                $mes = 'junio';
                break;

            case "July":
                $mes = 'julio';
                break;

            case "August":
                $mes = 'agosto';
                break;

            case "September":
                $mes = 'septiembre';
                break;

            case "October":
                $mes = 'octubre';
                break;

            case "November":
                $mes = 'noviembre';
                break;

            case "December":
                $mes = 'diciembre';
                break;

        }

        return $mes;
    }

    function get_grupo()
    {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT id_grupo as grupo FROM grupos_usuarios WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        $row = $query->row();
        return $row->grupo;
    }

    function get_permisos($modulo)
    {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT modulo FROM permisos WHERE id_usuario = ? AND modulo = ? AND activo = 1;";
        $query = $this->ci->db->query($sql, array($id, $modulo));
        $row = $query->row();
        if ($row) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_firmas()
    {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT firma FROM firmas WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        $row = $query->row();
        if ($row) {
            return $row->firma;
        } else {
            return NULL;
        }
    }

    function get_firmas_arreglo()
    {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT firma FROM firmas WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        $row = $query->result();
        if ($row) {
            return $row;
        } else {
            return NULL;
        }
    }

    function puede_firmar()
    {
        $id = $this->ci->tank_auth->get_user_id();
        $sql = "SELECT firma FROM firmas WHERE id_usuario = ?;";
        $query = $this->ci->db->query($sql, array($id));
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return NULL;
        }
    }

    function generar_poliza_diario_egresos($numero = NULL)
    {

//        $this->debugeo->imprimir_pre($value);

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre . " " . $nombre_encontrado->apellido_paterno . " " . $nombre_encontrado->apellido_materno;

//        Se toma el numero del contrarecibo que se va a buscar
            $contrarecibo = $numero;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if ($ultimo) {
                $last = $ultimo->ultimo + 1;
            } //        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";

//        Se llama a la funci?n que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_contrarecibo = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($contrarecibo, $query_caratula);

//            $this->debugeo->imprimir_pre($datos_caratula_contrarecibo);

//        Se prepara el query para tomar los datos del detalle de compromiso que est?n ligados al contrarecibo
            $query_detalle_compromiso = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que est? ligado con el contrarecibo
            $datos_detalle_compromiso = $this->ci->ciclo_model->datos_compromisoDetalle($datos_caratula_contrarecibo->numero_compromiso, $query_detalle_compromiso);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle_compromiso) {
                throw new Exception('No hay partidas dentro del compromiso ['.$datos_caratula_contrarecibo->numero_compromiso.']');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;
            $total_nomina = FALSE;
            $resultado_insertar_caratula_query = FALSE;
            $resultado_insertar_abono_query = FALSE;
            $resultado_insertar_cargo_query = FALSE;

            $this->ci->db->trans_begin();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, id_proveedor, proveedor, contrarecibo, concepto_especifico, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?);";

            if (isset($datos_caratula_contrarecibo->proveedor) && $datos_caratula_contrarecibo->proveedor != NULL && $datos_caratula_contrarecibo->proveedor != "") {
                $datos_caratula_poliza = array(
                    $last,
                    'Diario',
                    $datos_caratula_contrarecibo->fecha_emision,
                    $datos_caratula_contrarecibo->concepto,
                    0,
                    $datos_caratula_contrarecibo->importe,
                    'espera',
                    $datos_caratula_contrarecibo->creado_por,
                    0,
                    0,
                    $datos_caratula_contrarecibo->id_proveedor,
                    $datos_caratula_contrarecibo->proveedor,
                    $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                    $datos_caratula_contrarecibo->concepto_especifico,
                    $datos_caratula_contrarecibo->documento,
                );
            } else {
                $datos_caratula_poliza = array(
                    $last,
                    'Diario',
                    $datos_caratula_contrarecibo->fecha_emision,
                    $datos_caratula_contrarecibo->concepto,
                    0,
                    $datos_caratula_contrarecibo->importe,
                    'espera',
                    $datos_caratula_contrarecibo->creado_por,
                    0,
                    0,
                    $datos_caratula_contrarecibo->id_persona,
                    $datos_caratula_contrarecibo->nombre_completo,
                    $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                    $datos_caratula_contrarecibo->concepto_especifico,
                    $datos_caratula_contrarecibo->documento,
                );
            }

            $resultado_insertar_caratula_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
            $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_poliza);

            foreach ($datos_detalle_compromiso as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_costo = $estructura->centro_de_costos;
                $partida = $estructura->partida;

                if ($estructura->partida == 33903 && $datos_caratula_contrarecibo->destino != "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo("33903-S");
                } elseif ($estructura->partida == 32201 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo("32201-H");
                } elseif ($estructura->partida == 33601 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo("33601-H");
                } elseif ($estructura->partida == 33901 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo("33901-H");
                } elseif ($estructura->partida == 23801 && $row->iva > 0) {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo("23801-G");
                } elseif ($estructura->partida == 23801 && $row->iva <= 0) {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo("23801-E");
                } else {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo($estructura->partida);
                }

                if (!$datos_cuentas) {

                    $datos_actualizar_caratula = array(
                        'no_partidas' => 0,
                        'cargos' => 0,
                        'abonos' => 0,
                        'concepto' => "No existe cuenta contable asociada a la partida: ".$estructura->partida,
                    );

                    $this->ci->db->where('numero_poliza', $last);
                    $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

                    $datos_actualizar_contrarecibo = array(
                        'poliza' => 1,
                    );

                    $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
                    $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_contrarecibo);

                    $this->ci->db->trans_commit();

                    $respuesta = array(
                        "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la P&oacute;liza de Diario No. ' . $last . '</div>',
                    );

                    return $respuesta["mensaje"];

                }

//                $this->debugeo->imprimir_pre($datos_cuentas);

                foreach ($datos_cuentas as $cuenta) {

                    $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                    $query = $this->ci->db->query($sql, array($estructura->fuente_de_financiamiento));
                    $resultado_fuente = $query->row();

                    $nombre_fuente_financiamiento = $resultado_fuente->descripcion;

                    if ($datos_caratula_contrarecibo->destino == "Honorarios") {

                        $isr = round($row->subtotal * .10, 2);
                        $iva_retenido = round(($row->iva / 3) * 2, 2);
                        $total_pagar = $row->importe - $isr;
                        $total_pagar -= $iva_retenido;

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                        $datos_caratula_contrarecibo->concepto_especifico,
                                    );

                                    $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->subtotal,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->subtotal;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a. retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva retenido") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $iva_retenido,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $iva_retenido;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $row->iva,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.s.r.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.s.r") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "isr") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $isr,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $isr;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_abono);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_abono . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_abono;
                                }

                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta_insertar));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $total_pagar,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $cargos += $total_pagar;

                                $total_filas += 1;

                            }

                        }

                    } elseif ($estructura->capitulo == 1000) {

                        $total_nomina = TRUE;

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nombre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $resultado_cuenta_cargo->nombre,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            }
                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $abonos += $row->importe;
                                continue;
                            }
                        }
                    } else {

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos($cuenta->cuenta_cargo, "7.9.9.3.1") !== FALSE || strpos($cuenta->cuenta_cargo, "7.6.2.1.4") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos($cuenta->cuenta_cargo, "7.6.2.1.1") !== FALSE || strpos($cuenta->cuenta_cargo, "7.6.2.1.2") !== FALSE) {

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += 0.0;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nombre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->subtotal,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $resultado_cuenta_cargo->nombre,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->subtotal;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a.') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a') !== FALSE && $row->iva > 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->iva,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } elseif ($cuenta->cuenta_abono == "7.9.9.1.0002" || $cuenta->cuenta_abono == "7.6.1.1.1.1000") {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->subtotal,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->subtotal;

                                $total_filas += 1;
                            } else {
                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_abono);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_abono . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_abono_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_abono_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_abono;
                                }

                                $sql_abono = "SELECT cuenta_padre AS id_padre, nombre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta_insertar));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $resultado_cuenta_abono->nombre,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            }

                        }
                    }

                }
            }

            if ($total_nomina) {
                $query_insertar_nomina = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                $datos_insertar_nomina = array(
                    $last,
                    'Diario',
                    1712,
                    '2.1.1.1.1.0001',
                    '2.1.1.1.1',
                    6,
                    $datos_caratula_contrarecibo->concepto,
                    0.0,
                    $datos_caratula_contrarecibo->importe,
                    $resultado_fuente->descripcion,
                    'Sueldos y Salarios por Pagar',
                    $centro_costo,
                    $partida,
                );

                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_nomina, $datos_insertar_nomina);
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_contrarecibo = array(
                'poliza' => 1,
            );

            $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
            $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_contrarecibo);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la P&oacute;liza de Diario No. ' . $last . '</div>',
            );

            return $respuesta["mensaje"];

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '.</div>',
            );

            return $respuesta["mensaje"];
        }
    }

    function generar_poliza_egresos($numero = NULL)
    {

//        $this->debugeo->imprimir_pre($value["movimiento"]);

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre . " " . $nombre_encontrado->apellido_paterno . " " . $nombre_encontrado->apellido_materno;

            $movimiento = $numero;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if ($ultimo) {
                $last = $ultimo->ultimo + 1;
            } //        De lo contrario se inicia en 1
            else {
                $last = 1;
            }
            $query_caratula_movimiento = "SELECT numero FROM mov_bancos_pagos_contrarecibos WHERE movimiento = ?;";

            $datos_caratula_movimiento = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($movimiento, $query_caratula_movimiento);

            $contrarecibo = $datos_caratula_movimiento->numero;

//            $this->debugeo->imprimir_pre($contrarecibo);

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";

//        Se llama a la funci�n que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_contrarecibo = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($contrarecibo, $query_caratula);

//            $this->debugeo->imprimir_pre($datos_caratula_contrarecibo);

//        Se prepara el query para tomar los datos del detalle de compromiso que est�n ligados al contrarecibo
            $query_detalle_compromiso = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que est� ligado con el contrarecibo
            $datos_detalle_compromiso = $this->ci->ciclo_model->datos_compromisoDetalle($datos_caratula_contrarecibo->numero_compromiso, $query_detalle_compromiso);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle_compromiso) {
                throw new Exception('No hay partidas dentro del compromiso.');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;

            $resultado_insertar_cargo_query = 0;
            $resultado_insertar_abono_query = 0;

            $this->ci->db->select('numero_poliza')->from('mov_polizas_cabecera')->where('contrarecibo', $datos_caratula_contrarecibo->id_contrarecibo_caratula);
            $query_numero_poliza_diario = $this->ci->db->get();
            $numero_poliza_diario = $query_numero_poliza_diario->row_array();

            if (!isset($numero_poliza_diario["numero_poliza"])) {
                throw new Exception('No se ha generado la p�liza de Diario del contrarecibo ' . $datos_caratula_contrarecibo->id_contrarecibo_caratula);
            }

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, movimiento, concepto_especifico, contrarecibo, id_proveedor, proveedor, poliza_contrarecibo, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            if (isset($datos_caratula_contrarecibo->proveedor) && $datos_caratula_contrarecibo->proveedor != NULL && $datos_caratula_contrarecibo->proveedor != "") {
                $datos_caratula_poliza = array(
                    $last,
                    'Egresos',
                    $datos_caratula_contrarecibo->fecha_emision,
                    $datos_caratula_contrarecibo->concepto,
                    0,
                    $datos_caratula_contrarecibo->importe,
                    'espera',
                    $nombre_completo,
                    0,
                    0,
                    $movimiento,
                    $datos_caratula_contrarecibo->concepto_especifico,
                    $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                    $datos_caratula_contrarecibo->id_proveedor,
                    $datos_caratula_contrarecibo->proveedor,
                    $numero_poliza_diario["numero_poliza"],
                    $datos_caratula_contrarecibo->documento,
                );
            } else {
                $datos_caratula_poliza = array(
                    $last,
                    'Egresos',
                    $datos_caratula_contrarecibo->fecha_emision,
                    $datos_caratula_contrarecibo->concepto,
                    0,
                    $datos_caratula_contrarecibo->importe,
                    'espera',
                    $nombre_completo,
                    0,
                    0,
                    $movimiento,
                    $datos_caratula_contrarecibo->concepto_especifico,
                    $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                    $datos_caratula_contrarecibo->id_persona,
                    $datos_caratula_contrarecibo->nombre_completo,
                    $numero_poliza_diario["numero_poliza"],
                    $datos_caratula_contrarecibo->documento,
                );
            }

            $resultado_insertar_caratula = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->ci->db->where('movimiento', $movimiento);
            $this->ci->db->update('mov_bancos_movimientos', $datos_actualizar_poliza);

            foreach ($datos_detalle_compromiso as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_costo = $estructura->centro_de_costos;
                $partida = $estructura->partida;

                if ($estructura->partida == 33903 && $datos_caratula_contrarecibo->destino != "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("33903-S");
                } elseif ($estructura->partida == 32201 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("32201-H");
                } elseif ($estructura->partida == 33601 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("33601-H");
                } elseif ($estructura->partida == 33901 && $datos_caratula_contrarecibo->destino == "Honorarios") {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento("33901-H");
                } else {
                    $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento($estructura->partida);
                }

//                $this->debugeo->imprimir_pre($datos_cuentas);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                $this->debugeo->imprimir_pre($cuenta);

                    $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                    $query = $this->ci->db->query($sql, array($estructura->fuente_de_financiamiento));
                    $resultado_fuente = $query->row();

                    $nombre_fuente_financiamiento = $resultado_fuente->descripcion;

                    if ($estructura->partida == 23801 && $row->iva > 0) {

                        if ($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesoreria de la Federacion)";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif ($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000) {
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Egresos',
                            5511,
                            '2.1.1.2.1.1.0001',
                            '2.1.1.2.1.1',
                            7,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            'Proveedor Generico de Materiales para Comercializar',
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            $id_cuenta,
                            $cuenta_banco,
                            $id_padre,
                            $nivel,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            $nombre_cuenta,
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            1163,
                            "1.1.2.8.1.0002",
                            "1.1.2.8.1",
                            6,
                            $datos_caratula_contrarecibo->concepto,
                            $row->iva,
                            0.0,
                            $resultado_fuente->descripcion,
                            "I.V.A. Material para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            1166,
                            "1.1.2.8.2.0002",
                            "1.1.2.8.2",
                            6,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->iva,
                            $resultado_fuente->descripcion,
                            "I.V.A. Material para Comercializar",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            7811,
                            "8.2.7.1.1",
                            "8.2.7.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Pagado",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            7808,
                            "8.2.6.1.1",
                            "8.2.6.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Ejercido",
                            $centro_costo,
                            $partida,
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->iva;
                        $abonos += $row->iva;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $total_filas += 6;
                        break;

                    } elseif ($estructura->partida == 23801 && $row->iva <= 0) {

                        if ($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesoreria de la Federacion)";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif ($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000) {
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle
                            (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?),
                            (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Egresos',
                            5511,
                            '2.1.1.2.1.1.0001',
                            '2.1.1.2.1.1',
                            7,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            'Proveedor Generico de Materiales para Comercializar',
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            $id_cuenta,
                            $cuenta_banco,
                            $id_padre,
                            $nivel,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            $nombre_cuenta,
                            $centro_costo,
                            $partida,


                            $last,
                            'Egresos',
                            7811,
                            "8.2.7.1.1",
                            "8.2.7.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            $row->importe,
                            0.0,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Pagado",
                            $centro_costo,
                            $partida,

                            $last,
                            'Egresos',
                            7808,
                            "8.2.6.1.1",
                            "8.2.6.1",
                            5,
                            $datos_caratula_contrarecibo->concepto,
                            0.0,
                            $row->importe,
                            $resultado_fuente->descripcion,
                            "Presupuesto de Egresos Ejercido",
                            $centro_costo,
                            $partida,
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $cargos += $row->importe;
                        $abonos += $row->importe;

                        $total_filas += 4;
                        break;

                    } elseif ($datos_caratula_contrarecibo->destino == "Honorarios") {

                        $isr = round($row->subtotal * .10, 2);
                        $iva_retenido = round(($row->iva / 3) * 2, 2);
                        $total_pagar = $row->importe - $isr;
                        $total_pagar -= $iva_retenido;

                        if ($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesoreria de la Federacion)";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif ($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000) {
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "iva") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                    }

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                        $datos_caratula_contrarecibo->concepto_especifico,
                                    );

                                    $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "banco") !== FALSE) {

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $total_pagar,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $total_pagar;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_cargo);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_cargo . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_cargo;
                                }

                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta_insertar));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $total_pagar,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $total_pagar;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a. retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a retenido') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva retenido') !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $iva_retenido,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $iva_retenido;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                    $resultado_cuenta_abono = $query_abono->row();

                                    $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                    if ($existe_cuenta == FALSE) {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                    }

                                    $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_abono = array(
                                        $last,
                                        'Diario',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_abono,
                                        $resultado_cuenta_abono->id_padre,
                                        $resultado_cuenta_abono->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        0.0,
                                        $row->iva,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_abono,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                    $abonos += $row->iva;

                                    $total_filas += 1;
                                }
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "i.s.r.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.s.r") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "isr") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $isr,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $isr;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "banco") !== FALSE) {

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $total_pagar,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $total_pagar;

                                $total_filas += 1;
                            } else {

                                $cuenta_insertar = '';

                                $_cuenta = explode(".", $cuenta->cuenta_abono);

                                if ($_cuenta[0] == 5) {
                                    $cuenta_buscar = $cuenta->cuenta_abono . "." . $centro_costo;
                                    $this->ci->db->select('cuenta')->from('cat_cuentas_contables')->where('cuenta', $cuenta_buscar);
                                    $query_cuenta_cargo_buscar = $this->ci->db->get();
                                    $provisional = $query_cuenta_cargo_buscar->row_array();
                                    if ($provisional) {
                                        $cuenta_insertar = $provisional["cuenta"];
                                    } else {
                                        throw new Exception('No existe la cuenta contable ' . $cuenta_buscar . '.');
                                    }
                                } else {
                                    $cuenta_insertar = $cuenta->cuenta_abono;
                                }

                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta_insertar));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Diario',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta_insertar,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $total_pagar,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                    $datos_caratula_contrarecibo->concepto_especifico,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $cargos += $total_pagar;

                                $total_filas += 1;

                            }

                        }

                    } else {
                        if ($estructura->fuente_de_financiamiento == 1) {
                            $id_cuenta = 1758;
                            $cuenta_banco = "2.1.2.1.1.1.0010";
                            $nombre_cuenta = "TESOFE (Tesoreria de la Federacion)";
                            $id_padre = "2.1.2.1.1.1";
                            $nivel = 7;

                        } elseif ($estructura->fuente_de_financiamiento == 4 && $estructura->capitulo == 1000) {
                            $id_cuenta = 128;
                            $cuenta_banco = "1.1.1.2.1.4.7150";
                            $nombre_cuenta = "Cta. 0643357150";
                            $id_padre = "1.1.1.2.1.4";
                            $nivel = 7;
                        } else {
                            $id_cuenta = 116;
                            $cuenta_banco = "1.1.1.2.1.1.1497";
                            $nombre_cuenta = "Cta. 0443361497";
                            $id_padre = "1.1.1.2.1.1";
                            $nivel = 7;
                        }

                        if ($cuenta->cuenta_cargo) {

                            if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva') !== FALSE) {
                                if ($row->iva !== 0) {
                                    $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                    $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                    $resultado_cuenta_cargo = $query_cargo->row();

                                    $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                    $datos_cargo = array(
                                        $last,
                                        'Egresos',
                                        $cuenta->id_correlacion_partidas_contables,
                                        $cuenta->cuenta_cargo,
                                        $resultado_cuenta_cargo->id_padre,
                                        $resultado_cuenta_cargo->nivel,
                                        $datos_caratula_contrarecibo->concepto,
                                        $row->iva,
                                        0.0,
                                        $resultado_fuente->descripcion,
                                        $cuenta->nombre_cargo,
                                        $centro_costo,
                                        $partida,
                                    );

                                    $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                    $cargos += $row->iva;

                                    $total_filas += 1;

                                }
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de egresos") !== FALSE) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_cargo), "banco") !== FALSE) {

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Egresos',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->subtotal,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->subtotal;

                                $total_filas += 1;
                            } else {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    $row->importe,
                                    0.0,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_cargo,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->importe;

                                $total_filas += 1;
                            }

                        }

                        if ($cuenta->cuenta_abono) {

                            if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a.') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva') !== FALSE && $row->iva > 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->iva,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;

                            } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de egresos") !== FALSE) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } elseif (strpos(strtolower($cuenta->nombre_abono), "banco") !== FALSE) {

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $id_cuenta,
                                    $cuenta_banco,
                                    $id_padre,
                                    $nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $nombre_cuenta,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            } else {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Egresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $datos_caratula_contrarecibo->concepto,
                                    0.0,
                                    $row->importe,
                                    $resultado_fuente->descripcion,
                                    $cuenta->nombre_abono,
                                    $centro_costo,
                                    $partida,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->importe;

                                $total_filas += 1;
                            }

                        }
                    }
                }
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_movimiento = array(
                'poliza' => 1,
            );

            $this->ci->db->where('movimiento', $movimiento);
            $this->ci->db->update('mov_bancos_movimientos', $datos_actualizar_movimiento);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la p&oacute;liza de egresos No. ' . $last . '.</div>',
            );

            return $respuesta["mensaje"];

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->delete('mov_poliza_detalle');

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->delete('mov_polizas_cabecera');


            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '.</div>',
            );

            return $respuesta["mensaje"];
        }

    }

    function generar_poliza_ingresos($numero)
    {
        $recaudado = $numero;

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre . " " . $nombre_encontrado->apellido_paterno . " " . $nombre_encontrado->apellido_materno;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if ($ultimo) {
                $last = $ultimo->ultimo + 1;
            } //        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

            $query = "SELECT * FROM mov_recaudado_caratula WHERE numero = ?";
            $resultado = $this->ci->recaudacion_model->get_datos_devengado_caratula($recaudado, $query);

//            $this->debugeo->imprimir_pre($resultado);

            $resultado_detalle = $this->ci->recaudacion_model->get_datos_recaudado_detalle_automatico($recaudado);
            $devengado = $resultado->numero_devengado;

//            $this->debugeo->imprimir_pre($resultado_detalle);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$resultado_detalle) {
                throw new Exception('No hay movimientos dentro del devengado.');
            }

            $centro_recaudacion = '';
            $clase = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;

            $this->ci->db->trans_begin();

            $this->ci->db->select('numero_poliza')->from('mov_polizas_cabecera')->where('no_devengado', $devengado);
            $query_numero_devengado_diario = $this->ci->db->get();
            $numero_devengado_diario = $query_numero_devengado_diario->row_array();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, no_devengado, no_recaudado, concepto_especifico, clave_cliente, cliente, no_movimiento, poliza_devengado) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ? ,?);";
            $datos_caratula_poliza = array(
                $last,
                'Ingresos',
                $resultado->fecha_solicitud,
                $resultado->descripcion,
                0, //$total_filas
                $resultado->importe_total,
                'espera',
                $nombre_completo,
                0, //$cargos
                0, //$abonos
                $devengado,
                $recaudado,
                $resultado->descripcion,
                $resultado->clave_cliente,
                $resultado->cliente,
                $resultado->no_movimiento,
                $numero_devengado_diario["numero_poliza"],
            );

            $resultado_insertar_caratula = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            foreach ($resultado_detalle as $row) {
//                $this->debugeo->imprimir_pre($row);

                $estructura = json_decode($row->estructura);

//                $this->debugeo->imprimir_pre($estructura);

                $centro_recaudacion = $estructura->centro_de_recaudacion;
                $clase = $estructura->clase;

                $datos_cuentas = $this->ci->recaudacion_model->tomar_cuentas_recaudacion($centro_recaudacion);

                foreach ($datos_cuentas as $cuenta) {

                    if (!$cuenta) {
                        throw new Exception('No existe cuenta contable asociada a la partida.');
                    }

//                    $this->debugeo->imprimir_pre($cuenta);

                    if ($cuenta->cuenta_cargo) {
                        if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                                $resultado_cuenta_cargo = $query_cargo->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                                }

                                $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_cargo = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_cargo,
                                    $resultado_cuenta_cargo->id_padre,
                                    $resultado_cuenta_cargo->nivel,
                                    $resultado->descripcion,
                                    $row->iva,
                                    0.0,
                                    4,
                                    $cuenta->nombre_cargo,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                                $cargos += $row->iva;

                                $total_filas += 1;
                            }
                        } else {
                            $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                            $resultado_cuenta_cargo = $query_cargo->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_cargo);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_cargo . ' ' . $cuenta->nombre_cargo);
                            }

                            $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_cargo = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_cargo,
                                $resultado_cuenta_cargo->id_padre,
                                $resultado_cuenta_cargo->nivel,
                                $resultado->descripcion,
                                $row->total_importe,
                                0.0,
                                4,
                                $cuenta->nombre_cargo,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                            $cargos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }

                    if ($cuenta->cuenta_abono) {
                        if (strpos(strtolower($cuenta->nombre_abono), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), "iva") !== FALSE) {
                            if ($row->iva !== 0) {
                                $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                                $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                                $resultado_cuenta_abono = $query_abono->row();

                                $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                                if ($existe_cuenta == FALSE) {
                                    throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                                }

                                $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                                $datos_abono = array(
                                    $last,
                                    'Ingresos',
                                    $cuenta->id_correlacion_partidas_contables,
                                    $cuenta->cuenta_abono,
                                    $resultado_cuenta_abono->id_padre,
                                    $resultado_cuenta_abono->nivel,
                                    $resultado->descripcion,
                                    0.0,
                                    $row->iva,
                                    4,
                                    $cuenta->nombre_abono,
                                    $centro_recaudacion,
                                    $clase,
                                    0,
                                );

                                $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                                $abonos += $row->iva;

                                $total_filas += 1;
                            }
                        } else {
                            $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                            $resultado_cuenta_abono = $query_abono->row();

                            $existe_cuenta = $this->_revisar_cuenta($resultado_cuenta_abono);

                            if ($existe_cuenta == FALSE) {
                                throw new Exception('No existe la cuenta contable ' . $cuenta->cuenta_abono . ' ' . $cuenta->nombre_abono);
                            }

                            $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida, concepto_especifico) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_abono = array(
                                $last,
                                'Ingresos',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_abono,
                                $resultado_cuenta_abono->id_padre,
                                $resultado_cuenta_abono->nivel,
                                $resultado->descripcion,
                                0.0,
                                $row->total_importe,
                                4,
                                $cuenta->nombre_abono,
                                $centro_recaudacion,
                                $clase,
                                0,
                            );

                            $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                            $abonos += $row->total_importe;

                            $total_filas += 1;
                        }
                    }
                }
            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_recaudado = array(
                'poliza' => 1,
            );

            $this->ci->db->where('numero', $recaudado);
            $this->ci->db->update('mov_recaudado_caratula', $datos_actualizar_recaudado);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la P&oacute;liza de Ingresos No. ' . $last . '</div>',
            );

            return $respuesta["mensaje"];

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '.</div>',
            );

            return $respuesta["mensaje"];
        }
    }

    function generar_poliza_facturas_egresos_directa($datos = NULL)
    {

//        $this->debugeo->imprimir_pre($value["movimiento"]);

        try {

            $this->ci->db->trans_begin();

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if ($ultimo) {
                $last = $ultimo->ultimo + 1;
            } //        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;
            $datos_cuentas = array();

            $resultado_insertar_cargo_query = 0;
            $resultado_insertar_abono_query = 0;

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, movimiento, concepto_especifico, contrarecibo, id_proveedor, proveedor, poliza_contrarecibo, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            $datos_caratula_poliza = array(
                $last,
                'Diario',
                $datos["fecha"],
                "Factura devengado egresos",
                0,
                $datos["total"],
                'espera',
                "Armonniza",
                0,
                0,
                0,
                "Factura devengado egresos",
                0,
                "",
                "",
                0,
                $datos["folio"],
            );

            $resultado_insertar_caratula = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            if ($this->ci->db->trans_status() === FALSE) {
                $this->ci->db->trans_rollback();
            } else {
                $this->ci->db->trans_commit();
            }

            $this->ci->db->trans_begin();

            if ($datos["mov"] == "D") {
                $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento_egresos($datos["centro_recaudacion"] . "D");
//                $this->ci->debugeo->imprimir_pre($datos["centro_recaudacion"]."D");
            } else {
                $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_movimiento_egresos($datos["centro_recaudacion"] . "CR");
            }

//                $this->debugeo->imprimir_pre($datos_cuentas);

            if ($datos["descuento"] != 0) {
                $importe = $datos["total"] - $datos["descuento"];
            } else {
                $importe = $datos["total"];
            }

            $nombre_fuente_financiamiento = "Ingresos Propios";

            foreach ($datos_cuentas as $cuenta) {

//                $this->debugeo->imprimir_pre($cuenta);

                if ($cuenta->cuenta_cargo) {

                    if (strpos(strtolower($cuenta->nombre_cargo), "i.v.a.") !== FALSE || strpos(strtolower($cuenta->nombre_cargo), "i.v.a") !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva') !== FALSE) {
                        if ($datos["iva"] !== 0) {
                            $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                            $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                            $resultado_cuenta_cargo = $query_cargo->row();

                            $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                            $datos_cargo = array(
                                $last,
                                'Diario',
                                $cuenta->id_correlacion_partidas_contables,
                                $cuenta->cuenta_cargo,
                                $resultado_cuenta_cargo->id_padre,
                                $resultado_cuenta_cargo->nivel,
                                "Factura devengado egresos",
                                $datos["iva"],
                                0.0,
                                $nombre_fuente_financiamiento,
                                $cuenta->nombre_cargo,
                                $datos["ssscta1"],
                                $datos["ssscta1"],
                            );

                            $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                            $cargos += $datos["iva"];

                            $total_filas += 1;

                        }
                    } elseif (strpos(strtolower($cuenta->nombre_cargo), "descuento") !== FALSE) {
                        $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                        $resultado_cuenta_cargo = $query_cargo->row();

                        $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_cargo = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_cargo,
                            $resultado_cuenta_cargo->id_padre,
                            $resultado_cuenta_cargo->nivel,
                            "Factura devengado egresos",
                            $datos["descuento"],
                            0.0,
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_cargo,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                        $cargos += $datos["descuento"];

                        $total_filas += 1;
                    } elseif (strpos(strtolower($cuenta->nombre_cargo), "presupuesto de") !== FALSE) {
                        $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                        $resultado_cuenta_cargo = $query_cargo->row();

                        $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_cargo = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_cargo,
                            $resultado_cuenta_cargo->id_padre,
                            $resultado_cuenta_cargo->nivel,
                            "Factura devengado egresos",
                            $datos["impbru"],
                            0.0,
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_cargo,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                        $cargos += $datos["impbru"];

                        $total_filas += 1;
                    } elseif (strpos(strtolower($cuenta->nombre_cargo), "servicio") !== FALSE) {
//                        break;
                    } else {

                        $sql_cargo = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_cargo = $this->ci->db->query($sql_cargo, array($cuenta->cuenta_cargo));
                        $resultado_cuenta_cargo = $query_cargo->row();

                        $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta,  cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_cargo = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_cargo,
                            $resultado_cuenta_cargo->id_padre,
                            $resultado_cuenta_cargo->nivel,
                            "Factura devengado egresos",
                            $datos["impbru"],
                            0.0,
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_cargo,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                        $cargos += $datos["impbru"];

                        $total_filas += 1;
                    }

                }

                if ($cuenta->cuenta_abono) {

                    if (strpos(strtolower($cuenta->nombre_abono), 'i.v.a.') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'i.v.a') !== FALSE || strpos(strtolower($cuenta->nombre_abono), 'iva') !== FALSE && $row->iva > 0) {
                        $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                        $resultado_cuenta_abono = $query_abono->row();

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_abono,
                            $resultado_cuenta_abono->id_padre,
                            $resultado_cuenta_abono->nivel,
                            "Factura devengado egresos",
                            0.0,
                            $datos["iva"],
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_abono,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $abonos += $datos["iva"];

                        $total_filas += 1;

                    } elseif (strpos(strtolower($cuenta->nombre_abono), "descuento") !== FALSE) {
                        $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                        $resultado_cuenta_abono = $query_abono->row();

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_abono,
                            $resultado_cuenta_abono->id_padre,
                            $resultado_cuenta_abono->nivel,
                            "Factura devengado egresos",
                            0.0,
                            $datos["descuento"],
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_abono,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $abonos += $datos["descuento"];

                        $total_filas += 1;
                    } elseif (strpos(strtolower($cuenta->nombre_abono), "presupuesto de") !== FALSE) {
                        $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                        $resultado_cuenta_abono = $query_abono->row();

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_abono,
                            $resultado_cuenta_abono->id_padre,
                            $resultado_cuenta_abono->nivel,
                            "Factura devengado egresos",
                            0.0,
                            $datos["impbru"],
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_abono,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $abonos += $datos["impbru"];

                        $total_filas += 1;
                    } elseif (strpos(strtolower($cuenta->nombre_abono), "servicio") !== FALSE) {
//                        continue;
                    } else {
                        $sql_abono = "SELECT cuenta_padre AS id_padre, nivel FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_abono = $this->ci->db->query($sql_abono, array($cuenta->cuenta_abono));
                        $resultado_cuenta_abono = $query_abono->row();

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle (numero_poliza, tipo_poliza, fecha, hora, fecha_real, id_cuenta, cuenta, id_padre, nivel, concepto, debe, haber, subsidio, nombre, centro_costo, partida) VALUES (?, ?, NOW(), NOW(), NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                        $datos_abono = array(
                            $last,
                            'Diario',
                            $cuenta->id_correlacion_partidas_contables,
                            $cuenta->cuenta_abono,
                            $resultado_cuenta_abono->id_padre,
                            $resultado_cuenta_abono->nivel,
                            "Factura devengado egresos",
                            0.0,
                            $datos["total"],
                            $nombre_fuente_financiamiento,
                            $cuenta->nombre_abono,
                            $datos["ssscta1"],
                            $datos["ssscta1"],
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $abonos += $datos["total"];

                        $total_filas += 1;
                    }

                }

            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            if ($this->ci->db->trans_status() === FALSE) {
                $this->ci->db->trans_rollback();
            } else {
                $this->ci->db->trans_commit();
            }

            return TRUE;

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            return FALSE;
        }

    }

    function insertar_poliza_directa_detalle($datos = NULL)
    {
        try {
            $this->ci->db->trans_begin();

            $this->ci->db->select('nombre')->from('cat_cuentas_contables')->where('cuenta', $datos["cuenta"]);
            $query_cuenta_existe = $this->ci->db->get();
            $existe_cuenta = $query_cuenta_existe->row_array();


            $datos_insertar = array(
                "numero_poliza" => $datos["numero_poliza"],
                "cuenta" => $datos["cuenta"],
                "descripcion_cuenta" => $existe_cuenta["nombre"],
                "cargos" => $datos["cargos"],
                "abonos" => $datos["abonos"],
            );

            $this->ci->db->insert('poliza_detalle_cierre_anual', $datos_insertar);

            if ($this->ci->db->trans_status() === FALSE) {

                $this->ci->db->trans_rollback();
                throw new Exception('Ha ocurrido un error al insertar los datos, por favor intentelo de nuevo.');

            } else {

                $this->ci->db->trans_commit();
                return TRUE;
            }
        } catch (Exception $e) {

            $this->ci->db->trans_rollback();
            return $e->getMessage();

        }

    }

    private function _revisar_cuenta($arreglo_cuenta)
    {
        if (isset($arreglo_cuenta->id_padre))
            return TRUE;
        return false;
    }


    function insertar_poliza_cierre_anual_balanza($datos = NULL)
    {

        try {

            $this->ci->db->trans_begin();

            $this->ci->db->select('nombre')->from('mov_poliza_detalle', 'mov_poliza_cabecera')->where('cuenta', $datos["cuenta"]);
            $query_cuenta_existe = $this->ci->db->get();
            $existe_cuenta = $query_cuenta_existe->result_array();

            $datos_insertar = array(

                "numero_poliza" => $datos["numero_poliza"],
                "cuenta" => $datos["cuenta"],
                "descripcion_cuenta" => $existe_cuenta["nombre"],
                "cargos" => $datos["cargos"],
                "abonos" => $datos["abonos"],
            );

            $this->ci->db->insert('mov_polizas_cabecera', $datos_insertar);
            $this->ci->db->insert('mov_poliza_detalle', $datos_insertar);

            //        $this->debugeo->imprimir_pre(cargos);
            //        $this->debugeo->imprimir_pre(abonos);

            if ($this->ci->db->trans_status() === FALSE) {

                $this->ci->db->trans_rollback();
                throw new Exception('Ha ocurrido un error al insertar los datos en la base de datos, por favor intentelo de nuevo.');

            } else {

                $this->ci->db->trans_commit();
                return TRUE;
            }
        } catch (Exception $e) {

            $this->ci->db->trans_rollback();
            return $e->getMessage();


        }

    }

    function escapar_caracteres($string = NULL) {
        $string_limpio = "";

        $string_limpio = mb_convert_encoding($string, 'HTML-ENTITIES', "UTF-8");

        return $string_limpio;
    }

    function generar_poliza_diario_egresos_nuevo($numero = NULL) {
        // $numero = 7;

        try {
            $id = $this->ci->tank_auth->get_user_id();

            $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
            $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
            $nombre_encontrado = $resultado_query_usuario->row();
            $nombre_completo = $nombre_encontrado->nombre . " " . $nombre_encontrado->apellido_paterno . " " . $nombre_encontrado->apellido_materno;

//        Se toma el numero del contrarecibo que se va a buscar
            $contrarecibo = $numero;

//        Se inicizaliza la variable con el ultimo valor de las polizas
            $last = 0;
//        Se toma el numero de la ultima poliza
            $ultimo = $this->ci->ciclo_model->ultima_poliza();

//        Se le suma uno al ultimo valor de las polizas
            if ($ultimo) {
                $last = $ultimo->ultimo + 1;
            } //        De lo contrario se inicia en 1
            else {
                $last = 1;
            }

//        Se prepara el query para llamar todos los datos de la caratula del contrarecibo
            $query_caratula = "SELECT * FROM mov_contrarecibo_caratula WHERE id_contrarecibo_caratula = ?;";

//        Se llama a la funci�n que se encarga de tomar todos los datos de la caratula del contrarecibo
            $datos_caratula_contrarecibo = $this->ci->ciclo_model->get_datos_contrarecibo_caratula($contrarecibo, $query_caratula);

//        Se prepara el query para tomar los datos del detalle de compromiso que est�n ligados al contrarecibo
            $query_detalle_compromiso = "SELECT *, COLUMN_JSON(nivel) AS estructura FROM mov_compromiso_detalle WHERE numero_compromiso = ?;";

//        Se llama ala funcion que se encarga de tomar todos los datos del detalle del compromiso que est� ligado con el contrarecibo
            $datos_detalle_compromiso = $this->ci->ciclo_model->datos_compromisoDetalle($datos_caratula_contrarecibo->numero_compromiso, $query_detalle_compromiso);

//        Se crea un arreglo donde se van a guardar los resultados del detalle del compromiso
            $datos_detalle_poliza = array();

            if (!$datos_detalle_compromiso) {
                throw new Exception('No hay partidas dentro del compromiso.');
            }

            $centro_costo = '';
            $partida = '';
            $nombre_fuente_financiamiento = '';
            $total_filas = 0;
            $cargos = 0;
            $abonos = 0;
            $resultado_insertar_caratula_query = FALSE;
            $resultado_insertar_abono_query = FALSE;
            $resultado_insertar_cargo_query = FALSE;

            $this->ci->db->trans_begin();

            $query_insertar_caratula_poliza = "INSERT INTO mov_polizas_cabecera (numero_poliza, tipo_poliza, fecha, fecha_real, concepto, enfirme, sifirme, no_partidas, importe, cancelada, estatus, creado_por, autorizada, cargos, abonos, id_proveedor, proveedor, contrarecibo, concepto_especifico, no_movimiento) VALUES (?, ?, ?, NOW(), ?, 0, 0, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?);";

            if (isset($datos_caratula_contrarecibo->proveedor) && $datos_caratula_contrarecibo->proveedor != NULL && $datos_caratula_contrarecibo->proveedor != "") {
                $datos_caratula_poliza = array(
                    $last,
                    'Diario',
                    $datos_caratula_contrarecibo->fecha_emision,
                    $datos_caratula_contrarecibo->concepto,
                    0,
                    $datos_caratula_contrarecibo->importe,
                    'espera',
                    $nombre_completo,
                    0,
                    0,
                    $datos_caratula_contrarecibo->id_proveedor,
                    $datos_caratula_contrarecibo->proveedor,
                    $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                    $datos_caratula_contrarecibo->concepto_especifico,
                    $datos_caratula_contrarecibo->documento,
                );
            } else {
                $datos_caratula_poliza = array(
                    $last,
                    'Diario',
                    $datos_caratula_contrarecibo->fecha_emision,
                    $datos_caratula_contrarecibo->concepto,
                    0,
                    $datos_caratula_contrarecibo->importe,
                    'espera',
                    $nombre_completo,
                    0,
                    0,
                    $datos_caratula_contrarecibo->id_persona,
                    $datos_caratula_contrarecibo->nombre_completo,
                    $datos_caratula_contrarecibo->id_contrarecibo_caratula,
                    $datos_caratula_contrarecibo->concepto_especifico,
                    $datos_caratula_contrarecibo->documento,
                );
            }

            $resultado_insertar_caratula_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_caratula_poliza, $datos_caratula_poliza);

            $datos_actualizar_poliza = array(
                'poliza' => 1,
            );

            $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
            $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_poliza);

            foreach ($datos_detalle_compromiso as $key => $value) {

                $estructura = json_decode($value->estructura, TRUE);

                $datos_cuentas = $this->ci->ciclo_model->tomar_cuentas_contrarecibo($estructura["partida"]);

                foreach ($datos_cuentas as $key_cuenta => $value_cuenta) {

                    $sql = "SELECT descripcion FROM cat_clasificador_fuentes_financia WHERE codigo = ?;";
                    $query = $this->ci->db->query($sql, array($estructura["fuente_de_financiamiento"]));
                    $resultado_fuente = $query->row();

                    $nombre_fuente_financiamiento = $resultado_fuente->descripcion;

                    if(isset($value_cuenta->cuenta_cargo)) {

                        $sql_cargo = "SELECT * FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_cargo = $this->ci->db->query($sql_cargo, array($value_cuenta->cuenta_cargo));
                        $resultado_cuenta_cargo = $query_cargo->row_array();

                        $query_insertar_cargo = "INSERT INTO mov_poliza_detalle (
                            numero_poliza, tipo_poliza, fecha, hora,
                            fecha_real, id_cuenta, cuenta, id_padre,
                            nivel, concepto, debe, haber,
                            subsidio, nombre, centro_costo, partida) VALUES (
                            ?, ?, NOW(), NOW(),
                            NOW(), ?, ?, ?,
                            ?, ?, ?, ?,
                            ?, ?, ?, ?);";

                        $datos_cargo = array(
                            $last, 'Diario',
                            $value_cuenta->id_correlacion_partidas_contables, $value_cuenta->cuenta_cargo, $resultado_cuenta_cargo["cuenta_padre"],
                            $resultado_cuenta_cargo["nivel"], $datos_caratula_contrarecibo->concepto_especifico, $value->importe, 0.0,
                            $nombre_fuente_financiamiento, $resultado_cuenta_cargo["nombre"], $estructura["centro_de_costos"], $estructura["partida"],
                        );

                        $resultado_insertar_cargo_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_cargo, $datos_cargo);

                        $cargos += $value->importe;

                        $total_filas += 1;
                    }

                    if(isset($value_cuenta->cuenta_abono)) {
                        
                        $sql_abono = "SELECT * FROM cat_cuentas_contables WHERE cuenta = ?;";
                        $query_abono = $this->ci->db->query($sql_abono, array($value_cuenta->cuenta_abono));
                        $resultado_cuenta_abono = $query_abono->row_array();

                        $query_insertar_abono = "INSERT INTO mov_poliza_detalle (
                            numero_poliza, tipo_poliza, fecha, hora,
                            fecha_real, id_cuenta, cuenta, id_padre,
                            nivel, concepto, debe, haber,
                            subsidio, nombre, centro_costo, partida) VALUES (
                            ?, ?, NOW(), NOW(),
                            NOW(), ?, ?, ?,
                            ?, ?, ?, ?,
                            ?, ?, ?, ?);";

                        $datos_abono = array(
                            $last, 'Diario',
                            $value_cuenta->id_correlacion_partidas_contables, $value_cuenta->cuenta_abono, $resultado_cuenta_abono["cuenta_padre"],
                            $resultado_cuenta_abono["nivel"], $datos_caratula_contrarecibo->concepto_especifico, 0.0, $value->importe,
                            $nombre_fuente_financiamiento, $resultado_cuenta_abono["nombre"], $estructura["centro_de_costos"], $estructura["partida"],
                        );

                        $resultado_insertar_abono_query = $this->ci->ciclo_model->insertar_detalle_poliza($query_insertar_abono, $datos_abono);

                        $abonos += $value->importe;

                        $total_filas += 1;

                    }

                }

            }

            $datos_actualizar_caratula = array(
                'no_partidas' => $total_filas,
                'cargos' => $cargos,
                'abonos' => $abonos,
            );

            $this->ci->db->where('numero_poliza', $last);
            $this->ci->db->update('mov_polizas_cabecera', $datos_actualizar_caratula);

            $datos_actualizar_contrarecibo = array(
                'poliza' => 1,
            );

            $this->ci->db->where('id_contrarecibo_caratula', $contrarecibo);
            $this->ci->db->update('mov_contrarecibo_caratula', $datos_actualizar_contrarecibo);

            $this->ci->db->trans_commit();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Se ha generado con &eacute;xito la P&oacute;liza de Diario No. ' . $last . '</div>',
            );

            return $respuesta["mensaje"];

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $e->getMessage() . '.</div>',
            );

            return $respuesta["mensaje"];
        }
    }

    function enviar_correo($destinatario, $remitente, $nombre_remitente, $asunto, $mensaje) {

        // Se crea la configuracion para poder enviar el correo
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.1and1.mx',
            'smtp_port' => 25,
            'smtp_user' => 'daniel.rodriguez@armonniza.com',
            'smtp_pass' => 'Daniel2014Rodriguez',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );

        // Se carga la libreria junto con su configuracion
        $this->ci->load->library('email', $config);
        // Se cambian las lineas finales para evitar errores de codificacion
        $this->ci->email->set_newline("\r\n");
        // Se agrega el remitente
        $this->ci->email->from($remitente, $nombre_remitente);
        // Se agrega el destinatario
        $this->ci->email->to($destinatario);
        // Se agrega el asunto
        $this->ci->email->subject($asunto);
        // Se agrega el mensaje
        $this->ci->email->message($mensaje);

        if ($this->ci->email->send()) {
            return TRUE;
        } else {
            return FALSE;
            // echo(json_encode($this->ci->email->print_debugger()));
        }
    }

    function generar_compromiso($numero_precompro, $id_factura) {
        
        // Se inicia la transacci�n
        $this->ci->db->trans_begin();

        try {
            // Se toman los datos de la factura
            $factura = $this->ci->api_xmlrpc->ver_datos_factura($id_factura);
            // Se toma los datos del encabezado del precompromiso
            $encabezado_precompromiso = $this->ci->extranet_model->tomar_encabezado_precompromiso($numero_precompro);
            // Se toma los datos del detalle del precompromiso
            $detalle_precompromiso = $this->ci->extranet_model->tomar_detalle_precompromiso($numero_precompro);

            // $this->ci->debugeo->imprimir_pre($factura);

            // Se aparte el n�mero del compromiso
            $last = 0;
            // Se toma el numero del ultimo compromiso
            $ultimo = $this->ci->ciclo_model->ultimo_compromiso();

            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
            else {
                $last = 1;
            }

            $this->ci->ciclo_model->apartarcompromiso($last);

            // Se inicializa la variable que contiene la respuesta de la inserci�n de datos
            $respuesta = array();

            // Se borra el detalle del compromiso, si es que existe previamente
            $this->ci->db->where('numero_compromiso', $last);
            $this->ci->db->delete('mov_compromiso_detalle');

            // Se llama a la funci�n para copiar el detalle del precompromiso al detalle del compromiso
            // La cual devuelve los mensajes de exito o error al calcular el presupuesto disponible
            $resultado = $this->ci->ciclo_model->copiarDatosPrecompromisoAutomatico($numero_precompro, $last, $factura);

            // Se cambia el estatus del compromiso
            $encabezado_precompromiso["estatus"] = "activo";
            $encabezado_precompromiso["fecha_solicitud"] = $encabezado_precompromiso["fecha_emision"];
            $encabezado_precompromiso["ultimo_compromiso"] = $last;
            $encabezado_precompromiso["no_precompromiso"] = $encabezado_precompromiso["numero_pre"];

            $encabezado_precompromiso["total_hidden"] = $factura["total"];
            $encabezado_precompromiso["fecha_entrega"] = $encabezado_precompromiso["fecha_emision"];
            $encabezado_precompromiso["id_tipo_impresion"] = 0;
            $encabezado_precompromiso["condicion_entrega"] = "";
            $encabezado_precompromiso["tipo_compromiso"] = $encabezado_precompromiso["tipo_requisicion"];
            $encabezado_precompromiso["tipo_radio"] = $encabezado_precompromiso["tipo_gasto"];
            $encabezado_precompromiso["check_firme"] = 1;
            $encabezado_precompromiso["tipo_impresion"] = "";
            $encabezado_precompromiso["nombre_persona"] = "";
            $encabezado_precompromiso["zona_marginada_check"] = $encabezado_precompromiso["zona_marginada"];
            $encabezado_precompromiso["zona_mas_economica_check"] = $encabezado_precompromiso["zona_mas_economica"];
            $encabezado_precompromiso["zona_menos_economica_check"] = $encabezado_precompromiso["zona_menos_economica"];
            $encabezado_precompromiso["firma1"] = 1;
            $encabezado_precompromiso["firma2"] = 1;
            $encabezado_precompromiso["firma3"] = 1;
            $encabezado_precompromiso["descripcion_general"] = $encabezado_precompromiso["descripcion"];

            // Se ingresa la fecha de autorizacion
            $encabezado_precompromiso["fecha_autoriza"] = date("Y-m-d");

//            Se suma el total del importe del compromiso, al total que actualmente est� en el precompromiso
            $total = number_format($encabezado_precompromiso["total_compromiso"], 2, '.', '') + number_format($factura["total"], 2, '.', '');

//            Si el resultado es mayor al total del precompromiso, se tiene que volver a autorizar el compromiso,
//            es por eso que las firmas se pasan en 0 aunque est� previamente autorizado
            if(number_format($total, 2, '.', '') > number_format($encabezado_precompromiso["total"], 2, '.', '')) {
                throw new Exception('El saldo de la factura que se est� autorizando supera el total de la cantidad dentro del precompromiso.');    
            } else {
//            Se llama a la funci�n que se encarga de actualizar el saldo de las partidas del compromiso
                $respuesta_paso_1 = $this->ci->ciclo_model->actualizar_saldo_compromiso($encabezado_precompromiso);

                if (strpos($respuesta_paso_1["mensaje"], 'danger') !== false) {
                    throw new Exception('Hubo un error al generar el compromiso, por favor, contacte a su administrador.');
                }

                // Se actualiza el total comprometido del precompromiso
                $query_actualizar = "UPDATE mov_precompromiso_caratula SET total_compromiso = ? WHERE numero_pre = ?;";
                $query_final = $this->ci->db->query($query_actualizar, array($total, $encabezado_precompromiso["numero_pre"]));

                // Se insertan los datos de la caratula del compromiso
                $respuesta_paso_2 = $this->ci->ciclo_model->insertar_caratula_compromiso($encabezado_precompromiso);

                if($respuesta_paso_2) {
                    $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Se ha generado el compromiso '.$last.'.</div>';
                }

            }

            if ($this->ci->db->trans_status() === FALSE) {
                $this->ci->db->trans_rollback();
            } else {
                $this->ci->db->trans_commit();
                $respuesta["compromiso"] = $last;
            }

            log_message('info', 'El usuario '.$this->ci->tank_auth->get_username().', ha generado el compromiso '.$last.' al autorizar la factura No. '.$id_factura);

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => $e->getMessage()
                );
        }

        // $this->ci->debugeo->imprimir_pre($respuesta);

        return $respuesta;
    }

    function generar_contrarecibo($compromiso, $id_factura) {
        // Se inicia la transacci�n
        $this->ci->db->trans_begin();

        // Se inicializa la variable que contiene la respuesta de la inserci�n de datos
        $respuesta = array();

        try {
            // Se toman los datos de la factura
            $factura = $this->ci->api_xmlrpc->ver_datos_factura($id_factura);
            // Se toma los datos del encabezado del precompromiso
            $encabezado_compromiso = $this->ci->extranet_model->tomar_encabezado_compromiso($compromiso);
            // Se toma los datos del detalle del precompromiso
            $detalle_compromiso = $this->ci->extranet_model->tomar_detalle_compromiso($compromiso);

            // $this->ci->debugeo->imprimir_pre($encabezado_compromiso);

            // Se aparte el contrarecibo
            $last = 0;

            // Se toma el numero del ultimo precompromiso
            $ultimo = $this->ci->ciclo_model->ultimo_contrarecibo();

            if($ultimo) {
                $last = $ultimo->ultimo + 1;
            }
            else {
                $last = 1;
            }

            $this->ci->ciclo_model->apartarContrarecibo($last);

            // Se llama a la funci�n para copiar el detalle del precompromiso al detalle del compromiso
            // La cual devuelve los mensajes de exito o error al calcular el presupuesto disponible
            $this->ci->ciclo_model->copiarDatosCompromiso($compromiso, $last);

            $this->ci->ciclo_model->marcar_devengado($compromiso, date("Y-m-d"));
            $encabezado_compromiso["estatus"] = "activo";

            $encabezado_compromiso["destino"] = $encabezado_compromiso["lugar_entrega"];
            $encabezado_compromiso["destino"] = $encabezado_compromiso["lugar_entrega"];
            $encabezado_compromiso["num_compromiso"] = $encabezado_compromiso["numero_compromiso"];
            $encabezado_compromiso["clave_proveedor"] = $encabezado_compromiso["id_proveedor"];
            $encabezado_compromiso["proveedor"] = $encabezado_compromiso["nombre_proveedor"];
            $encabezado_compromiso["concepto"] = $encabezado_compromiso["concepto_especifico"];
            $encabezado_compromiso["descripcion"] = $encabezado_compromiso["descripcion_general"];
            $encabezado_compromiso["documento"] = $factura["folio"];
            $encabezado_compromiso["fecha_pago"] = date("Y-m-d");
            $encabezado_compromiso["fecha_solicita"] = date("Y-m-d");
            $encabezado_compromiso["firme"] = 1;
            $encabezado_compromiso["importe"] = $factura["total"];
            $encabezado_compromiso["tipo_documento"] = "Factura";
            $encabezado_compromiso["contrarecibo"] = $last;

            $resultado_insertar = $this->ci->ciclo_model->insertar_caratula_contrarecibo($encabezado_compromiso);

            // $this->ci->debugeo->imprimir_pre($resultado_insertar);

            $poliza = "";

            // Si el resultado es exitoso, se le indica al usuario
            if($resultado_insertar == "exito") {
                if ($this->ci->db->trans_status() === FALSE) {
                    $this->ci->db->trans_rollback();
                    throw new Exception('Hubo un error al generar el contrarecibo, por favor, contacte a su administrador.');
                }
                else {
                    $this->ci->db->trans_commit();
                    $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="fa fa-check-circle fa-2x ic-msj"></i> Se ha creado el contra recibo '.$last.'.</div>';
                    $respuesta["contrarecibo"] = $last;
                }

            }

            // Esto se tiene que descomentar para poder poder generar las p�lizas de diario una vez que est� lista la matriz de conversi�n
             $poliza = $this->generar_poliza_diario_egresos_nuevo($last);
             $respuesta["mensaje"] .= $poliza;

            log_message('info', 'El usuario '.$this->ci->tank_auth->get_username().', ha generado el contrarecibo '.$last);

        } catch (Exception $e) {
            $this->ci->db->trans_rollback();

            $respuesta = array(
                "mensaje" => '<div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                '.$e->getMessage().'
                            </div>',
                        );
        }

        return $respuesta;
    }

    function cadena_random_proveedor() {
        $random = '';

        do {
            $random = "A".substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
            $this->ci->db->select('clave_prov')->from('cat_proveedores')->where('clave_prov', $random);
            $query = $this->ci->db->get();
        } while ($query->row() != NULL);

        return $random;
    }

    function marcar_factura_pagada($contrarecibo) {
        
        $this->ci->db->trans_begin();

        $this->ci->db->select('documento')
                        ->from('mov_contrarecibo_caratula')
                        ->where('id_contrarecibo_caratula', $contrarecibo);
        $query_documento_contrarecibo = $this->ci->db->get();
        $documento_contrarecibo = $query_documento_contrarecibo->row_array();

        $DB1 = $this->ci->load->database('extranet', TRUE);

        $datos_actualizar = array(
            'estatus' => 5,
        );

        $DB1->where('folio', $documento_contrarecibo["documento"]);
        $DB1->update('facturas_xml', $datos_actualizar);

        if ($this->ci->db->trans_status() === FALSE) {
            $this->ci->db->trans_rollback();
            return FALSE;
        } else {
            $this->ci->db->trans_commit();
            return TRUE;
        }

    }
}