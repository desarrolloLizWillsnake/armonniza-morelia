<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_xmlrpc {

    function __construct() {
        $this->ci =& get_instance();
    }

	function activar_proveedor_remoto($id_proveedor) {

        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');

        // $this->ci->debugeo->imprimir_pre($server_url);
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Activar_Proveedor');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_proveedor,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
        
	}

    function desactivar_proveedor_remoto($id_proveedor) {

        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');

        // $this->ci->debugeo->imprimir_pre($server_url);
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Desactivar_Proveedor');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_proveedor,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
        
    }

    function ver_proveedor_remoto($id_proveedor) {
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Ver_Proveedor');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_proveedor,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
    }

    function insertar_proveedor_remoto($datos) {
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Insertar_Proveedor');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array_values($datos);
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
    }

    function ver_datos_factura($id_factura) {
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Ver_Factura');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_factura,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
    }

    function autorizar_factura_1($id_factura, $fecha) {

        $id = $this->ci->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');

        // $this->ci->debugeo->imprimir_pre($server_url);
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Autorizar_Factura_1');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_factura,
            $nombre_completo,
            $fecha,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
    }

    function autorizar_factura_2($id_factura, $fecha, $contrarecibo) {

        $id = $this->ci->tank_auth->get_user_id();

        $query_usuario = "SELECT nombre, apellido_paterno, apellido_materno FROM datos_usuario WHERE id_usuario = ?;";
        $resultado_query_usuario = $this->ci->db->query($query_usuario, array($id));
        $nombre_encontrado = $resultado_query_usuario->row();
        $nombre_completo = $nombre_encontrado->nombre." ".$nombre_encontrado->apellido_paterno." ".$nombre_encontrado->apellido_materno;

        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');

        // $this->ci->debugeo->imprimir_pre($server_url);
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Autorizar_Factura_2');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_factura,
            $nombre_completo,
            $fecha,
            $contrarecibo,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
    }

    function rechazar_factura($id_factura, $observaciones) {

        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');

        // $this->ci->debugeo->imprimir_pre($server_url);
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->ci->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Rechazar_Factura');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $id_factura,
            $observaciones,
        );
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
        
    }
}