var datos_tabla_cuenta_cargo;
var datos_tabla_cuenta_abono;

$('#tabla_cuenta_cargo tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_cargo = $('#tabla_cuenta_cargo').DataTable().row( this ).data();
});

$('#modal_cuenta_cargo').on('hidden.bs.modal', function () {
    $('#cuenta_cargo').val(datos_tabla_cuenta_cargo[0]);
    $('#descripcion_cuenta_cargo').val(datos_tabla_cuenta_cargo[1]);
});

$('#tabla_cuenta_abono tbody').on( 'click', 'tr', function () {
    datos_tabla_cuenta_abono = $('#tabla_cuenta_abono').DataTable().row( this ).data();
});

$('#modal_cuenta_abono').on('hidden.bs.modal', function () {
    $('#cuenta_abono').val(datos_tabla_cuenta_abono[0]);
    $('#descripcion_cuenta_abono').val(datos_tabla_cuenta_abono[1]);
});

$('#check_firme').on("change", function() {
    if($('#check_firme').is(':checked')){
        $("#firme").val(1);
    } else {
        $("#firme").val(0);
    }
});

$("#guardar_movimiento").on( "click", function(e) {
    e.preventDefault();
    $(".forma_movimiento").validate({
        focusInvalid: true,
        rules: {
            cuenta_cargo:{
                required: true
            },
            cuenta_abono:{
                required: true
            }
        },
        messages: {
            cuenta_cargo: {
                required: "* Este campo es obligatorio"
            },
            cuenta_abono: {
                required: "* Este campo es obligatorio"
            }
        }
    }).form();

    if($(".forma_movimiento").valid(e)) {
        e.preventDefault();

        var map = {};
        $(":input").each(function () {
            map[$(this).attr("name")] = $(this).val();
        });

        if ($('#check_firme').is(':checked')) {
            map.check_firme = 1;
        } else {
            map.check_firme = 0;
        }

        $.ajax({
            url: '/ciclo/generar_poliza_movimiento_extrapresupuesto',
            method: 'POST',
            dataType: 'json',
            data: map,
            success: function (s) {
                $("#resultado_insertar_caratula").html(s.mensaje);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
});

$(document).ready(function() {

    $('#tabla_cuenta_cargo').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_cuentas_contables",
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

    $('#tabla_cuenta_abono').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/ciclo/tabla_cuentas_contables",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent":
                '<a data-dismiss="modal"><i class="fa fa-check"></i></a>'
        } ]
    } );

});
