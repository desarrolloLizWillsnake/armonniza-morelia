var nivel1;
var nivel2;
var nivel3;
var nivel4;
var nivel5;

var nivel_buscar1;
var nivel_buscar2;
var nivel_buscar3;
var nivel_buscar4;
var nivel_buscar5;

// Create a Bar Chart with Morris
var chart = Morris.Bar({
    element: 'grafica_partida',
    data: [ ],
    xkey: 'y',
    ykeys: ['a'],
    barColors: ['#B6CE33'],
    hideHover: 'auto',
    labels: ["Dinero"],
    hoverCallback: function (index, options, content, row) {
        return row.y + " $" + $.number( row.a, 2 );
    },
    resize: true
});

$("#forma_nivel1").click(function(){
    $("#buscar_nivel3").prop('disabled', true);
    $( "#select_nivel3" ).html( '' );
    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );

    $('#forma_nivel1 option:selected').each(function(){
        nivel1 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel2",
                type: "POST",
                data: { nivel1: nivel1 },
                success:function(result) {
                    $("#input_nivel1").val(nivel1);
                    $("#buscar_nivel2").prop('disabled', false);
                    $( "#select_nivel2" ).html( '' );
                    $( "#select_nivel2" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel1").keyup(function(){
    nivel_buscar1 = $('#buscar_nivel1').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel1",
            type: "POST",
            data: { nivel_buscar1: nivel_buscar1  },
            success:function(result) {
                $( "#select_nivel1" ).html( '' );
                $( "#select_nivel1" ).append( result );
            }
        }
    );
});

$("#forma_nivel2").click(function(){

    $("#buscar_nivel4").prop('disabled', true);
    $( "#select_nivel4" ).html( '' );
    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );

    $('#forma_nivel2 option:selected').each(function(){
        nivel2 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel3",
                type: "POST",
                data: { nivel1: nivel1, nivel2: nivel2 },
                success: function(result) {
                    $("#input_nivel2").val(nivel2);
                    $("#buscar_nivel3").prop('disabled', false);
                    $( "#select_nivel3" ).html( '' );
                    $( "#select_nivel3" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel2").keyup(function(){
    nivel_buscar2 = $('#buscar_nivel2').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel2",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel_buscar2
            },
            success:function(result) {
                $( "#select_nivel2" ).html( '' );
                $( "#select_nivel2" ).append( result );
            }
        }
    );
});

$("#forma_nivel3").click(function(){

    $("#buscar_nivel5").prop('disabled', true);
    $( "#select_nivel5" ).html( '' );

    $('#forma_nivel3 option:selected').each(function(){
        nivel3 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel4",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3
                },
                success: function(result) {
                    $("#input_nivel3").val(nivel3);
                    $("#buscar_nivel4").prop('disabled', false);
                    $( "#select_nivel4" ).html( '' );
                    $( "#select_nivel4" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel3").keyup(function(){
    nivel_buscar3 = $('#buscar_nivel3').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel3",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel_buscar3
            },
            success:function(result) {
                $( "#select_nivel3" ).html( '' );
                $( "#select_nivel3" ).append( result );
            }
        }
    );
});

$("#forma_nivel4").click(function(){

    $('#forma_nivel4 option:selected').each(function(){
        nivel4 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_nivel5",
                type: "POST",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4
                },
                success: function(result) {
                    $("#input_nivel4").val(nivel4);
                    $("#buscar_nivel4").prop('disabled', false);
                    $( "#select_nivel5" ).html( '' );
                    $( "#select_nivel5" ).append( result );
                }
            }
        );
    });
});

$("#buscar_nivel4").keyup(function(){
    nivel_buscar4 = $('#buscar_nivel4').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel4",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel_buscar4
            },
            success:function(result) {
                $( "#select_nivel4" ).html( '' );
                $( "#select_nivel4" ).append( result );
            }
        }
    );
});

$("#forma_nivel5").click(function(){

    $('#forma_nivel5 option:selected').each(function(){
        nivel5 = $(this).val();
        $.ajax( {
                url: "/ingresos/obtener_datosUltimoNivel",
                type: "POST",
                dataType: "json",
                data: {
                    nivel1: nivel1,
                    nivel2: nivel2,
                    nivel3: nivel3,
                    nivel4: nivel4,
                    nivel5: nivel5
                }
            }
        ).done(function( data ) {
                $("#input_nivel5").val(nivel4);
                $("#elegir_ultimoNivel").prop('disabled', false);
                // When the response to the AJAX request comes back render the chart with new data
                chart.setData(
                    [
                        { y: "Autorizado", a: data.total_anual },
                        { y: "por Recibir", a: data.saldo },
                        { y: "Devengado", a: data.mes_precompromiso },
                        { y: "Cobrado", a: data.mes_compromiso }
                    ]);
            })
            .fail(function(e) {
                // If there is no communication between the server, show an error
                alert( "error occured" );
            });
    });
});

$("#buscar_nivel5").keyup(function(){
    nivel_buscar5 = $('#buscar_nivel5').val();
    $.ajax( {
            url: "/ingresos/buscar_nivel5",
            type: "POST",
            data: {
                nivel_buscar1: nivel1,
                nivel_buscar2: nivel2,
                nivel_buscar3: nivel3,
                nivel_buscar4: nivel4,
                nivel_buscar5: nivel_buscar5
            },
            success:function(result) {
                $( "#select_nivel5" ).html( '' );
                $( "#select_nivel5" ).append( result );
            }
        }
    );
});

$("#egresos_elegir_agregar_nivel").on( "change", function(){
    var nivel = $("#egresos_elegir_agregar_nivel option:selected").val();

    switch (nivel) {
        case "1":
            agregar_nivel1();
            break;
        case "2":
            agregar_nivel2();
            break;
        case "3":
            agregar_nivel3();
            break;
        case "4":
            agregar_nivel4();
            break;
        case "5":
            agregar_nivel5();
            break;
    }


});

function agregar_nivel1() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#gerencia_agregar_select").hide();
    $("#centro_de_recaudacion_agregar_select").hide();
    $("#rubro_agregar_select").hide();
    $("#tipo_agregar_select").hide();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Gerencia" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n de Gerencia" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel2() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#gerencia_agregar_select").show();
    $("#centro_de_recaudacion_agregar_select").hide();
    $("#rubro_agregar_select").hide();
    $("#tipo_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Centro de Recaudaci&oacute;n" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n de Centro de Recaudaci&oacute;n" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel3() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#gerencia_agregar_select").show();
    $("#centro_de_recaudacion_agregar_select").show();
    $("#rubro_agregar_select").hide();
    $("#tipo_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Rubro" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n de Rubro" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel4() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#gerencia_agregar_select").show();
    $("#centro_de_recaudacion_agregar_select").show();
    $("#rubro_agregar_select").show();
    $("#tipo_agregar_select").hide();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Tipo" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n de Tipo" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function agregar_nivel5() {
    $("#egresos_modal_agregar_contenido").html('');

    $("#gerencia_agregar_select").show();
    $("#centro_de_recaudacion_agregar_select").show();
    $("#rubro_agregar_select").show();
    $("#tipo_agregar_select").show();

    tomar_nivel_1();

    $("#egresos_modal_agregar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Clase" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n de Clase" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Agregar" />' +
        '</div>');
}

function tomar_nivel_1() {
    $( "#gerencia_agregar_select" ).html('');

    $.ajax( {
            url: "/ingresos/obtener_nivel1",
            type: "POST",
            success:function(result) {
                $( "#gerencia_agregar_select" ).append( '<option value="">Elegir Gerencia</option>' );
                $( "#gerencia_agregar_select" ).append( result );
            }
        }
    );
}

$("#gerencia_agregar_select").on("change", function(){
    nivel1 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel2",
            type: "POST",
            data: { nivel1: nivel1 },
            success:function(result) {
                $( "#centro_de_recaudacion_agregar_select" ).html( '' );
                $( "#centro_de_recaudacion_agregar_select" ).append( '<option value="">Elegir Centro de Recaudaci&oacute;n</option>' );
                $( "#centro_de_recaudacion_agregar_select" ).append( result );
            }
        }
    );
});

$("#centro_de_recaudacion_agregar_select").on("change", function(){
    nivel1 = $('#gerencia_agregar_select option:selected').val();
    nivel2 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel3",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2
            },
            success:function(result) {
                $( "#rubro_agregar_select" ).html( '' );
                $( "#rubro_agregar_select" ).append( '<option value="">Elegir Rubro</option>' );
                $( "#rubro_agregar_select" ).append( result );
            }
        }
    );
});

$("#rubro_agregar_select").on("change", function(){
    nivel1 = $('#gerencia_agregar_select option:selected').val();
    nivel2 = $('#centro_de_recaudacion_agregar_select option:selected').val();
    nivel3 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel4",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3
            },
            success:function(result) {
                $( "#tipo_agregar_select" ).html( '' );
                $( "#tipo_agregar_select" ).append( '<option value="">Elegir Tipo</option>' );
                $( "#tipo_agregar_select" ).append( result );
            }
        }
    );
});

$("#tipo_agregar_select").on("change", function(){
    nivel1 = $('#gerencia_agregar_select option:selected').val();
    nivel2 = $('#centro_de_recaudacion_agregar_select option:selected').val();
    nivel3 = $('#rubro_agregar_select option:selected').val();
    nivel4 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel5",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4
            },
            success:function(result) {
                $( "#concepto_agregar_select" ).html( '' );
                $( "#concepto_agregar_select" ).append( '<option value="">Elegir Concepto</option>' );
                $( "#concepto_agregar_select" ).append( result );
            }
        }
    );
});

$("#form_agregar_partida").on("submit", function(e) {
    e.preventDefault();
    var map = {};
    var nivel = 1;

    $("#form_agregar_partida select").each(function () {
        var valor = "";
        if ($(this).val() == null) {
            valor = "";
        } else {
            valor = $(this).val();
        }
        map["nivel" + nivel] = valor;
        nivel += 1;
    });
    map["clave"] = $("#clave").val();
    map["descripcion"] = $("#descripcion").val();

    $.ajax( {
            url: "/ingresos/agregar_nivel",
            type: "POST",
            dataType: 'json',
            data: map,
            success:function(result) {
                $("#egresos_modal_agregar_contenido").append(result.mensaje);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        }
    );

});

$("#egresos_elegir_editar_nivel").on( "change", function(){
    var nivel = $("#egresos_elegir_editar_nivel option:selected").val();

    switch (nivel) {
        case "1":
            editar_nivel1();
            break;
        case "2":
            editar_nivel2();
            break;
        case "3":
            editar_nivel3();
            break;
        case "4":
            editar_nivel4();
            break;
        case "5":
            editar_nivel5();
            break;
    }


});

function editar_nivel1() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").hide();
    $("#centro_de_costos_editar_select").hide();
    $("#capitulo_editar_select").hide();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Gerencia" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Gerencia" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel2() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").hide();
    $("#capitulo_editar_select").hide();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Centro de Recaudaci&oacute;n" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Centro de Recaudaci&oacute;n" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel3() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").hide();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Rubro" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Rubro" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel4() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").show();
    $("#concepto_editar_select").hide();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Tipo" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Tipo" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

function editar_nivel5() {
    $("#egresos_modal_editar_contenido").html('');

    $("#fuente_financiamiento_editar_select").show();
    $("#programa_financiamiento_editar_select").show();
    $("#centro_de_costos_editar_select").show();
    $("#capitulo_editar_select").show();
    $("#concepto_editar_select").show();
    $("#partida_editar_select").hide();

    tomar_nivel_editar_1();

    $("#egresos_modal_editar_contenido").append(
        '<div>' +
        '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Clase" required />' +
        '<br />' +
        '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Clase" required />' +
        '<hr />' +
        '<input type="submit" class="btn btn-green" value="Editar" />' +
        '</div>');
}

// function editar_nivel6() {
//     $("#egresos_modal_editar_contenido").html('');

//     $("#fuente_financiamiento_editar_select").show();
//     $("#programa_financiamiento_editar_select").show();
//     $("#centro_de_costos_editar_select").show();
//     $("#capitulo_editar_select").show();
//     $("#concepto_editar_select").show();
//     $("#partida_editar_select").show();

//     tomar_nivel_editar_1();

//     $("#egresos_modal_editar_contenido").append(
//         '<div>' +
//         '<input type="text" class="form-control" name="clave" id="clave" placeholder="Clave de Partida" required />' +
//         '<br />' +
//         '<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripci&oacute;n Partida" required />' +
//         '<hr />' +
//         '<input type="submit" class="btn btn-green" value="Editar" />' +
//         '</div>');
// }

function tomar_nivel_editar_1() {
    $( "#fuente_financiamiento_editar_select" ).html('');

    $.ajax( {
            url: "/ingresos/obtener_nivel1",
            type: "POST",
            success:function(result) {
                $( "#fuente_financiamiento_editar_select" ).append( '<option value="">Elegir Gerencia</option>' );
                $( "#fuente_financiamiento_editar_select" ).append( result );
            }
        }
    );
}

$("#fuente_financiamiento_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#fuente_financiamiento_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel2",
            type: "POST",
            data: { nivel1: nivel1 },
            success:function(result) {
                $( "#programa_financiamiento_editar_select" ).html( '' );
                $( "#programa_financiamiento_editar_select" ).append( '<option value="">Elegir Centro de Recaudaci&oacute;n</option>' );
                $( "#programa_financiamiento_editar_select" ).append( result );
            }
        }
    );
});

$("#programa_financiamiento_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#programa_financiamiento_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel3",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2
            },
            success:function(result) {
                $( "#centro_de_costos_editar_select" ).html( '' );
                $( "#centro_de_costos_editar_select" ).append( '<option value="">Elegir Rubro</option>' );
                $( "#centro_de_costos_editar_select" ).append( result );
            }
        }
    );
});

$("#centro_de_costos_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#centro_de_costos_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_editar_select option:selected').val();
    nivel3 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel4",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3
            },
            success:function(result) {
                $( "#capitulo_editar_select" ).html( '' );
                $( "#capitulo_editar_select" ).append( '<option value="">Elegir Tipo</option>' );
                $( "#capitulo_editar_select" ).append( result );
            }
        }
    );
});

$("#capitulo_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#capitulo_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);

    nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
    nivel2 = $('#programa_financiamiento_editar_select option:selected').val();
    nivel3 = $('#centro_de_costos_editar_select option:selected').val();
    nivel4 = $(this).val();
    $.ajax( {
            url: "/ingresos/obtener_nivel5",
            type: "POST",
            data: {
                nivel1: nivel1,
                nivel2: nivel2,
                nivel3: nivel3,
                nivel4: nivel4
            },
            success:function(result) {
                $( "#concepto_editar_select" ).html( '' );
                $( "#concepto_editar_select" ).append( '<option value="">Elegir Clase</option>' );
                $( "#concepto_editar_select" ).append( result );
            }
        }
    );
});

// $("#concepto_editar_select").on("change", function(){
//     $("#clave").val($(this).val());
//     $("#clave_anterior").val($(this).val());
//     var cadena = $("#concepto_editar_select option:selected").text();
//     cadena = cadena.substring(cadena.indexOf("-") + 3);
//     $("#descripcion").val(cadena);
//     $("#descripcion_anterior").val(cadena);

//     nivel1 = $('#fuente_financiamiento_editar_select option:selected').val();
//     nivel2 = $('#programa_financiamiento_editar_select option:selected').val();
//     nivel3 = $('#centro_de_costos_editar_select option:selected').val();
//     nivel4 = $('#capitulo_editar_select option:selected').val();
//     nivel5 = $(this).val();
//     $.ajax( {
//             url: "/ingresos/obtener_nivel6",
//             type: "POST",
//             data: {
//                 nivel1: nivel1,
//                 nivel2: nivel2,
//                 nivel3: nivel3,
//                 nivel4: nivel4,
//                 nivel5: nivel5
//             },
//             success:function(result) {
//                 $( "#partida_editar_select" ).html( '' );
//                 $( "#partida_editar_select" ).append( '<option value="">Elegir Partida</option>' );
//                 $( "#partida_editar_select" ).append( result );
//             }
//         }
//     );
// });

$("#partida_editar_select").on("change", function(){
    $("#clave").val($(this).val());
    $("#clave_anterior").val($(this).val());
    var cadena = $("#partida_editar_select option:selected").text();
    cadena = cadena.substring(cadena.indexOf("-") + 3);
    $("#descripcion").val(cadena);
    $("#descripcion_anterior").val(cadena);
});

$("#form_editar_partida").on("submit", function(e) {
    e.preventDefault();
    var map = {};
    var nivel = 1;

    $("#form_editar_partida select").each(function () {
        var valor = "";
        if ($(this).val() == null) {
            valor = "";
        } else {
            valor = $(this).val();
        }
        map["nivel" + nivel] = valor;
        nivel += 1;
    });
    map["clave"] = $("#clave").val();
    map["descripcion"] = $("#descripcion").val();
    map["clave_anterior"] = $("#clave_anterior").val();
    map["descripcion_anterior"] = $("#descripcion_anterior").val();

    $.ajax( {
            url: "/ingresos/editar_nivel",
            type: "POST",
            dataType: 'json',
            data: map,
            success:function(result) {
                $("#egresos_modal_editar_contenido").append(result.mensaje);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        }
    );

});