/**
 * Created by Lizbeth on 28/08/15.
 */

$("#guardar_privilegios").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {

        if ($(this).attr("type") == "checkbox") {

            if($(this).is(':checked')){
                map[$(this).attr("name")] = 1;
            } else {
                map[$(this).attr("name")] = 0;
            }

        } else {
            map[$(this).attr("name")] = $(this).val();
        }
    });

    //console.log(map);

    $.ajax({
        url: '/administrador/editar_privilegios',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){

            $("#resultado_insertar_caratula").html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});

$('#select_todos').click(function(event) {
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    }
    else {
        $(':checkbox').each(function() {
            this.checked = false;
        });
    }
});