var tipo_precompromiso;
var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "order": [[ 0, "desc" ]],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/contabilidad/tabla_indice_polizas",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  8 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }, {
            "targets": [ 6 ],
            "render": function ( data, type, row ) {
                var firme = '';
                if (data == 0) {
                    firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                } else if (data == 1) {
                    firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
                }
                return firme;
            }
        }, {
            "targets": [ 7 ],
            "render": function ( data, type, row ) {
                var estatus = '';
                if (data == "activo") {
                    estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
                } else if (data == "espera") {
                    estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
                } else {
                    estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
                }
                return estatus;
            }
        }, {
            "targets": [ -1 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var opciones = '';
                if (row[8] == 1) {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
                } else {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
                return opciones;
            }
        } ]
    });

    $('.datos_tabla_ingresos').dataTable({
        "order": [[ 0, "desc" ]],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/contabilidad/tabla_indice_polizasIngresos",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  9 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 5 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }, {
            "targets": [ 7 ],
            "render": function ( data, type, row ) {
                var firme = '';
                if (data == 0) {
                    firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                } else if (data == 1) {
                    firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
                }
                return firme;
            }
        }, {
            "targets": [ 8 ],
            "render": function ( data, type, row ) {
                var estatus = '';
                if (data == "activo") {
                    estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
                } else if (data == "espera") {
                    estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
                } else {
                    estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
                }
                return estatus;
            }
        }, {
            "targets": [ -1 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var opciones = '';
                if (row[9] == 1) {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
                } else {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
                return opciones;
            }
        } ]
    });

    $('.datos_tabla_egresos').dataTable({
        "order": [[ 0, "desc" ]],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/contabilidad/tabla_indice_polizasEgresos",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  9 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 5 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }, {
            "targets": [ 7 ],
            "render": function ( data, type, row ) {
                var firme = '';
                if (data == 0) {
                    firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                } else if (data == 1) {
                    firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
                }
                return firme;
            }
        }, {
            "targets": [ 8 ],
            "render": function ( data, type, row ) {
                var estatus = '';
                if (data == "activo") {
                    estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
                } else if (data == "espera") {
                    estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
                } else {
                    estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
                }
                return estatus;
            }
        }, {
            "targets": [ -1 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var opciones = '';
                if (row[9] == 1) {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
                } else {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
                return opciones;
            }
        } ]
    });

    $('.datos_tabla_diario').dataTable({
        "order": [[ 0, "desc" ]],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/contabilidad/tabla_indice_polizasDiario",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  10 ,
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 2 ],
            "visible": false
        }, {
            "targets": [ 1 ],
            "render": function ( data, type, row ) {
                var numero = '';
                if(data != 0) {
                    numero = data;
                } else if(data == 0 && row[2] != 0) {
                    numero = row[2];
                } else {
                    numero = 0;
                }
                return numero;
            }
        }, {
            "targets": [ 6 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }, {
            "targets": [ 8 ],
            "render": function ( data, type, row ) {
                var firme = '';
                if (data == 0) {
                    firme = '<button type="button" class="btn btn-circle btn-firmec" disabled><i class="fa fa-times"></i></button>';
                } else if (data == 1) {
                    firme = '<button type="button" class="btn btn-circle btn-firmea" disabled><i class="fa fa-check"></i></button>';
                }
                return firme;
            }
        }, {
            "targets": [ 9 ],
            "render": function ( data, type, row ) {
                var estatus = '';
                if (data == "activo") {
                    estatus = '<button type="button" class="btn btn-success disabled">Activo</button>';
                } else if (data == "espera") {
                    estatus = '<button type="button" class="btn btn-warning disabled">Espera</button>';
                } else {
                    estatus = '<button type="button" class="btn btn-danger disabled">Cancelado</button>';
                }
                return estatus;
            }
        }, {
            "targets": [ -1 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var opciones = '';
                if (row[10] == 1) {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_regenerar" data-tooltip="Volver a Generar Póliza"><i class="fa fa-refresh"></i></a>';
                } else {
                    opciones = '<a href="' + js_base_url("contabilidad/ver_poliza/" + row[0]) + '" data-tooltip="Ver"><i class="fa fa-eye"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/editar_poliza/" + row[0]) + '" data-tooltip="Editar"><i class="fa fa-edit"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Cancelar"><i class="fa fa-remove"></i></a>'+
                        '<a href="' + js_base_url("contabilidad/imprimir_poliza/" + row[0]) + '" data-tooltip="Imprimir"><i class="fa fa-print"></i></a>';
                }
                return opciones;
            }
        } ]
    });

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
    $("#cancelar_poliza").val(datos_tabla[0]);
    $("#regenerar_poliza").val(datos_tabla[0]);
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#cancelar_poliza').val(datos_tabla[0]);
});

$('.modal_regenerar').on('hide.bs.modal', function (event) {
    var modal = $(this);
    $("#elegir_regenerar_poliza").show();
});

$("#elegir_cancelar_poliza").click(function() {
    var poliza = $("#cancelar_poliza").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/cancelar_poliza_caratula",
        data: {
            poliza: poliza
        }
    })
        .done(function( data ) {
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#elegir_regenerar_poliza").click(function() {
    var poliza = $("#regenerar_poliza").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/regenerar_poliza",
        data: {
            poliza: poliza
        },
        beforeSend: function (data) {
            $('#resultado_regenerar_poliza').html( '<img src="'+js_base_url("img/ajax-loader2.gif")+'" style="width: 20%;" />');
        }
    })
        .done(function( data ) {
            $("#resultado_regenerar_poliza").html(data.mensaje);
            $("#elegir_regenerar_poliza").hide();
            table.ajax.reload();
            //$('.modal_regenerar').modal('hide');
        })
        .fail(function(e) {
            console.log(e);
            console.log(e.responseText);
        });
});

function BtnTodas() {
    document.getElementById('tab-todas').style.display='block';
    document.getElementById('tab-diario').style.display='none';
    document.getElementById('tab-ingresos').style.display='none';
    document.getElementById('tab-egresos').style.display='none';
    document.getElementById('btn-todas').className='active';
    document.getElementById('btn-diario').className='noactive';
    document.getElementById('btn-ingresos').className='noactive';
    document.getElementById('btn-egresos').className='noactive';

}
function BtnDiario() {
    document.getElementById('tab-diario').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-ingresos').style.display='none';
    document.getElementById('tab-egresos').style.display='none';
    document.getElementById('btn-diario').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-ingresos').className='noactive';
    document.getElementById('btn-egresos').className='noactive';

}
function BtnIngresos() {
    document.getElementById('tab-ingresos').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-diario').style.display='none';
    document.getElementById('tab-egresos').style.display='none';
    document.getElementById('btn-ingresos').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-diario').className='noactive';
    document.getElementById('btn-egresos').className='noactive';
}
function BtnEgresos() {
    document.getElementById('tab-egresos').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-diario').style.display='none';
    document.getElementById('tab-ingresos').style.display='none';
    document.getElementById('btn-egresos').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-diario').className='noactive';
    document.getElementById('btn-ingresos').className='noactive';
}