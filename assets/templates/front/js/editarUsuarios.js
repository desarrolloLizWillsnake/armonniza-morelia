/**
 * Created by Lizbeth on 31/07/15.
 */

$("#guardar_usuario").on( "click", function(e) {
    e.preventDefault();

    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });

    map.tipo_radio = $('input[name=tipo_radio]:checked').val();

    $.ajax({
        url: '/administrador/insertar_usuario',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $("#resultado_insertar_caratula").html(s.mensaje);
            $("#guardar_usuario").prop('disabled', true);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
});
