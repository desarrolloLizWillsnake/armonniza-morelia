var tabla; // use a global for the submit and return data rendering in the examples
 
$(document).ready(function() {
    tabla = new $.fn.dataTable.Editor( {
        "ajax": "/extranet/tabla_proveedores_extranet",
        "table": "#datos_tabla",
        "fields": [ {
                "label": "Clave Proveedor:",
                "name": "clave_prov"
            }, {
                "label": "Nombre:",
                "name": "nombre"
            }, {
                "label": "Email:",
                "name": "email"
            }, {
                "label": "Calle:",
                "name": "calle"
            }, {
                "label": "No. Exterior:",
                "name": "no_exterior"
            }, {
                "label": "No. Interior:",
                "name": "no_interior"
            }, {
                "label": "Colonia:",
                "name": "colonia"
            }, {
                "label": "Código Postal:",
                "name": "cp"
            }, {
                "label": "Ciudad:",
                "name": "ciudad"
            }, {
                "label": "País:",
                "name": "pais"
            }, {
                "label": "Estado:",
                "name": "estado"
            }, {
                "label": "Contacto:",
                "name": "contacto"
            }, {
                "label": "RFC:",
                "name": "RFC"
            }, {
                "label": "No. Cuenta:",
                "name": "cuenta"
            }, {
                "label": "CLABE:",
                "name": "clabe"
            }, {
                "label": "Banco:",
                "name": "banco"
            }, {
                "label": "PyME:",
                "name": "mypyme"
            }
        ]
    } );
 
    $('#datos_tabla').DataTable( {
        dom: "Bfrtip",
        ajax: {
            url: "/extranet/tabla_proveedores_extranet",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "clave_prov" },
            { data: "nombre" },
            { data: "email" },
            { data: "calle" },
            { data: "no_exterior" },
            { data: "no_interior" },
            { data: "colonia" },
            { data: "cp" },
            { data: "ciudad" },
            { data: "pais" },
            { data: "estado" },
            { data: "contacto" },
            { data: "RFC" },
            { data: "cuenta" },
            { data: "clabe" },
            { data: "banco" },
            // { data: "mypyme" },
            { data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
        ],
        select: true,
        buttons: [
            { extend: "create", editor: tabla },
            { extend: "edit",   editor: tabla },
            { extend: "remove", editor: tabla }
        ]
    } );
} );