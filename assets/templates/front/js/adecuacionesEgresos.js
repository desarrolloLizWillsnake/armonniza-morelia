/**
 * Created by Daniel on 17-Dec-14.
 */
var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "ajax": "/egresos/tabla_indice_adecuaciones",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        } ],
    });

    $('.datos_tabla_ampliaciones').dataTable({
        "ajax": "/egresos/tabla_indice_adecuaciones_ampliaciones",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('.datos_tabla_reducciones').dataTable({
        "ajax": "/egresos/tabla_indice_adecuaciones_reducciones",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $('.datos_tabla_transferencias').dataTable({
        "ajax": "/egresos/tabla_indice_adecuaciones_transferencias",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
    $('#cancelar_adecuacion').val(datos_tabla[0]);
});

$('#ampliacion').click(function(e) {
    e.preventDefault();
    window.location.href = '/egresos/agregar_ampliacion';
});

$('#reduccion').click(function(e) {
    e.preventDefault();
    window.location.href = '/egresos/agregar_reduccion';
});

$('#transferencia').click(function(e) {
    e.preventDefault();
    window.location.href = '/egresos/agregar_transferencia';
});

$("#elegir_cancelar_adecuacion").click(function() {
    var adecuacion = $("#cancelar_adecuacion").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/egresos/cancelar_adecuacion",
        data: {
            adecuacion: adecuacion
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

function BtnTodas() {
    document.getElementById('tab-todas').style.display='block';
    document.getElementById('tab-ampliaciones').style.display='none';
    document.getElementById('tab-reducciones').style.display='none';
    document.getElementById('tab-transferencias').style.display='none';
    document.getElementById('btn-todas').className='active';
    document.getElementById('btn-ampliaciones').className='noactive';
    document.getElementById('btn-reducciones').className='noactive';
    document.getElementById('btn-transferencias').className='noactive';
}
function BtnAmpliaciones() {
    document.getElementById('tab-ampliaciones').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-reducciones').style.display='none';
    document.getElementById('tab-transferencias').style.display='none';
    document.getElementById('btn-ampliaciones').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-reducciones').className='noactive';
    document.getElementById('btn-transferencias').className='noactive';
}
function BtnReducciones() {
    document.getElementById('tab-reducciones').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-ampliaciones').style.display='none';
    document.getElementById('tab-transferencias').style.display='none';
    document.getElementById('btn-reducciones').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-ampliaciones').className='noactive';
    document.getElementById('btn-transferencias').className='noactive';
}
function BtnTransferencias() {
    document.getElementById('tab-transferencias').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-ampliaciones').style.display='none';
    document.getElementById('tab-reducciones').style.display='none';
    document.getElementById('btn-transferencias').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-ampliaciones').className='noactive';
    document.getElementById('btn-reducciones').className='noactive';
}
