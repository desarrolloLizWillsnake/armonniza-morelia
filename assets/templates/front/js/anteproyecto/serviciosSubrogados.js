var editor;
var datos_tabla;
var check_cerrado = 0;

 $(document).ready(function() {

   $.ajax({
        url: '/anteproyecto/cerrar_aprobado',
        method: 'POST',
        dataType: 'json',
        success: function(s){
            check_cerrado = s;
            renderTabla(check_cerrado);
        }
    });

});

function renderTabla(check) {
    editor = new $.fn.dataTable.Editor({
        "ajax": "tabla_servicios_subrogados",
        "table": "#datos_tabla",
        "fields": [{
            label: "Centro de Costos",
            name: "centro_costos"
        }, {
            label: "Descripcion Centro de Costos",
            name: "descripcion_centro_costos"
        }, {
            label: "Partida",
            name: "partida"
        }, {
            label: "Descripcion Partida",
            name: "descripcion_partida"
        }, {
            label: "Enero",
            name: "enero"
        }, {
            label: "Febrero",
            name: "febrero"
        }, {
            label: "Marzo",
            name: "marzo"
        }, {
            label: "Abril",
            name: "abril"
        }, {
            label: "Mayo",
            name: "mayo"
        }, {
            label: "Junio",
            name: "junio"
        }, {
            label: "Julio",
            name: "julio"
        }, {
            label: "Agosto",
            name: "agosto"
        }, {
            label: "Septiembre",
            name: "septiembre"
        }, {
            label: "Octubre",
            name: "octubre"
        }, {
            label: "Noviembre",
            name: "noviembre"
        }, {
            label: "Diciembre",
            name: "diciembre"
        }, {
            label: "Total",
            name: "total"
        },

        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title: "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Plan de Cuenta Contable",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title: "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Plan de Cuenta Contable",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title: "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "�Esta seguro que desea eliminar los %d Planes de Cuentas Contables seleccionados?",
                    1: "�Esta seguro que desea eliminar el Plan de Cuenta Contable seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    });

    if(check_cerrado == 0) {
        editor.disable([
            'centro_costos',
            'descripcion_centro_costos',
            'partida',
            'descripcion_partida',
            'enero',
            'febrero',
            'marzo',
            'abril',
            'mayo',
            'junio',
            'julio',
            'agosto',
            'septiembre',
            'octubre',
            'noviembre',
            'diciembre',
            'total',
        ]);
    } else {
        editor.disable([
            'centro_costos',
            'descripcion_centro_costos',
            'partida',
            'descripcion_partida',
            'enero',
            'febrero',
            'marzo',
            'abril',
            'mayo',
            'junio',
            'julio',
            'agosto',
            'septiembre',
            'octubre',
            'noviembre',
            'diciembre',
            'total',
        ]);
    }


    $('#datos_tabla').DataTable({
        dom: "lfrtip",
        ajax: {
            url: "tabla_servicios_subrogados",
            type: "POST"
        },
        serverSide: true,
        columns: [
            {data: "centro_costos"},
            {data: "descripcion_centro_costos"},
            {data: "partida"},
            {data: "descripcion_partida"},

            {data: "enero", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "febrero", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "marzo", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "abril", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "mayo", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "junio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "julio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "agosto", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "septiembre", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "octubre", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "noviembre", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "diciembre", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "total", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

        ],
        keys: {
            columns: ':not(:first-child)',
            editor: editor
        },
        select: {
            style: 'os',
            selector: 'td:first-child',
            blurable: true
        },
        buttons: [
            {extend: "create", editor: editor},
            {extend: "edit", editor: editor},
            {extend: "remove", editor: editor}
        ],
        language: {
            "sProcessing":      "Procesando...",
            "sLengthMenu":      "Mostrar _MENU_ registros",
            "sZeroRecords":     "No se encontraron resultados",
            "sEmptyTable":      "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":            "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":       "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":    "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":     "",
            "sSearch":          "Buscar ",
            "sUrl":             "",
            "sInfoThousands":   ",",
            "sLoadingRecords":  "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        columnDefs: [{
            render: function (data, type, row) {
                return '<a data-toggle="modal" data-target=".modal_ver_detalle" data-tooltip="Ver">' + data + '</a>';
            },
            targets: 0
        }, {
            visible: false,
            targets: [3, 1]
        }]
    });
}

$("#datos_tabla tbody").on('click','tr', function () {

    var id = $(this).attr('id');

    var centro_costos = $('tr#'+ id).find('td:first').text();
    var partida = $('tr#'+ id).find('td:nth-child(2)').text();

    $.ajax({
        method: "POST",
        url: "/anteproyecto/descripcion",
        data: {partida: partida, centro_costos: centro_costos}
    })
        .done(function( data ) {
            console.log(data);
            $('#detalle_partida').text(data.descripcion_partida);
            $('#detalle_centro_costos').text(data.descripcion_centro_costos);
            $('#no_partida').text(data.partida);
        });

});


  $("#boton_cerrar_presupuesto").on( "click", function() {

    $.ajax({
        url: '/anteproyecto/cerrar_presupuesto_aprobado',
        method: 'POST',
        dataType: 'json',
        success: function(s){
            location.reload();
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});
