var editor;

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        "ajax": "tabla_catalogo_usuarios",
        "table": "#datos_tabla",
        "fields": [ {
            "label": "Centro de Costos:",
            "name": "centro_costos"
        }, {
            "label": "Descripci&oacute;n:",
            "name": "descripcion"
            }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Centro de Costos",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Centro de Costos",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar Centro de Costos",
                submit: "Borrar",
                confirm: {
                    _: "&iquest;Esta seguro que desea eliminar el Centro de Costos seleccionado?",
                    1: "&iquest;Esta seguro que desea eliminar el Centro de Costos seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('#datos_tabla').DataTable( {
        dom: "l<frtip>",
        ajax: {
            url: "tabla_catalogo_usuarios",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "centro_costos" },
            { data: "descripcion" }
        ],
        select: true,
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor }
        ],
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning�n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );

/*function getListaUsuarios() {

    var listaUsuarios = new Array();

    $.ajax({
        url: '/anteproyecto/select_usuarios',
        async: false,
        dataType: 'json',
        method: 'POST',
        success: function(s){
            //console.log(s);
            for (i = 0; i < s.length; i++) {
                obj = { "label" : s[i].id, "value" : s[i].id};
                listaUsuarios.push(obj);
            }

        },
        error: function(e){
            console.log(e.responseText);
        }
    });

    return listaUsuarios;
}*/