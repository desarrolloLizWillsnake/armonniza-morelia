var tabla_cc_busqueda;
var tabla_partidas_busqueda;
var search_centro_costos = "";
var search_partida = "";

$(document).ready(function() {

    $('#tabla_cc_busqueda').dataTable({
        "ajax": "tabla_cc_busqueda",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "&Uacute;ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#tabla_partidas_busqueda').dataTable({
        "ajax": "tabla_partidas_busqueda",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "&Uacute;ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('#datos_tabla_busqueda').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "&Uacute;ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

});



$('#tabla_cc_busqueda tbody').on( 'click', 'tr', function () {
    tabla_cc_busqueda = $('#tabla_cc_busqueda').DataTable().row( this ).data();

    if(search_centro_costos.indexOf(tabla_cc_busqueda[0]) > -1) {
        search_centro_costos = search_centro_costos.replace(tabla_cc_busqueda[0]+",", "")
    } else {
        search_centro_costos = search_centro_costos.concat(tabla_cc_busqueda[0]+",");
    }
});

$('#modal_centro_costos').on('hidden.bs.modal', function () {
    $('#centro_costos').val(search_centro_costos);
});

$('#tabla_partidas_busqueda tbody').on( 'click', 'tr', function () {
    tabla_partidas_busqueda = $('#tabla_partidas_busqueda').DataTable().row( this ).data();

    if(search_partida.indexOf(tabla_partidas_busqueda[0]) > -1) {
        search_partida = search_partida.replace(tabla_partidas_busqueda[0]+",", "")
    } else {
        search_partida = search_partida.concat(tabla_partidas_busqueda[0]+",");
    }
});

$('#modal_partida').on('hidden.bs.modal', function () {
    $('#partida').val(search_partida);
});


