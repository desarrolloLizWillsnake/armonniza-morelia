var editor;

$.fn.dataTable.TableTools.buttons.download = $.extend(
    true,
    {},
    $.fn.dataTable.TableTools.buttonBase,
    {
        "sButtonText": "Download",
        "sUrl":      "",
        "sType":     "POST",
        "fnData":    false,
        "fnClick": function( button, config ) {
            var dt = new $.fn.dataTable.Api( this.s.dt );
            var data = dt.ajax.params() || {};

            if ( config.fnData ) {
                config.fnData( data );
            }

            var iframe = $('<iframe/>', {
                id: "RemotingIFrame"
            }).css( {
                border: 'none',
                width: 0,
                height: 0
            } )
                .appendTo( 'body' );

            var contentWindow = iframe[0].contentWindow;
            contentWindow.document.open();
            contentWindow.document.close();

            var form = contentWindow.document.createElement( 'form' );
            form.setAttribute( 'method', config.sType );
            form.setAttribute( 'action', config.sUrl );

            var input = contentWindow.document.createElement( 'input' );
            input.name = 'json';
            input.value = JSON.stringify( data );

            form.appendChild( input );
            contentWindow.document.body.appendChild( form );
            form.submit();
        }
    }
);

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "/catalogos/tabla_cucop",
        table: ".datos_tabla",
        fields: [ {
            label: "Clave CUCop",
            name: "cucop"
        }, {
            label: "Clave SSM",
            name: "clave_ssm"
        }, {
            label: "Descripción",
            name: "descripcion"
        }, {
            label: "U/M",
            name:  "unidad",
            dataProp: "1",
            type:  "select",
            ipOpts: getListaUnidadMedida()

        },
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Concepto de Gasto",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Concepto de Gasto",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "¿Esta seguro que desea eliminar los %d conceptos de gasto seleccionados?",
                    1: "¿Esta seguro que desea eliminar el concepto de gasto seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('.datos_tabla').DataTable( {
        dom: "Tfrtip",
        ajax: {
            url: "/catalogos/tabla_cucop",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "cucop" },
            { data: "clave_ssm" },
            { data: "descripcion" },
            { data: "unidad" }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );

function getListaUnidadMedida() {

    var listaUnidadMedida = new Array();

    $.ajax({
        url: '/catalogos/get_unidades_medida',
        async: false,
        dataType: 'json',
        method: 'POST',
        success: function(s){
            for (i = 0; i < s.length; i++) {
                obj = { "label" : s[i].descripcion, "value" : s[i].clave.toUpperCase()};
                listaUnidadMedida.push(obj);
            }

        },
        error: function(e){
            console.log(e.responseText);
        }
    });

    return listaUnidadMedida;
}