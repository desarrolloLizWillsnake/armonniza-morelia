var datos_tabla;

$(document).ready(function() {

    $('#datos_tabla').dataTable({
        "ajax": "/nomina/tabla_empleados_indice",
        "aaSorting": [],
        "language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });

    $('#tabla_proveedores_extranet').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_proveedor_con_contrato",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  -1 ,
            "searchable": false
        } ]
    });

    $('#tabla_proveedores_extranet2').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_proveedor_sin_contrato",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  -1 ,
            "searchable": false
        } ]
    });

    $('#tabla_facturas').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_facturas_proveedor",
            "type": "POST",
            "data": function ( d ) {
                d.proveedor = $('#rfc_proveedor').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        },
        "columnDefs": [ {
            "targets": [ 0, 6, 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 8 ],
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }]
    });

    $('#tabla_facturas_por_autorizar_glosa').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_facturas_proveedor_glosa",
            "type": "POST",
            "data": function ( d ) {
                d.proveedor = $('#rfc_proveedor').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        },
        "columnDefs": [ {
            "targets": [ 0, 6, 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 9 ],
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }]
    });

    $('#tabla_proveedores_extranet3').dataTable({
        "ajax": "/extranet/tabla_proveedor_indice3",
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        }
    });

    $('#tabla_facturas_glosa').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_facturas_proveedor_glosa",
            "type": "POST",
            "data": function ( d ) {
                d.proveedor = $('#rfc_proveedor').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        },
        "columnDefs": [ {
            "targets": [ 0, 6, 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }]
    });

    $('#tabla_facturas_autorizadas').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_facturas_aprobadas",
            "type": "POST",
            "data": function ( d ) {
                d.proveedor = $('#rfc_proveedor').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        },
        "columnDefs": [ {
            "targets": [ 0, 6, 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }]
    });

});

$("#guardar_nota_glosa").on("click", function(e){
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/autorizar_factura_2",
        data:{
            factura: $("#id_factura").val(),
            autorizacion: $('input[name=radio_autorizar]:checked').val(),
            titulo: $('#titulo').val(),
            observaciones: $('#descripcion_archivo').val(),
            rfc_proveedor: $("#rfc_proveedor").val(),
            id_precompromiso: $("#id_precompromiso").val()
        }
    })
        .done(function( data ) {
            $("#id_factura").val('');
            $("#mensaje_guardar").html(data.mensaje);
            refrescarDatos();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#guardar_nota").on("click", function(e){
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/autorizar_factura_1",
        data:{
            factura: $("#id_factura").val(),
            autorizacion: $('input[name=radio_autorizar]:checked').val(),
            titulo: $('#titulo').val(),
            observaciones: $('#descripcion_archivo').val(),
            rfc_proveedor: $("#rfc_proveedor").val()
        }
    })
        .done(function( data ) {
            $("#id_factura").val('');
            $("#mensaje_guardar").html(data.mensaje);
            refrescarDatos();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$('#tabla_proveedores_extranet tbody').on( 'click', 'tr', function () {
    datos_tabla = $('#tabla_proveedores_extranet').DataTable().row( this ).data();
    $("#proveedor").val(datos_tabla[1]);
    $("#rfc_proveedor").val(datos_tabla[0]);
    refrescarDatos();
});

$('#tabla_facturas tbody').on( 'click', 'tr', function () {
    var datos_de_tabla = $('#tabla_facturas').DataTable().row( this ).data();
    $("#id_factura").val(datos_de_tabla[0]);
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/tomar_datos_factura",
        data:{
            factura: datos_de_tabla[0]
        }
    })
        .done(function( data ) {
            $('#fecha').val(data.fecha_emision);
            $('#rfc').val(data.rfc_receptor);
            $('#folio').val(data.folio);
            $('#estado').val(data.estado);
            $('#importe').val("$ "+$.number( data.total, 2 ));
            $('#colonia').val(data.colonia_receptor);
            $('#estado').val(data.estado_receptor);
            $('#observaciones').html(data.observaciones_proveedor);
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$('#tabla_facturas_por_autorizar_glosa tbody').on( 'click', 'tr', function () {
    var datos_de_tabla = $('#tabla_facturas_por_autorizar_glosa').DataTable().row( this ).data();
    $("#id_factura").val(datos_de_tabla[0]);
    $("#id_precompromiso").val(datos_de_tabla[3]);
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/tomar_datos_factura",
        data:{
            factura: datos_de_tabla[0]
        }
    })
        .done(function( data ) {
            $('#fecha').val(data.fecha_emision);
            $('#rfc').val(data.rfc_receptor);
            $('#folio').val(data.folio);
            $('#estado').val(data.estado);
            $('#importe').val("$ "+$.number( data.total, 2 ));
            $('#colonia').val(data.colonia_receptor);
            $('#estado').val(data.estado_receptor);
            $('#observaciones').html(data.observaciones_proveedor);
            //$('#seriecsd').val(data.no_certificado);
            //$('#seriesat').val(data.no_certificado_sat);
            $('#folio_fiscal').val(data.uuid);
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$('#tabla_proveedores_extranet2 tbody').on( 'click', 'tr', function () {
    datos_tabla = $('#tabla_proveedores_extranet2').DataTable().row( this ).data();
    $("#proveedor").val(datos_tabla[1]);
    $("#rfc_proveedor").val(datos_tabla[0]);
    refrescarDatos();
});

function refrescarDatos() {
    var tabla1 = $('#tabla_facturas').DataTable();
    var tabla2 = $('#tabla_facturas_glosa').DataTable();
    var tabla3 = $('#tabla_facturas_por_autorizar_glosa').DataTable();
    var tabla4 = $('#tabla_facturas_autorizadas').DataTable();
    
    tabla1.ajax.reload();
    tabla2.ajax.reload();
    tabla3.ajax.reload();
    tabla4.ajax.reload();
}

$('.modal_ver').on('show.bs.modal', function () {

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../extranet/tomar_datos_factura",
        data: {
            factura: $("#id_factura").val()
        }
    })
        .done(function( data ) {

            $('#rfc_emisor_ver').html(data.rfc_emisor);
            $('#no_exterior_emisor_ver').html(data.numero_ext_emisor);
            $('#no_interior_emisor_ver').html(data.numero_ent_emisor);
            $('#colonia_emisor_ver').html(data.colonia_emisor);
            $('#municipio_emisor_ver').html(data.municipio_emisor);
            $('#estado_emisor_ver').html(data.estado_emisor);
            $('#pais_emisor_ver').html(data.pais_emisor);
            $('#calle_emisor_ver').html(data.calle_emisor);

            $('#rfc_receptor_ver').html(data.rfc_receptor);
            $('#no_exterior_receptor_ver').html(data.numero_ext_receptor);
            $('#no_interior_receptor_ver').html(data.numero_int_receptor);
            $('#colonia_receptor_ver').html(data.colonia_receptor);
            $('#municipio_receptor_ver').html(data.municipio_receptor);
            $('#estado_receptor_ver').html(data.estado_receptor);
            $('#pais_receptor_ver').html(data.pais_receptor);
            $('#calle_receptor_ver').html(data.calle_receptor_ver);
            $('#nombre_receptor_ver').html(data.nombre_receptor);
            $('#calle_receptor_ver').html(data.calle_receptor);
            //$('#regimen_receptor_ver').html(data.calle_receptor);

            $('#fecha_emision_ver').html(data.fecha_emision);
            //$('#regimen_fiscal_ver').html(data.regimen_fiscal);
            $('#tipo_comprobante_ver').html(data.tipo_comprobante);
            $('#numero_comprobante_ver').html(data.numero_comprobante);
            $('#folio_ver').html(data.folio);
            $('#metodo_pago_ver').html(data.metodo_pago);
            $('#iva_ver').html(data.valor_iva);
            $('#tasa_iva_ver').html(data.tasa_iva);
            $('#subtotal_ver').html(data.subtotal);
            $('#total_ver').html(data.total);
            $('#nombre_cliente_ver').html(data.nombre_cliente);
            //$('#no_certificado_ver').html(data.no_certificado);
            //$('#certificado_ver').html(data.certificado);
            //$('#sello_ver').html(data.sello);

            $('#uuid_ver').html(data.uuid);
            //$('#no_certificado_sat_ver').html(data.no_certificado_sat);
            //$('#sello_sat_ver').html(data.sello_sat);
            //$('#observaciones_proveedor_ver').html(data.observaciones_proveedor);
            $('#observaciones_ver').html(data.observaciones);

        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});