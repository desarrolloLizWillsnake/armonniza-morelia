var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        "ajax": "tabla_indice_matriz",
        "table": ".datos_tabla",
        "fields": [ {
            "label": "ID",
            "name": "id_correlacion_partidas_contables"
        }, {
            "label": "Partida",
            "name": "clave"
        }, {
            "label": "Descripción",
            "name": "descripcion"
        }, {
            "label": "Cuenta Cargo",
            "name": "cuenta_cargo"
        }, {
            "label": "Descripción",
            "name": "nombre_cargo"
        }, {
            "label": "Cuenta Abono",
            "name": "cuenta_abono"
        }, {
            "label": "Descripción",
            "name": "nombre_abono"
        }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo concepto en Matriz de Conversión",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar concepto en Matriz de Conversión",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "¿Está seguro que desea eliminar los %d conceptos de la Matriz de Conversión seleccionados?",
                    1: "¿Está seguro que desea eliminar el concepto de la Matriz de Conversión seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('.datos_tabla').DataTable( {
        dom: 'T<"clear">lfrtip',
        ajax: {
            url: "tabla_indice_matriz",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "id_correlacion_partidas_contables" },
            { data: "clave" },
            { data: "descripcion" },
            { data: "cuenta_cargo" },
            { data: "nombre_cargo" },
            { data: "cuenta_abono" },
            { data: "nombre_abono" }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  0 ,
            "visible": false,
            "searchable": false
        }]
    } );
} );

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
    $('#borrar_matriz').val(datos_tabla[0]);
});

$('#agregar_matriz').click(function(e) {
    e.preventDefault();
    window.location.href = '/contabilidad/agregar_matriz';
});

$('#descargar_plantilla').click(function(e) {
    e.preventDefault();  //stop the browser from following
    window.location.href = '../plantillas/plantilla_matriz.xls';
});

$("#elegir_borrar_matriz").click(function() {
    var matriz = $("#borrar_matriz").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/borrar_matriz",
        data: {
            matriz: matriz
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                table.ajax.reload();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});