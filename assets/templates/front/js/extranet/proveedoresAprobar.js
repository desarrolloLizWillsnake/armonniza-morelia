var datos_proveedor;
var datos_proveedor_aprobado;

$(document).ready(function() {

    $('#datos_tabla').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_proveedores_aprobar",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  [ 0, 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 6 ],
            "render": function ( data, type, row ) {
                var estatus = '';
                if(data == 0 || data == null) {
                    estatus += '<button type="button" class="btn btn-estatus btn-warning" disabled>Pendiente</button>';
                } else {
                    estatus += '<button type="button" class="btn btn-estatus btn-success" disabled>Aprobado</button>';
                }
                return estatus;
            }
        }, {
            "targets": [ -1 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var acciones = '<a class="ver_datos_proveedor_boton" data-toggle="modal" data-target=".modal_ver" data-tooltip="Ver Datos"><i class="fa fa-eye"></i></a>';
                if(row[6] == 0 || row[6] == null) {
                    acciones += '<a data-toggle="modal" data-target=".modal_activar" data-tooltip="Aprobar"><i class="fa fa-check"></i></a>'+
                                '<a data-toggle="modal" data-target=".modal_desactivar" data-tooltip="Desaprobar"><i class="fa fa-remove"></i></a>';
                } else {
                    acciones += '<a data-toggle="modal" data-target=".modal_desactivar" data-tooltip="Desaprobar"><i class="fa fa-remove"></i></a>';
                }
                return acciones;
            }
        } ]
    });


    $('#datos_tabla_aprobado').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_proveedores_aprobados",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets":  [ 0, 9 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 6 ],
            "render": function ( data, type, row ) {
                var estatus = '';
                if(data == 0 || data == null) {
                    estatus += '<button type="button" class="btn btn-estatus btn-warning" disabled>Pendiente</button>';
                } else {
                    estatus += '<button type="button" class="btn btn-estatus btn-success" disabled>Aprobado</button>';
                }
                return estatus;
            }
        }, {
            "targets": [ -1 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var acciones = '<a class="ver_datos_proveedor_boton" data-toggle="modal" data-target=".modal_ver" data-tooltip="Ver Datos"><i class="fa fa-eye"></i></a>';
                if(row[6] == 0 || row[6] == null) {
                    acciones += '<a data-toggle="modal" data-target=".modal_activar" data-tooltip="Aprobar"><i class="fa fa-check"></i></a>'+
                        '<a data-toggle="modal" data-target=".modal_desactivar" data-tooltip="Desaprobar"><i class="fa fa-remove"></i></a>';
                } else {
                    acciones += '<a data-toggle="modal" data-target=".modal_desactivar" data-tooltip="Desaprobar"><i class="fa fa-remove"></i></a>';
                }
                return acciones;
            }
        } ]
    });

});

$('#datos_tabla tbody').on( 'click', 'tr', function () {
    datos_proveedor = $('#datos_tabla').DataTable().row( this ).data();
    $('#aprobar_proveedor').val(datos_proveedor[7]);
    $('#id_proveedor').val(datos_proveedor[0]);
});

$('#datos_tabla_aprobado tbody').on( 'click', 'tr', function () {
    datos_proveedor_aprobado = $('#datos_tabla_aprobado').DataTable().row( this ).data();
    $('#aprobar_proveedor').val(datos_proveedor_aprobado[9]);
    $('#id_proveedor').val(datos_proveedor_aprobado[0]);
});

$("#elegir_activar").on( "click", function () {
    
    var table = $('#datos_tabla').DataTable();
    var table2 = $('#datos_tabla_aprobado').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/activar_proveedor",
        data: {
            proveedor: $('#aprobar_proveedor').val(),
            id_proveedor: $('#id_proveedor').val()
        }
    })
        .done(function( data ) {
            $('#mensaje_resultado').val(data.mensaje);
            $('#modal_resultado').modal('show');
            table.ajax.reload();
            table2.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#elegir_desactivar").on( "click", function () {
    
    var table = $('#datos_tabla').DataTable();
    var table2 = $('#datos_tabla_aprobado').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/desactivar_proveedor",
        data: {
            proveedor: $('#aprobar_proveedor').val(),
            id_proveedor: $('#id_proveedor').val()
        }
    })
        .done(function( data ) {
            $('#mensaje_resultado').val(data.mensaje);
            $('#modal_resultado').modal('show');
            table.ajax.reload();
            table2.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$('.modal_ver').on('show.bs.modal', function () {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/ver_datos_proveedor",
        data: {
            proveedor: $('#aprobar_proveedor').val()
        }
    })
        .done(function( data ) {
            if(data.ciudad == "MX") {
                data.ciudad = "México";
            }
            $("#comprobante_pago_ver").attr("src",data.comprobante);
            $("#descargar_imagen").attr("href",data.comprobante);
            $("#descargar_imagen").attr("download",data.comprobante);
            $('#rfc_ver').html(data.username.toUpperCase());
            $('#pyme_ver').html(data.pyme);
            $('#nombre_ver').html(data.nombre + " " + data.apellidos + data.razon_social);

            $('#pais_ver').html(data.ciudad);
            $('#estado_ver').html(data.estado);
            $('#ciudad_ver').html(data.ciudad);
            $('#calle_ver').html(data.calle);
            $('#colonia_ver').html(data.colonia);
            $('#num_exterior_ver').html(data.numero_exterior);
            $('#num_interior_ver').html(data.numero_interior);
            $('#cp_ver').html(data.codigo_postal);
            $('#nombre_facturas_ver').html(data.nombre_facturas);
            $('#a_paterno_facturas_ver').html(data.apellido_paterno_facturas);
            $('#a_materno_facturas_ver').html(data.apellido_materno_facturas);
            $('#correo_facturas_ver').html(data.email_facturas);
            $('#nombre_ventas_ver').html(data.nombre_ventas);
            $('#a_paterno_ventas_ver').html(data.apellido_paterno_ventas);
            $('#a_materno_ventas_ver').html(data.apellido_materno_ventas);
            $('#correo_ventas_ver').html(data.email_ventas);
            $('#moneda_ver').html(data.moneda);
            $('#banco_ver').html(data.banco);
            $('#no_cuenta_ver').html(data.no_cuenta);
            $('#clabe_ver').html(data.CLABE);
            $('#telefono_facturas_ver').html(data.telefono_facturas);
            $('#telefono_ventas_ver').html(data.telefono_ventas);
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});