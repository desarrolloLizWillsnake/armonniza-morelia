$(document).ready(function() {

	$('#tabla_detalle_noticia').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_noticias_extranet",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        }
    });

});

$("#guardar_noticia").on("click", function(e){

	var tabla = $('#tabla_detalle_noticia').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/extranet/insertar_noticia",
        data:{            
            mensaje: $('#observaciones').val(),
            asunto: $('#tipo_mensaje option:selected').val()
        }
    })
        .done(function( data ) {
            $("#mensaje_guardar").html(data.mensaje);
            tabla.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});