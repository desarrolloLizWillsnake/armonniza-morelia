var datos_proveedor;

$(document).ready(function() {

	$('#tabla_facturas').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_facturas_estado_cuenta",
            "type": "POST",
            "data": function ( d ) {
                d.proveedor = $('#rfc_proveedor').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        },
        "columnDefs": [ {
            "targets": [ 6, 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }]
    });

    $('#tabla_proveedores').DataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/extranet/tabla_proveedores_cuentas",
            "type": "POST"
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        }
    });
});

$('#tabla_proveedores tbody').on( 'click', 'tr', function () {
    datos_tabla = $('#tabla_proveedores').DataTable().row( this ).data();
    $("#rfc_proveedor").val(datos_tabla[0]);    
    $("#proveedor").val(datos_tabla[1]);
    refrescarDatos();
});

$('#boton_exportar').on( 'click', function () {
    console.log($("#rfc_proveedor").val());
});

function refrescarDatos() {
    var tabla1 = $('#tabla_facturas').DataTable();
    
    tabla1.ajax.reload();
}